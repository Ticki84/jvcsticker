// ==UserScript==
// @name        JVCSticker++
// @namespace   JVCSticker++
// @include     http://www.jeuxvideo.com/forums/*
// @include     https://www.jeuxvideo.com/forums/*
// @version     1.6.0
// @grant       GM_addStyle
// @updateURL   https://ticki84.github.io/JVCSticker++.meta.js
// @downloadURL https://ticki84.github.io/JVCSticker++.user.js
// @require     http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js
// @require     https://ticki84.github.io/jquery.modal.min.js
// @connect     github.io
// @connect     jvcsticker.site88.net
// @grant       GM_xmlhttpRequest
// @grant       GM_getValue
// @grant       GM_setValue
// @grant       GM_listValues
// @grant       GM_deleteValue
// @grant       GM_log
// @grant       GM_info
// @icon        http://jv.stkr.fr/p/1kki
// @author      Ticki84
// @copyright   2016+, Ticki84
// @noframes
// ==/UserScript==
function deleteGlobalVar() {
    var keys = GM_listValues();
    for (var i = 0, key = null; key = keys[i]; i++) {
        GM_deleteValue(key);
    }
}

function addStyle(style) {
    var head = document.getElementsByTagName('HEAD')[0];
    var ele = head.appendChild(window.document.createElement('style'));
    ele.innerHTML = style;
    return ele;
}
addStyle(`@import "https://ticki84.github.io/jquery.modal.min.css";
progress[value]::-moz-progress-bar { 
  background-image:
    -moz-linear-gradient(
      135deg, 
      transparent 33%, 
      rgba(0, 0, 0, 0.1) 33%, 
      rgba(0, 0, 0, 0.1) 66%, 
      transparent 66% 
    ),
    -moz-linear-gradient(
      top, 
      rgba(255, 255, 255, 0.25), 
      rgba(0, 0, 0, 0.25)
    ),
    -moz-linear-gradient(
      left, 
      #D10028, 
      #00B241
    );
progress[value]::-webkit-progress-bar {
  background-color: #eee;
  border-radius: 2px;
  box-shadow: 0 2px 5px rgba(0, 0, 0, 0.25) inset;
}
progress[value]::-webkit-progress-value {
  background-image:
	   -webkit-linear-gradient(-45deg, 
	                           transparent 33%, rgba(0, 0, 0, .1) 33%, 
	                           rgba(0,0, 0, .1) 66%, transparent 66%),
	   -webkit-linear-gradient(top, 
	                           rgba(255, 255, 255, .25), 
	                           rgba(0, 0, 0, .25)),
	   -webkit-linear-gradient(left, #D10028, #00B241);

    border-radius: 2px; 
    background-size: 35px 20px, 100% 100%, 100% 100%;
}`);
if (GM_getValue('dontShowUpTo') === undefined) {
    GM_setValue('dontShowUpTo', GM_info.script.version);
}
GM_xmlhttpRequest({
    method: 'GET',
    url: 'https://ticki84.github.io/JVCSticker++.xml?_=' + Date.now(),
    headers: {
        'User-Agent': 'Mozilla/5.0',
        'Accept': 'text/xml'
    },
    onload: function(response) {
        var responseXML = null;
        if (!response.responseXML) {
            responseXML = new DOMParser().parseFromString(response.responseText, 'text/xml');
        }
        var scriptElement = document.createElement('script');
        scriptElement.type = 'text/javascript';
        scriptElement.src = 'https://ticki84.github.io/jquery.modal.min.js';
        document.body.appendChild(scriptElement);

        function generateChangelog() {
            var i = 0;
            var text = '';
            while (response.responseXML.getElementsByTagName('version')[i]) {
                if (response.responseXML.getElementsByTagName('version')[i].childNodes[0].nodeValue > GM_info.script.version) {
                    text += '<p>Version ' + response.responseXML.getElementsByTagName('version')[i].childNodes[0].nodeValue + ':';
                    var changelog = response.responseXML.getElementsByTagName('changelog')[i].childNodes[0].nodeValue.replace(new RegExp('-', 'g'), '<br>-');
                    text += changelog;
                    text += '<br>';
                } else {
                    break;
                }
                i++;
            }
            return text;
        }
        if (response.responseXML.getElementsByTagName('version')[0].childNodes[0].nodeValue > GM_info.script.version && response.responseXML.getElementsByTagName('version')[0].childNodes[0].nodeValue > GM_getValue('dontShowUpTo', '1.0.0')) {
            var addHTML = document.createElement('div');
            addHTML.innerHTML = ' <div id="updateModal" style="display:none; text-align: center;"><div class="modal-header"><h3 class="modal-title">Nouvelle version!</h3></div><div class="modal-body"><br><a href="https://ticki84.github.io/JVCSticker++.user.js" target="_blank">Une nouvelle version de JVCSticker++ est disponible!</a><br><br><br><p>Version actuelle: ' + GM_info.script.version + '</p><p>Dernière version: ' + response.responseXML.getElementsByTagName('version')[0].childNodes[0].nodeValue + '</p><br><br><p><u>Changelog:</u></p>' + generateChangelog() + '<br><br><p><h4>Voulez-vous effectuez la mise à jour maintenant?</h4></p><br><label><input type="checkbox" id="showNV"> Ne plus m\'avertir jusqu\'à la prochaine mise à jour</label><br><br></div><div class="modal-footer"><button type="button" id="updateOui" class="btn btn-default" data-dismiss="modal">Oui</button> <a rel="modal:close"><button type="button" id="updateNon" class="btn btn-default" data-dismiss="modal">Non</button></a></div></div><p style="display:none;"><a href="#updateModal" id="toUpdateModal" rel="modal:open">Open Modal</a></p>';
            document.body.appendChild(addHTML);
            document.getElementById('updateOui').addEventListener('click', function() {
                window.open('https://ticki84.github.io/JVCSticker++.user.js', '_blank');
            });
            document.getElementById('updateNon').addEventListener('click', function() {
                if (document.getElementById('showNV').checked) {
                    GM_setValue('dontShowUpTo', response.responseXML.getElementsByTagName('version')[0].childNodes[0].nodeValue);
                }
                init(response.responseXML.getElementsByTagName('version')[0].childNodes[0].nodeValue);
            });
            document.getElementById('toUpdateModal').click();
        } else {
            init(response.responseXML.getElementsByTagName('version')[0].childNodes[0].nodeValue);
        }
    }
});

function init(lastVersion) {
    var varNames = [
        'espacementStickers',
        'tailleStickers',
        'tailleFenetre',
        'modifierCouleurPosts',
        'couleurBackground',
        'couleurBordure',
        'supprimerFond',
        'tailleSticker',
        'stickerCliquable',
        'supprStickersBan',
        'supprDesPosts',
        'stickerMessageDeSuppr',
        'sons',
        'webmPlayer',
        'youtubePlayer',
        'vocarooPlayer',
        'playerSignature',
        'xBarreIcones',
        'intervalle'
    ];
    var valDefault = [
        '10',
        '44',
        '150',
        true,
        '#FFF',
        '#d5d5d5',
        true,
        '100',
        true,
        true,
        false,
        true,
        true,
        true,
        true,
        true,
        false,
        '2',
        '360'
    ];
    for (var i = 0; i < varNames.length; i++) {
        if (GM_getValue(varNames[i]) === undefined) {
            GM_setValue(varNames[i], valDefault[i]);
        }
    }
    var espacementStickers = GM_getValue('espacementStickers');
    var tailleStickers = GM_getValue('tailleStickers');
    var tailleFenetre = GM_getValue('tailleFenetre');
    var modifierCouleurPosts = GM_getValue('modifierCouleurPosts');
    var couleurBackground = GM_getValue('couleurBackground');
    var couleurBordure = GM_getValue('couleurBordure');
    var supprimerFond = GM_getValue('supprimerFond');
    var tailleSticker = GM_getValue('tailleSticker');
    var stickerCliquable = GM_getValue('stickerCliquable');
    var supprStickersBan = GM_getValue('supprStickersBan');
    var supprDesPosts = GM_getValue('supprDesPosts');
    var stickerMessageDeSuppr = GM_getValue('stickerMessageDeSuppr');
    var sons = GM_getValue('sons');
    var webmPlayer = GM_getValue('webmPlayer');
    var youtubePlayer = GM_getValue('youtubePlayer');
    var vocarooPlayer = GM_getValue('vocarooPlayer');
    var playerSignature = GM_getValue('playerSignature');
    var xBarreIcones = GM_getValue('xBarreIcones');
    var intervalle = GM_getValue('intervalle');
    if (GM_getValue('allCat') !== undefined && GM_getValue('allCat').indexOf('catYoutube') === -1) {
        GM_deleteValue('allCat');
        GM_deleteValue('catRealNames');
        GM_deleteValue('catIcons');
        GM_deleteValue('catToShow');
    }
    if (GM_getValue('allCat') !== undefined && GM_getValue('allCat').indexOf('catSmiley') === -1) {
        GM_deleteValue('allCat');
        GM_deleteValue('catRealNames');
        GM_deleteValue('catIcons');
        GM_deleteValue('catToShow');
        GM_setValue('xBarreIcones', '2');
    }
    var allCat = [];
    var catRealNames = [];
    var catIcons = [];
    var catToShow = [];
    var catPopulaires = [];
    var catCache = [];
    if (GM_getValue('lTimestamp') === undefined || (Math.floor(Date.now() / 60000) - GM_getValue('lTimestamp') >= intervalle) || GM_getValue('allCat') === undefined || GM_getValue('catRealNames') === undefined || GM_getValue('catIcons') === undefined || GM_getValue('catToShow') === undefined || GM_getValue('catToShowS') === undefined || GM_getValue('catPopulaires') === undefined) {
        GM_deleteValue('lTimestamp');
        GM_xmlhttpRequest({
            method: 'GET',
            url: 'http://jvcsticker.site88.net/CoreJVCSticker++.xml?_=' + Date.now(),
            headers: {
                'User-Agent': 'Mozilla/5.0',
                'Accept': 'text/xml'
            },
            onload: function(response) {
                var responseXML = null;
                if (!response.responseXML) {
                    responseXML = new DOMParser().parseFromString(response.responseText, 'text/xml');
                }
              
              var total = 4 * response.responseXML.getElementsByTagName('sticker').length;
              var addnHTML = document.createElement('div');
            addnHTML.innerHTML = '<div id="loadingModal" style="display:none; text-align: center; width: 50%;"><div class="modal-header"><h3 class="modal-title">Chargement des stickers...</h3></div><div class="modal-body"><br><br><progress id="upProgress" style="width:60%;height:8%" }" value="0" max="' + total + '"></progress><br><br><br></div></div><p style="display:none;"><a href="#loadingModal" id="toCloseLoadingModal" rel="modal:close">Close Modal</a><a href="#loadingModal" id="toLoadingModal" rel="modal:open">Open Modal</a></p>';
              document.body.appendChild(addnHTML);
            document.getElementById('toLoadingModal').click();
              var progressBar = document.getElementById('upProgress');
              
              
                var u = 0;
                while (response.responseXML.getElementsByTagName('var')[u] !== undefined) {
                    allCat.push(response.responseXML.getElementsByTagName('var')[u].childNodes[0].nodeValue);
                    catRealNames.push(response.responseXML.getElementsByTagName('nom')[u].childNodes[0].nodeValue);
                    catIcons.push(response.responseXML.getElementsByTagName('icone')[u].childNodes[0].nodeValue);
                    u = u + 1;
                }
                GM_setValue('allCat', JSON.stringify(allCat));
                GM_setValue('catRealNames', JSON.stringify(catRealNames));
                GM_setValue('catIcons', JSON.stringify(catIcons));
                if (GM_getValue('catToShow') === undefined || GM_getValue('catToShowS') === undefined) {
                    u = 0;
                    while (response.responseXML.getElementsByTagName('default')[u] !== undefined) {
                        if (response.responseXML.getElementsByTagName('default')[u].childNodes[0].nodeValue == 'true') {
                            catToShow.push(response.responseXML.getElementsByTagName('default')[u].parentNode.getElementsByTagName('var')[0].childNodes[0].nodeValue);
                        }
                        u = u + 1;
                    }
                    GM_setValue('catToShow', JSON.stringify(catToShow));
                    GM_setValue('catToShowS', JSON.stringify(catToShow));
                }
                var E = [];
                for (var r = 0; r < allCat.length; r++) {
                    var C = [];
                    var D = [];
                    var nbStickers = response.responseXML.getElementsByTagName('var')[allCat.length - 1 - r].parentNode.getElementsByTagName('stickers')[0].getElementsByTagName('sticker').length;
                    for (var t = 0; t < nbStickers; t++) {
                        C.push(response.responseXML.getElementsByTagName('var')[allCat.length - 1 - r].parentNode.getElementsByTagName('stickers')[0].getElementsByTagName('sticker')[nbStickers - 1 - t].getElementsByTagName('counter')[0].childNodes[0].nodeValue);
                        D.push(response.responseXML.getElementsByTagName('var')[allCat.length - 1 - r].parentNode.getElementsByTagName('stickers')[0].getElementsByTagName('sticker')[nbStickers - 1 - t].getElementsByTagName('url')[0].childNodes[0].nodeValue);
                      progressBar.value += 1;
                    }
                    for (var i = 0; i < D.length; i++) {
                        E.push({
                            'C': C[i],
                            'D': D[i]
                        });
                    }
                }
                E.sort(function(a, b) {
                    return a.C - b.C;
                });
                D = [];
                var arrLenE = E.length;
                for (var i = 0; i < 30; i++) {
                    catPopulaires.push(E[arrLenE - 1 - i].D);
                }
                GM_setValue('catPopulaires', JSON.stringify(catPopulaires));
              progressBar.value = total / 2;
                for (var r = 0; r < allCat.length; r++) {
                    var A = [];
                    var B = [];
                    var nbStickers = response.responseXML.getElementsByTagName('var')[r].parentNode.getElementsByTagName('stickers')[0].getElementsByTagName('sticker').length;
                    for (var t = 0; t < nbStickers; t++) {
                        A.push(response.responseXML.getElementsByTagName('var')[r].parentNode.getElementsByTagName('stickers')[0].getElementsByTagName('sticker')[nbStickers - 1 - t].getElementsByTagName('counter')[0].childNodes[0].nodeValue);
                        B.push(response.responseXML.getElementsByTagName('var')[r].parentNode.getElementsByTagName('stickers')[0].getElementsByTagName('sticker')[nbStickers - 1 - t].getElementsByTagName('url')[0].childNodes[0].nodeValue);
                        progressBar.value += 1;
                    }
                    var all = [];
                    for (var i = 0; i < B.length; i++) {
                        all.push({
                            'A': A[i],
                            'B': B[i]
                        });
                    }
                    all.sort(function(a, b) {
                        return a.A - b.A;
                    });
                    B = [];
                    var arrLen = all.length;
                    for (var i = 0; i < arrLen; i++) {
                        B.push(all[arrLen - 1 - i].B);
                      progressBar.value += 1;
                    }
                    catCache.push(B);
                    GM_setValue(allCat[r], JSON.stringify(B));
                  progressBar.value = total;
                }
              GM_setValue('lTimestamp', Math.floor(Date.now() / 60000));
              //document.getElementById('toCloseLoadingModal').click();
      listsLoaded();
            }
        });
    } else {
        allCat = JSON.parse(GM_getValue('allCat'));
        catRealNames = JSON.parse(GM_getValue('catRealNames'));
        catIcons = JSON.parse(GM_getValue('catIcons'));
        catToShow = JSON.parse(GM_getValue('catToShow'));
        catPopulaires = JSON.parse(GM_getValue('catPopulaires'));
      for (var r = 0; r < allCat.length; r++) {
                  catCache.push(JSON.parse(GM_getValue(allCat[r])));
                }
      listsLoaded();
    }
    if (GM_getValue('stickersBanListe') === undefined) {
        GM_setValue('stickersBanListe', JSON.stringify([
            'http://jv.stkr.fr/p/1miq',
            'http://jv.stkr.fr/p/1min',
            'http://jv.stkr.fr/p/1mim',
            'http://jv.stkr.fr/p/1mig-fr',
            'http://jv.stkr.fr/p/1mij-fr',
            'http://jv.stkr.fr/p/1mio',
            'http://jv.stkr.fr/p/1mik',
            'http://jv.stkr.fr/p/1mip',
            'http://jv.stkr.fr/p/1mif',
            'http://jv.stkr.fr/p/1mii-fr',
            'http://jv.stkr.fr/p/1mih-fr',
            'http://jv.stkr.fr/p/1mil',
            'http://jv.stkr.fr/p/1mie-fr',
            'http://jv.stkr.fr/p/1mid-fr',
            'http://jv.stkr.fr/p/1myf',
            'http://jv.stkr.fr/p/1my7',
            'http://jv.stkr.fr/p/1myc',
            'http://jv.stkr.fr/p/1my9',
            'http://jv.stkr.fr/p/1myb',
            'http://jv.stkr.fr/p/1my6',
            'http://jv.stkr.fr/p/1mye',
            'http://jv.stkr.fr/p/1myx',
            'http://jv.stkr.fr/p/1myd',
            'http://jv.stkr.fr/p/1my4',
            'http://jv.stkr.fr/p/1my8',
            'http://jv.stkr.fr/p/1mya',
            'http://jv.stkr.fr/p/1my5',
            'http://jv.stkr.fr/p/1n28'
        ]));
    }
    var stickersBanListe = JSON.parse(GM_getValue('stickersBanListe'));
    var isLoaded = false;
  function listsLoaded () {
    if (modifierCouleurPosts) {
        GM_addStyle('.bloc-message-forum:nth-of-type(2n+1){background: ' + couleurBackground + ';border: 1px solid ' + couleurBordure + ';} .stickersM{max-width:' + 200 + 'px !important; max-height: ' + 200 + 'px !important}');
    }

    function clique(url) {
        GM_xmlhttpRequest({
            method: "GET",
            url: "http://jvcsticker.site88.net/countericr.php?url=" + url
        });
        var area = document.getElementsByClassName('area-editor')[0];
        var start = area.selectionStart;
        var end = area.selectionEnd;
        var text = area.value;
        var before = text.substring(0, start);
        var after = text.substring(end, text.length);
        if (url.substr(0, 18) == 'http://jv.stkr.fr/') {
            var newText = '[[sticker:' + url.slice(18, -7) + ']]';
            area.value = (before + newText + after);
            area.selectionStart = area.selectionEnd = start + newText.length;
        } else {
            var newText = ' ' + url + ' ';
            area.value = (before + newText + after);
            area.selectionStart = area.selectionEnd = start + newText.length;
        }
        area.focus();
        $(area).trigger('change');
    }

    function ajouterSticker(url) {
        var nSticker = document.createElement('li');
        nSticker.className = 'f-stkr-w';
        nSticker.innerHTML = '<div class="f-stkr f-no-sml" style="width:' + document.getElementsByClassName('f-stkrs f-cfx')[0].offsetWidth / espacementStickers + 'px"><span class="f-hlpr"></span><img src="' + url + '" style="max-height:' + tailleStickers + 'px;max-width:' + tailleStickers + 'px" data-code=""></div>';
        nSticker.onclick = function() {
            clique(url);
        };
        document.getElementsByClassName('f-stkrs f-cfx')[0].appendChild(nSticker);
    }

    function ajouterStickers(section) {
        if (section == 'Populaires' && (!(document.getElementsByClassName('f-stkrs f-cfx')[0].firstChild) || document.getElementsByClassName('f-stkrs f-cfx')[0].firstChild.firstChild.secondChild.src != catPopulaires[0])) {
            for (var q = 0; q < catPopulaires.length; q++) {
                ajouterSticker(catPopulaires[q]);
            }
        } else {
            for (var e = 0; e < allCat.length; e++) {
                if (section == catRealNames[e] && (!(document.getElementsByClassName('f-stkrs f-cfx')[0].firstChild) || document.getElementsByClassName('f-stkrs f-cfx')[0].firstChild.firstChild.secondChild.src != catCache[e][0])) {
                    for (var q = 0; q < catCache[e].length; q++) {
                        ajouterSticker(catCache[e][q]);
                    }
                    break;
                }
            }
        }
    }

    function charger(section) {
        var foo = document.getElementsByClassName('f-stkrs f-cfx')[0];
        while (foo.firstChild) {
            foo.removeChild(foo.firstChild);
        }
        var canvas = document.querySelector('div[data-flg-tt="' + section + '"]');
        var numChildren = canvas.parentElement.children.length;
        for (var i = 0; i < numChildren; i++) {
            if (canvas.parentElement.children[i].className.indexOf('f-active') != -1) {
                canvas.parentElement.children[i].className = canvas.parentElement.children[i].className.slice(0, -9);
                break;
            }
        }
        canvas.className += ' f-active';
        ajouterStickers(section);
    }

    function tArea() {
        document.getElementById('message_topic').removeEventListener('click', tArea);
        check();
    }

    function generateUpdate() {
        if (lastVersion > GM_info.script.version) {
            return '<a href="https://ticki84.github.io/JVCSticker++.user.js" target="_blank">Une nouvelle version de JVCSticker++ est disponible!</a><br><br><p>Version actuelle: ' + GM_info.script.version + '</p><p>Dernière version: ' + lastVersion + '</p>';
        } else {
            return '<p>Vous possédez la dernière version de JVCSticker++ (' + GM_info.script.version + ')</p>';
        }
    }

    function generateBanList() {
        var text = '';
        for (var g = 0; g < stickersBanListe.length; g++) {
            text += '<option>' + stickersBanListe[g] + '</option>';
        }
        return text;
    }

    function generateShownCat() {
        var text = '';
        for (var g = 0; g < catToShow.length; g++) {
            var idx = allCat.indexOf(catToShow[g]);
            text += '<option id="' + allCat[idx] + '">' + catRealNames[idx] + '</option>';
        }
        return text;
    }

    function generateHiddenCat() {
        var text = '';
        for (var g = 0; g < allCat.length; g++) {
            if (catToShow.indexOf(allCat[g]) == -1) {
                text += '<option id="' + allCat[g] + '">' + catRealNames[g] + '</option>';
            }
        }
        return text;
    }

    function chargerBarre() {
        isLoaded = true;
        setTimeout(function() {
            var canvasPop = document.querySelector('div[data-flg-tt="Populaires"]').parentElement;
            while (canvasPop.firstChild) {
                canvasPop.removeChild(canvasPop.firstChild);
            }
            var populaires = document.createElement('div');
            populaires.className = 'f-tab f-h f-active';
            populaires.style.width = '25px';
            populaires.style.height = '23px';
            populaires.style.lineHeight = '23px';
            populaires.style.fontSize = '14px';
            populaires.setAttribute('data-flg-tt', 'Populaires');
            populaires.innerHTML = '$<div style="display: none;" class="f-ttw"><div style="top: -26px; left: -9px;" class="f-inner"><div class="f-tt">Populaires</div><div class="f-arr"></div></div></div>';
            populaires.onclick = function() {
                charger('Populaires');
            };
            document.querySelector('div[data-flg-tt="Biblioth&egrave;que"]').parentElement.setAttribute('style', 'display:none !important;height:0px !important;width:0px !important');
            canvasPop.appendChild(populaires);
            var timeoutActive = false;
            var hbLoaded = false;
            var observerPopu = new MutationObserver(function(mutations, mutPopu) {
                if (document.getElementsByClassName('f-stkrs f-cfx')[0].getElementsByClassName('f-stkr-w').length > 27) {
                    hbLoaded = true;
                    mutPopu.disconnect();
                    charger('Populaires');
                } else if (timeoutActive === false) {
                    timeoutActive = true;
                    setTimeout(function() {
                        if (!hbLoaded) {
                            mutPopu.disconnect();
                            charger('Populaires');
                        }
                    }, 5000);
                }
                return;
            });
            observerPopu.observe(document.body, {
                childList: true,
                subtree: true,
                attributes: false,
                characterData: false
            });
            var shownListS = JSON.parse(GM_getValue('catToShow'));
            var listeBanS = JSON.parse(GM_getValue('stickersBanListe'));
            var MAJManuelle = false;
            var addOptions = document.createElement('div');
            addOptions.innerHTML = ' <div id="optionsModal" style="display:none;"><div class="modal-header"><h2 class="modal-title" style="color:#d13321; text-align:center;"><u>Options de JVCSticker++</u></h2></div><div class="modal-body"><div style="text-align: center;"><br><br>' + generateUpdate() + '<br></div><div class="optCategorie"><table><h3>Prévisualisation</h3><tr><td style="text-align: left;padding-left:10%;"><label for="espacementStickers">Espacement des stickers de la prévisualisation</label></td><td style="text-align: center;"><input type="number" id="espacementStickers" min="5" max="15" style="text-align:center;" value="' + GM_getValue('espacementStickers') + '"></td></tr><tr><td style="text-align: left;padding-left:10%;"><label for="tailleFenetre">Taille de la fenêtre (en pixel)</label></td><td style="text-align: center;"><input type="number" id="tailleFenetre" min="75" max="450" style="text-align:center;" value="' + GM_getValue('tailleFenetre') + '"></td></tr><tr><td style="text-align: left;padding-left:10%;"><label for="tailleStickers">Taille des stickers (en pixel)</label></td><td style="text-align: center;"><input type="number" id="tailleStickers" min="22" max="88" style="text-align:center;" value="' + GM_getValue('tailleStickers') + '"></td></tr><tr><td style="text-align: left;padding-left:10%;"><label for="xBarreIcones">Nombre de ligne(s) d\'icônes</label></td><td style="text-align: center;"><input type="number" id="xBarreIcones" min="1" max="5" style="text-align:center;" value="' + GM_getValue('xBarreIcones') + '"></td></tr></table><br></div><div class="optCategorie"><table><h3>Catégories</h3><tr><td style="text-align: left;padding-left:10%;"><select id="afficher" style="width:60%">' + generateHiddenCat() + '</select></td><td style="text-align: center;"><button type="button" class="btn btn-default" id="afficherSel">Afficher la catégorie</button></td></tr><tr><td style="text-align: left;padding-left:10%;"><select id="cacher" style="width:60%">' + generateShownCat() + '</select></td><td style="text-align: center;"><button type="button" class="btn btn-default" id="cacherSel">Cacher la catégorie</button></td></tr></table><br></div><div class="optCategorie"><table><h3>Options des stickers</h3><tr><td style="text-align: left;padding-left:10%;"><label for="tailleSticker">Taille des stickers (en %)</label></td><td style="text-align: center;"><input type="number" id="tailleSticker" min="50" max="300" style="text-align:center;" value="' + GM_getValue('tailleSticker') + '"></td></tr><tr><td style="text-align: left;padding-left:10%;"><label for="stickerCliquable">Stickers cliquables</label></td><td style="text-align: center;"><input type="checkbox" id="stickerCliquable"></td></tr><tr><td style="text-align: left;padding-left:10%;"><label for="sons">Ajouter des sons aux stickers</label></td><td style="text-align: center;"><input type="checkbox" id="sons"></td></tr><tr><td style="text-align: left;padding-left:10%;"><label for="intervalle">Intervalle de mise à jour des statistiques</label></td><td style="text-align: center;"><select id="intervalle" style="width:60%"><option>Toutes les 15 minutes</option><option>Toutes les 30 minutes</option><option>Toutes les heures</option><option>Toutes les 3 heures</option><option>Toutes les 6 heures</option><option>Toutes les 12 heures</option><option>Toutes les 24 heures</option></select></td></tr><tr><td colSpan=\'2\'><button type="button" class="btn btn-default" id="MAJManuelle">Mettre à jour les statistiques maintenant</button></td></tr></table><br></div><div class="optCategorie"><table><h3>Thème général</h3><tr><td style="text-align: left;padding-left:10%;"><label for="modifierCouleurPosts">Modifier la couleur des posts bleus</label></td><td style="text-align: center;"><input type="checkbox" id="modifierCouleurPosts"></td></tr><tr><td style="text-align: left;padding-left:12.5%;"><label for="couleurBackground">Couleur de fond</label></td><td style="text-align: center;"><input type="text" id="couleurBackground" style="text-align:center;" value="' + GM_getValue('couleurBackground') + '"></td></tr><tr><td style="text-align: left;padding-left:12.5%;"><label for="couleurBordure">Couleur des bordures</label></td><td style="text-align: center;"><input type="text" id="couleurBordure" style="text-align:center;" value="' + GM_getValue('couleurBordure') + '"></td></tr><tr><td style="text-align: left;padding-left:10%;"><label for="supprimerFond">Supprimer le fond blanc des images transparentes</label></td><td style="text-align: center;"><input type="checkbox" id="supprimerFond"></td></tr></table><br></div><div class="optCategorie"><table><h3>Bannissement de stickers</h3><tr><td style="text-align: left;padding-left:10%;"><label for="supprStickersBan">Supprimer les stickers bannis</label></td><td style="text-align: center;"><input type="checkbox" id="supprStickersBan"></td></tr><tr><td style="text-align: left;padding-left:12.5%;"><label for="stickerMessageDeSuppr">Afficher un message pour remplacer les stickers/posts bannis</label></td><td style="text-align: center;"><input type="checkbox" id="stickerMessageDeSuppr"></td></tr><tr><td style="text-align: left;padding-left:12.5%;"><label for="supprDesPosts">Supprimer les posts contenant un sticker banni</label></td><td style="text-align: center;"><input type="checkbox" id="supprDesPosts"></td></tr><tr><td style="text-align: left;padding-left:10%;"><input type="text" id="banStickerName" style="width: 60%"></td><td style="text-align: center;"><button type="button" class="btn btn-default" id="ajouterBan">Ajouter à la liste des stickers bannis</button></td></tr><tr><td style="text-align: left;padding-left:10%;"><select id="listeBan" size="4" style="width:61%">' + generateBanList() + '</select></td><td style="text-align: center;"><button type="button" class="btn btn-default" id="supprBan">Supprimer de la liste</button></td></tr></table><br></div><div class="optCategorie"><table><h3>Autres</h3><tr><td style="text-align: left;padding-left:10%;"><label for="webmPlayer">Lecture des webm</label></td><td style="text-align: center;"><input type="checkbox" id="webmPlayer"></td></tr><tr><td style="text-align: left;padding-left:10%;"><label for="youtubePlayer">Lecture des vidéos Youtube</label></td><td style="text-align: center;"><input type="checkbox" id="youtubePlayer"></td></tr><tr><td style="text-align: left;padding-left:10%;"><label for="vocarooPlayer">Lecture des Vocaroo</label></td><td style="text-align: center;"><input type="checkbox" id="vocarooPlayer"></td></tr><tr><td style="text-align: left;padding-left:10%;"><label for="playerSignature">Jouer les médias des signatures</label></td><td style="text-align: center;"><input type="checkbox" id="playerSignature"></td></tr><tr><td colSpan=\'2\'><button type="button" class="btn btn-default" id="resetAll">Réinitialiser toutes les options par défaut</button></td></tr></table><br></div></div><div class="modal-footer" style="text-align: center;"><button type="button" class="btn btn-default" data-dismiss="modal" id="valider">Sauvegarder</button> <a rel="modal:close" id="annulerBtn"><button type="button" class="btn btn-default" data-dismiss="modal" id="annuler">Annuler</button></a></div></div><p style="display:none;"><a href="#optionsModal" id="toOptionsModal" rel="modal:open">Open Modal</a></p>';
            document.body.appendChild(addOptions);
            switch (intervalle) {
                case '15':
                    document.getElementById('intervalle').selectedIndex = '0';
                    break;
                case '30':
                    document.getElementById('intervalle').selectedIndex = '1';
                    break;
                case '60':
                    document.getElementById('intervalle').selectedIndex = '2';
                    break;
                case '180':
                    document.getElementById('intervalle').selectedIndex = '3';
                    break;
                case '360':
                    document.getElementById('intervalle').selectedIndex = '4';
                    break;
                case '720':
                    document.getElementById('intervalle').selectedIndex = '5';
                    break;
                case '1440':
                    document.getElementById('intervalle').selectedIndex = '6';
                    break;
                default:
                    document.getElementById('intervalle').selectedIndex = '3';
            }
            var checkboxList = [
                'stickerCliquable',
                'sons',
                'modifierCouleurPosts',
                'supprimerFond',
                'supprStickersBan',
                'stickerMessageDeSuppr',
                'supprDesPosts',
                'webmPlayer',
                'youtubePlayer',
                'vocarooPlayer',
                'playerSignature'
            ];
            for (var h = 0; h < checkboxList.length; h++) {
                if (GM_getValue(checkboxList[h]) === true) {
                    document.getElementById(checkboxList[h]).setAttribute('checked', 'checked');
                } else {
                    if (document.getElementById(checkboxList[h]).checked == 'checked') {
                        document.getElementById(checkboxList[h]).removeAttribute('checked');
                    }
                }
            }
            document.getElementById('modifierCouleurPosts').addEventListener('click', function() {
                if (!(document.getElementById('modifierCouleurPosts').checked)) {
                    document.getElementById('couleurBackground').parentElement.parentElement.style.display = 'none';
                    document.getElementById('couleurBordure').parentElement.parentElement.style.display = 'none';
                } else {
                    document.getElementById('couleurBackground').parentElement.parentElement.removeAttribute('style');
                    document.getElementById('couleurBordure').parentElement.parentElement.removeAttribute('style');
                }
            });
            document.getElementById('supprStickersBan').addEventListener('click', function() {
                if (!(document.getElementById('supprStickersBan').checked)) {
                    document.getElementById('stickerMessageDeSuppr').parentElement.parentElement.style.display = 'none';
                    document.getElementById('supprDesPosts').parentElement.parentElement.style.display = 'none';
                } else {
                    document.getElementById('stickerMessageDeSuppr').parentElement.parentElement.removeAttribute('style');
                    document.getElementById('supprDesPosts').parentElement.parentElement.removeAttribute('style');
                }
            });
            if (!document.getElementById('modifierCouleurPosts').checked) {
                document.getElementById('couleurBackground').parentElement.parentElement.style.display = 'none';
                document.getElementById('couleurBordure').parentElement.parentElement.style.display = 'none';
            }
            if (!document.getElementById('supprStickersBan').checked) {
                document.getElementById('stickerMessageDeSuppr').parentElement.parentElement.style.display = 'none';
                document.getElementById('supprDesPosts').parentElement.parentElement.style.display = 'none';
            }
            document.getElementById('resetAll').addEventListener('click', function() {
                document.getElementById('espacementStickers').value = '10';
                document.getElementById('tailleFenetre').value = '150';
                document.getElementById('tailleStickers').value = '44';
                document.getElementById('xBarreIcones').value = '2';
                shownListS = JSON.parse(GM_getValue('catToShowS'));
                var newAfficher = '';
                for (var g = 0; g < allCat.length; g++) {
                    if (shownListS.indexOf(allCat[g]) == -1) {
                        newAfficher += '<option id="' + allCat[g] + '">' + catRealNames[g] + '</option>';
                    }
                }
                document.getElementById('afficher').innerHTML = newAfficher;
                var newCacher = '';
                for (var g = 0; g < shownListS.length; g++) {
                    var idxG = allCat.indexOf(shownListS[g]);
                    newCacher += '<option id="' + allCat[idxG] + '">' + catRealNames[idxG] + '</option>';
                }
                document.getElementById('cacher').innerHTML = newCacher;
                document.getElementById('tailleSticker').value = '100';
                document.getElementById('stickerCliquable').checked = true;
                document.getElementById('sons').checked = true;
                document.getElementById('intervalle').selectedIndex = '3';
                MAJManuelle = false;
                document.getElementById('modifierCouleurPosts').checked = true;
                document.getElementById('couleurBackground').parentElement.parentElement.removeAttribute('style');
                document.getElementById('couleurBordure').parentElement.parentElement.removeAttribute('style');
                document.getElementById('couleurBackground').value = '#FFF';
                document.getElementById('couleurBordure').value = '#d5d5d5';
                document.getElementById('supprimerFond').checked = true;
                document.getElementById('supprStickersBan').checked = true;
                document.getElementById('stickerMessageDeSuppr').parentElement.parentElement.removeAttribute('style');
                document.getElementById('supprDesPosts').parentElement.parentElement.removeAttribute('style');
                document.getElementById('stickerMessageDeSuppr').checked = true;
                document.getElementById('supprDesPosts').checked = false;
                document.getElementById('banStickerName').value = '';
                listeBanS = [
                    'http://jv.stkr.fr/p/1miq',
                    'http://jv.stkr.fr/p/1min',
                    'http://jv.stkr.fr/p/1mim',
                    'http://jv.stkr.fr/p/1mig-fr',
                    'http://jv.stkr.fr/p/1mij-fr',
                    'http://jv.stkr.fr/p/1mio',
                    'http://jv.stkr.fr/p/1mik',
                    'http://jv.stkr.fr/p/1mip',
                    'http://jv.stkr.fr/p/1mif',
                    'http://jv.stkr.fr/p/1mii-fr',
                    'http://jv.stkr.fr/p/1mih-fr',
                    'http://jv.stkr.fr/p/1mil',
                    'http://jv.stkr.fr/p/1mie-fr',
                    'http://jv.stkr.fr/p/1mid-fr',
                    'http://jv.stkr.fr/p/1myf',
                    'http://jv.stkr.fr/p/1my7',
                    'http://jv.stkr.fr/p/1myc',
                    'http://jv.stkr.fr/p/1my9',
                    'http://jv.stkr.fr/p/1myb',
                    'http://jv.stkr.fr/p/1my6',
                    'http://jv.stkr.fr/p/1mye',
                    'http://jv.stkr.fr/p/1myx',
                    'http://jv.stkr.fr/p/1myd',
                    'http://jv.stkr.fr/p/1my4',
                    'http://jv.stkr.fr/p/1my8',
                    'http://jv.stkr.fr/p/1mya',
                    'http://jv.stkr.fr/p/1my5',
                    'http://jv.stkr.fr/p/1n28'
                ];
                var newBan = '';
                for (var g = 0; g < listeBanS.length; g++) {
                    newBan += '<option>' + listeBanS[g] + '</option>';
                }
                document.getElementById('listeBan').innerHTML = newBan;
                document.getElementById('webmPlayer').checked = true;
                document.getElementById('youtubePlayer').checked = true;
                document.getElementById('vocarooPlayer').checked = true;
                document.getElementById('playerSignature').checked = false;
            });
            document.getElementById('afficherSel').addEventListener('click', function() {
                var idx = document.getElementById('afficher').selectedIndex;
                if (typeof document.getElementById('afficher').options[idx] != 'undefined') {
                    var nameIdx = catRealNames.indexOf(document.getElementById('afficher').options[idx].text);
                    var found = false;
                    if (shownListS.length === 0) {
                        shownListS.push(allList[nameIdx]);
                        found = true;
                    }
                    if (!(found)) {
                        for (var z = 0; z < nameIdx; z++) {
                            if (shownListS.indexOf(allCat[nameIdx - z]) != -1) {
                                found = true;
                                shownListS.splice(shownListS.indexOf(allCat[nameIdx - z]) + 1, 0, allCat[nameIdx]);
                                break;
                            }
                        }
                    }
                    if (!(found)) {
                        for (var z = 0; z < allCat.length - nameIdx; z++) {
                            if (shownListS.indexOf(allCat[nameIdx + z]) != -1) {
                                shownListS.splice(shownListS.indexOf(allCat[nameIdx + z]) + 1, 0, allCat[nameIdx]);
                                break;
                            }
                        }
                    }
                    var newAfficher = '';
                    for (var g = 0; g < allCat.length; g++) {
                        if (shownListS.indexOf(allCat[g]) == -1) {
                            newAfficher += '<option id="' + allCat[g] + '">' + catRealNames[g] + '</option>';
                        }
                    }
                    document.getElementById('afficher').innerHTML = newAfficher;
                    var nameSave = allCat[catRealNames.indexOf(document.getElementById('cacher').options[document.getElementById('cacher').selectedIndex].text)];
                    var newCacher = '';
                    for (var g = 0; g < shownListS.length; g++) {
                        var idxG = allCat.indexOf(shownListS[g]);
                        newCacher += '<option id="' + allCat[idxG] + '">' + catRealNames[idxG] + '</option>';
                    }
                    document.getElementById('cacher').innerHTML = newCacher;
                    document.getElementById('cacher').selectedIndex = shownListS.indexOf(nameSave);
                }
            });
            document.getElementById('cacherSel').addEventListener('click', function() {
                var idx = document.getElementById('cacher').selectedIndex;
                if (typeof document.getElementById('cacher').options[idx] != 'undefined') {
                    var name = allCat[catRealNames.indexOf(document.getElementById('cacher').options[idx].text)];
                    shownListS.splice(shownListS.indexOf(name), 1);
                    var nameSave = allCat[catRealNames.indexOf(document.getElementById('afficher').options[document.getElementById('afficher').selectedIndex].text)];
                    var newAfficher = '';
                    var tempList = [];
                    for (var g = 0; g < allCat.length; g++) {
                        if (shownListS.indexOf(allCat[g]) == -1) {
                            tempList.push(allCat[g]);
                            newAfficher += '<option id="' + allCat[g] + '">' + catRealNames[g] + '</option>';
                        }
                    }
                    document.getElementById('afficher').innerHTML = newAfficher;
                    document.getElementById('afficher').selectedIndex = tempList.indexOf(nameSave);
                    var newCacher = '';
                    for (var g = 0; g < shownListS.length; g++) {
                        var idxG = allCat.indexOf(shownListS[g]);
                        newCacher += '<option id="' + allCat[idxG] + '">' + catRealNames[idxG] + '</option>';
                    }
                    document.getElementById('cacher').innerHTML = newCacher;
                }
            });
            document.getElementById('MAJManuelle').addEventListener('click', function() {
                MAJManuelle = true;
            });
            document.getElementById('ajouterBan').addEventListener('click', function() {
                listeBanS.push(document.getElementById('banStickerName').value);
                var text = '';
                for (var g = 0; g < listeBanS.length; g++) {
                    text += '<option>' + listeBanS[g] + '</option>';
                }
                document.getElementById('listeBan').innerHTML = text;
                document.getElementById('listeBan').selectedIndex = listeBanS.indexOf(document.getElementById('banStickerName').value);
                document.getElementById('banStickerName').value = '';
            });
            document.getElementById('supprBan').addEventListener('click', function() {
                var idx = document.getElementById('listeBan').selectedIndex;
                if (idx != -1) {
                    listeBanS.splice(idx, 1);
                    var text = '';
                    for (var g = 0; g < listeBanS.length; g++) {
                        text += '<option>' + listeBanS[g] + '</option>';
                    }
                    document.getElementById('listeBan').innerHTML = text;
                    document.getElementById('listeBan').selectedIndex = idx;
                    if (document.getElementById('listeBan').selectedIndex == -1) {
                        document.getElementById('listeBan').selectedIndex = idx - 1;
                    }
                }
            });
            $('#optionsModal').on('modal:close', function() {
                var textList = [
                    'espacementStickers',
                    'tailleFenetre',
                    'tailleStickers',
                    'xBarreIcones',
                    'tailleSticker',
                    'couleurBackground',
                    'couleurBordure'
                ];
                for (var h = 0; h < textList.length; h++) {
                    document.getElementById(textList[h]).value = GM_getValue(textList[h]);
                }
                var checkboxList = [
                    'stickerCliquable',
                    'sons',
                    'modifierCouleurPosts',
                    'supprimerFond',
                    'supprStickersBan',
                    'stickerMessageDeSuppr',
                    'supprDesPosts',
                    'webmPlayer',
                    'youtubePlayer',
                    'vocarooPlayer',
                    'playerSignature'
                ];
                for (var h = 0; h < checkboxList.length; h++) {
                    document.getElementById(checkboxList[h]).checked = GM_getValue(checkboxList[h]);
                }
                document.getElementById('banStickerName').value = '';
                shownListS = JSON.parse(GM_getValue('catToShow'));
                var newAfficher = '';
                for (var g = 0; g < allCat.length; g++) {
                    if (shownListS.indexOf(allCat[g]) == -1) {
                        newAfficher += '<option id="' + allCat[g] + '">' + catRealNames[g] + '</option>';
                    }
                }
                document.getElementById('afficher').innerHTML = newAfficher;
                var newCacher = '';
                for (var g = 0; g < shownListS.length; g++) {
                    var idxG = allCat.indexOf(shownListS[g]);
                    newCacher += '<option id="' + allCat[idxG] + '">' + catRealNames[idxG] + '</option>';
                }
                document.getElementById('cacher').innerHTML = newCacher;
                listeBanS = JSON.parse(GM_getValue('stickersBanListe'));
                var newBan = '';
                for (var g = 0; g < listeBanS.length; g++) {
                    newBan += '<option>' + listeBanS[g] + '</option>';
                }
                switch (GM_getValue('intervalle')) {
                    case '15':
                        document.getElementById('intervalle').selectedIndex = '0';
                        break;
                    case '30':
                        document.getElementById('intervalle').selectedIndex = '1';
                        break;
                    case '60':
                        document.getElementById('intervalle').selectedIndex = '2';
                        break;
                    case '180':
                        document.getElementById('intervalle').selectedIndex = '3';
                        break;
                    case '360':
                        document.getElementById('intervalle').selectedIndex = '4';
                        break;
                    case '720':
                        document.getElementById('intervalle').selectedIndex = '5';
                        break;
                    case '1440':
                        document.getElementById('intervalle').selectedIndex = '6';
                        break;
                    default:
                        document.getElementById('intervalle').selectedIndex = '3';
                }
                MAJManuelle = false;
                document.getElementById('listeBan').innerHTML = newBan;
            });
            document.getElementById('valider').addEventListener('click', function() {
                var optNames = [
                    'espacementStickers',
                    'tailleFenetre',
                    'tailleStickers',
                    'xBarreIcones',
                    'tailleSticker',
                    'couleurBackground',
                    'couleurBordure'
                ];
                for (var d = 0; d < optNames.length; d++) {
                    GM_setValue(optNames[d], document.getElementById(optNames[d]).value);
                }
                GM_setValue('catToShow', JSON.stringify(shownListS));
                var optCheckbox = [
                    'stickerCliquable',
                    'sons',
                    'modifierCouleurPosts',
                    'supprimerFond',
                    'supprStickersBan',
                    'stickerMessageDeSuppr',
                    'supprDesPosts',
                    'webmPlayer',
                    'youtubePlayer',
                    'vocarooPlayer',
                    'playerSignature'
                ];
                for (var d = 0; d < optCheckbox.length; d++) {
                    if (document.getElementById(optCheckbox[d]).checked === true || document.getElementById(optCheckbox[d]).checked == 'checked') {
                        GM_setValue(optCheckbox[d], true);
                    } else {
                        GM_setValue(optCheckbox[d], false);
                    }
                }
                switch (document.getElementById('intervalle').selectedIndex) {
                    case 0:
                        GM_setValue('intervalle', '15');
                        break;
                    case 1:
                        GM_setValue('intervalle', '30');
                        break;
                    case 2:
                        GM_setValue('intervalle', '60');
                        break;
                    case 3:
                        GM_setValue('intervalle', '180');
                        break;
                    case 4:
                        GM_setValue('intervalle', '360');
                        break;
                    case 5:
                        GM_setValue('intervalle', '720');
                        break;
                    case 6:
                        GM_setValue('intervalle', '1440');
                        break;
                    default:
                        GM_setValue('intervalle', '180');
                }
                if (MAJManuelle) {
                    GM_deleteValue('lTimestamp');
                }
                GM_setValue('stickersBanListe', JSON.stringify(listeBanS));
                document.getElementById('annulerBtn').click();
            });
            var canvasHide = document.querySelector('div[data-flg-tt="Biblioth&egrave;que"]').parentElement.parentElement;
            var refresh = document.createElement('div');
            refresh.className = 'f-tabs-r';
            refresh.innerHTML = '<img src="http://image.noelshack.com/fichiers/2016/23/1465690515-refresh.png" height="23" width="25"></img><img src="http://image.noelshack.com/fichiers/2016/24/1465930183-application-x-desktop.png" height="23" width="25"></img>';
            canvasHide.insertBefore(refresh, canvasHide.secondChild);
            refresh.firstChild.addEventListener('click', function() {
                window.open('https://ticki84.github.io/JVCSticker++.user.js', '_blank');
            });
            refresh.children[1].addEventListener('click', function() {
                document.getElementById('toOptionsModal').click();
            });
            canvasPop.style.width = $(canvasPop.parentElement).outerWidth(true) - $(canvasPop.parentElement).children().last().outerWidth(true) + 'px';
            canvasPop.style.height = xBarreIcones * $(canvasPop.parentElement).outerHeight(true) + 'px';
            var fenSticker = document.getElementsByClassName('f-stkrs-w f-mid-fill-h')[0];
            fenSticker.style.height = tailleFenetre + 'px';
            fenSticker.parentElement.style.height = tailleFenetre + 'px';
            fenSticker.parentElement.parentElement.style.height = tailleFenetre + 'px';
            if (supprimerFond) {
                var f = [];
                var x = document.getElementsByClassName('img-shack');

                function createf(i) {
                    return function() {
                        var z = x[i].alt;
                        var newSrc = x[i].src;
                        if (z.includes('/fichiers/') && (z.endsWith('.png') || z.endsWith('.gif'))) {
                            newSrc = x[i].alt;
                        } else if (z.includes('/minis/') && (z.endsWith('.png') || z.endsWith('.gif'))) {
                            newSrc = x[i].alt;
                            newSrc = newSrc.replace('/minis/', '/fichiers/');
                        } else if (x[i].src.includes('/minis/') && (z.endsWith('.png') || z.endsWith('.gif'))) {
                            newSrc = newSrc.replace('/minis/', '/fichiers/');
                            if (z.endsWith('.gif')) {
                                newSrc = newSrc.replace('.png', '.gif');
                            }
                        } else if (x[i].src.includes('/minis/') && (z.endsWith('.jpg') || z.endsWith('.jpeg'))) {
                            newSrc = newSrc.replace('/minis/', '/fichiers/');
                            if (z.endsWith('.jpg')) {
                                newSrc = newSrc.replace('.png', '.jpg');
                            } else if (z.endsWith('.jpeg')) {
                                newSrc = newSrc.replace('.png', '.jpeg');
                            }
                        }
                        var newImg = new Image();
                        newImg.addEventListener('load', function() {
                            if (this.width / this.height > 4 / 3 * 0.99 && this.width / this.height < 4 / 3 * 1.01) {
                                if (!stickerCliquable) {
                                    x[i].parentElement.style.display = 'none';
                                    x[i].parentElement.parentElement.insertBefore(x[i], x[i].parentElement);
                                }
                                x[i].src = this.src;
                            }
                        });
                        newImg.src = newSrc;
                    };
                }
                for (var i = 0; i < x.length; i++) {
                    f[i] = createf(i);
                    f[i]();
                }
                if (tailleSticker != 100) {
                    var y = document.getElementsByClassName('img-stickers');
                    for (var i = 0; i < y.length; i++) {
                        y[i].width = y[i].width * tailleSticker / 100;
                        y[i].height = y[i].height * tailleSticker / 100;
                        if (!(y[i].className.includes('stickersM'))) {
                            y[i].className += ' stickersM';
                        }
                    }
                }
            }
            if (!supprimerFond && tailleSticker != 100) {
                var x = document.getElementsByClassName('img-shack');
                for (var i = 0; i < x.length; i++) {
                    x[i].width = x[i].width * tailleSticker / 100;
                    x[i].height = x[i].height * tailleSticker / 100;
                }
                if (tailleSticker != 100) {
                    var y = document.getElementsByClassName('img-stickers');
                    for (var i = 0; i < y.length; i++) {
                        y[i].width = y[i].width * tailleSticker / 100;
                        y[i].height = y[i].height * tailleSticker / 100;
                        if (!(y[i].className.includes('stickersM'))) {
                            y[i].className += ' stickersM';
                        }
                    }
                }
            }
            if (sons) {
                var audioList = [];
                var audioN = 0;
                var imgsList = document.getElementsByClassName('img-shack');
                for (var i = 0; i < imgsList.length; i++) {
                    {
                        if ($.inArray(imgsList[i].alt, [
                                'http://image.noelshack.com/fichiers/2016/24/1466366189-risitas1.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366191-risitas2.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366191-risitas3.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366681-risitas4.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366189-risitas5.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366188-risitas6.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366195-risitas7.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366192-risitas8.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366195-risitas9.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366197-risitas10.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366197-risitas11.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366196-risitas12.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366200-risitas13.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366203-risitas14.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366200-risitas15.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366202-risitas16.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366202-risitas17.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366203-risitas18.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366344-risitas19.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366202-risitas20.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366203-risitas21.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366206-risitas22.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366207-risitas23.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366209-risitas24.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366209-risitas25.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366209-risitas26.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366210-risitas27.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366214-risitas28.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366214-risitas29.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366666-risitas30.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366218-risitas31.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366212-risitas32.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366325-risitas34.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366224-risitas35.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366244-risitas36.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366330-risitas37.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366339-risitas39.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366268-risitas40.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366292-risitas42.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366277-risitas43.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366232-risitas44.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366310-risitas45.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366329-risitas47.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366220-risitas48.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366324-risitas49.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366318-risitas50.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366219-risitas54.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366240-risitas55.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366251-risitas56.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366269-risitas57.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366224-risitas58.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366342-risitas59.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366296-risitas60.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366334-risitas61.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366281-risitas62.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366260-risitas63.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366344-risitas64.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474465875-risitas65.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366218-risitas66.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366276-risitas69.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366324-risitas71.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366258-risitas73.png',
                                'http://image.noelshack.com/fichiers/2016/25/1466554357-risitas74.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366344-risitas75.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366299-risitas76.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366257-risitas77.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366265-risitas78.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366349-risitas79.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366232-risitas80.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366584-risitas81.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366586-risitas82.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366667-risitas83.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366587-risitas84.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366588-risitas85.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366597-risitas86.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366594-risitas87.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366594-risitas88.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366592-risitas89.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366601-risitas90.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366598-risitas91.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366596-risitas92.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366605-risitas93.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366602-risitas94.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366603-risitas95.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366609-risitas96.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366606-risitas97.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366609-risitas98.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366611-risitas100.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366611-risitas101.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366614-risitas102.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366615-risitas103.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366614-risitas104.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366619-risitas105.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366614-risitas106.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366616-risitas107.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366618-risitas109.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366621-risitas110.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366628-risitas111.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366696-risitas113.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366637-risitas114.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366648-risitas115.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366664-risitas117.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366677-risitas118.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474465875-risitas119.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366668-risitas121.png',
                                'http://image.noelshack.com/fichiers/2016/26/1467309404-risitas124.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366661-risitas125.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366661-risitas126.png',
                                'http://image.noelshack.com/fichiers/2016/25/1466715113-risitas128.png',
                                'http://image.noelshack.com/fichiers/2016/25/1466715109-risitas129.png',
                                'http://image.noelshack.com/fichiers/2016/25/1466715106-risitas130.png',
                                'http://image.noelshack.com/fichiers/2016/25/1466715107-risitas131.png',
                                'http://image.noelshack.com/fichiers/2016/25/1466715113-risitas133.png',
                                'http://image.noelshack.com/fichiers/2016/25/1466715112-risitas134.png',
                                'http://image.noelshack.com/fichiers/2016/25/1466715223-risitas135.png',
                                'http://image.noelshack.com/fichiers/2016/25/1466715116-risitas137.png',
                                'http://image.noelshack.com/fichiers/2016/25/1466715155-risitas138.png',
                                'http://image.noelshack.com/fichiers/2016/25/1466715458-risitas140.png',
                                'http://image.noelshack.com/fichiers/2016/25/1466715124-risitas141.png',
                                'http://image.noelshack.com/fichiers/2016/25/1466715120-risitas142.png',
                                'http://image.noelshack.com/fichiers/2016/25/1466715125-risitas143.png',
                                'http://image.noelshack.com/fichiers/2016/25/1466715128-risitas144.png',
                                'http://image.noelshack.com/fichiers/2016/25/1466715125-risitas145.png',
                                'http://image.noelshack.com/fichiers/2016/25/1466715203-risitas147.png',
                                'http://image.noelshack.com/fichiers/2016/25/1466715141-risitas148.png',
                                'http://image.noelshack.com/fichiers/2016/25/1466715156-risitas149.png',
                                'http://image.noelshack.com/fichiers/2016/25/1466715156-risitas150.png',
                                'http://image.noelshack.com/fichiers/2016/25/1466715212-risitas151.png',
                                'http://image.noelshack.com/fichiers/2016/25/1466729105-risitas152.png',
                                'http://image.noelshack.com/fichiers/2016/25/1466729105-risitas153.png',
                                'http://image.noelshack.com/fichiers/2016/25/1466729105-risitas154.png',
                                'http://image.noelshack.com/fichiers/2016/26/1467309317-risitas155.png',
                                'http://image.noelshack.com/fichiers/2016/26/1467309317-risitas156.png',
                                'http://image.noelshack.com/fichiers/2016/26/1467309317-risitas157.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469405727-risitas159.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469405726-risitas160.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469405726-risitas161.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469405728-risitas165.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469541951-risitas180.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469541952-risitas182.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469541983-risitas191.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469541975-risitas192.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469541956-risitas197.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469541957-risitas199.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469541957-risitas202.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469541961-risitas210.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469541962-risitas211.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469541962-risitas212.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469541962-risitas213.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469541964-risitas214.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469541964-risitas215.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469541963-risitas217.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469541965-risitas219.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469541964-risitas220.png',
                                'http://image.noelshack.com/fichiers/2016/36/1473264104-risitas221.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469541972-risitas229.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469541972-risitas230.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469541972-risitas231.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469971029-risitas233.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469971029-risitas235.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469971029-risitas236.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469971030-risitas238.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469971030-risitas239.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469971031-risitas240.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469971032-risitas241.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469971032-risitas242.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469971032-risitas243.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469971032-risitas244.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469971035-risitas248.png',
                                'http://image.noelshack.com/fichiers/2016/31/1470494333-risitas260.png',
                                'http://image.noelshack.com/fichiers/2016/31/1470494337-risitas261.png',
                                'http://image.noelshack.com/fichiers/2016/31/1470494334-risitas262.png',
                                'http://image.noelshack.com/fichiers/2016/31/1470494332-risitas263.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474492404-risitas264.gif',
                                'http://image.noelshack.com/fichiers/2016/31/1470494341-risitas267.png',
                                'http://image.noelshack.com/fichiers/2016/31/1470494345-risitas274.png',
                                'http://image.noelshack.com/fichiers/2016/31/1470494345-risitas275.png',
                                'http://image.noelshack.com/fichiers/2016/31/1470494347-risitas276.png',
                                'http://image.noelshack.com/fichiers/2016/31/1470494366-risitas283.png',
                                'http://image.noelshack.com/fichiers/2016/31/1470494361-risitas284.png',
                                'http://image.noelshack.com/fichiers/2016/31/1470494491-risitas285.png',
                                'http://image.noelshack.com/fichiers/2016/31/1470494356-risitas286.png',
                                'http://image.noelshack.com/fichiers/2016/31/1470494464-risitas291.png',
                                'http://image.noelshack.com/fichiers/2016/31/1470494358-risitas292.png',
                                'http://image.noelshack.com/fichiers/2016/31/1470494483-risitas297.png',
                                'http://image.noelshack.com/fichiers/2016/31/1470494359-risitas299.png',
                                'http://image.noelshack.com/fichiers/2016/31/1470494480-risitas301.png',
                                'http://image.noelshack.com/fichiers/2016/31/1470494378-risitas304.png',
                                'http://image.noelshack.com/fichiers/2016/31/1470494470-risitas306.png',
                                'http://image.noelshack.com/fichiers/2016/31/1470494371-risitas307.png',
                                'http://image.noelshack.com/fichiers/2016/31/1470494372-risitas309.png',
                                'http://image.noelshack.com/fichiers/2016/31/1470494500-risitas311.png',
                                'http://image.noelshack.com/fichiers/2016/31/1470494355-risitas312.png',
                                'http://image.noelshack.com/fichiers/2016/31/1470494431-risitas314.png',
                                'http://image.noelshack.com/fichiers/2016/31/1470494545-risitas315.png',
                                'http://image.noelshack.com/fichiers/2016/31/1470494496-risitas320.png',
                                'http://image.noelshack.com/fichiers/2016/31/1470494414-risitas321.png',
                                'http://image.noelshack.com/fichiers/2016/32/1470920767-risitas324.png',
                                'http://image.noelshack.com/fichiers/2016/32/1470920768-risitas325.png',
                                'http://image.noelshack.com/fichiers/2016/32/1470920769-risitas331.png',
                                'http://image.noelshack.com/fichiers/2016/32/1470920769-risitas332.png',
                                'http://image.noelshack.com/fichiers/2016/32/1470920769-risitas333.png',
                                'http://image.noelshack.com/fichiers/2016/32/1470920769-risitas334.png',
                                'http://image.noelshack.com/fichiers/2016/32/1470920769-risitas335.png',
                                'http://image.noelshack.com/fichiers/2016/32/1470920769-risitas336.png',
                                'http://image.noelshack.com/fichiers/2016/32/1470920773-risitas337.png',
                                'http://image.noelshack.com/fichiers/2016/32/1470920779-risitas339.png',
                                'http://image.noelshack.com/fichiers/2016/32/1470920774-risitas344.png',
                                'http://image.noelshack.com/fichiers/2016/32/1470920778-risitas348.png',
                                'http://image.noelshack.com/fichiers/2016/32/1470920781-risitas349.png',
                                'http://image.noelshack.com/fichiers/2016/32/1470920781-risitas350.png',
                                'http://image.noelshack.com/fichiers/2016/32/1470920779-risitas351.png',
                                'http://image.noelshack.com/fichiers/2016/32/1470920779-risitas355.png',
                                'http://image.noelshack.com/fichiers/2016/32/1470920774-risitas359.png',
                                'http://image.noelshack.com/fichiers/2016/32/1470920782-risitas360.png',
                                'http://image.noelshack.com/fichiers/2016/32/1470920776-risitas361.png',
                                'http://image.noelshack.com/fichiers/2016/32/1470920798-risitas362.png',
                                'http://image.noelshack.com/fichiers/2016/32/1470920788-risitas363.png',
                                'http://image.noelshack.com/fichiers/2016/32/1470920780-risitas364.png',
                                'http://image.noelshack.com/fichiers/2016/32/1470920778-risitas366.png',
                                'http://image.noelshack.com/fichiers/2016/32/1470920783-risitas367.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490210-risitas370.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490212-risitas376.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490211-risitas377.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490212-risitas378.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490212-risitas379.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490213-risitas380.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490213-risitas381.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490214-risitas382.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490219-risitas389.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490216-risitas392.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490216-risitas393.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490220-risitas396.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490217-risitas397.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490219-risitas399.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490220-risitas400.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490220-risitas401.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490221-risitas402.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490220-risitas403.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490221-risitas406.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490222-risitas407.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490223-risitas409.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490271-risitas410.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490224-risitas411.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490223-risitas412.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490224-risitas413.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490224-risitas414.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490224-risitas415.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490226-risitas417.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490228-risitas423.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490266-risitas424.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490228-risitas426.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490231-risitas427.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490230-risitas428.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490232-risitas430.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490233-risitas431.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490234-risitas432.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490234-risitas433.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490236-risitas435.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490239-risitas441.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490241-risitas443.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490278-risitas449.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490278-risitas450.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490278-risitas452.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490279-risitas454.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490280-risitas455.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490289-risitas459.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490283-risitas462.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490289-risitas469.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490285-risitas470.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490306-risitas471.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490303-risitas472.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490301-risitas473.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490306-risitas481.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490304-risitas482.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490307-risitas483.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490303-risitas484.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490294-risitas485.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490286-risitas486.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490305-risitas491.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490287-risitas492.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490292-risitas493.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490281-risitas496.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490306-risitas497.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490300-risitas499.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490286-risitas503.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490296-risitas505.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490306-risitas507.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490305-risitas509.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490285-risitas511.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490287-risitas514.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490294-risitas515.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490280-risitas516.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490281-risitas517.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490307-risitas518.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490286-risitas519.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490295-risitas520.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490287-risitas521.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490295-risitas522.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490280-risitas523.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490289-risitas525.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490302-risitas527.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490310-risitas528.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490310-risitas530.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490309-risitas531.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490310-risitas532.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490309-risitas533.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490312-risitas534.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490313-risitas536.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490313-risitas538.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490312-risitas539.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490312-risitas540.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490314-risitas541.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490314-risitas542.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490313-risitas543.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490314-risitas544.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490314-risitas545.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490328-risitas547.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490326-risitas548.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490327-risitas549.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490316-risitas550.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490317-risitas551.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490321-risitas552.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490331-risitas553.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490317-risitas554.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490321-risitas556.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490321-risitas592.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490320-risitas593.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490326-risitas594.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490331-risitas597.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490329-risitas598.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490324-risitas600.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490328-risitas601.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490323-risitas596.png'
                            ]) > -1) {
                            imgsList[i].addEventListener('mouseover', function() {
                                audioList.push(new Audio('http://jscore.comli.com/risitas' + Math.floor((Math.random() * 5) + 1) + '.ogg'));
                                audioList[audioN].play();
                                audioN++;
                            });
                            imgsList[i].addEventListener('mouseout', function() {
                                audioList[audioN - 1].pause();
                            });
                        } else if ($.inArray(imgsList[i].alt, [
                                'http://image.noelshack.com/fichiers/2016/36/1473263957-risitas33.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366228-risitas38.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366269-risitas41.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366330-risitas46.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366261-risitas51.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366306-risitas52.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366335-risitas67.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366253-risitas68.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366306-risitas70.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366275-risitas72.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366609-risitas99.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366618-risitas108.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366621-risitas112.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366648-risitas116.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366644-risitas120.png',
                                'http://image.noelshack.com/fichiers/2016/24/1466366655-risitas123.png',
                                'http://image.noelshack.com/fichiers/2016/25/1466435859-risitas127.png',
                                'http://image.noelshack.com/fichiers/2016/25/1466715111-risitas132.png',
                                'http://image.noelshack.com/fichiers/2016/25/1466715116-risitas136.png',
                                'http://image.noelshack.com/fichiers/2016/25/1466715116-risitas139.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469405730-risitas162.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469405728-risitas163.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469405727-risitas164.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469541950-risitas174.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469541951-risitas175.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469541950-risitas176.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469541950-risitas177.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469541950-risitas178.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469541951-risitas179.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469541953-risitas185.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469541953-risitas189.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469541954-risitas190.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469541955-risitas193.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469541954-risitas194.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469541955-risitas195.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469541955-risitas196.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469541957-risitas198.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469541958-risitas201.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469541959-risitas205.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469541959-risitas206.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469541959-risitas207.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469541960-risitas208.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469541961-risitas209.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469541965-risitas218.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469541966-risitas222.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469541967-risitas223.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469541968-risitas224.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469541968-risitas225.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469541970-risitas226.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469541969-risitas227.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469541969-risitas228.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469541973-risitas232.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469971030-risitas234.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469971030-risitas237.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469971032-risitas245.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469971032-risitas246.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469971033-risitas247.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469971035-risitas250.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469971035-risitas251.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469971035-risitas252.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469971035-risitas253.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469971035-risitas254.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469971057-risitas255.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469971037-risitas256.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469971037-risitas257.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469971038-risitas258.png',
                                'http://image.noelshack.com/fichiers/2016/31/1470494335-risitas259.png',
                                'http://image.noelshack.com/fichiers/2016/31/1470494349-risitas277.png',
                                'http://image.noelshack.com/fichiers/2016/31/1470494347-risitas278.png',
                                'http://image.noelshack.com/fichiers/2016/31/1470494353-risitas280.png',
                                'http://image.noelshack.com/fichiers/2016/31/1470494481-risitas282.png',
                                'http://image.noelshack.com/fichiers/2016/31/1470494359-risitas287.png',
                                'http://image.noelshack.com/fichiers/2016/31/1470494488-risitas288.png',
                                'http://image.noelshack.com/fichiers/2016/31/1470494478-risitas289.png',
                                'http://image.noelshack.com/fichiers/2016/31/1470494474-risitas290.png',
                                'http://image.noelshack.com/fichiers/2016/31/1470494392-risitas293.png',
                                'http://image.noelshack.com/fichiers/2016/31/1470494495-risitas298.png',
                                'http://image.noelshack.com/fichiers/2016/31/1470494355-risitas300.png',
                                'http://image.noelshack.com/fichiers/2016/31/1470494485-risitas302.png',
                                'http://image.noelshack.com/fichiers/2016/31/1470494365-risitas303.png',
                                'http://image.noelshack.com/fichiers/2016/31/1470494363-risitas305.png',
                                'http://image.noelshack.com/fichiers/2016/31/1470494388-risitas310.png',
                                'http://image.noelshack.com/fichiers/2016/31/1470494349-risitas316.png',
                                'http://image.noelshack.com/fichiers/2016/31/1470494380-risitas317.png',
                                'http://image.noelshack.com/fichiers/2016/31/1470494487-risitas318.png',
                                'http://image.noelshack.com/fichiers/2016/31/1470494475-risitas319.png',
                                'http://image.noelshack.com/fichiers/2016/31/1470494370-risitas322.png',
                                'http://image.noelshack.com/fichiers/2016/31/1470494363-risitas323.png',
                                'http://image.noelshack.com/fichiers/2016/32/1470920768-risitas330.png',
                                'http://image.noelshack.com/fichiers/2016/32/1470920783-risitas338.png',
                                'http://image.noelshack.com/fichiers/2016/32/1470920771-risitas341.png',
                                'http://image.noelshack.com/fichiers/2016/32/1470920773-risitas342.png',
                                'http://image.noelshack.com/fichiers/2016/32/1470920771-risitas343.png',
                                'http://image.noelshack.com/fichiers/2016/32/1470920781-risitas345.png',
                                'http://image.noelshack.com/fichiers/2016/32/1470920773-risitas346.png',
                                'http://image.noelshack.com/fichiers/2016/32/1470920771-risitas347.png',
                                'http://image.noelshack.com/fichiers/2016/32/1470920771-risitas352.png',
                                'http://image.noelshack.com/fichiers/2016/32/1470920781-risitas353.png',
                                'http://image.noelshack.com/fichiers/2016/32/1470920776-risitas354.png',
                                'http://image.noelshack.com/fichiers/2016/32/1470920782-risitas365.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490210-risitas368.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490211-risitas369.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490211-risitas373.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490212-risitas375.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490214-risitas383.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490214-risitas384.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490213-risitas385.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490215-risitas386.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490214-risitas388.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490216-risitas390.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490216-risitas391.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490233-risitas395.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490218-risitas398.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490221-risitas404.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490221-risitas405.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490223-risitas408.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490225-risitas416.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490226-risitas418.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490226-risitas419.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490227-risitas420.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490227-risitas421.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490228-risitas422.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490267-risitas425.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490231-risitas429.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490235-risitas434.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490251-risitas436.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490237-risitas437.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490238-risitas440.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490240-risitas442.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490243-risitas444.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490242-risitas445.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490244-risitas446.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490244-risitas447.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490278-risitas448.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490278-risitas451.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490278-risitas453.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490293-risitas456.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490291-risitas457.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490300-risitas458.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490302-risitas460.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490282-risitas461.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490293-risitas463.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490298-risitas464.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490291-risitas465.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490285-risitas466.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490291-risitas467.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490303-risitas468.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490300-risitas474.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490292-risitas475.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490279-risitas476.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490291-risitas477.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490283-risitas478.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490298-risitas479.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490288-risitas480.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490304-risitas487.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490306-risitas488.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490297-risitas494.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490283-risitas495.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490293-risitas498.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490287-risitas500.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490298-risitas501.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490293-risitas502.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490300-risitas504.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490282-risitas506.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490303-risitas510.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490281-risitas512.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490296-risitas524.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490289-risitas526.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490309-risitas529.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490312-risitas537.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490317-risitas546.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490321-risitas555.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490322-risitas557.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490315-risitas558.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490325-risitas559.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490323-risitas560.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490323-risitas561.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490325-risitas562.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490324-risitas563.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490319-risitas564.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490323-risitas565.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490330-risitas566.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490316-risitas567.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490330-risitas568.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490316-risitas569.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490329-risitas570.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490319-risitas571.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490328-risitas572.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490322-risitas573.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490319-risitas574.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490319-risitas575.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490316-risitas576.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490331-risitas577.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490328-risitas578.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490330-risitas579.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490323-risitas580.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490319-risitas581.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490326-risitas582.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490331-risitas583.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490323-risitas584.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490325-risitas585.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490330-risitas586.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490329-risitas587.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490327-risitas588.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490327-risitas589.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490317-risitas590.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490330-risitas591.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490331-risitas595.png'
                            ]) > -1) {
                            imgsList[i].addEventListener('mouseover', function() {
                                audioList.push(new Audio('http://jscore.comli.com/risitas6.ogg'));
                                audioList[audioN].play();
                                audioN++;
                            });
                            imgsList[i].addEventListener('mouseout', function() {
                                audioList[audioN - 1].pause();
                            });
                        } else if ($.inArray(imgsList[i].alt, [
                                'http://image.noelshack.com/fichiers/2016/26/1467309320-risitas158.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469405738-risitas166.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469405729-risitas167.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474465875-risitas168.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469405729-risitas169.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469405730-risitas170.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469405730-risitas171.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469405729-risitas172.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469541950-risitas173.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469541952-risitas181.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469541951-risitas183.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469541953-risitas184.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469541953-risitas187.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469541953-risitas188.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469541957-risitas200.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469541962-risitas203.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469541958-risitas204.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469541976-risitas216.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469971034-risitas249.png',
                                'http://image.noelshack.com/fichiers/2016/31/1470494343-risitas272.png',
                                'http://image.noelshack.com/fichiers/2016/31/1470494344-risitas273.png',
                                'http://image.noelshack.com/fichiers/2016/31/1470494348-risitas279.png',
                                'http://image.noelshack.com/fichiers/2016/31/1470494367-risitas281.png',
                                'http://image.noelshack.com/fichiers/2016/31/1470494493-risitas294.png',
                                'http://image.noelshack.com/fichiers/2016/31/1470494353-risitas295.png',
                                'http://image.noelshack.com/fichiers/2016/31/1470494496-risitas296.png',
                                'http://image.noelshack.com/fichiers/2016/31/1470494477-risitas308.png',
                                'http://image.noelshack.com/fichiers/2016/31/1470494491-risitas313.png',
                                'http://image.noelshack.com/fichiers/2016/32/1470920767-risitas326.png',
                                'http://image.noelshack.com/fichiers/2016/32/1470920767-risitas327.png',
                                'http://image.noelshack.com/fichiers/2016/32/1470920767-risitas328.png',
                                'http://image.noelshack.com/fichiers/2016/32/1470920767-risitas329.png',
                                'http://image.noelshack.com/fichiers/2016/32/1470920775-risitas340.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490210-risitas371.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490210-risitas372.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490286-risitas489.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490284-risitas490.png'
                            ]) > -1) {
                            imgsList[i].addEventListener('mouseover', function() {
                                audioList.push(new Audio('http://jscore.comli.com/risitas' + Math.floor((Math.random() * 4) + 7) + '.ogg'));
                                audioList[audioN].play();
                                audioN++;
                            });
                            imgsList[i].addEventListener('mouseout', function() {
                                audioList[audioN - 1].pause();
                            });
                        } else if (imgsList[i].alt == 'http://image.noelshack.com/fichiers/2016/24/1466366250-risitas53.png') {
                            imgsList[i].addEventListener('mouseover', function() {
                                audioList.push(new Audio('http://jscore.comli.com/risitas15.ogg'));
                                audioList[audioN].play();
                                audioN++;
                            });
                            imgsList[i].addEventListener('mouseout', function() {
                                audioList[audioN - 1].pause();
                            });
                        } else if ($.inArray(imgsList[i].alt, [
                                'http://image.noelshack.com/fichiers/2016/31/1470494339-risitas265.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490311-risitas535.png'
                            ]) > -1) {
                            imgsList[i].addEventListener('mouseover', function() {
                                audioList.push(new Audio('http://jscore.comli.com/risitas14.ogg'));
                                audioList[audioN].play();
                                audioN++;
                            });
                            imgsList[i].addEventListener('mouseout', function() {
                                audioList[audioN - 1].pause();
                            });
                        } else if ($.inArray(imgsList[i].alt, [
                                'http://image.noelshack.com/fichiers/2016/32/1470920783-risitas356.png',
                                'http://image.noelshack.com/fichiers/2016/32/1470920777-risitas357.png',
                                'http://image.noelshack.com/fichiers/2016/32/1470920772-risitas358.png'
                            ]) > -1) {
                            imgsList[i].addEventListener('mouseover', function() {
                                audioList.push(new Audio('http://jscore.comli.com/risitas12.ogg'));
                                audioList[audioN].play();
                                audioN++;
                            });
                            imgsList[i].addEventListener('mouseout', function() {
                                audioList[audioN - 1].pause();
                            });
                        } else if ($.inArray(imgsList[i].alt, [
                                'http://image.noelshack.com/fichiers/2016/38/1474490212-risitas374.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490216-risitas387.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474490218-risitas394.png'
                            ]) > -1) {
                            imgsList[i].addEventListener('mouseover', function() {
                                audioList.push(new Audio('http://jscore.comli.com/risitas13.ogg'));
                                audioList[audioN].play();
                                audioN++;
                            });
                            imgsList[i].addEventListener('mouseout', function() {
                                audioList[audioN - 1].pause();
                            });
                        } else if ($.inArray(imgsList[i].alt, [
                                'http://image.noelshack.com/fichiers/2016/26/1467335935-jesus1.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469405913-jesus2.png',
                                'http://image.noelshack.com/fichiers/2016/26/1467335935-jesus3.png',
                                'http://image.noelshack.com/fichiers/2016/26/1467335935-jesus4.png',
                                'http://image.noelshack.com/fichiers/2016/36/1473263674-jesus5.png',
                                'http://image.noelshack.com/fichiers/2016/31/1470494264-jesus6.png',
                                'http://image.noelshack.com/fichiers/2016/31/1470494265-jesus8.png',
                                'http://image.noelshack.com/fichiers/2016/32/1470920800-jesus9.png',
                                'http://image.noelshack.com/fichiers/2016/32/1470920814-jesus10.png',
                                'http://image.noelshack.com/fichiers/2016/32/1470920799-jesus11.png',
                                'http://image.noelshack.com/fichiers/2016/32/1470920800-jesus12.png',
                                'http://image.noelshack.com/fichiers/2016/32/1470920800-jesus13.png',
                                'http://image.noelshack.com/fichiers/2016/32/1470920800-jesus14.png',
                                'http://image.noelshack.com/fichiers/2016/32/1470920805-jesus15.png',
                                'http://image.noelshack.com/fichiers/2016/32/1470920806-jesus16.png',
                                'http://image.noelshack.com/fichiers/2016/32/1470920801-jesus17.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474488554-jesus18.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474488554-jesus19.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474488578-jesus20.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474488554-jesus21.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474488554-jesus22.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474488554-jesus23.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474488555-jesus24.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474488631-jesus25.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474488637-jesus26.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474488632-jesus27.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474488556-jesus28.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474488557-jesus29.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474488558-jesus30.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474488559-jesus31.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474488559-jesus32.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474488560-jesus33.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474488561-jesus34.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474488562-jesus35.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474488562-jesus36.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474488564-jesus37.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474488564-jesus38.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474488566-jesus39.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474488566-jesus40.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474488567-jesus41.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474488567-jesus42.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474488569-jesus43.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474488569-jesus44.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474488570-jesus45.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474488571-jesus46.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474488571-jesus47.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474488576-jesus48.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474488573-jesus49.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474488574-jesus50.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474587152-jesus51.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474587152-jesus52.jpg'
                            ]) > -1) {
                            imgsList[i].addEventListener('mouseover', function() {
                                audioList.push(new Audio('http://jscore.comli.com/jesus1.ogg'));
                                audioList[audioN].play();
                                audioN++;
                            });
                            imgsList[i].addEventListener('mouseout', function() {
                                audioList[audioN - 1].pause();
                            });
                        } else if ($.inArray(imgsList[i].alt, [
                                'http://image.noelshack.com/fichiers/2016/23/1465723887-img2.png',
                                'http://image.noelshack.com/fichiers/2016/26/1467378765-img39.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469402313-img13.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469402314-img14.png'
                            ]) > -1) {
                            imgsList[i].addEventListener('mouseover', function() {
                                if (Math.floor(Math.random() * 2) == 0) {
                                    audioList.push(new Audio('http://jscore.comli.com/finkiel.ogg'));
                                } else {
                                    audioList.push(new Audio('http://jscore.comli.com/6.ogg'));
                                }
                                audioList[audioN].play();
                                audioN++;
                            });
                            imgsList[i].addEventListener('mouseout', function() {
                                audioList[audioN - 1].pause();
                            });
                        } else if ($.inArray(imgsList[i].alt, [
                                'http://image.noelshack.com/fichiers/2016/30/1469402312-img11.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469402314-img12.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469402319-img15.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469541881-img9.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469541882-img13.png',
                                'http://image.noelshack.com/fichiers/2016/30/1469541882-img16.png'
                            ]) > -1) {
                            imgsList[i].addEventListener('mouseover', function() {
                                audioList.push(new Audio('http://jscore.comli.com/kairi' + Math.floor((Math.random() * 2) + 1) + '.ogg'));
                                audioList[audioN].play();
                                audioN++;
                            });
                            imgsList[i].addEventListener('mouseout', function() {
                                audioList[audioN - 1].pause();
                            });
                        } else if (imgsList[i].alt == 'http://image.noelshack.com/fichiers/2016/30/1469541878-img1.png') {
                            imgsList[i].addEventListener('mouseover', function() {
                                audioList.push(new Audio('http://jscore.comli.com/succ.ogg'));
                                audioList[audioN].play();
                                audioN++;
                            });
                            imgsList[i].addEventListener('mouseout', function() {
                                audioList[audioN - 1].pause();
                            });
                        } else if (imgsList[i].alt == 'http://image.noelshack.com/fichiers/2016/23/1465693975-img15.png') {
                            imgsList[i].addEventListener('mouseover', function() {
                                audioList.push(new Audio('http://jscore.comli.com/1.ogg'));
                                audioList[audioN].play();
                                audioN++;
                            });
                            imgsList[i].addEventListener('mouseout', function() {
                                audioList[audioN - 1].pause();
                            });
                        } else if ($.inArray(imgsList[i].alt, [
                                'http://image.noelshack.com/fichiers/2016/32/1470919031-img45.png',
                                'http://image.noelshack.com/fichiers/2016/32/1470919030-img46.png'
                            ]) > -1) {
                            imgsList[i].addEventListener('mouseover', function() {
                                audioList.push(new Audio('http://jscore.comli.com/2.ogg'));
                                audioList[audioN].play();
                                audioN++;
                            });
                            imgsList[i].addEventListener('mouseout', function() {
                                audioList[audioN - 1].pause();
                            });
                        } else if (imgsList[i].alt == 'http://image.noelshack.com/fichiers/2016/30/1469402310-img5.png') {
                            imgsList[i].addEventListener('mouseover', function() {
                                audioList.push(new Audio('http://jscore.comli.com/3.ogg'));
                                audioList[audioN].play();
                                audioN++;
                            });
                            imgsList[i].addEventListener('mouseout', function() {
                                audioList[audioN - 1].pause();
                            });
                        } else if ($.inArray(imgsList[i].alt, [
                                'http://image.noelshack.com/fichiers/2016/25/1466425578-img8.png',
                                'http://image.noelshack.com/fichiers/2016/25/1466425579-img9.png'
                            ]) > -1) {
                            imgsList[i].addEventListener('mouseover', function() {
                                audioList.push(new Audio('http://jscore.comli.com/4.ogg'));
                                audioList[audioN].play();
                                audioN++;
                            });
                            imgsList[i].addEventListener('mouseout', function() {
                                audioList[audioN - 1].pause();
                            });
                        } else if (imgsList[i].alt == 'http://image.noelshack.com/fichiers/2016/32/1470919013-img44.png') {
                            imgsList[i].addEventListener('mouseover', function() {
                                audioList.push(new Audio('http://jscore.comli.com/5.ogg'));
                                audioList[audioN].play();
                                audioN++;
                            });
                            imgsList[i].addEventListener('mouseout', function() {
                                audioList[audioN - 1].pause();
                            });
                        } else if (imgsList[i].alt == 'http://image.noelshack.com/fichiers/2016/26/1467250892-sans-titre-2.png') {
                            imgsList[i].addEventListener('mouseover', function() {
                                audioList.push(new Audio('http://jscore.comli.com/7.ogg'));
                                audioList[audioN].play();
                                audioN++;
                            });
                            imgsList[i].addEventListener('mouseout', function() {
                                audioList[audioN - 1].pause();
                            });
                        } else if (imgsList[i].alt == 'http://image.noelshack.com/fichiers/2016/23/1465750045-img22.png') {
                            imgsList[i].addEventListener('mouseover', function() {
                                audioList.push(new Audio('http://jscore.comli.com/8.ogg'));
                                audioList[audioN].play();
                                audioN++;
                            });
                            imgsList[i].addEventListener('mouseout', function() {
                                audioList[audioN - 1].pause();
                            });
                        } else if (imgsList[i].alt == 'http://image.noelshack.com/fichiers/2016/21/1464198142-vivaelblancos.png') {
                            imgsList[i].addEventListener('mouseover', function() {
                                audioList.push(new Audio('http://jscore.comli.com/9.ogg'));
                                audioList[audioN].play();
                                audioN++;
                            });
                            imgsList[i].addEventListener('mouseout', function() {
                                audioList[audioN - 1].pause();
                            });
                        } else if ($.inArray(imgsList[i].alt, [
                                'http://image.noelshack.com/fichiers/2016/24/1465847273-nelly1.png',
                                'http://image.noelshack.com/fichiers/2016/24/1465847279-nelly2.png',
                                'http://image.noelshack.com/fichiers/2016/24/1465847289-nelly4.png',
                                'http://image.noelshack.com/fichiers/2016/24/1465847293-nelly5.png',
                                'http://image.noelshack.com/fichiers/2016/24/1465847298-nelly6.png'
                            ]) > -1) {
                            imgsList[i].addEventListener('mouseover', function() {
                                audioList.push(new Audio('http://jscore.comli.com/10.ogg'));
                                audioList[audioN].play();
                                audioN++;
                            });
                            imgsList[i].addEventListener('mouseout', function() {
                                audioList[audioN - 1].pause();
                            });
                        } else if ($.inArray(imgsList[i].alt, [
                                'http://image.noelshack.com/fichiers/2016/25/1466425576-img2.png',
                                'http://image.noelshack.com/fichiers/2016/25/1466425576-img3.png',
                                'http://image.noelshack.com/fichiers/2016/25/1466425578-img7.png'
                            ]) > -1) {
                            imgsList[i].addEventListener('mouseover', function() {
                                audioList.push(new Audio('http://jscore.comli.com/11.ogg'));
                                audioList[audioN].play();
                                audioN++;
                            });
                            imgsList[i].addEventListener('mouseout', function() {
                                audioList[audioN - 1].pause();
                            });
                        } else if ($.inArray(imgsList[i].alt, [
                                'http://image.noelshack.com/fichiers/2016/23/1465696903-img24.png',
                                'http://image.noelshack.com/fichiers/2016/23/1465696904-img25.png',
                                'http://image.noelshack.com/fichiers/2016/23/1465696904-img26.png',
                                'http://image.noelshack.com/fichiers/2016/23/1465696906-img27.png'
                            ]) > -1) {
                            imgsList[i].addEventListener('mouseover', function() {
                                audioList.push(new Audio('http://jscore.comli.com/12.ogg'));
                                audioList[audioN].play();
                                audioN++;
                            });
                            imgsList[i].addEventListener('mouseout', function() {
                                audioList[audioN - 1].pause();
                            });
                        } else if (imgsList[i].alt == 'http://image.noelshack.com/fichiers/2016/23/1465731617-valls.png') {
                            imgsList[i].addEventListener('mouseover', function() {
                                audioList.push(new Audio('http://jscore.comli.com/13.ogg'));
                                audioList[audioN].play();
                                audioN++;
                            });
                            imgsList[i].addEventListener('mouseout', function() {
                                audioList[audioN - 1].pause();
                            });
                        } else if (imgsList[i].alt == 'http://image.noelshack.com/fichiers/2016/23/1465750044-img21.png') {
                            imgsList[i].addEventListener('mouseover', function() {
                                audioList.push(new Audio('http://jscore.comli.com/14.ogg'));
                                audioList[audioN].play();
                                audioN++;
                            });
                            imgsList[i].addEventListener('mouseout', function() {
                                audioList[audioN - 1].pause();
                            });
                        } else if (imgsList[i].alt == 'http://image.noelshack.com/fichiers/2016/32/1470919005-img28.png') {
                            imgsList[i].addEventListener('mouseover', function() {
                                audioList.push(new Audio('http://jscore.comli.com/15.ogg'));
                                audioList[audioN].play();
                                audioN++;
                            });
                            imgsList[i].addEventListener('mouseout', function() {
                                audioList[audioN - 1].pause();
                            });
                        } else if (imgsList[i].alt == 'http://image.noelshack.com/fichiers/2016/23/1465739711-img41.png') {
                            imgsList[i].addEventListener('mouseover', function() {
                                audioList.push(new Audio('http://jscore.comli.com/16.ogg'));
                                audioList[audioN].play();
                                audioN++;
                            });
                            imgsList[i].addEventListener('mouseout', function() {
                                audioList[audioN - 1].pause();
                            });
                        } else if ($.inArray(imgsList[i].alt, [
                                'http://image.noelshack.com/fichiers/2016/38/1474488782-tv4.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474488703-tv5.png',
                                'http://image.noelshack.com/fichiers/2016/38/1474488705-tv6.png'
                            ]) > -1) {
                            imgsList[i].addEventListener('mouseover', function() {
                                audioList.push(new Audio('http://jscore.comli.com/1' + Math.floor((Math.random() * 3) + 7) + '.ogg'));
                                audioList[audioN].play();
                                audioN++;
                            });
                            imgsList[i].addEventListener('mouseout', function() {
                                audioList[audioN - 1].pause();
                            });
                        }
                    }
                }
            }
            if (webmPlayer || youtubePlayer || vocarooPlayer) {
                var links = document.getElementsByClassName('xXx');
                for (var i = 0; i < links.length; i++) {
                    if (playerSignature || (!(playerSignature) && !(links[i].parentElement.parentElement.className.includes('signature-msg')))) {
                        if (webmPlayer) {
                            if (links[i].href.startsWith('http://webm.land/w/')) {
                                links[i].style.display = 'none';
                                var videoId = links[i].href.replace('http://webm.land/w/', '').replace('/', '') + '.webm';
                                video = document.createElement('video');
                                video.width = '320';
                                video.height = '240';
                                video.innerHTML = '<source src="http://webm.land/media/' + videoId + '" type="video/webm">';
                                video.setAttribute('loop', 'loop');
                                links[i].parentElement.insertBefore(video, links[i]);
                                video.addEventListener('mouseover', function() {
                                    $(this).get(0).currentTime = 0;
                                    $(this).get(0).play();
                                });
                                video.addEventListener('mouseout', function() {
                                    $(this).get(0).pause();
                                });
                            } else if (links[i].href.endsWith('.webm')) {
                                links[i].style.display = 'none';
                                video = document.createElement('video');
                                video.width = '320';
                                video.height = '240';
                                video.innerHTML = '<source src="' + links[i].href + '" type="video/webm">';
                                video.setAttribute('loop', 'loop');
                                links[i].parentElement.insertBefore(video, links[i]);
                                video.addEventListener('mouseover', function() {
                                    $(this).get(0).currentTime = 0;
                                    $(this).get(0).play();
                                });
                                video.addEventListener('mouseout', function() {
                                    $(this).get(0).pause();
                                });
                            }
                        }
                        if (youtubePlayer) {
                            if (links[i].href.startsWith('https://www.youtube.com/watch?v=') || links[i].href.startsWith('https://youtu.be/')) {
                                var getId = '';
                                if (links[i].href.startsWith('https://www.youtube.com/watch?v=')) {
                                    var videoIndex = links[i].href.indexOf('v=');
                                    var upIndex = links[i].href.indexOf('&', videoIndex);
                                    if (upIndex == -1) {
                                        getId = links[i].href.substring(videoIndex + 2);
                                    } else {
                                        getId = links[i].href.substring(videoIndex + 2, upIndex);
                                    }
                                } else if (links[i].href.startsWith('https://youtu.be/')) {
                                    var upIndex = links[i].href.indexOf('?', 17);
                                    if (upIndex == -1) {
                                        getId = links[i].href.substring(17);
                                    } else {
                                        getId = links[i].href.substring(17, upIndex);
                                    }
                                }
                                links[i].style.display = 'none';
                                video = document.createElement('object');
                                video.setAttribute('style', 'width:100%;height:100%;width: 320px; height: 240px; float: none; clear: both; margin: 2px auto;');
                                var timeIndex = links[i].href.indexOf('t=');
                                if (timeIndex != -1) {
                                    var totalSec = 0;
                                    var upTo = links[i].href.indexOf('&', timeIndex);
                                    if (upTo == -1) {
                                        links[i].href = links[i].href + '&';
                                        upTo = links[i].href.indexOf('&', timeIndex);
                                    }
                                    var hourIndex = links[i].href.indexOf('h', timeIndex);
                                    var minIndex = links[i].href.indexOf('m', timeIndex);
                                    var secIndex = links[i].href.indexOf('s', timeIndex);
                                    var isHour = false;
                                    if (hourIndex != -1 && hourIndex < upTo) {
                                        isHour = true;
                                    }
                                    var isMin = false;
                                    if (minIndex != -1 && minIndex < upTo) {
                                        isMin = true;
                                    }
                                    var isSec = false;
                                    if (secIndex != -1 && secIndex < upTo) {
                                        isSec = true;
                                    }
                                    if (isHour) {
                                        totalSec += Number((links[i].href.substring(timeIndex + 2, hourIndex)) * 3600);
                                    }
                                    if (isMin) {
                                        if (isHour) {
                                            totalSec += Number((links[i].href.substring(hourIndex + 1, minIndex)) * 60);
                                        } else {
                                            totalSec += Number((links[i].href.substring(timeIndex + 2, minIndex)) * 60);
                                        }
                                    }
                                    if (isSec) {
                                        if (isMin) {
                                            totalSec += Number(links[i].href.substring(minIndex + 1, secIndex));
                                        } else if (isHour) {
                                            totalSec += Number(links[i].href.substring(hourIndex + 1, secIndex));
                                        } else {
                                            totalSec += Number(links[i].href.substring(timeIndex + 2, secIndex));
                                        }
                                    }
                                    if (!isHour && !isMin && !isSec) {
                                        totalSec += Number(links[i].href.substring(timeIndex + 2, upTo));
                                    }
                                    getId += '?start=' + totalSec;
                                }
                                video.setAttribute('data', 'http://www.youtube.com/embed/' + getId);
                                links[i].parentElement.insertBefore(video, links[i]);
                            }
                        }
                        if (vocarooPlayer) {
                            if (links[i].href.startsWith('http://vocaroo.com/i/')) {
                                links[i].style.display = 'none';
                                var vocarooId = links[i].href.replace('http://vocaroo.com/i/', '').replace('/', '');
                                vocaroo = document.createElement('object');
                                vocaroo.width = '148';
                                vocaroo.height = '44';
                                vocaroo.innerHTML = '<param name="movie" value="http://vocaroo.com/player.swf?playMediaID=' + vocarooId + '&autoplay=0"></param><param name="wmode" value="transparent"></param><embed src="http://vocaroo.com/player.swf?playMediaID=' + vocarooId + '&autoplay=0" width="148" height="44" wmode="transparent" type="application/x-shockwave-flash"></embed>';
                                links[i].parentElement.insertBefore(vocaroo, links[i]);
                            }
                        }
                    }
                }
            }
            if (!stickerCliquable && !supprimerFond) {
                var fc = [];
                var ix = document.getElementsByClassName('img-shack');

                function createfc(i) {
                    return function() {
                        var z = ix[i].alt;
                        var newSrc = ix[i].src;
                        if (z.includes('/fichiers/') && (z.endsWith('.png') || z.endsWith('.gif'))) {
                            newSrc = ix[i].alt;
                        } else if (z.includes('/minis/') && (z.endsWith('.png') || z.endsWith('.gif'))) {
                            newSrc = ix[i].alt;
                            newSrc = newSrc.replace('/minis/', '/fichiers/');
                        } else if (ix[i].src.includes('/minis/') && (z.endsWith('.png') || z.endsWith('.gif'))) {
                            newSrc = newSrc.replace('/minis/', '/fichiers/');
                            if (z.endsWith('.gif')) {
                                newSrc = newSrc.replace('.png', '.gif');
                            }
                        } else if (ix[i].src.includes('/minis/') && (z.endsWith('.jpg') || z.endsWith('.jpeg'))) {
                            newSrc = newSrc.replace('/minis/', '/fichiers/');
                            if (z.endsWith('.jpg')) {
                                newSrc = newSrc.replace('.png', '.jpg');
                            } else if (z.endsWith('.jpeg')) {
                                newSrc = newSrc.replace('.png', '.jpeg');
                            }
                        }
                        var newImg = new Image();
                        newImg.addEventListener('load', function() {
                            if (this.width / this.height > 4 / 3 * 0.99 && this.width / this.height < 4 / 3 * 1.01) {
                                ix[i].parentElement.style.display = 'none';
                                ix[i].parentElement.parentElement.insertBefore(ix[i], ix[i].parentElement);
                            }
                        });
                        newImg.src = newSrc;
                    };
                }
                for (var i = 0; i < ix.length; i++) {
                    fc[i] = createfc(i);
                    fc[i]();
                }
            }
            var editModifier = document.getElementsByClassName('picto-msg-crayon');
            if (editModifier.length > 0) {
                for (var i = 0; i < editModifier.length; i++) {
                    editModifier[i].addEventListener('click', function() {
                        var observerLoad = new MutationObserver(function(mutations, mutLoad) {
                            if (document.querySelector('div[data-flg-tt="Hap"]')) {
                                mutLoad.disconnect();
                                chargerBarre();
                            }
                            return;
                        });
                        observerLoad.observe(document.body, {
                            childList: true,
                            subtree: true,
                            attributes: false,
                            characterData: false
                        });
                    });
                    var observerStickers = new MutationObserver(function(mutations, mutStickers) {
                        var canvasStickers = document.querySelector('button[data-edit="stickers"]');
                        if (canvasStickers) {
                            mutStickers.disconnect();
                            canvasStickers.onclick = function() {
                                check();
                            };
                        }
                        return;
                    });
                    observerStickers.observe(document.body, {
                        childList: true,
                        subtree: true,
                        attributes: false,
                        characterData: false
                    });
                    var observerButtonA = new MutationObserver(function(mutations, mutButtons) {
                        var canvasButtons = document.getElementsByClassName('btn-annuler-modif-msg');
                        if (canvasButtons) {
                            mutButtons.disconnect();
                            for (var j = 0; j < canvasButtons.length; j++) {
                                canvasButtons[j].addEventListener('click', function() {
                                    var observerTArea = new MutationObserver(function(mutations, mutTArea) {
                                        var canvasTArea = document.getElementById('message_topic');
                                        if (canvasTArea) {
                                            mutTArea.disconnect();
                                            canvasTArea.addEventListener('click', tArea());
                                        }
                                        return;
                                    });
                                    observerTArea.observe(document.body, {
                                        childList: true,
                                        subtree: true,
                                        attributes: false,
                                        characterData: false
                                    });
                                });
                            }
                        }
                        return;
                    });
                    observerButtonA.observe(document.body, {
                        childList: true,
                        subtree: true,
                        attributes: false,
                        characterData: false
                    });
                    var observerButtonE = new MutationObserver(function(mutations, mutButtons) {
                        var canvasButtons = document.getElementsByClassName('btn btn-editer-msg');
                        if (canvasButtons) {
                            mutButtons.disconnect();
                            for (var j = 0; j < canvasButtons.length; j++) {
                                canvasButtons[j].addEventListener('click', function() {
                                    var observerTArea = new MutationObserver(function(mutations, mutTArea) {
                                        var canvasTArea = document.getElementById('message_topic');
                                        if (canvasTArea) {
                                            mutTArea.disconnect();
                                            canvasTArea.addEventListener('click', tArea());
                                        }
                                        return;
                                    });
                                    observerTArea.observe(document.body, {
                                        childList: true,
                                        subtree: true,
                                        attributes: false,
                                        characterData: false
                                    });
                                });
                            }
                        }
                        return;
                    });
                    observerButtonE.observe(document.body, {
                        childList: true,
                        subtree: true,
                        attributes: false,
                        characterData: false
                    });
                }
            }
            allCat = JSON.parse(GM_getValue('allCat'));
            catRealNames = JSON.parse(GM_getValue('catRealNames'));
            catIcons = JSON.parse(GM_getValue('catIcons'));
            catToShow = JSON.parse(GM_getValue('catToShow'));
            var funcs = [];

            function createfunc(g) {
                return function() {
                    var idx = allCat.indexOf(catToShow[g]);
                    var tab = document.createElement('div');
                    tab.className = 'f-tab f-h';
                    tab.style.width = '25px';
                    tab.style.height = '23px';
                    tab.style.lineHeight = '23px';
                    tab.style.fontSize = '14px';
                    tab.setAttribute('data-flg-tt', catRealNames[idx]);
                    tab.innerHTML = catIcons[idx] + '<div style="display: none;" class="f-ttw"><div style="top: -26px; left: -9px;" class="f-inner"><div class="f-tt">' + catRealNames[idx] + '</div><div class="f-arr"></div></div></div>';
                    tab.addEventListener('click', function() {
                        charger(catRealNames[idx]);
                    });
                    canvasPop.appendChild(tab);
                };
            }
            for (var g = 0; g < catToShow.length; g++) {
                funcs[g] = createfunc(g);
            }
            for (var g = 0; g < catToShow.length; g++) {
                funcs[g]();
            }
        }, 100);
    }

    function check() {
        if (document.querySelector('button[data-edit="stickers"]').className.indexOf('active') != -1) {
            var observerLoad = new MutationObserver(function(mutations, mutLoad) {
                if (document.querySelector('div[data-flg-tt="Hap"]')) {
                    mutLoad.disconnect();
                    chargerBarre();
                }
                return;
            });
            observerLoad.observe(document.body, {
                childList: true,
                subtree: true,
                attributes: false,
                characterData: false
            });
        }
    }
    var observerHap = new MutationObserver(function(mutations, mutHap) {
        if (document.querySelector('div[data-flg-tt="Hap"]')) {
            mutHap.disconnect();
            chargerBarre();
        }
        return;
    });
    observerHap.observe(document.body, {
        childList: true,
        subtree: true,
        attributes: false,
        characterData: false
    });
    if (supprStickersBan) {
        var observerCharg = new MutationObserver(function(mutations, mutCharg) {
            var canvasCharg = document.getElementsByClassName('bloc-pagi-default');
            if (canvasCharg[1]) {
                mutCharg.disconnect();
                var imgs = document.getElementsByClassName('img-stickers');
                var funcs = [];
                for (var i = 0; i < imgs.length; i++) {
                    if ($.inArray(imgs[i].src, stickersBanListe) > -1) {
                        if (!(supprDesPosts)) {
                            if (stickerMessageDeSuppr) {
                                if (!(imgs[i].previousSibling) || !(imgs[i].previousSibling.style) || imgs[i].previousSibling.style.color != 'red') {
                                    var message = document.createElement('div');
                                    message.style.color = 'red';
                                    message.innerHTML = 'Sticker supprimé! ';
                                    imgs[i].parentElement.insertBefore(message, imgs[i]);
                                } else {
                                    imgs[i].previousSibling.innerHTML += 'Sticker supprimé! ';
                                }
                            }
                            imgs[i].parentNode.removeChild(imgs[i]);
                            i--;
                        } else {
                            if (imgs[i].offsetParent != null) {
                                var img = imgs[i];
                                while (typeof img.parentElement != 'undefined' && img.parentElement.className != 'bloc-message-forum ') {
                                    img = img.parentElement;
                                }
                                img.style.display = 'none';
                                if (stickerMessageDeSuppr) {
                                    var showBan = document.createElement('div');
                                    showBan.className = 'conteneur-message n-displayed';
                                    showBan.style.color = 'red';
                                    showBan.style.textAlign = 'center';
                                    showBan.style.marginBottom = '15px';
                                    showBan.innerHTML = 'Ce post a été supprimé car il contenait un sticker banni. Cliquez ici pour l\'afficher.';
                                    showBan.addEventListener('click', function() {
                                        if (this.className.endsWith('n-displayed')) {
                                            this.parentElement.lastElementChild.removeAttribute('style');
                                            this.innerHTML = 'Ce post a été supprimé car il contenait un sticker banni. Cliquez ici pour le cacher.';
                                            $(this).removeClass('n-displayed');
                                        } else {
                                            this.parentElement.lastElementChild.style.display = 'none';
                                            this.innerHTML = 'Ce post a été supprimé car il contenait un sticker banni. Cliquez ici pour l\'afficher.';
                                            this.className += ' n-displayed';
                                        }
                                    });
                                    img.parentElement.insertBefore(showBan, img);
                                }
                            }
                        }
                    }
                }
            }
            return;
        });
        observerCharg.observe(document.body, {
            childList: true,
            subtree: true,
            attributes: false,
            characterData: false
        });
    }
    var observerHide = new MutationObserver(function(mutations, mutHide) {
        var canvasHide = document.querySelector('div[data-flg-tt="Biblioth&egrave;que"]');
        if (canvasHide) {
            mutHide.disconnect();
            canvasHide.parentElement.setAttribute('style', 'display:none !important;height:0px !important;width:0px !important');
        }
        return;
    });
    observerHide.observe(document.body, {
        childList: true,
        subtree: true,
        attributes: false,
        characterData: false
    });
    var observerStickers = new MutationObserver(function(mutations, mutStickers) {
        var canvasStickers = document.querySelector('button[data-edit="stickers"]');
        if (canvasStickers) {
            mutStickers.disconnect();
            canvasStickers.onclick = function() {
                check();
            };
        }
        return;
    });
    observerStickers.observe(document.body, {
        childList: true,
        subtree: true,
        attributes: false,
        characterData: false
    });
    setTimeout(function() {
        if (!isLoaded) {
            chargerBarre();
        }
    }, 10000);
  }
}