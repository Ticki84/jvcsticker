// ==UserScript==
// @name        JVCSticker++
// @namespace   JVCSticker++
// @include     http://www.jeuxvideo.com/forums/*
// @include     https://www.jeuxvideo.com/forums/*
// @version     2.0.0
// @grant       GM_addStyle
// @updateURL   https://ticki84.github.io/JVCSticker++.meta.js
// @downloadURL https://ticki84.github.io/JVCSticker++.user.js
// @require     https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.3/jquery.min.js
// @require     https://ticki84.github.io/jquery.modal.min.js
// @require     https://swisnl.github.io/jQuery-contextMenu/dist/jquery.contextMenu.js
// @require     https://swisnl.github.io/jQuery-contextMenu/dist/jquery.ui.position.min.js
// @connect     github.io
// @connect     jvcsticker.esy.es
// @grant       GM_xmlhttpRequest
// @grant       GM_getValue
// @grant       GM_setValue
// @grant       GM_listValues
// @grant       GM_deleteValue
// @grant       GM_registerMenuCommand
// @grant       GM_log
// @icon        http://jv.stkr.fr/p/1kki
// @author      Ticki84
// @copyright   2016+, Ticki84
// @noframes
// ==/UserScript==
//GM_deleteValue('catFavoris');
function delTimestamp() {
    GM_deleteValue('lTimestamp');
}
function delFavoris() {
    GM_deleteValue('catFavoris');
}
GM_registerMenuCommand('JVCSticker++ - Recharger les stickers', delTimestamp);
GM_registerMenuCommand('JVCSticker++ - Supprimer les favoris', delFavoris);

function deleteGlobalVar() {
    var keys = GM_listValues();
    for (var i = 0, key = null; key = keys[i]; i++) {
        GM_deleteValue(key);
    }
}

function addStyle(style) {
    var head = document.getElementsByTagName('HEAD')[0];
    var ele = head.appendChild(window.document.createElement('style'));
    ele.innerHTML = style;
    return ele;
}
addStyle('@import "https://ticki84.github.io/jquery.modal.min.css";');
addStyle('@import "https://swisnl.github.io/jQuery-contextMenu/dist/jquery.contextMenu.css";');
addStyle('@import "https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css";');
if (GM_getValue('dontShowUpTo') === undefined) {
    GM_setValue('dontShowUpTo', GM_info.script.version);
}
GM_xmlhttpRequest({
    method: 'GET',
    url: 'https://ticki84.github.io/JVCSticker++.xml?_=' + Date.now(),
    headers: {
        'User-Agent': 'Mozilla/5.0',
        'Accept': 'text/xml'
    },
    onload: function(response) {
        var responseXML = null;
        if (!response.responseXML) {
            responseXML = new DOMParser().parseFromString(response.responseText, 'text/xml');
        }
        var scriptElement = document.createElement('script');
        scriptElement.type = 'text/javascript';
        scriptElement.src = 'https://ticki84.github.io/jquery.modal.min.js';
        document.body.appendChild(scriptElement);

        function generateChangelog() {
            var i = 0;
            var text = '';
            while (response.responseXML.getElementsByTagName('version')[i]) {
                if (response.responseXML.getElementsByTagName('version')[i].childNodes[0].nodeValue > GM_info.script.version) {
                    text += '<p>Version ' + response.responseXML.getElementsByTagName('version')[i].childNodes[0].nodeValue + ':';
                    var changelog = response.responseXML.getElementsByTagName('changelog')[i].childNodes[0].nodeValue.replace(new RegExp('-', 'g'), '<br>-');
                    text += changelog;
                    text += '<br>';
                } else {
                    break;
                }
                i++;
            }
            return text;
        }
        if (response.responseXML.getElementsByTagName('version')[0].childNodes[0].nodeValue > GM_info.script.version && response.responseXML.getElementsByTagName('version')[0].childNodes[0].nodeValue > GM_getValue('dontShowUpTo', '1.0.0')) {
            var addHTML = document.createElement('div');
            addHTML.innerHTML = ' <div id="updateModal" style="display:none; text-align: center;"><div class="modal-header"><h3 class="modal-title">Nouvelle version!</h3></div><div class="modal-body"><br><a href="https://ticki84.github.io/JVCSticker++.user.js" target="_blank">Une nouvelle version de JVCSticker++ est disponible!</a><br><br><br><p>Version actuelle: ' + GM_info.script.version + '</p><p>Dernière version: ' + response.responseXML.getElementsByTagName('version')[0].childNodes[0].nodeValue + '</p><br><br><p><u>Changelog:</u></p>' + generateChangelog() + '<br><br><p><h4>Voulez-vous effectuez la mise à jour maintenant?</h4></p><br><label><input type="checkbox" id="showNV"> Ne plus m\'avertir jusqu\'à la prochaine mise à jour</label><br><br></div><div class="modal-footer"><button type="button" id="updateOui" class="btn btn-default" data-dismiss="modal">Oui</button> <a rel="modal:close"><button type="button" id="updateNon" class="btn btn-default" data-dismiss="modal">Non</button></a></div></div><p style="display:none;"><a href="#updateModal" id="toUpdateModal" rel="modal:open">Open Modal</a></p>';
            document.body.appendChild(addHTML);
            document.getElementById('updateOui').addEventListener('click', function() {
                window.open('https://ticki84.github.io/JVCSticker++.user.js', '_blank');
            });
            document.getElementById('updateNon').addEventListener('click', function() {
                if (document.getElementById('showNV').checked) {
                    GM_setValue('dontShowUpTo', response.responseXML.getElementsByTagName('version')[0].childNodes[0].nodeValue);
                }
                init(response.responseXML.getElementsByTagName('version')[0].childNodes[0].nodeValue);
            });
            document.getElementById('toUpdateModal').click();
        } else {
            init(response.responseXML.getElementsByTagName('version')[0].childNodes[0].nodeValue);
        }
    }
});

function init(lastVersion) {
    var varNames = [
        'espacementStickers',
        'tailleStickers',
        'tailleFenetre',
        'modifierCouleurPosts',
        'couleurBackground',
        'couleurBordure',
        'supprimerFond',
        'tailleSticker',
        'stickerCliquable',
        'supprStickersBan',
        'supprDesPosts',
        'stickerMessageDeSuppr',
        'sons',
        'webmPlayer',
        'youtubePlayer',
        'vocarooPlayer',
        'widthPlayer',
        'heightPlayer',
        'playerSignature',
        'intervalle',
        'xBarreHistorique',
        'homeCat',
        'stickersBanListe',
        'historique'
    ];
    var valDefault = [
        '10',
        '44',
        '150',
        true,
        '#FFF',
        '#d5d5d5',
        true,
        '100',
        true,
        true,
        false,
        true,
        true,
        true,
        true,
        true,
        '320',
        '240',
        false,
        '360',
        '1',
        'Populaires',
        JSON.stringify([
            'http://jv.stkr.fr/p/1miq',
            'http://jv.stkr.fr/p/1min',
            'http://jv.stkr.fr/p/1mim',
            'http://jv.stkr.fr/p/1mig-fr',
            'http://jv.stkr.fr/p/1mij-fr',
            'http://jv.stkr.fr/p/1mio',
            'http://jv.stkr.fr/p/1mik',
            'http://jv.stkr.fr/p/1mip',
            'http://jv.stkr.fr/p/1mif',
            'http://jv.stkr.fr/p/1mii-fr',
            'http://jv.stkr.fr/p/1mih-fr',
            'http://jv.stkr.fr/p/1mil',
            'http://jv.stkr.fr/p/1mie-fr',
            'http://jv.stkr.fr/p/1mid-fr',
            'http://jv.stkr.fr/p/1myf',
            'http://jv.stkr.fr/p/1my7',
            'http://jv.stkr.fr/p/1myc',
            'http://jv.stkr.fr/p/1my9',
            'http://jv.stkr.fr/p/1myb',
            'http://jv.stkr.fr/p/1my6',
            'http://jv.stkr.fr/p/1mye',
            'http://jv.stkr.fr/p/1myx',
            'http://jv.stkr.fr/p/1myd',
            'http://jv.stkr.fr/p/1my4',
            'http://jv.stkr.fr/p/1my8',
            'http://jv.stkr.fr/p/1mya',
            'http://jv.stkr.fr/p/1my5',
            'http://jv.stkr.fr/p/1n28'
        ]),
        JSON.stringify([])
    ];
    for (var i = 0; i < varNames.length; i++) {
        if (GM_getValue(varNames[i]) === undefined) {
            GM_setValue(varNames[i], valDefault[i]);
        }
    }
    var espacementStickers = GM_getValue('espacementStickers');
    var tailleStickers = GM_getValue('tailleStickers');
    var tailleFenetre = GM_getValue('tailleFenetre');
    var modifierCouleurPosts = GM_getValue('modifierCouleurPosts');
    var couleurBackground = GM_getValue('couleurBackground');
    var couleurBordure = GM_getValue('couleurBordure');
    var supprimerFond = GM_getValue('supprimerFond');
    var tailleSticker = GM_getValue('tailleSticker');
    var stickerCliquable = GM_getValue('stickerCliquable');
    var supprStickersBan = GM_getValue('supprStickersBan');
    var supprDesPosts = GM_getValue('supprDesPosts');
    var stickerMessageDeSuppr = GM_getValue('stickerMessageDeSuppr');
    var sons = GM_getValue('sons');
    var webmPlayer = GM_getValue('webmPlayer');
    var youtubePlayer = GM_getValue('youtubePlayer');
    var vocarooPlayer = GM_getValue('vocarooPlayer');
    var widthPlayer = GM_getValue('widthPlayer');
    var heightPlayer = GM_getValue('heightPlayer');
    var playerSignature = GM_getValue('playerSignature');
    var intervalle = GM_getValue('intervalle');
    var xBarreHistorique = GM_getValue('xBarreHistorique');
    var homeCat = GM_getValue('homeCat');
    var stickersBanListe = JSON.parse(GM_getValue('stickersBanListe'));
    var historique = JSON.parse(GM_getValue('historique'));
    if (historique.length > espacementStickers * xBarreHistorique - xBarreHistorique) {
        historique.splice(espacementStickers * xBarreHistorique - xBarreHistorique);
        GM_setValue('historique', JSON.stringify(historique));
    }
    var allCat = [];
    var catRealNames = [];
    var catIcons = [];
    var catToShow = [];
    var catPopulaires = [];
    if (GM_getValue('catFavoris') === undefined) {
        GM_setValue('catFavoris', JSON.stringify([]));
    }
    var catFavoris = JSON.parse(GM_getValue('catFavoris'))
    var catCache = [];
    var nCatPopulaires = [];
    var countMenu = 0;
    if (GM_getValue('lTimestamp') === undefined || (Math.floor(Date.now() / 60000) - GM_getValue('lTimestamp') >= intervalle) || GM_getValue('allCat') === undefined || GM_getValue('catRealNames') === undefined || GM_getValue('catIcons') === undefined || GM_getValue('catToShow') === undefined || GM_getValue('catToShowS') === undefined || GM_getValue('catPopulaires') === undefined) {
        GM_deleteValue('lTimestamp');
        GM_xmlhttpRequest({
            method: 'GET',
            url: 'http://jvcsticker.esy.es/specials/CoreJVCSticker++.xml?_=' + Date.now(),
            headers: {
                'User-Agent': 'Mozilla/5.0',
                'Accept': 'text/xml'
            },
            onload: function(response) {
                var responseXML = null;
                if (!response.responseXML) {
                    responseXML = new DOMParser().parseFromString(response.responseText, 'text/xml');
                }
                var varCache = response.responseXML.getElementsByTagName('var');
                for (var ab = 0; ab < varCache.length; ab++) {
                    allCat.push(varCache[ab].childNodes[0].nodeValue);
                    catRealNames.push(varCache[ab].parentNode.getElementsByTagName('nom')[0].childNodes[0].nodeValue);
                    catIcons.push(varCache[ab].parentNode.getElementsByTagName('icone')[0].childNodes[0].nodeValue);
                }
                GM_setValue('allCat', JSON.stringify(allCat));
                GM_setValue('catRealNames', JSON.stringify(catRealNames));
                GM_setValue('catIcons', JSON.stringify(catIcons));
                var needReset = false;
                var tmpCatToShow = JSON.parse(GM_getValue('catToShow'));
                if (tmpCatToShow !== undefined) {
                    for (var ac = 0; ac < tmpCatToShow.length; ac++) {
                        if (allCat.indexOf(tmpCatToShow[ac]) == -1) {
                            needReset = true;
                            break;
                        }
                    }
                }
                if (GM_getValue('catToShow') === undefined || GM_getValue('catToShowS') === undefined || needReset == true) {
                    var defaultCache = response.responseXML.getElementsByTagName('default');
                    for (var aa = 0; aa < defaultCache.length; aa++) {
                        if (defaultCache[aa].childNodes[0].nodeValue == 'true') {
                            catToShow.push(varCache[aa].childNodes[0].nodeValue);
                        }
                    }
                    GM_setValue('catToShow', JSON.stringify(catToShow));
                    GM_setValue('catToShowS', JSON.stringify(catToShow));
                }
                var bestStckr = [];
                for (var r = 0; r < allCat.length; r++) {
                    var catStckr = [];
                    var stickersCache = response.responseXML.getElementsByTagName('stickers')[r].getElementsByTagName('sticker');
                    var nbStickers = stickersCache.length;
                    for (var t = 0; t < nbStickers; t++) {
                        var sound = stickersCache[t].getElementsByTagName('sounds')[0].childNodes.length;
                        if (sound !== 0) {
                            sound = stickersCache[t].getElementsByTagName('sounds')[0].childNodes[0].nodeValue.split(' ');
                        } else {
                            sound = '';
                        }
                        catStckr.push({
                            'url': stickersCache[t].getElementsByTagName('url')[0].childNodes[0].nodeValue,
                            'counter': stickersCache[t].getElementsByTagName('counter')[0].childNodes[0].nodeValue,
                            'tags': stickersCache[t].getElementsByTagName('tags')[0].childNodes[0].nodeValue.split(' '),
                            'sounds': sound
                        });
                    }

                    var toLen = 0;
                    if (catStckr.length > 40) {
                        toLen = 40;
                    } else {
                        toLen = catStckr.length - 1
                    }
                    for (var i = 0; i < toLen; i++) {
                        bestStckr.push({
                            'url': catStckr[i].url,
                            'counter': catStckr[i].counter
                        });
                    }

                    var favsLen = catFavoris.length;
                    for (var i = 0; i < favsLen; i++) {
                        var idxFav = catStckr.map(function(e) { return e.url; }).indexOf(catFavoris[favsLen - 1 - i]);
                        if (idxFav != -1) {
                            var tmp = catStckr[idxFav];
                            catStckr.splice(idxFav, 1);
                            catStckr.unshift(tmp);
                        }
                    }
                    catCache.push(catStckr);
                    GM_setValue(allCat[r], JSON.stringify(catStckr));
                }
                bestStckr.sort(function(a, b) {
                    return b.counter - a.counter;
                });
                for (var i = 0; i < 40; i++) {
                    nCatPopulaires.push(bestStckr[i].url);
                }
                catToShow = JSON.parse(GM_getValue('catToShow'));
                GM_setValue('lTimestamp', Math.floor(Date.now() / 60000));
                listsLoaded();
            }
        });
    } else {
        allCat = JSON.parse(GM_getValue('allCat'));
        catRealNames = JSON.parse(GM_getValue('catRealNames'));
        catIcons = JSON.parse(GM_getValue('catIcons'));
        catToShow = JSON.parse(GM_getValue('catToShow'));
        catPopulaires = JSON.parse(GM_getValue('catPopulaires'));
        for (var r = 0; r < allCat.length; r++) {
            catCache.push(JSON.parse(GM_getValue(allCat[r])));
        }
        listsLoaded();
    }
    var isLoaded = false;

    function listsLoaded() {
        var Latinise={};
        Latinise.latin_map={"Á":"A","Ă":"A","Ắ":"A","Ặ":"A","Ằ":"A","Ẳ":"A","Ẵ":"A","Ǎ":"A","Â":"A","Ấ":"A","Ậ":"A","Ầ":"A","Ẩ":"A","Ẫ":"A","Ä":"A","Ǟ":"A","Ȧ":"A","Ǡ":"A","Ạ":"A","Ȁ":"A","À":"A","Ả":"A","Ȃ":"A","Ā":"A","Ą":"A","Å":"A","Ǻ":"A","Ḁ":"A","Ⱥ":"A","Ã":"A","Ꜳ":"AA","Æ":"AE","Ǽ":"AE","Ǣ":"AE","Ꜵ":"AO","Ꜷ":"AU","Ꜹ":"AV","Ꜻ":"AV","Ꜽ":"AY","Ḃ":"B","Ḅ":"B","Ɓ":"B","Ḇ":"B","Ƀ":"B","Ƃ":"B","Ć":"C","Č":"C","Ç":"C","Ḉ":"C","Ĉ":"C","Ċ":"C","Ƈ":"C","Ȼ":"C","Ď":"D","Ḑ":"D","Ḓ":"D","Ḋ":"D","Ḍ":"D","Ɗ":"D","Ḏ":"D","ǲ":"D","ǅ":"D","Đ":"D","Ƌ":"D","Ǳ":"DZ","Ǆ":"DZ","É":"E","Ĕ":"E","Ě":"E","Ȩ":"E","Ḝ":"E","Ê":"E","Ế":"E","Ệ":"E","Ề":"E","Ể":"E","Ễ":"E","Ḙ":"E","Ë":"E","Ė":"E","Ẹ":"E","Ȅ":"E","È":"E","Ẻ":"E","Ȇ":"E","Ē":"E","Ḗ":"E","Ḕ":"E","Ę":"E","Ɇ":"E","Ẽ":"E","Ḛ":"E","Ꝫ":"ET","Ḟ":"F","Ƒ":"F","Ǵ":"G","Ğ":"G","Ǧ":"G","Ģ":"G","Ĝ":"G","Ġ":"G","Ɠ":"G","Ḡ":"G","Ǥ":"G","Ḫ":"H","Ȟ":"H","Ḩ":"H","Ĥ":"H","Ⱨ":"H","Ḧ":"H","Ḣ":"H","Ḥ":"H","Ħ":"H","Í":"I","Ĭ":"I","Ǐ":"I","Î":"I","Ï":"I","Ḯ":"I","İ":"I","Ị":"I","Ȉ":"I","Ì":"I","Ỉ":"I","Ȋ":"I","Ī":"I","Į":"I","Ɨ":"I","Ĩ":"I","Ḭ":"I","Ꝺ":"D","Ꝼ":"F","Ᵹ":"G","Ꞃ":"R","Ꞅ":"S","Ꞇ":"T","Ꝭ":"IS","Ĵ":"J","Ɉ":"J","Ḱ":"K","Ǩ":"K","Ķ":"K","Ⱪ":"K","Ꝃ":"K","Ḳ":"K","Ƙ":"K","Ḵ":"K","Ꝁ":"K","Ꝅ":"K","Ĺ":"L","Ƚ":"L","Ľ":"L","Ļ":"L","Ḽ":"L","Ḷ":"L","Ḹ":"L","Ⱡ":"L","Ꝉ":"L","Ḻ":"L","Ŀ":"L","Ɫ":"L","ǈ":"L","Ł":"L","Ǉ":"LJ","Ḿ":"M","Ṁ":"M","Ṃ":"M","Ɱ":"M","Ń":"N","Ň":"N","Ņ":"N","Ṋ":"N","Ṅ":"N","Ṇ":"N","Ǹ":"N","Ɲ":"N","Ṉ":"N","Ƞ":"N","ǋ":"N","Ñ":"N","Ǌ":"NJ","Ó":"O","Ŏ":"O","Ǒ":"O","Ô":"O","Ố":"O","Ộ":"O","Ồ":"O","Ổ":"O","Ỗ":"O","Ö":"O","Ȫ":"O","Ȯ":"O","Ȱ":"O","Ọ":"O","Ő":"O","Ȍ":"O","Ò":"O","Ỏ":"O","Ơ":"O","Ớ":"O","Ợ":"O","Ờ":"O","Ở":"O","Ỡ":"O","Ȏ":"O","Ꝋ":"O","Ꝍ":"O","Ō":"O","Ṓ":"O","Ṑ":"O","Ɵ":"O","Ǫ":"O","Ǭ":"O","Ø":"O","Ǿ":"O","Õ":"O","Ṍ":"O","Ṏ":"O","Ȭ":"O","Ƣ":"OI","Ꝏ":"OO","Ɛ":"E","Ɔ":"O","Ȣ":"OU","Ṕ":"P","Ṗ":"P","Ꝓ":"P","Ƥ":"P","Ꝕ":"P","Ᵽ":"P","Ꝑ":"P","Ꝙ":"Q","Ꝗ":"Q","Ŕ":"R","Ř":"R","Ŗ":"R","Ṙ":"R","Ṛ":"R","Ṝ":"R","Ȑ":"R","Ȓ":"R","Ṟ":"R","Ɍ":"R","Ɽ":"R","Ꜿ":"C","Ǝ":"E","Ś":"S","Ṥ":"S","Š":"S","Ṧ":"S","Ş":"S","Ŝ":"S","Ș":"S","Ṡ":"S","Ṣ":"S","Ṩ":"S","Ť":"T","Ţ":"T","Ṱ":"T","Ț":"T","Ⱦ":"T","Ṫ":"T","Ṭ":"T","Ƭ":"T","Ṯ":"T","Ʈ":"T","Ŧ":"T","Ɐ":"A","Ꞁ":"L","Ɯ":"M","Ʌ":"V","Ꜩ":"TZ","Ú":"U","Ŭ":"U","Ǔ":"U","Û":"U","Ṷ":"U","Ü":"U","Ǘ":"U","Ǚ":"U","Ǜ":"U","Ǖ":"U","Ṳ":"U","Ụ":"U","Ű":"U","Ȕ":"U","Ù":"U","Ủ":"U","Ư":"U","Ứ":"U","Ự":"U","Ừ":"U","Ử":"U","Ữ":"U","Ȗ":"U","Ū":"U","Ṻ":"U","Ų":"U","Ů":"U","Ũ":"U","Ṹ":"U","Ṵ":"U","Ꝟ":"V","Ṿ":"V","Ʋ":"V","Ṽ":"V","Ꝡ":"VY","Ẃ":"W","Ŵ":"W","Ẅ":"W","Ẇ":"W","Ẉ":"W","Ẁ":"W","Ⱳ":"W","Ẍ":"X","Ẋ":"X","Ý":"Y","Ŷ":"Y","Ÿ":"Y","Ẏ":"Y","Ỵ":"Y","Ỳ":"Y","Ƴ":"Y","Ỷ":"Y","Ỿ":"Y","Ȳ":"Y","Ɏ":"Y","Ỹ":"Y","Ź":"Z","Ž":"Z","Ẑ":"Z","Ⱬ":"Z","Ż":"Z","Ẓ":"Z","Ȥ":"Z","Ẕ":"Z","Ƶ":"Z","Ĳ":"IJ","Œ":"OE","ᴀ":"A","ᴁ":"AE","ʙ":"B","ᴃ":"B","ᴄ":"C","ᴅ":"D","ᴇ":"E","ꜰ":"F","ɢ":"G","ʛ":"G","ʜ":"H","ɪ":"I","ʁ":"R","ᴊ":"J","ᴋ":"K","ʟ":"L","ᴌ":"L","ᴍ":"M","ɴ":"N","ᴏ":"O","ɶ":"OE","ᴐ":"O","ᴕ":"OU","ᴘ":"P","ʀ":"R","ᴎ":"N","ᴙ":"R","ꜱ":"S","ᴛ":"T","ⱻ":"E","ᴚ":"R","ᴜ":"U","ᴠ":"V","ᴡ":"W","ʏ":"Y","ᴢ":"Z","á":"a","ă":"a","ắ":"a","ặ":"a","ằ":"a","ẳ":"a","ẵ":"a","ǎ":"a","â":"a","ấ":"a","ậ":"a","ầ":"a","ẩ":"a","ẫ":"a","ä":"a","ǟ":"a","ȧ":"a","ǡ":"a","ạ":"a","ȁ":"a","à":"a","ả":"a","ȃ":"a","ā":"a","ą":"a","ᶏ":"a","ẚ":"a","å":"a","ǻ":"a","ḁ":"a","ⱥ":"a","ã":"a","ꜳ":"aa","æ":"ae","ǽ":"ae","ǣ":"ae","ꜵ":"ao","ꜷ":"au","ꜹ":"av","ꜻ":"av","ꜽ":"ay","ḃ":"b","ḅ":"b","ɓ":"b","ḇ":"b","ᵬ":"b","ᶀ":"b","ƀ":"b","ƃ":"b","ɵ":"o","ć":"c","č":"c","ç":"c","ḉ":"c","ĉ":"c","ɕ":"c","ċ":"c","ƈ":"c","ȼ":"c","ď":"d","ḑ":"d","ḓ":"d","ȡ":"d","ḋ":"d","ḍ":"d","ɗ":"d","ᶑ":"d","ḏ":"d","ᵭ":"d","ᶁ":"d","đ":"d","ɖ":"d","ƌ":"d","ı":"i","ȷ":"j","ɟ":"j","ʄ":"j","ǳ":"dz","ǆ":"dz","é":"e","ĕ":"e","ě":"e","ȩ":"e","ḝ":"e","ê":"e","ế":"e","ệ":"e","ề":"e","ể":"e","ễ":"e","ḙ":"e","ë":"e","ė":"e","ẹ":"e","ȅ":"e","è":"e","ẻ":"e","ȇ":"e","ē":"e","ḗ":"e","ḕ":"e","ⱸ":"e","ę":"e","ᶒ":"e","ɇ":"e","ẽ":"e","ḛ":"e","ꝫ":"et","ḟ":"f","ƒ":"f","ᵮ":"f","ᶂ":"f","ǵ":"g","ğ":"g","ǧ":"g","ģ":"g","ĝ":"g","ġ":"g","ɠ":"g","ḡ":"g","ᶃ":"g","ǥ":"g","ḫ":"h","ȟ":"h","ḩ":"h","ĥ":"h","ⱨ":"h","ḧ":"h","ḣ":"h","ḥ":"h","ɦ":"h","ẖ":"h","ħ":"h","ƕ":"hv","í":"i","ĭ":"i","ǐ":"i","î":"i","ï":"i","ḯ":"i","ị":"i","ȉ":"i","ì":"i","ỉ":"i","ȋ":"i","ī":"i","į":"i","ᶖ":"i","ɨ":"i","ĩ":"i","ḭ":"i","ꝺ":"d","ꝼ":"f","ᵹ":"g","ꞃ":"r","ꞅ":"s","ꞇ":"t","ꝭ":"is","ǰ":"j","ĵ":"j","ʝ":"j","ɉ":"j","ḱ":"k","ǩ":"k","ķ":"k","ⱪ":"k","ꝃ":"k","ḳ":"k","ƙ":"k","ḵ":"k","ᶄ":"k","ꝁ":"k","ꝅ":"k","ĺ":"l","ƚ":"l","ɬ":"l","ľ":"l","ļ":"l","ḽ":"l","ȴ":"l","ḷ":"l","ḹ":"l","ⱡ":"l","ꝉ":"l","ḻ":"l","ŀ":"l","ɫ":"l","ᶅ":"l","ɭ":"l","ł":"l","ǉ":"lj","ſ":"s","ẜ":"s","ẛ":"s","ẝ":"s","ḿ":"m","ṁ":"m","ṃ":"m","ɱ":"m","ᵯ":"m","ᶆ":"m","ń":"n","ň":"n","ņ":"n","ṋ":"n","ȵ":"n","ṅ":"n","ṇ":"n","ǹ":"n","ɲ":"n","ṉ":"n","ƞ":"n","ᵰ":"n","ᶇ":"n","ɳ":"n","ñ":"n","ǌ":"nj","ó":"o","ŏ":"o","ǒ":"o","ô":"o","ố":"o","ộ":"o","ồ":"o","ổ":"o","ỗ":"o","ö":"o","ȫ":"o","ȯ":"o","ȱ":"o","ọ":"o","ő":"o","ȍ":"o","ò":"o","ỏ":"o","ơ":"o","ớ":"o","ợ":"o","ờ":"o","ở":"o","ỡ":"o","ȏ":"o","ꝋ":"o","ꝍ":"o","ⱺ":"o","ō":"o","ṓ":"o","ṑ":"o","ǫ":"o","ǭ":"o","ø":"o","ǿ":"o","õ":"o","ṍ":"o","ṏ":"o","ȭ":"o","ƣ":"oi","ꝏ":"oo","ɛ":"e","ᶓ":"e","ɔ":"o","ᶗ":"o","ȣ":"ou","ṕ":"p","ṗ":"p","ꝓ":"p","ƥ":"p","ᵱ":"p","ᶈ":"p","ꝕ":"p","ᵽ":"p","ꝑ":"p","ꝙ":"q","ʠ":"q","ɋ":"q","ꝗ":"q","ŕ":"r","ř":"r","ŗ":"r","ṙ":"r","ṛ":"r","ṝ":"r","ȑ":"r","ɾ":"r","ᵳ":"r","ȓ":"r","ṟ":"r","ɼ":"r","ᵲ":"r","ᶉ":"r","ɍ":"r","ɽ":"r","ↄ":"c","ꜿ":"c","ɘ":"e","ɿ":"r","ś":"s","ṥ":"s","š":"s","ṧ":"s","ş":"s","ŝ":"s","ș":"s","ṡ":"s","ṣ":"s","ṩ":"s","ʂ":"s","ᵴ":"s","ᶊ":"s","ȿ":"s","ɡ":"g","ᴑ":"o","ᴓ":"o","ᴝ":"u","ť":"t","ţ":"t","ṱ":"t","ț":"t","ȶ":"t","ẗ":"t","ⱦ":"t","ṫ":"t","ṭ":"t","ƭ":"t","ṯ":"t","ᵵ":"t","ƫ":"t","ʈ":"t","ŧ":"t","ᵺ":"th","ɐ":"a","ᴂ":"ae","ǝ":"e","ᵷ":"g","ɥ":"h","ʮ":"h","ʯ":"h","ᴉ":"i","ʞ":"k","ꞁ":"l","ɯ":"m","ɰ":"m","ᴔ":"oe","ɹ":"r","ɻ":"r","ɺ":"r","ⱹ":"r","ʇ":"t","ʌ":"v","ʍ":"w","ʎ":"y","ꜩ":"tz","ú":"u","ŭ":"u","ǔ":"u","û":"u","ṷ":"u","ü":"u","ǘ":"u","ǚ":"u","ǜ":"u","ǖ":"u","ṳ":"u","ụ":"u","ű":"u","ȕ":"u","ù":"u","ủ":"u","ư":"u","ứ":"u","ự":"u","ừ":"u","ử":"u","ữ":"u","ȗ":"u","ū":"u","ṻ":"u","ų":"u","ᶙ":"u","ů":"u","ũ":"u","ṹ":"u","ṵ":"u","ᵫ":"ue","ꝸ":"um","ⱴ":"v","ꝟ":"v","ṿ":"v","ʋ":"v","ᶌ":"v","ⱱ":"v","ṽ":"v","ꝡ":"vy","ẃ":"w","ŵ":"w","ẅ":"w","ẇ":"w","ẉ":"w","ẁ":"w","ⱳ":"w","ẘ":"w","ẍ":"x","ẋ":"x","ᶍ":"x","ý":"y","ŷ":"y","ÿ":"y","ẏ":"y","ỵ":"y","ỳ":"y","ƴ":"y","ỷ":"y","ỿ":"y","ȳ":"y","ẙ":"y","ɏ":"y","ỹ":"y","ź":"z","ž":"z","ẑ":"z","ʑ":"z","ⱬ":"z","ż":"z","ẓ":"z","ȥ":"z","ẕ":"z","ᵶ":"z","ᶎ":"z","ʐ":"z","ƶ":"z","ɀ":"z","ﬀ":"ff","ﬃ":"ffi","ﬄ":"ffl","ﬁ":"fi","ﬂ":"fl","ĳ":"ij","œ":"oe","ﬆ":"st","ₐ":"a","ₑ":"e","ᵢ":"i","ⱼ":"j","ₒ":"o","ᵣ":"r","ᵤ":"u","ᵥ":"v","ₓ":"x"};
        String.prototype.latinise=function(){return this.replace(/[^A-Za-z0-9\[\] ]/g,function(a){return Latinise.latin_map[a]||a})};
        String.prototype.latinize=String.prototype.latinise;
        String.prototype.isLatin=function(){return this==this.latinise()}
        String.prototype.levenstein = function(string) {
            var a = this, b = string + "", m = [], i, j, min = Math.min;
            if (!(a && b)) return (b || a).length;
            for (i = 0; i <= b.length; m[i] = [i++]) {}
            for (j = 0; j <= a.length; m[0][j] = j++) {}
            for (i = 1; i <= b.length; i++) {
                for (j = 1; j <= a.length; j++) {
                    m[i][j] = b.charAt(i - 1) == a.charAt(j - 1)
                        ? m[i - 1][j - 1]
                    : m[i][j] = min(
                        m[i - 1][j - 1] + 1, 
                        min(m[i][j - 1] + 1, m[i - 1 ][j]))
                }
            }
            return m[b.length][a.length];
        }
        function uniq_fast(a) {
            var seen = {};
            var out = [];
            var len = a.length;
            var j = 0;
            for(var i = 0; i < len; i++) {
                var item = a[i];
                if(seen[item] !== 1) {
                    seen[item] = 1;
                    out[j++] = item;
                }
            }
            return out;
        }
        if (modifierCouleurPosts) {
            GM_addStyle('.bloc-message-forum:nth-of-type(2n+1){background: ' + couleurBackground + ';border: 1px solid ' + couleurBordure + ';} .stickersM{max-width:' + 200 + 'px !important; max-height: ' + 200 + 'px !important}');
        }
        var funcsA = [];
        var observerArea = new MutationObserver(function(mutations, mutArea) {
            var stickersArea = document.getElementsByClassName('f-lyt-default f-state-popular f-jvc-thm')[0];
            if (document.getElementsByClassName('f-stkrs f-cfx')[0] != undefined) {
                mutArea.disconnect();
                var historiqueCont = document.createElement('div');
                historiqueCont.id = 'historiqueStkr';
                historiqueCont.style = 'background:' + $(document.getElementsByClassName('container-content')[0]).css('background-color') + ';text-align:center;';
                stickersArea.insertBefore(historiqueCont, stickersArea.firstChild);
                for (var q = 0; q < historique.length; q++) {
                    var nSticker = document.createElement('li');
                    nSticker.className = 'f-stkr-w';
                    nSticker.style = 'display:inline-block;cursor:pointer;text-align:center;margin-left:2px; margin-right:2px';
                    nSticker.innerHTML = '<div class="f-stkr f-no-sml jvcstckr-stckr" style="width:' + document.getElementsByClassName('f-stkrs f-cfx')[0].offsetWidth / espacementStickers + 'px"><span class="f-hlpr"></span><img src="' + historique[q] + '" style="max-height:' + tailleStickers + 'px;max-width:' + tailleStickers + 'px" data-code=""></div>';

                    function createfuncA(Z) {
                        return function() {
                            nSticker.onclick = function() {
                                clique(Z);
                            };
                        };
                    }
                    funcsA[historique[q]] = createfuncA(historique[q]);
                    funcsA[historique[q]]();
                    document.getElementById('historiqueStkr').appendChild(nSticker);
                }
            }
            return;
        });
        observerArea.observe(document.body, {
            childList: true,
            subtree: true,
            attributes: false,
            characterData: false
        });

        /*var observerAd = new MutationObserver(function(mutations, mutAd) {
            if (document.getElementsByClassName('footer-links')[0] != undefined) {
                mutAd.disconnect();
                var adA = document.createElement('div');
        adA.style = "text-align:center;";
        adA.innerHTML = "<img src='http://image.noelshack.com/fichiers/2016/41/1476554101-e1.png'></img>";
        var zoneA = document.getElementById('page-messages-forum');
                if (zoneA == undefined) {
                    zoneA = document.getElementById('page-topics');
                }
        zoneA.parentElement.insertBefore(adA, zoneA);
        var rightCol = document.getElementById('forum-right-col');
        var adB = document.createElement('div');
        adB.style = "text-align:center;";
        adB.innerHTML = "<img src='http://image.noelshack.com/fichiers/2016/41/1476554101-e2.png'></img>";
        var zoneB = rightCol.getElementsByClassName('panel panel-jv-forum')[0];
        rightCol.insertBefore(adB, zoneB);
        var adC = document.createElement('div');
        adC.style = "text-align:center;";
        adC.innerHTML = "<img src='http://image.noelshack.com/fichiers/2016/41/1476554101-e3.png'></img>";
        var zoneC = rightCol.getElementsByClassName('panel panel-jv-forum')[1];
        rightCol.insertBefore(adC, zoneC);
        var adD = document.createElement('div');
        adD.style = "text-align:center;";
        adD.innerHTML = "<img src='http://image.noelshack.com/fichiers/2016/41/1476554101-e4.png'></img>";
        var zoneD = rightCol.getElementsByClassName('panel panel-jv-forum')[2];
                if (zoneD != undefined) {
                    rightCol.insertBefore(adD, zoneD);
                }
        var adE = document.createElement('div');
        adE.style = "text-align:center;margin-bottom:20px;";
        adE.innerHTML = "<img src='http://image.noelshack.com/fichiers/2016/41/1476554102-e5.png'></img>";
        var zoneE = document.getElementsByClassName('footer-links')[0];
        zoneE.insertBefore(adE, zoneE.firstChild);
            }
            return;
        });
        observerAd.observe(document.body, {
            childList: true,
            subtree: true,
            attributes: false,
            characterData: false
        });*/


        function clique(url) {
            GM_xmlhttpRequest({
                method: "POST",
                url: "http://jvcsticker.esy.es/req/req_counter.php",
                data: "url=" + url,
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded"
                }});
            var idxHisto = historique.indexOf(url);
            var historiqueStkr = document.getElementById('historiqueStkr');
            if (idxHisto != -1) {
                historique.splice(idxHisto, 1);
                historiqueStkr.removeChild(historiqueStkr.children[idxHisto]);
            } else {
                if (historiqueStkr.children[espacementStickers * xBarreHistorique - xBarreHistorique - 1] != undefined) {
                    historiqueStkr.removeChild(historiqueStkr.children[espacementStickers * xBarreHistorique - xBarreHistorique - 1]);
                }
            }
            var nSticker = document.createElement('li');
            nSticker.className = 'f-stkr-w';
            nSticker.style = 'display:inline-block;cursor:pointer;text-align:center;margin-left:2px; margin-right:2px';
            nSticker.innerHTML = '<div class="f-stkr f-no-sml jvcstckr-stckr" style="width:' + document.getElementsByClassName('f-stkrs f-cfx')[0].offsetWidth / espacementStickers + 'px"><span class="f-hlpr"></span><img src="' + url + '" style="max-height:' + tailleStickers + 'px;max-width:' + tailleStickers + 'px" data-code=""></div>';
            nSticker.addEventListener('click', function () {clique(this.getElementsByTagName('img')[0].src);});

            historiqueStkr.insertBefore(nSticker, historiqueStkr.firstChild);
            historique.unshift(url);
            historique.splice(espacementStickers * xBarreHistorique - xBarreHistorique);
            GM_setValue('historique', JSON.stringify(historique));
            var area = document.getElementsByClassName('area-editor')[0];
            var start = area.selectionStart;
            var end = area.selectionEnd;
            var text = area.value;
            var before = text.substring(0, start);
            var after = text.substring(end, text.length);
            if (url.substr(0, 18) == 'http://jv.stkr.fr/') {
                var newText = '[[sticker:' + url.slice(18, -7) + ']]';
                area.value = (before + newText + after);
                area.selectionStart = area.selectionEnd = start + newText.length;
            } else {
                var newText = ' ' + url + ' ';
                area.value = (before + newText + after);
                area.selectionStart = area.selectionEnd = start + newText.length;
            }
            area.focus();
            var ev = new Event('change');
            area.dispatchEvent(ev);
        }
        var isCatPopulaires = false;
        var isCatFavoris = false;
        var isSearching = false;

        function ajouterSticker(url) {
            var nSticker = document.createElement('li');
            nSticker.className = 'f-stkr-w';
            nSticker.style = 'display:inline-block;cursor:pointer;text-align:center;margin-left:2px; margin-right:2px';
            nSticker.innerHTML = '<div class="f-stkr f-no-sml jvcstckr-stckr" style="width:' + document.getElementsByClassName('f-stkrs f-cfx')[0].offsetWidth / espacementStickers + 'px"><span class="f-hlpr"></span><img src="' + url + '" style="max-height:' + tailleStickers + 'px;max-width:' + tailleStickers + 'px" data-code=""></div>';
            nSticker.addEventListener('click', function () {clique(this.getElementsByTagName('img')[0].src);});

            var nLi = document.getElementsByClassName('f-stkrs f-cfx')[0].appendChild(nSticker);
        }

        function ajouterStickers(section) {
            if (section == 'Populaires' && (!(document.getElementsByClassName('f-stkrs f-cfx')[0].firstChild) || document.getElementsByClassName('f-stkrs f-cfx')[0].firstChild.firstChild.secondChild.src != catPopulaires[0])) {
                isCatPopulaires = true;
                isCatFavoris = false;
                isSearching = false;
                for (var q = 0; q < catPopulaires.length; q++) {
                    ajouterSticker(catPopulaires[q]);
                }
                var stkrItems = document.getElementsByClassName('f-stkrs f-cfx')[0].children;
                for (var q = 0; q < stkrItems.length; q++) {
                    stkrItems[q].firstChild.children[1].firstChild.click();
                }
            } else if (section == 'Mes Favoris' && (!(document.getElementsByClassName('f-stkrs f-cfx')[0].firstChild) || document.getElementsByClassName('f-stkrs f-cfx')[0].firstChild.firstChild.secondChild.src != catFavoris[0])) {
                isCatPopulaires = false;
                isCatFavoris = true;
                isSearching = false;
                for (var q = 0; q < catFavoris.length; q++) {
                    ajouterSticker(catFavoris[q]);
                }
                var stkrItems = document.getElementsByClassName('f-stkrs f-cfx')[0].children;
                for (var q = 0; q < stkrItems.length; q++) {
                    stkrItems[q].firstChild.children[1].firstChild.click();
                }
            } else {
                isCatPopulaires = false;
                isCatFavoris = false;
                isSearching = false;
                for (var e = 0; e < allCat.length; e++) {
                    if (section == catRealNames[e] && (!(document.getElementsByClassName('f-stkrs f-cfx')[0].firstChild) || document.getElementsByClassName('f-stkrs f-cfx')[0].firstChild.firstChild.secondChild.src != catCache[e][0])) {
                        for (var q = 0; q < catCache[e].length; q++) {
                            ajouterSticker(catCache[e][q].url);
                        }
                        var stkrItems = document.getElementsByClassName('f-stkrs f-cfx')[0].children;
                        for (var q = 0; q < stkrItems.length; q++) {
                            stkrItems[q].firstChild.children[1].firstChild.click();
                        }
                        break;
                    }
                }
            }
        }

        function charger(section) {
            if (document.getElementById('g_search') !== undefined) {
                document.getElementById('g_search').value = '';
            }
            var foo = document.getElementsByClassName('f-stkrs f-cfx')[0];
            while (foo.firstChild) {
                foo.removeChild(foo.firstChild);
            }
            var canvas = document.querySelector('div[data-flg-tt="' + section + '"]');
            var numChildren = canvas.parentElement.children.length;
            for (var i = 0; i < numChildren; i++) {
                if (canvas.parentElement.children[i].className.indexOf('f-active') != -1) {
                    canvas.parentElement.children[i].className = canvas.parentElement.children[i].className.slice(0, -9);
                    break;
                }
            }
            canvas.className += ' f-active';
            ajouterStickers(section);
        }

        function addFav(url, target) {
            var canvas = document.querySelector('div[data-flg-tt="Populaires"]').parentElement.children;
            if (canvas[0].className.indexOf('f-active') != -1 || isSearching) {
                for (var i = 0; i < catCache.length; i++) {
                    var idxFav = catCache[i].map(function(e) { return e.url; }).indexOf(url);
                    if (idxFav != -1) {
                        var tmp = catCache[i][idxFav];
                        catCache[i].splice(idxFav, 1);
                        catCache[i].unshift(tmp);
                        GM_setValue(allCat[i], JSON.stringify(catCache[i]));
                        break;
                    }
                }
            } else {
                for (var i = 2; i < canvas.length; i++) {
                    if (canvas[i].className.indexOf('f-active') != -1) {
                        var idxFav = catCache[i - 2].map(function(e) { return e.url; }).indexOf(url);
                        if (idxFav != -1) {
                            var tmp = catCache[i - 2][idxFav];
                            catCache[i - 2].splice(idxFav, 1);
                            catCache[i - 2].unshift(tmp);
                            GM_setValue(allCat[i - 2], JSON.stringify(catCache[i - 2]));
                            break;
                        }
                    }
                }
            }
            catFavoris.unshift(url);
            GM_setValue('catFavoris', JSON.stringify(catFavoris));
            if (!isCatPopulaires && !isSearching) {
                target.parentElement.parentElement.removeChild(target.parentElement);
                var neSticker = document.createElement('li');
                neSticker.className = 'f-stkr-w';
                neSticker.style = 'display:inline-block;cursor:pointer;text-align:center;margin-left:2px; margin-right:2px';
                neSticker.innerHTML = '<div class="f-stkr f-no-sml jvcstckr-stckr" style="width:' + document.getElementsByClassName('f-stkrs f-cfx')[0].offsetWidth / espacementStickers + 'px"><span class="f-hlpr"></span><img src="' + url + '" style="max-height:' + tailleStickers + 'px;max-width:' + tailleStickers + 'px" data-code=""></div>';
                neSticker.addEventListener('click', function () {clique(this.getElementsByTagName('img')[0].src);});
                document.getElementsByClassName('f-stkrs f-cfx')[0].insertBefore(neSticker, document.getElementsByClassName('f-stkrs f-cfx')[0].firstChild);
            }
        }

        function delFav(url, target) {
            var canvas = document.querySelector('div[data-flg-tt="Populaires"]').parentElement.children;
            if (canvas[0].className.indexOf('f-active') != -1 || canvas[1].className.indexOf('f-active') != -1 || isSearching) {
                for (var i = 0; i < catCache.length; i++) {
                    var idxFav = catCache[i].map(function(e) { return e.url; }).indexOf(url);
                    if (idxFav != -1) {
                        var tmp = catCache[i][idxFav];
                        catCache[i].splice(idxFav, 1);
                        catCache[i].push(tmp);
                        GM_setValue(allCat[i], JSON.stringify(catCache[i]));
                        break;
                    }
                }
            } else {
                for (var i = 2; i < canvas.length; i++) {
                    if (canvas[i].className.indexOf('f-active') != -1) {
                        var idxFav = catCache[i - 2].map(function(e) { return e.url; }).indexOf(url);
                        if (idxFav != -1) {
                            var tmp = catCache[i - 2][idxFav];
                            catCache[i - 2].splice(idxFav, 1);
                            catCache[i - 2].push(tmp);
                            GM_setValue(allCat[i - 2], JSON.stringify(catCache[i - 2]));
                            break;
                        }
                    }
                }
            }
            catFavoris.splice(catFavoris.indexOf(url), 1);
            GM_setValue('catFavoris', JSON.stringify(catFavoris));
            if (!isCatPopulaires && !isSearching) {
                target.parentElement.parentElement.removeChild(target.parentElement);
                if (!isCatFavoris) {
                    var neSticker = document.createElement('li');
                    neSticker.className = 'f-stkr-w';
                    neSticker.style = 'display:inline-block;cursor:pointer;text-align:center;margin-left:2px; margin-right:2px';
                    neSticker.innerHTML = '<div class="f-stkr f-no-sml jvcstckr-stckr" style="width:' + document.getElementsByClassName('f-stkrs f-cfx')[0].offsetWidth / espacementStickers + 'px"><span class="f-hlpr"></span><img src="' + url + '" style="max-height:' + tailleStickers + 'px;max-width:' + tailleStickers + 'px" data-code=""></div>';
                    neSticker.addEventListener('click', function () {clique(this.getElementsByTagName('img')[0].src);});
                    document.getElementsByClassName('f-stkrs f-cfx')[0].appendChild(neSticker);
                }
            }
        }

        function moveTop(url, target) {
            var canvas = document.querySelector('div[data-flg-tt="Populaires"]').parentElement.children;
            if (canvas[1].className.indexOf('f-active') != -1) {
                for (var i = 0; i < catCache.length; i++) {
                    var idxFav = catCache[i].map(function(e) { return e.url; }).indexOf(url);
                    if (idxFav != -1) {
                        var tmp = catCache[i][idxFav];
                        catCache[i].splice(idxFav, 1);
                        catCache[i].unshift(tmp);
                        GM_setValue(allCat[i], JSON.stringify(catCache[i]));
                        break;
                    }
                }
            } else {
                for (var i = 2; i < canvas.length; i++) {
                    if (canvas[i].className.indexOf('f-active') != -1) {
                        var idxFav = catCache[i - 2].map(function(e) { return e.url; }).indexOf(url);
                        if (idxFav != -1) {
                            var tmp = catCache[i - 2][idxFav];
                            catCache[i - 2].splice(idxFav, 1);
                            catCache[i - 2].unshift(tmp);
                            GM_setValue(allCat[i - 2], JSON.stringify(catCache[i - 2]));
                            break;
                        }
                    }
                }
            }
            catFavoris.splice(catFavoris.indexOf(url), 1);
            catFavoris.unshift(url);
            GM_setValue('catFavoris', JSON.stringify(catFavoris));
            target.parentElement.parentElement.removeChild(target.parentElement);
            var neSticker = document.createElement('li');
            neSticker.className = 'f-stkr-w';
            neSticker.style = 'display:inline-block;cursor:pointer;text-align:center;margin-left:2px; margin-right:2px';
            neSticker.innerHTML = '<div class="f-stkr f-no-sml jvcstckr-stckr" style="width:' + document.getElementsByClassName('f-stkrs f-cfx')[0].offsetWidth / espacementStickers + 'px"><span class="f-hlpr"></span><img src="' + url + '" style="max-height:' + tailleStickers + 'px;max-width:' + tailleStickers + 'px" data-code=""></div>';
            neSticker.addEventListener('click', function () {clique(this.getElementsByTagName('img')[0].src);});
            document.getElementsByClassName('f-stkrs f-cfx')[0].insertBefore(neSticker, document.getElementsByClassName('f-stkrs f-cfx')[0].firstChild);
        }

        function moveUp(url, target) {
            var previousUrl = target.parentElement.previousSibling.getElementsByTagName('img')[0].src;
            var previousUrlIdx = -1;
            var canvas = document.querySelector('div[data-flg-tt="Populaires"]').parentElement.children;
            if (canvas[1].className.indexOf('f-active') != -1) {
                for (var i = 0; i < catCache.length; i++) {
                    var idxFav = catCache[i].map(function(e) { return e.url; }).indexOf(url);
                    if (idxFav != -1) {
                        previousUrlIdx = catCache[i].map(function(e) { return e.url; }).indexOf(previousUrl);
                        if (previousUrlIdx != -1) {
                            var tmp = catCache[i][idxFav];
                            catCache[i].splice(idxFav, 1);
                            catCache[i].splice(previousUrlIdx, 0, tmp);
                            GM_setValue(allCat[i], JSON.stringify(catCache[i]));
                        }
                        previousUrlIdx = catFavoris.indexOf(url) - 1;
                        break;
                    }
                }
            } else {
                for (var i = 2; i < canvas.length; i++) {
                    if (canvas[i].className.indexOf('f-active') != -1) {
                        var idxFav = catCache[i - 2].map(function(e) { return e.url; }).indexOf(url);
                        if (idxFav != -1) {
                            var tmp = catCache[i - 2][idxFav];
                            catCache[i - 2].splice(idxFav, 1);
                            previousUrlIdx = catCache[i - 2].map(function(e) { return e.url; }).indexOf(previousUrl);
                            if (previousUrlIdx != -1) {
                                catCache[i - 2].splice(previousUrlIdx, 0, tmp);
                                GM_setValue(allCat[i - 2], JSON.stringify(catCache[i - 2]));
                            }
                            break;
                        }
                    }
                }
            }
            catFavoris.splice(catFavoris.indexOf(url), 1);
            catFavoris.splice(catFavoris.indexOf(previousUrl), 0, url);
            GM_setValue('catFavoris', JSON.stringify(catFavoris));
            target.parentElement.parentElement.removeChild(target.parentElement);
            var neSticker = document.createElement('li');
            neSticker.className = 'f-stkr-w';
            neSticker.style = 'display:inline-block;cursor:pointer;text-align:center;margin-left:2px; margin-right:2px';
            neSticker.innerHTML = '<div class="f-stkr f-no-sml jvcstckr-stckr" style="width:' + document.getElementsByClassName('f-stkrs f-cfx')[0].offsetWidth / espacementStickers + 'px"><span class="f-hlpr"></span><img src="' + url + '" style="max-height:' + tailleStickers + 'px;max-width:' + tailleStickers + 'px" data-code=""></div>';
            neSticker.addEventListener('click', function () {clique(this.getElementsByTagName('img')[0].src);});
            document.getElementsByClassName('f-stkrs f-cfx')[0].insertBefore(neSticker, document.getElementsByClassName('f-stkrs f-cfx')[0].childNodes[previousUrlIdx]);
        }

        function moveDown(url, target) {
            var nextUrl = target.parentElement.nextSibling.getElementsByTagName('img')[0].src;
            var nextUrlIdx = 0;
            var canvas = document.querySelector('div[data-flg-tt="Populaires"]').parentElement.children;
            if (canvas[1].className.indexOf('f-active') != -1) {
                for (var i = 0; i < catCache.length; i++) {
                    var idxFav = catCache[i].map(function(e) { return e.url; }).indexOf(url);
                    if (idxFav != -1) {
                        nextUrlIdx = catCache[i].map(function(e) { return e.url; }).indexOf(nextUrl);
                        if (nextUrlIdx != -1) {
                            var tmp = catCache[i][idxFav];
                            catCache[i].splice(idxFav, 1);
                            catCache[i].splice(nextUrlIdx, 0, tmp);
                            GM_setValue(allCat[i], JSON.stringify(catCache[i]));
                        }
                        nextUrlIdx = catFavoris.indexOf(url);
                        break;
                    }
                }
            } else {
                for (var i = 2; i < canvas.length; i++) {
                    if (canvas[i].className.indexOf('f-active') != -1) {
                        var idxFav = catCache[i - 2].map(function(e) { return e.url; }).indexOf(url);
                        if (idxFav != -1) {
                            var tmp = catCache[i - 2][idxFav];
                            catCache[i - 2].splice(idxFav, 1);
                            nextUrlIdx = catCache[i - 2].map(function(e) { return e.url; }).indexOf(nextUrl);
                            if (nextUrlIdx != -1) {
                                catCache[i - 2].splice(nextUrlIdx + 1, 0, tmp);
                                GM_setValue(allCat[i - 2], JSON.stringify(catCache[i - 2]));
                            }
                            break;
                        }
                    }
                }
            }
            catFavoris.splice(catFavoris.indexOf(url), 1);
            catFavoris.splice(catFavoris.indexOf(nextUrl) + 1, 0, url);
            GM_setValue('catFavoris', JSON.stringify(catFavoris));
            target.parentElement.parentElement.removeChild(target.parentElement);
            var neSticker = document.createElement('li');
            neSticker.className = 'f-stkr-w';
            neSticker.style = 'display:inline-block;cursor:pointer;text-align:center;margin-left:2px; margin-right:2px';
            neSticker.innerHTML = '<div class="f-stkr f-no-sml jvcstckr-stckr" style="width:' + document.getElementsByClassName('f-stkrs f-cfx')[0].offsetWidth / espacementStickers + 'px"><span class="f-hlpr"></span><img src="' + url + '" style="max-height:' + tailleStickers + 'px;max-width:' + tailleStickers + 'px" data-code=""></div>';
            neSticker.addEventListener('click', function () {clique(this.getElementsByTagName('img')[0].src);});
            document.getElementsByClassName('f-stkrs f-cfx')[0].insertBefore(neSticker, document.getElementsByClassName('f-stkrs f-cfx')[0].childNodes[nextUrlIdx].nextSibling);
        }

        function moveBottom(url, target) {
            var nextUrl = target.parentElement.nextSibling;
            var nextUrlIdx = 0;
            var canvas = document.querySelector('div[data-flg-tt="Populaires"]').parentElement.children;
            if (canvas[1].className.indexOf('f-active') != -1) {
                for (var i = 0; i < catCache.length; i++) {
                    var idxFav = catCache[i].map(function(e) { return e.url; }).indexOf(url);
                    if (idxFav != -1) {
                        var lastStkr = nextUrl.parentElement.lastChild;
                        while (catCache[i].map(function(e) { return e.url; }).indexOf(lastStkr.getElementsByTagName('img')[0].src) == -1) {
                            if (lastStkr.getElementsByTagName('img')[0].src == url) {
                                break;
                            }
                            lastStkr = lastStkr.previousSibling;
                        }
                        nextUrlIdx = catCache[i].map(function(e) { return e.url; }).indexOf(lastStkr.getElementsByTagName('img')[0].src);
                        if (nextUrlIdx != -1) {
                            var tmp = catCache[i][idxFav];
                            catCache[i].splice(idxFav, 1);
                            catCache[i].splice(nextUrlIdx, 0, tmp);
                            GM_setValue(allCat[i], JSON.stringify(catCache[i]));
                        }
                        nextUrlIdx = catFavoris.length - 1;
                        break;
                    }
                }
            } else {
                for (var i = 2; i < canvas.length; i++) {
                    if (canvas[i].className.indexOf('f-active') != -1) {
                        var idxFav = catCache[i - 2].map(function(e) { return e.url; }).indexOf(url);
                        if (idxFav != -1) {
                            var tmp = catCache[i - 2][idxFav];
                            catCache[i - 2].splice(idxFav, 1);
                            while (catFavoris.indexOf(nextUrl.getElementsByTagName('img')[0].src) != -1) {
                                nextUrl = nextUrl.nextSibling;
                            }
                            nextUrlIdx = catCache[i - 2].map(function(e) { return e.url; }).indexOf(nextUrl.getElementsByTagName('img')[0].src);
                            if (nextUrlIdx != -1) {
                                catCache[i - 2].splice(nextUrlIdx, 0, tmp);
                                GM_setValue(allCat[i - 2], JSON.stringify(catCache[i - 2]));
                            }
                            break;
                        }
                    }
                }
            }
            catFavoris.splice(catFavoris.indexOf(url), 1);
            catFavoris.push(url);
            GM_setValue('catFavoris', JSON.stringify(catFavoris));
            target.parentElement.parentElement.removeChild(target.parentElement);
            var neSticker = document.createElement('li');
            neSticker.className = 'f-stkr-w';
            neSticker.style = 'display:inline-block;cursor:pointer;text-align:center;margin-left:2px; margin-right:2px';
            neSticker.innerHTML = '<div class="f-stkr f-no-sml jvcstckr-stckr" style="width:' + document.getElementsByClassName('f-stkrs f-cfx')[0].offsetWidth / espacementStickers + 'px"><span class="f-hlpr"></span><img src="' + url + '" style="max-height:' + tailleStickers + 'px;max-width:' + tailleStickers + 'px" data-code=""></div>';
            neSticker.addEventListener('click', function () {clique(this.getElementsByTagName('img')[0].src);});
            document.getElementsByClassName('f-stkrs f-cfx')[0].insertBefore(neSticker, document.getElementsByClassName('f-stkrs f-cfx')[0].childNodes[nextUrlIdx]);
        }

        function modCat(url) {
            document.getElementById('m_2_urls').value = url;
            var ev1 = new Event('input');
            document.getElementById('m_2_urls').dispatchEvent(ev1);
            document.getElementById("m_sel").selectedIndex = 2;
            var ev2 = new Event('change');
            document.getElementById('m_sel').dispatchEvent(ev2);
            document.getElementById('toReqModal').click();
        }

        function modTags(url) {
            document.getElementById('m_3_url').value = url;
            var ev1 = new Event('input');
            document.getElementById('m_3_url').dispatchEvent(ev1);
            document.getElementById("m_sel").selectedIndex = 3;
            var ev2 = new Event('change');
            document.getElementById('m_sel').dispatchEvent(ev2);
            document.getElementById('toReqModal').click();
        }

        function modSons(url) {
            document.getElementById('m_4_url').value = url;
            var ev1 = new Event('input');
            document.getElementById('m_4_url').dispatchEvent(ev1);
            document.getElementById("m_sel").selectedIndex = 4;
            var ev2 = new Event('change');
            document.getElementById('m_sel').dispatchEvent(ev2);
            document.getElementById('toReqModal').click();
        }

        function modGSons(url) {
            document.getElementById('m_5_url').value = url;
            var ev1 = new Event('input');
            document.getElementById('m_5_url').dispatchEvent(ev1);
            document.getElementById("m_sel").selectedIndex = 5;
            var ev2 = new Event('change');
            document.getElementById('m_sel').dispatchEvent(ev2);
            document.getElementById('toReqModal').click();
        }

        function repDLink(url) {
            document.getElementById('m_6_url').value = url;
            var ev1 = new Event('input');
            document.getElementById('m_6_url').dispatchEvent(ev1);
            document.getElementById("m_sel").selectedIndex = 6;
            var ev2 = new Event('change');
            document.getElementById('m_sel').dispatchEvent(ev2);
            document.getElementById('toReqModal').click();
        }

        function repDupl1(url) {
            document.getElementById('m_7_url').value = url;
            var ev1 = new Event('input');
            document.getElementById('m_7_url').dispatchEvent(ev1);
            document.getElementById("m_sel").selectedIndex = 7;
            var ev2 = new Event('change');
            document.getElementById('m_sel').dispatchEvent(ev2);
            if (document.getElementById('m_7_newurl').value != '') {
                document.getElementById('toReqModal').click();
            }
        }

        function repDupl2(url) {
            document.getElementById('m_7_newurl').value = url;
            var ev1 = new Event('input');
            document.getElementById('m_7_newurl').dispatchEvent(ev1);
            document.getElementById("m_sel").selectedIndex = 7;
            var ev2 = new Event('change');
            document.getElementById('m_sel').dispatchEvent(ev2);
            if (document.getElementById('m_7_url').value != '') {
                document.getElementById('toReqModal').click();
            }
        }

        function delStckr(url) {
            document.getElementById('m_8_url').value = url;
            var ev1 = new Event('input');
            document.getElementById('m_8_url').dispatchEvent(ev1);
            document.getElementById("m_sel").selectedIndex = 8;
            var ev2 = new Event('change');
            document.getElementById('m_sel').dispatchEvent(ev2);
            document.getElementById('toReqModal').click();
        }

        $(function(){
            $.contextMenu({
                selector: '.jvcstckr-stckr', 
                build: function($trigger, e) {
                    var url = e.target.parentElement.getElementsByTagName('img')[0].src;
                    var target = e.target.parentElement;

                    var hasSons = false;
                    if (((url.startsWith('http://') || url.startsWith('https://')) && url.includes('image.noelshack.com') && (url.endsWith('.png') || url.endsWith('.gif') || url.endsWith('.jpg') || url.endsWith('.jpeg'))) || (url.startsWith('http://jv.stkr.fr/p/') && url.endsWith('?f-ed=1'))) {
                        for (var yb = 0; yb < catCache.length; yb++) {
                            var url_idx = catCache[yb].map(function(e) { return e.url; }).indexOf(url);
                            if (url_idx != -1) {
                                if (catCache[yb][url_idx].sounds.length > 0) {
                                    hasSons = true;
                                }
                                break;
                            }
                        }
                    }

                    if (hasSons) {
                        if (!isCatFavoris && catFavoris.indexOf(url) == -1) {
                            return {
                                callback: function(key, options) {
                                    if (key == 'addFav') {
                                        addFav(url, target);
                                    } else if (key == 'modCat') {
                                        modCat(url);
                                    } else if (key == 'modTags') {
                                        modTags(url);
                                    } else if (key == 'modSons') {
                                        modSons(url);
                                    } else if (key == 'modGSons') {
                                        modGSons(url);
                                    } else if (key == 'repDLink') {
                                        repDLink(url);
                                    } else if (key == 'repDupl1') {
                                        repDupl1(url);
                                    } else if (key == 'repDupl2') {
                                        repDupl2(url);
                                    } else if (key == 'delStckr') {
                                        delStckr(url);
                                    }
                                },
                                items: {
                                    "addFav": {name: "Ajouter aux favoris", icon: "fa-star"},
                                    "sep1": "---------",
                                    "modCat": {name: "Modifier la catégorie du sticker", icon: "fa-folder-open"},
                                    "modTags": {name: "Modifier les tags du sticker", icon: "fa-tags"},
                                    "modSons": {name: "Modifier les sons du sticker", icon: "fa-volume-up"},
                                    "modGSons": {name: "Modifier les sons du groupe", icon: "fa-volume-up"},
                                    "repDLink": {name: "Reporter un lien mort", icon: "fa-chain-broken"},
                                    "repDupl1": {name: "Reporter un doublon (sticker original)", icon: "fa-files-o"},
                                    "repDupl2": {name: "Reporter un doublon (doublon du sticker)", icon: "fa-files-o"},
                                    "delStckr": {name: "Supprimer le sticker", icon: "fa-trash"},
                                    "sep2": "---------",
                                    "fermer": {name: "Fermer", icon: "fa-times"}
                                }
                            };
                        } else if (!isCatPopulaires && !isSearching) {
                            if (target.parentElement.previousSibling != null && target.parentElement.nextSibling != null && catFavoris.indexOf(target.parentElement.nextSibling.getElementsByTagName('img')[0].src) != -1) {
                                return {
                                    callback: function(key, options) {
                                        if (key == 'delFav') {
                                            delFav(url, target);
                                        } else if (key == 'moveTop') {
                                            moveTop(url, target);
                                        } else if (key == 'moveUp') {
                                            moveUp(url, target);
                                        } else if (key == 'moveDown') {
                                            moveDown(url, target);
                                        } else if (key == 'moveBottom') {
                                            moveBottom(url, target);
                                        } else if (key == 'modCat') {
                                            modCat(url);
                                        } else if (key == 'modTags') {
                                            modTags(url);
                                        } else if (key == 'modSons') {
                                            modSons(url);
                                        } else if (key == 'modGSons') {
                                            modGSons(url);
                                        } else if (key == 'repDLink') {
                                            repDLink(url);
                                        } else if (key == 'repDupl1') {
                                            repDupl1(url);
                                        } else if (key == 'repDupl2') {
                                            repDupl2(url);
                                        } else if (key == 'delStckr') {
                                            delStckr(url);
                                        }
                                    },
                                    items: {
                                        "delFav": {name: "Supprimer des favoris", icon: "fa-star-o"},
                                        "moveTop": {name: "Déplacer tout en haut", icon: "fa-angle-double-up"},
                                        "moveUp": {name: "Déplacer vers le haut", icon: "fa-angle-up"},
                                        "moveDown": {name: "Déplacer vers le bas", icon: "fa-angle-down"},
                                        "moveBottom": {name: "Déplacer tout en bas", icon: "fa-angle-double-down"},
                                        "sep1": "---------",
                                        "modCat": {name: "Modifier la catégorie du sticker", icon: "fa-folder-open"},
                                        "modTags": {name: "Modifier les tags du sticker", icon: "fa-tags"},
                                        "modSons": {name: "Modifier les sons du sticker", icon: "fa-volume-up"},
                                        "modGSons": {name: "Modifier les sons du groupe", icon: "fa-volume-up"},
                                        "repDLink": {name: "Reporter un lien mort", icon: "fa-chain-broken"},
                                        "repDupl1": {name: "Reporter un doublon (sticker original)", icon: "fa-files-o"},
                                        "repDupl2": {name: "Reporter un doublon (doublon du sticker)", icon: "fa-files-o"},
                                        "delStckr": {name: "Supprimer le sticker", icon: "fa-trash"},
                                        "sep2": "---------",
                                        "fermer": {name: "Fermer", icon: "fa-times"}
                                    }
                                };
                            } else if (target.parentElement.previousSibling != null) {
                                return {
                                    callback: function(key, options) {
                                        if (key == 'delFav') {
                                            delFav(url, target);
                                        } else if (key == 'moveTop') {
                                            moveTop(url, target);
                                        } else if (key == 'moveUp') {
                                            moveUp(url, target);
                                        } else if (key == 'modCat') {
                                            modCat(url);
                                        } else if (key == 'modTags') {
                                            modTags(url);
                                        } else if (key == 'modSons') {
                                            modSons(url);
                                        } else if (key == 'modGSons') {
                                            modGSons(url);
                                        } else if (key == 'repDLink') {
                                            repDLink(url);
                                        } else if (key == 'repDupl1') {
                                            repDupl1(url);
                                        } else if (key == 'repDupl2') {
                                            repDupl2(url);
                                        } else if (key == 'delStckr') {
                                            delStckr(url);
                                        }
                                    },
                                    items: {
                                        "delFav": {name: "Supprimer des favoris", icon: "fa-star-o"},
                                        "moveTop": {name: "Déplacer tout en haut", icon: "fa-angle-double-up"},
                                        "moveUp": {name: "Déplacer vers le haut", icon: "fa-angle-up"},
                                        "sep1": "---------",
                                        "modCat": {name: "Modifier la catégorie du sticker", icon: "fa-folder-open"},
                                        "modTags": {name: "Modifier les tags du sticker", icon: "fa-tags"},
                                        "modSons": {name: "Modifier les sons du sticker", icon: "fa-volume-up"},
                                        "modGSons": {name: "Modifier les sons du groupe", icon: "fa-volume-up"},
                                        "repDLink": {name: "Reporter un lien mort", icon: "fa-chain-broken"},
                                        "repDupl1": {name: "Reporter un doublon (sticker original)", icon: "fa-files-o"},
                                        "repDupl2": {name: "Reporter un doublon (doublon du sticker)", icon: "fa-files-o"},
                                        "delStckr": {name: "Supprimer le sticker", icon: "fa-trash"},
                                        "sep2": "---------",
                                        "fermer": {name: "Fermer", icon: "fa-times"}
                                    }
                                };
                            } else if (target.parentElement.nextSibling != null && catFavoris.indexOf(target.parentElement.nextSibling.getElementsByTagName('img')[0].src) != -1) {
                                return {
                                    callback: function(key, options) {
                                        if (key == 'delFav') {
                                            delFav(url, target);
                                        } else if (key == 'moveDown') {
                                            moveDown(url, target);
                                        } else if (key == 'moveBottom') {
                                            moveBottom(url, target);
                                        } else if (key == 'modCat') {
                                            modCat(url);
                                        } else if (key == 'modTags') {
                                            modTags(url);
                                        } else if (key == 'modSons') {
                                            modSons(url);
                                        } else if (key == 'modGSons') {
                                            modGSons(url);
                                        } else if (key == 'repDLink') {
                                            repDLink(url);
                                        } else if (key == 'repDupl1') {
                                            repDupl1(url);
                                        } else if (key == 'repDupl2') {
                                            repDupl2(url);
                                        } else if (key == 'delStckr') {
                                            delStckr(url);
                                        }
                                    },
                                    items: {
                                        "delFav": {name: "Supprimer des favoris", icon: "fa-star-o"},
                                        "moveDown": {name: "Déplacer vers le bas", icon: "fa-angle-down"},
                                        "moveBottom": {name: "Déplacer tout en bas", icon: "fa-angle-double-down"},
                                        "sep1": "---------",
                                        "modCat": {name: "Modifier la catégorie du sticker", icon: "fa-folder-open"},
                                        "modTags": {name: "Modifier les tags du sticker", icon: "fa-tags"},
                                        "modSons": {name: "Modifier les sons du sticker", icon: "fa-volume-up"},
                                        "modGSons": {name: "Modifier les sons du groupe", icon: "fa-volume-up"},
                                        "repDLink": {name: "Reporter un lien mort", icon: "fa-chain-broken"},
                                        "repDupl1": {name: "Reporter un doublon (sticker original)", icon: "fa-files-o"},
                                        "repDupl2": {name: "Reporter un doublon (doublon du sticker)", icon: "fa-files-o"},
                                        "delStckr": {name: "Supprimer le sticker", icon: "fa-trash"},
                                        "sep2": "---------",
                                        "fermer": {name: "Fermer", icon: "fa-times"}
                                    }
                                };
                            } else {
                                return {
                                    callback: function(key, options) {
                                        if (key == 'delFav') {
                                            delFav(url, target);
                                        } else if (key == 'modCat') {
                                            modCat(url);
                                        } else if (key == 'modTags') {
                                            modTags(url);
                                        } else if (key == 'modSons') {
                                            modSons(url);
                                        } else if (key == 'modGSons') {
                                            modGSons(url);
                                        } else if (key == 'repDLink') {
                                            repDLink(url);
                                        } else if (key == 'repDupl1') {
                                            repDupl1(url);
                                        } else if (key == 'repDupl2') {
                                            repDupl2(url);
                                        } else if (key == 'delStckr') {
                                            delStckr(url);
                                        }
                                    },
                                    items: {
                                        "delFav": {name: "Supprimer des favoris", icon: "fa-star-o"},
                                        "sep1": "---------",
                                        "modCat": {name: "Modifier la catégorie du sticker", icon: "fa-folder-open"},
                                        "modTags": {name: "Modifier les tags du sticker", icon: "fa-tags"},
                                        "modSons": {name: "Modifier les sons du sticker", icon: "fa-volume-up"},
                                        "modGSons": {name: "Modifier les sons du groupe", icon: "fa-volume-up"},
                                        "repDLink": {name: "Reporter un lien mort", icon: "fa-chain-broken"},
                                        "repDupl1": {name: "Reporter un doublon (sticker original)", icon: "fa-files-o"},
                                        "repDupl2": {name: "Reporter un doublon (doublon du sticker)", icon: "fa-files-o"},
                                        "delStckr": {name: "Supprimer le sticker", icon: "fa-trash"},
                                        "sep2": "---------",
                                        "fermer": {name: "Fermer", icon: "fa-times"}
                                    }
                                };
                            }
                        } else {
                            return {
                                callback: function(key, options) {
                                    if (key == 'delFav') {
                                        delFav(url, target);
                                    } else if (key == 'modCat') {
                                        modCat(url);
                                    } else if (key == 'modTags') {
                                        modTags(url);
                                    } else if (key == 'modSons') {
                                        modSons(url);
                                    } else if (key == 'modGSons') {
                                        modGSons(url);
                                    } else if (key == 'repDLink') {
                                        repDLink(url);
                                    } else if (key == 'repDupl1') {
                                        repDupl1(url);
                                    } else if (key == 'repDupl2') {
                                        repDupl2(url);
                                    } else if (key == 'delStckr') {
                                        delStckr(url);
                                    }
                                },
                                items: {
                                    "delFav": {name: "Supprimer des favoris", icon: "fa-star-o"},
                                    "sep1": "---------",
                                    "modCat": {name: "Modifier la catégorie du sticker", icon: "fa-folder-open"},
                                    "modTags": {name: "Modifier les tags du sticker", icon: "fa-tags"},
                                    "modSons": {name: "Modifier les sons du sticker", icon: "fa-volume-up"},
                                    "modGSons": {name: "Modifier les sons du groupe", icon: "fa-volume-up"},
                                    "repDLink": {name: "Reporter un lien mort", icon: "fa-chain-broken"},
                                    "repDupl1": {name: "Reporter un doublon (sticker original)", icon: "fa-files-o"},
                                    "repDupl2": {name: "Reporter un doublon (doublon du sticker)", icon: "fa-files-o"},
                                    "delStckr": {name: "Supprimer le sticker", icon: "fa-trash"},
                                    "sep2": "---------",
                                    "fermer": {name: "Fermer", icon: "fa-times"}
                                }
                            };
                        }
                    } else {
                        if (!isCatFavoris && catFavoris.indexOf(url) == -1) {
                            return {
                                callback: function(key, options) {
                                    if (key == 'addFav') {
                                        addFav(url, target);
                                    } else if (key == 'modCat') {
                                        modCat(url);
                                    } else if (key == 'modTags') {
                                        modTags(url);
                                    } else if (key == 'modSons') {
                                        modSons(url);
                                    } else if (key == 'repDLink') {
                                        repDLink(url);
                                    } else if (key == 'repDupl1') {
                                        repDupl1(url);
                                    } else if (key == 'repDupl2') {
                                        repDupl2(url);
                                    } else if (key == 'delStckr') {
                                        delStckr(url);
                                    }
                                },
                                items: {
                                    "addFav": {name: "Ajouter aux favoris", icon: "fa-star"},
                                    "sep1": "---------",
                                    "modCat": {name: "Modifier la catégorie du sticker", icon: "fa-folder-open"},
                                    "modTags": {name: "Modifier les tags du sticker", icon: "fa-tags"},
                                    "modSons": {name: "Modifier les sons du sticker", icon: "fa-volume-up"},
                                    "repDLink": {name: "Reporter un lien mort", icon: "fa-chain-broken"},
                                    "repDupl1": {name: "Reporter un doublon (sticker original)", icon: "fa-files-o"},
                                    "repDupl2": {name: "Reporter un doublon (doublon du sticker)", icon: "fa-files-o"},
                                    "delStckr": {name: "Supprimer le sticker", icon: "fa-trash"},
                                    "sep2": "---------",
                                    "fermer": {name: "Fermer", icon: "fa-times"}
                                }
                            };
                        } else if (!isCatPopulaires && !isSearching) {
                            if (target.parentElement.previousSibling != null && target.parentElement.nextSibling != null && catFavoris.indexOf(target.parentElement.nextSibling.getElementsByTagName('img')[0].src) != -1) {
                                return {
                                    callback: function(key, options) {
                                        if (key == 'delFav') {
                                            delFav(url, target);
                                        } else if (key == 'moveTop') {
                                            moveTop(url, target);
                                        } else if (key == 'moveUp') {
                                            moveUp(url, target);
                                        } else if (key == 'moveDown') {
                                            moveDown(url, target);
                                        } else if (key == 'moveBottom') {
                                            moveBottom(url, target);
                                        } else if (key == 'modCat') {
                                            modCat(url);
                                        } else if (key == 'modTags') {
                                            modTags(url);
                                        } else if (key == 'modSons') {
                                            modSons(url);
                                        } else if (key == 'repDLink') {
                                            repDLink(url);
                                        } else if (key == 'repDupl1') {
                                            repDupl1(url);
                                        } else if (key == 'repDupl2') {
                                            repDupl2(url);
                                        } else if (key == 'delStckr') {
                                            delStckr(url);
                                        }
                                    },
                                    items: {
                                        "delFav": {name: "Supprimer des favoris", icon: "fa-star-o"},
                                        "moveTop": {name: "Déplacer tout en haut", icon: "fa-angle-double-up"},
                                        "moveUp": {name: "Déplacer vers le haut", icon: "fa-angle-up"},
                                        "moveDown": {name: "Déplacer vers le bas", icon: "fa-angle-down"},
                                        "moveBottom": {name: "Déplacer tout en bas", icon: "fa-angle-double-down"},
                                        "sep1": "---------",
                                        "modCat": {name: "Modifier la catégorie du sticker", icon: "fa-folder-open"},
                                        "modTags": {name: "Modifier les tags du sticker", icon: "fa-tags"},
                                        "modSons": {name: "Modifier les sons du sticker", icon: "fa-volume-up"},
                                        "repDLink": {name: "Reporter un lien mort", icon: "fa-chain-broken"},
                                        "repDupl1": {name: "Reporter un doublon (sticker original)", icon: "fa-files-o"},
                                        "repDupl2": {name: "Reporter un doublon (doublon du sticker)", icon: "fa-files-o"},
                                        "delStckr": {name: "Supprimer le sticker", icon: "fa-trash"},
                                        "sep2": "---------",
                                        "fermer": {name: "Fermer", icon: "fa-times"}
                                    }
                                };
                            } else if (target.parentElement.previousSibling != null) {
                                return {
                                    callback: function(key, options) {
                                        if (key == 'delFav') {
                                            delFav(url, target);
                                        } else if (key == 'moveTop') {
                                            moveTop(url, target);
                                        } else if (key == 'moveUp') {
                                            moveUp(url, target);
                                        } else if (key == 'modCat') {
                                            modCat(url);
                                        } else if (key == 'modTags') {
                                            modTags(url);
                                        } else if (key == 'modSons') {
                                            modSons(url);
                                        } else if (key == 'repDLink') {
                                            repDLink(url);
                                        } else if (key == 'repDupl1') {
                                            repDupl1(url);
                                        } else if (key == 'repDupl2') {
                                            repDupl2(url);
                                        } else if (key == 'delStckr') {
                                            delStckr(url);
                                        }
                                    },
                                    items: {
                                        "delFav": {name: "Supprimer des favoris", icon: "fa-star-o"},
                                        "moveTop": {name: "Déplacer tout en haut", icon: "fa-angle-double-up"},
                                        "moveUp": {name: "Déplacer vers le haut", icon: "fa-angle-up"},
                                        "sep1": "---------",
                                        "modCat": {name: "Modifier la catégorie du sticker", icon: "fa-folder-open"},
                                        "modTags": {name: "Modifier les tags du sticker", icon: "fa-tags"},
                                        "modSons": {name: "Modifier les sons du sticker", icon: "fa-volume-up"},
                                        "repDLink": {name: "Reporter un lien mort", icon: "fa-chain-broken"},
                                        "repDupl1": {name: "Reporter un doublon (sticker original)", icon: "fa-files-o"},
                                        "repDupl2": {name: "Reporter un doublon (doublon du sticker)", icon: "fa-files-o"},
                                        "delStckr": {name: "Supprimer le sticker", icon: "fa-trash"},
                                        "sep2": "---------",
                                        "fermer": {name: "Fermer", icon: "fa-times"}
                                    }
                                };
                            } else if (target.parentElement.nextSibling != null && catFavoris.indexOf(target.parentElement.nextSibling.getElementsByTagName('img')[0].src) != -1) {
                                return {
                                    callback: function(key, options) {
                                        if (key == 'delFav') {
                                            delFav(url, target);
                                        } else if (key == 'moveDown') {
                                            moveDown(url, target);
                                        } else if (key == 'moveBottom') {
                                            moveBottom(url, target);
                                        } else if (key == 'modCat') {
                                            modCat(url);
                                        } else if (key == 'modTags') {
                                            modTags(url);
                                        } else if (key == 'modSons') {
                                            modSons(url);
                                        } else if (key == 'repDLink') {
                                            repDLink(url);
                                        } else if (key == 'repDupl1') {
                                            repDupl1(url);
                                        } else if (key == 'repDupl2') {
                                            repDupl2(url);
                                        } else if (key == 'delStckr') {
                                            delStckr(url);
                                        }
                                    },
                                    items: {
                                        "delFav": {name: "Supprimer des favoris", icon: "fa-star-o"},
                                        "moveDown": {name: "Déplacer vers le bas", icon: "fa-angle-down"},
                                        "moveBottom": {name: "Déplacer tout en bas", icon: "fa-angle-double-down"},
                                        "sep1": "---------",
                                        "modCat": {name: "Modifier la catégorie du sticker", icon: "fa-folder-open"},
                                        "modTags": {name: "Modifier les tags du sticker", icon: "fa-tags"},
                                        "modSons": {name: "Modifier les sons du sticker", icon: "fa-volume-up"},
                                        "repDLink": {name: "Reporter un lien mort", icon: "fa-chain-broken"},
                                        "repDupl1": {name: "Reporter un doublon (sticker original)", icon: "fa-files-o"},
                                        "repDupl2": {name: "Reporter un doublon (doublon du sticker)", icon: "fa-files-o"},
                                        "delStckr": {name: "Supprimer le sticker", icon: "fa-trash"},
                                        "sep2": "---------",
                                        "fermer": {name: "Fermer", icon: "fa-times"}
                                    }
                                };
                            } else {
                                return {
                                    callback: function(key, options) {
                                        if (key == 'delFav') {
                                            delFav(url, target);
                                        } else if (key == 'modCat') {
                                            modCat(url);
                                        } else if (key == 'modTags') {
                                            modTags(url);
                                        } else if (key == 'modSons') {
                                            modSons(url);
                                        } else if (key == 'repDLink') {
                                            repDLink(url);
                                        } else if (key == 'repDupl1') {
                                            repDupl1(url);
                                        } else if (key == 'repDupl2') {
                                            repDupl2(url);
                                        } else if (key == 'delStckr') {
                                            delStckr(url);
                                        }
                                    },
                                    items: {
                                        "delFav": {name: "Supprimer des favoris", icon: "fa-star-o"},
                                        "sep1": "---------",
                                        "modCat": {name: "Modifier la catégorie du sticker", icon: "fa-folder-open"},
                                        "modTags": {name: "Modifier les tags du sticker", icon: "fa-tags"},
                                        "modSons": {name: "Modifier les sons du sticker", icon: "fa-volume-up"},
                                        "repDLink": {name: "Reporter un lien mort", icon: "fa-chain-broken"},
                                        "repDupl1": {name: "Reporter un doublon (sticker original)", icon: "fa-files-o"},
                                        "repDupl2": {name: "Reporter un doublon (doublon du sticker)", icon: "fa-files-o"},
                                        "delStckr": {name: "Supprimer le sticker", icon: "fa-trash"},
                                        "sep2": "---------",
                                        "fermer": {name: "Fermer", icon: "fa-times"}
                                    }
                                };
                            }
                        } else {
                            return {
                                callback: function(key, options) {
                                    if (key == 'delFav') {
                                        delFav(url, target);
                                    } else if (key == 'modCat') {
                                        modCat(url);
                                    } else if (key == 'modTags') {
                                        modTags(url);
                                    } else if (key == 'modSons') {
                                        modSons(url);
                                    } else if (key == 'repDLink') {
                                        repDLink(url);
                                    } else if (key == 'repDupl1') {
                                        repDupl1(url);
                                    } else if (key == 'repDupl2') {
                                        repDupl2(url);
                                    } else if (key == 'delStckr') {
                                        delStckr(url);
                                    }
                                },
                                items: {
                                    "delFav": {name: "Supprimer des favoris", icon: "fa-star-o"},
                                    "sep1": "---------",
                                    "modCat": {name: "Modifier la catégorie du sticker", icon: "fa-folder-open"},
                                    "modTags": {name: "Modifier les tags du sticker", icon: "fa-tags"},
                                    "modSons": {name: "Modifier les sons du sticker", icon: "fa-volume-up"},
                                    "repDLink": {name: "Reporter un lien mort", icon: "fa-chain-broken"},
                                    "repDupl1": {name: "Reporter un doublon (sticker original)", icon: "fa-files-o"},
                                    "repDupl2": {name: "Reporter un doublon (doublon du sticker)", icon: "fa-files-o"},
                                    "delStckr": {name: "Supprimer le sticker", icon: "fa-trash"},
                                    "sep2": "---------",
                                    "fermer": {name: "Fermer", icon: "fa-times"}
                                }
                            };
                        }
                    }
                }
            });
        });

        function tArea() {
            document.getElementById('message_topic').removeEventListener('click', tArea);
            check();
        }

        function generateUpdate() {
            if (lastVersion > GM_info.script.version) {
                return '<a href="https://ticki84.github.io/JVCSticker++.user.js" target="_blank">Une nouvelle version de JVCSticker++ est disponible!</a><br><br><p>Version actuelle: ' + GM_info.script.version + '</p><p>Dernière version: ' + lastVersion + '</p>';
            } else {
                return '<p>Vous possédez la dernière version de JVCSticker++ (' + GM_info.script.version + ')</p>';
            }
        }

        function generateBanList() {
            var text = '';
            for (var g = 0; g < stickersBanListe.length; g++) {
                text += '<option>' + stickersBanListe[g] + '</option>';
            }
            return text;
        }

        function generateShownCat() {
            var text = '';
            for (var g = 0; g < catToShow.length; g++) {
                var idx = allCat.indexOf(catToShow[g]);
                text += '<option id="' + allCat[idx] + '">' + catRealNames[idx] + '</option>';
            }
            return text;
        }

        function generateHiddenCat() {
            var text = '';
            for (var g = 0; g < allCat.length; g++) {
                if (catToShow.indexOf(allCat[g]) == -1) {
                    text += '<option id="' + allCat[g] + '">' + catRealNames[g] + '</option>';
                }
            }
            return text;
        }

        function chargerEdit() {
            if (supprimerFond) {
                var f = [];
                var x = document.getElementsByClassName('img-shack');

                function createf(i) {
                    return function() {
                        var z = x[i].alt;
                        var newSrc = x[i].src;
                        if (z.includes('/fichiers/') && (z.endsWith('.png') || z.endsWith('.gif'))) {
                            newSrc = x[i].alt;
                        } else if (z.includes('/minis/') && (z.endsWith('.png') || z.endsWith('.gif'))) {
                            newSrc = x[i].alt;
                            newSrc = newSrc.replace('/minis/', '/fichiers/');
                        } else if (x[i].src.includes('/minis/') && (z.endsWith('.png') || z.endsWith('.gif'))) {
                            newSrc = newSrc.replace('/minis/', '/fichiers/');
                            if (z.endsWith('.gif')) {
                                newSrc = newSrc.replace('.png', '.gif');
                            }
                        } else if (x[i].src.includes('/minis/') && (z.endsWith('.jpg') || z.endsWith('.jpeg'))) {
                            newSrc = newSrc.replace('/minis/', '/fichiers/');
                            if (z.endsWith('.jpg')) {
                                newSrc = newSrc.replace('.png', '.jpg');
                            } else if (z.endsWith('.jpeg')) {
                                newSrc = newSrc.replace('.png', '.jpeg');
                            }
                        }
                        var newImg = new Image();
                        newImg.addEventListener('load', function() {
                            if (this.width / this.height > 4 / 3 * 0.99 && this.width / this.height < 4 / 3 * 1.01) {
                                if (!stickerCliquable) {
                                    x[i].parentElement.style.display = 'none';
                                    x[i].parentElement.parentElement.insertBefore(x[i], x[i].parentElement);
                                }
                                x[i].src = this.src;
                            }
                        });
                        newImg.src = newSrc;
                    };
                }
                for (var i = 0; i < x.length; i++) {
                    f[i] = createf(i);
                    f[i]();
                }
                if (tailleSticker != 100) {
                    var u = document.getElementsByClassName('img-shack');
                    for (var i = 0; i < u.length; i++) {
                        u[i].width = u[i].width * tailleSticker / 100;
                        u[i].height = u[i].height * tailleSticker / 100;
                    }
                    var y = document.getElementsByClassName('img-stickers');
                    for (var i = 0; i < y.length; i++) {
                        y[i].width = y[i].width * tailleSticker / 100;
                        y[i].height = y[i].height * tailleSticker / 100;
                        if (!(y[i].className.includes('stickersM'))) {
                            y[i].className += ' stickersM';
                        }
                    }
                }
            }
            if (!supprimerFond && tailleSticker != 100) {
                var x = document.getElementsByClassName('img-shack');
                for (var i = 0; i < x.length; i++) {
                    x[i].width = x[i].width * tailleSticker / 100;
                    x[i].height = x[i].height * tailleSticker / 100;
                }
                var y = document.getElementsByClassName('img-stickers');
                for (var i = 0; i < y.length; i++) {
                    y[i].width = y[i].width * tailleSticker / 100;
                    y[i].height = y[i].height * tailleSticker / 100;
                    if (!(y[i].className.includes('stickersM'))) {
                        y[i].className += ' stickersM';
                    }
                }
            }
            if (sons) {
                var funcsB = [];
                var audioList = [];
                var audioN = 0;
                var imgsList = document.getElementsByClassName('img-shack');

                function createfuncB(g) {
                    return function() {
                        var idxStckr = -1;
                        for (var idxCache = 0; idxCache < catCache.length; idxCache++) {
                            idxStckr = catCache[idxCache].map(function(e) { return e.url; }).indexOf(imgsList[g].alt);
                            if (idxStckr != -1) {
                                imgsList[g].addEventListener('mouseover', function() {
                                    audioList.push(new Audio(catCache[idxCache][idxStckr].sounds[Math.floor(Math.random() * catCache[idxCache][idxStckr].sounds.length)]));
                                    audioList[audioN].play();
                                    audioN++;
                                });
                                imgsList[g].addEventListener('mouseout', function() {
                                    audioList[audioN - 1].pause();
                                });
                                break;
                            }
                        }
                    };
                }
                for (var g = 0; g < imgsList.length; g++) {
                    funcsB[g] = createfuncB(g);
                }
                for (var g = 0; g < imgsList.length; g++) {
                    funcsB[g]();
                }
            }
            if (webmPlayer || youtubePlayer || vocarooPlayer) {
                var links = document.getElementsByClassName('xXx');
                for (var i = 0; i < links.length; i++) {
                    if (playerSignature || (!(playerSignature) && !(links[i].parentElement.parentElement.className.includes('signature-msg')))) {
                        if (webmPlayer) {
                            if (links[i].href.startsWith('http://webm.land/w/')) {
                                links[i].style.display = 'none';
                                var videoId = links[i].href.replace('http://webm.land/w/', '').replace('/', '') + '.webm';
                                video = document.createElement('video');
                                video.width = widthPlayer;
                                video.height = heightPlayer;
                                video.innerHTML = '<source src="http://webm.land/media/' + videoId + '" type="video/webm">';
                                video.setAttribute('loop', 'loop');
                                links[i].parentElement.insertBefore(video, links[i]);
                                video.addEventListener('mouseover', function() {
                                    $(this).get(0).currentTime = 0;
                                    $(this).get(0).play();
                                });
                                video.addEventListener('mouseout', function() {
                                    $(this).get(0).pause();
                                });
                            } else if (links[i].href.endsWith('.webm')) {
                                links[i].style.display = 'none';
                                video = document.createElement('video');
                                video.width = widthPlayer;
                                video.height = heightPlayer;
                                video.innerHTML = '<source src="' + links[i].href + '" type="video/webm">';
                                video.setAttribute('loop', 'loop');
                                links[i].parentElement.insertBefore(video, links[i]);
                                video.addEventListener('mouseover', function() {
                                    $(this).get(0).currentTime = 0;
                                    $(this).get(0).play();
                                });
                                video.addEventListener('mouseout', function() {
                                    $(this).get(0).pause();
                                });
                            }
                        }
                        if (youtubePlayer) {
                            if (links[i].href.startsWith('https://www.youtube.com/watch?v=') || links[i].href.startsWith('https://youtu.be/')) {
                                var getId = '';
                                if (links[i].href.startsWith('https://www.youtube.com/watch?v=')) {
                                    var videoIndex = links[i].href.indexOf('v=');
                                    var upIndex = links[i].href.indexOf('&', videoIndex);
                                    if (upIndex == -1) {
                                        getId = links[i].href.substring(videoIndex + 2);
                                    } else {
                                        getId = links[i].href.substring(videoIndex + 2, upIndex);
                                    }
                                } else if (links[i].href.startsWith('https://youtu.be/')) {
                                    var upIndex = links[i].href.indexOf('?', 17);
                                    if (upIndex == -1) {
                                        getId = links[i].href.substring(17);
                                    } else {
                                        getId = links[i].href.substring(17, upIndex);
                                    }
                                }
                                links[i].style.display = 'none';
                                video = document.createElement('object');
                                video.setAttribute('style', 'width:100%;height:100%;width: ' + widthPlayer + 'px; height: ' + heightPlayer + 'px; float: none; clear: both; margin: 2px auto;');
                                var timeIndex = links[i].href.indexOf('t=');
                                if (timeIndex != -1) {
                                    var totalSec = 0;
                                    var upTo = links[i].href.indexOf('&', timeIndex);
                                    if (upTo == -1) {
                                        links[i].href = links[i].href + '&';
                                        upTo = links[i].href.indexOf('&', timeIndex);
                                    }
                                    var hourIndex = links[i].href.indexOf('h', timeIndex);
                                    var minIndex = links[i].href.indexOf('m', timeIndex);
                                    var secIndex = links[i].href.indexOf('s', timeIndex);
                                    var isHour = false;
                                    if (hourIndex != -1 && hourIndex < upTo) {
                                        isHour = true;
                                    }
                                    var isMin = false;
                                    if (minIndex != -1 && minIndex < upTo) {
                                        isMin = true;
                                    }
                                    var isSec = false;
                                    if (secIndex != -1 && secIndex < upTo) {
                                        isSec = true;
                                    }
                                    if (isHour) {
                                        totalSec += Number((links[i].href.substring(timeIndex + 2, hourIndex)) * 3600);
                                    }
                                    if (isMin) {
                                        if (isHour) {
                                            totalSec += Number((links[i].href.substring(hourIndex + 1, minIndex)) * 60);
                                        } else {
                                            totalSec += Number((links[i].href.substring(timeIndex + 2, minIndex)) * 60);
                                        }
                                    }
                                    if (isSec) {
                                        if (isMin) {
                                            totalSec += Number(links[i].href.substring(minIndex + 1, secIndex));
                                        } else if (isHour) {
                                            totalSec += Number(links[i].href.substring(hourIndex + 1, secIndex));
                                        } else {
                                            totalSec += Number(links[i].href.substring(timeIndex + 2, secIndex));
                                        }
                                    }
                                    if (!isHour && !isMin && !isSec) {
                                        totalSec += Number(links[i].href.substring(timeIndex + 2, upTo));
                                    }
                                    getId += '?start=' + totalSec;
                                }
                                video.setAttribute('data', 'http://www.youtube.com/embed/' + getId);
                                links[i].parentElement.insertBefore(video, links[i]);
                            }
                        }
                        if (vocarooPlayer) {
                            if (links[i].href.startsWith('http://vocaroo.com/i/')) {
                                links[i].style.display = 'none';
                                var vocarooId = links[i].href.replace('http://vocaroo.com/i/', '').replace('/', '');
                                vocaroo = document.createElement('object');
                                vocaroo.width = '148';
                                vocaroo.height = '44';
                                vocaroo.innerHTML = '<param name="movie" value="http://vocaroo.com/player.swf?playMediaID=' + vocarooId + '&autoplay=0"></param><param name="wmode" value="transparent"></param><embed src="http://vocaroo.com/player.swf?playMediaID=' + vocarooId + '&autoplay=0" width="148" height="44" wmode="transparent" type="application/x-shockwave-flash"></embed>';
                                links[i].parentElement.insertBefore(vocaroo, links[i]);
                            }
                        }
                    }
                }
            }
            if (!stickerCliquable && !supprimerFond) {
                var fc = [];
                var ix = document.getElementsByClassName('img-shack');

                function createfc(i) {
                    return function() {
                        var z = ix[i].alt;
                        var newSrc = ix[i].src;
                        if (z.includes('/fichiers/') && (z.endsWith('.png') || z.endsWith('.gif'))) {
                            newSrc = ix[i].alt;
                        } else if (z.includes('/minis/') && (z.endsWith('.png') || z.endsWith('.gif'))) {
                            newSrc = ix[i].alt;
                            newSrc = newSrc.replace('/minis/', '/fichiers/');
                        } else if (ix[i].src.includes('/minis/') && (z.endsWith('.png') || z.endsWith('.gif'))) {
                            newSrc = newSrc.replace('/minis/', '/fichiers/');
                            if (z.endsWith('.gif')) {
                                newSrc = newSrc.replace('.png', '.gif');
                            }
                        } else if (ix[i].src.includes('/minis/') && (z.endsWith('.jpg') || z.endsWith('.jpeg'))) {
                            newSrc = newSrc.replace('/minis/', '/fichiers/');
                            if (z.endsWith('.jpg')) {
                                newSrc = newSrc.replace('.png', '.jpg');
                            } else if (z.endsWith('.jpeg')) {
                                newSrc = newSrc.replace('.png', '.jpeg');
                            }
                        }
                        var newImg = new Image();
                        newImg.addEventListener('load', function() {
                            if (this.width / this.height > 4 / 3 * 0.99 && this.width / this.height < 4 / 3 * 1.01) {
                                ix[i].parentElement.style.display = 'none';
                                ix[i].parentElement.parentElement.insertBefore(ix[i], ix[i].parentElement);
                            }
                        });
                        newImg.src = newSrc;
                    };
                }
                for (var i = 0; i < ix.length; i++) {
                    fc[i] = createfc(i);
                    fc[i]();
                }
            }
        }

        function chargerBarre() {
            isLoaded = true;
            setTimeout(function() {
                var canvasPop = document.querySelector('div[data-flg-tt="Populaires"]').parentElement;
                var saveHeight = $(document.querySelector('div[data-flg-tt="Populaires"]')).outerHeight(true);
                var saveWidth = $(document.querySelector('div[data-flg-tt="Populaires"]')).outerWidth(true);
                while (canvasPop.firstChild) {
                    canvasPop.removeChild(canvasPop.firstChild);
                }
                var populaires = document.createElement('div');
                populaires.className = 'f-tab f-h f-active';
                populaires.style.width = '25px';
                populaires.style.height = '23px';
                populaires.style.lineHeight = '23px';
                populaires.style.fontSize = '14px';
                populaires.setAttribute('data-flg-tt', 'Populaires');
                populaires.innerHTML = '$<div style="display: none;" class="f-ttw"><div style="top: -26px; left: -9px;" class="f-inner"><div class="f-tt">Populaires</div><div class="f-arr"></div></div></div>';
                populaires.onclick = function() {
                    charger('Populaires');
                };
                document.querySelector('div[data-flg-tt="Biblioth&egrave;que"]').parentElement.setAttribute('style', 'display:none !important;height:0px !important;width:0px !important');
                canvasPop.appendChild(populaires);
                var favoris = document.createElement('div');
                favoris.className = 'f-tab f-h';
                favoris.style.width = '25px';
                favoris.style.height = '23px';
                favoris.style.lineHeight = '23px';
                favoris.style.fontSize = '14px';
                favoris.setAttribute('data-flg-tt', 'Mes Favoris');
                favoris.innerHTML = 'â<div style="display: none;" class="f-ttw"><div style="top: -26px; left: -9px;" class="f-inner"><div class="f-tt">Mes Favoris</div><div class="f-arr"></div></div></div>';
                favoris.onclick = function() {
                    charger('Mes Favoris');
                };
                canvasPop.appendChild(favoris);
                var timeoutActive = false;
                var hbLoaded = false;
                var observerPopu = new MutationObserver(function(mutations, mutPopu) {
                    var popStickers = document.getElementsByClassName('f-stkrs f-cfx')[0].getElementsByClassName('f-stkr-w');
                    if (popStickers.length > 27) {
                        mutPopu.disconnect();
                        hbLoaded = true;
                        if (nCatPopulaires.length > 0) {
                            for (var h = 0; h < 20; h++) {
                                nCatPopulaires.unshift(popStickers[19 - h].getElementsByTagName('img')[0].src);
                            }
                            for (var h = 0; h < 20; h++) {
                                var idxO = nCatPopulaires.indexOf(nCatPopulaires[h], 20);
                                if (idxO != -1) {
                                    nCatPopulaires.splice(idxO, 1);
                                }
                            }
                            nCatPopulaires.splice(40);
                            catPopulaires = nCatPopulaires;
                            GM_setValue('catPopulaires', JSON.stringify(catPopulaires));
                        }
                        charger('Populaires');
                    } else if (timeoutActive === false) {
                        timeoutActive = true;
                        setTimeout(function() {
                            if (!hbLoaded) {
                                mutPopu.disconnect();
                                catPopulaires = JSON.parse(GM_getValue('catPopulaires'));
                                charger(homeCat);
                            }
                        }, 5000);
                    }
                    return;
                });
                observerPopu.observe(document.body, {
                    childList: true,
                    subtree: true,
                    attributes: false,
                    characterData: false
                });


                var addReq = document.createElement('div');
                function genCatListe(allowUndef) {
                    var text = '';
                    for (var b = 0; b < allCat.length; b++) {
                        text += '<option value="' + allCat[b] + '">' + catRealNames[b] + '</option>';
                    }
                    if (allowUndef) {
                        text += '<option value="undefCat" selected>Autre catégorie</option>';
                    }
                    return text;
                }
                function genIcoListe() {
                    var text = '';
                    var icons = '! " # $ % & \' ( ) 1 2 3 4 5 6 7 8 9 : ; < = ? @ A B C D E F G H I J K L M N O P Q R S T U V W X Y Z [ \\ ] ^ _ ` a b c d e f g h i j k l m n o p q r s t u v w x y z { | } ~ ¡ ¢ £ ¤ ¥ ¦ § ¨ © ª « ¬ ® ¯ ° ± ² ³ ´ µ ¶ ¸ ¹ º » ¼ ½ ¾ ¿ À Á Â Ã Ä Å Æ Ç È É Ê Ë Ì Í Î Ï Ð Ñ Ò Ó Ô Õ Ö × Ø Ù Ú Û Ü à á â ã ä å æ ç è é ê ë ì í ï ð ñ ò ó ô õ ö ÷ ø ù ú û ü ý þ'.split(' ');
                    for (var b = 0; b < icons.length; b++) {
                        text += '<option value="' + icons[b] + '">' + icons[b] + '</option>';
                    }
                    return text;
                }
                addReq.innerHTML = '<div id="reqModal" style="display:none;"><div class="modal-header"><h2 class="modal-title" style="color:#d13321; text-align:center;"><u>Effectuer une demande</u></h2></div><div class="modal-body"><div style="text-align: center;"><br><br><select id="m_sel"><option>Ajouter des stickers</option><option>Ajouter une catégorie</option><option>Modifier la catégorie de stickers</option><option>Modifier les tags d\'un sticker</option><option>Modifier les sons d\'un sticker</option><option>Modifier les sons d\'un groupe de stickers</option><option>Signaler un lien mort</option><option>Signaler un doublon</option><option>Supprimer un sticker</option></select><br></div>'
                    + '<div class="optCategorie" id="m_0"><table><h3>Ajouter des stickers</h3><tr><td style="text-align: left;padding-left:10%;"><label>Prévisualisations</label></td><td style="text-align:center;" id="m_0_imgs"></td></tr><tr><td style="text-align: left;padding-left:10%;"><label for="m_0_urls">Adresses des images</label></td><td style="text-align: center;"><textarea rows="5" cols="45" id="m_0_urls" placeholder="https://image.noelshack.com/fichiers/example1.png https://image.noelshack.com/fichiers/example2.gif  http://image.noelshack.com/fichiers/example3.png http://jv.stkr.fr/p/exemple?f-ed=1" style="text-align:center;border:1px solid red;"></textarea></td></tr><tr><td style="text-align: left;padding-left:10%;"><label for="m_0_cat">Catégorie</label></td><td style="text-align: center;"><select id="m_0_cat" style="width:50%">' + genCatListe(true) + '</select></td></tr><tr><td style="text-align: left;padding-left:10%;"><label for="m_0_tags">Tags</label></td><td style="text-align: center;"><textarea rows="3" cols="45" placeholder="tag1 tag2 tag3" id="m_0_tags" style="text-align:center;border:1px solid red;"></textarea></td></tr><tr><td style="text-align: left;padding-left:10%;"><label for="m_0_sons">Adresses des sons</label></td><td style="text-align: center;"><textarea rows="5" cols="45" placeholder="http://* https://*" id="m_0_sons" style="text-align:center;"></textarea></td></tr><tr><td colspan="2" style="text-align: center;"><p id="m_0_txt"><b style="color:red;">Veuillez saisir au moins une adresse!</b></p></td></tr></table><br></div>'
                    + '<div class="optCategorie" id="m_1" style="display:none;"><table><h3>Ajouter une catégorie</h3><tr><td style="text-align: left;padding-left:10%;"><label for="m_1_name">Nom de la catégorie</label></td><td style="text-align: center;"><input id="m_1_name" style="text-align:center;border:1px solid red;" placeholder="Ma Catégorie" size="50"></td></tr><tr><td style="text-align: left;padding-left:10%;"><label for="m_1_icone">Icône</label></td><td style="text-align:center;"><select id="m_1_icone" style="font-family:\'flgstickers\';font-size:2em;">' + genIcoListe() + '</select></td></tr><tr><td colspan="2" style="text-align: center;"><p id="m_1_txt"><b style="color:red;">Veuillez saisir le nom de la catégorie!</b></p></td></tr></table><br></div>'
                    + '<div class="optCategorie" id="m_2" style="display:none;"><table><h3>Modifier la catégorie de stickers</h3><tr><td style="text-align: left;padding-left:10%;"><label>Prévisualisations</label></td><td style="text-align:center;" id="m_2_imgs"></td></tr><tr><td style="text-align: left;padding-left:10%;"><label for="m_2_urls">Adresses des stickers</label></td><td style="text-align: center;"><textarea rows="5" cols="45" id="m_2_urls" placeholder="https://image.noelshack.com/fichiers/example1.png https://image.noelshack.com/fichiers/example2.gif  http://image.noelshack.com/fichiers/example3.png http://jv.stkr.fr/p/exemple?f-ed=1" style="text-align:center;border:1px solid red;"></textarea></td></tr><tr><td style="text-align: left;padding-left:10%;"><label for="m_2_cat">Catégorie</label></td><td style="text-align: center;"><select id="m_2_cat" style="width:50%">' + genCatListe(false) + '</select></td></tr><tr><td colspan="2" style="text-align: center;"><p id="m_2_txt"><b style="color:red;">Veuillez saisir l\'adresse d\'au moins un sticker!</b></p></td></tr></table><br></div>'
                    + '<div class="optCategorie" id="m_3" style="display:none;"><table><h3>Modifier les tags d\'un sticker</h3><tr><td style="text-align: left;padding-left:10%;"><label>Prévisualisation</label></td><td style="text-align:center;" id="m_3_img"></td></tr><tr><td style="text-align: left;padding-left:10%;"><label for="m_3_url">Adresse du sticker</label></td><td style="text-align: center;"><input type="url" id="m_3_url" style="text-align:center;border:1px solid red;" placeholder="http://image.noelshack.com/fichiers/exemple.png" size="50"></td></tr><tr><td style="text-align: left;padding-left:10%;"><label for="m_3_tags">Tags</label></td><td style="text-align: center;"><textarea rows="5" cols="45" id="m_3_tags" style="text-align:center;border:1px solid red;" placeholder="Indiquez d\'abord l\'adresse du sticker" disabled></textarea></td></tr><tr><td colspan="2" style="text-align: center;"><p id="m_3_txt"><b style="color:red;">Veuillez saisir l\'adresse d\'un sticker!</b></p></td></tr></table><br></div>'
                    + '<div class="optCategorie" id="m_4" style="display:none;"><table><h3>Modifier les sons d\'un sticker</h3><tr><td style="text-align: left;padding-left:10%;"><label>Prévisualisation</label></td><td style="text-align:center;" id="m_4_img"></td></tr><tr><td style="text-align: left;padding-left:10%;"><label for="m_4_url">Adresse du sticker</label></td><td style="text-align: center;"><input type="url" id="m_4_url" style="text-align:center;border:1px solid red;" placeholder="http://image.noelshack.com/fichiers/exemple.png" size="50"></td></tr><tr><td style="text-align: left;padding-left:10%;"><label for="m_4_sons">Adresses des sons</label></td><td style="text-align: center;"><textarea rows="5" cols="45" placeholder="Indiquez d\'abord l\'adresse du sticker" id="m_4_sons" style="text-align:center;border:1px solid red;" disabled></textarea></td></tr><tr><td colspan="2" style="text-align: center;"><p id="m_4_txt"><b style="color:red;">Veuillez saisir l\'adresse d\'un sticker!</b></p></td></tr></table><br></div>'
                    + '<div class="optCategorie" id="m_5" style="display:none;"><table><h3>Modifier les sons d\'un groupe de stickers</h3><tr><td style="text-align: left;padding-left:10%;"><label>Prévisualisations</label></td><td style="text-align:center;" id="m_5_imgs"></td></tr><tr><td style="text-align: left;padding-left:10%;"><label for="m_5_url">Adresse d\'un sticker du groupe</label></td><td style="text-align: center;"><input type="url" id="m_5_url" style="text-align:center;border:1px solid red;" placeholder="http://image.noelshack.com/fichiers/exemple.png" size="50"></td></tr><tr><td style="text-align: left;padding-left:10%;"><label for="m_5_sons">Adresses des sons du groupe</label></td><td style="text-align: center;"><textarea rows="5" cols="45" placeholder="Indiquez d\'abord l\'adresse d\'un sticker du groupe" id="m_5_sons" style="text-align:center;border:1px solid red;" disabled></textarea></td></tr><tr><td colspan="2" style="text-align: center;"><p id="m_5_txt"><b style="color:red;">Veuillez saisir l\'adresse d\'un sticker d\'un groupe!</b></p></td></tr></table><br></div>'
                    + '<div class="optCategorie" id="m_6" style="display:none;"><table><h3>Signaler un lien mort</h3><tr><td style="text-align: left;padding-left:10%;"><label>Prévisualisation</label></td><td style="text-align:center;" id="m_6_img"></td></tr><tr><td style="text-align: left;padding-left:10%;"><label for="m_6_url">Adresse du sticker</label></td><td style="text-align: center;"><input type="url" id="m_6_url" style="text-align:center;border:1px solid red;" placeholder="http://image.noelshack.com/fichiers/exemple.png" size="50"></td></tr><tr><td style="text-align: left;padding-left:10%;"><label>Catégorie</label></td><td style="text-align: center;max-width:40ch;"><p id="m_6_cat"></p></td></tr><tr><td style="text-align: left;padding-left:10%;"><label>Tags</label></td><td style="text-align: center;max-width:40ch;"><p id="m_6_tags"></p></td></tr><tr><td style="text-align: left;padding-left:10%;"><label>Adresses des sons</label></td><td style="text-align: center;max-width:40ch;"><p id="m_6_sons"></p></td></tr><tr><td style="text-align: left;padding-left:10%;"><label for="m_6_newurl">Adresse de remplacement</label></td><td style="text-align: center;"><input type="url" id="m_6_newurl" style="text-align:center;" placeholder="http://image.noelshack.com/fichiers/exemple.png" size="50"></td></tr><tr><td colspan="2" style="text-align: center;"><p id="m_6_txt"><b style="color:red;">Veuillez saisir l\'adresse d\'un sticker!</b></p></td></tr></table><br></div>'
                    + '<div class="optCategorie" id="m_7" style="display:none;"><table><h3>Signaler un doublon</h3><tr><td style="text-align: left;padding-left:10%;"><label>Prévisualisation</label></td><td style="text-align:center;" id="m_7_img"></td></tr><tr><td style="text-align: left;padding-left:10%;"><label for="m_7_url">Adresse originale du sticker</label></td><td style="text-align: center;"><input type="url" id="m_7_url" style="text-align:center;border:1px solid red;" placeholder="http://image.noelshack.com/fichiers/exemple.png" size="50"></td></tr><tr><td style="text-align: left;padding-left:10%;"><label for="m_7_newurl">Adresse du doublon</label></td><td style="text-align: center;"><input type="url" id="m_7_newurl" style="text-align:center;border:1px solid red;" placeholder="http://image.noelshack.com/fichiers/exemple.png" size="50"></td></tr><tr><td colspan="2" style="text-align: center;"><p id="m_7_txt"><b style="color:red;">Veuillez saisir l\'adresse d\'un sticker!</b></p></td></tr></table><br></div>'
                    + '<div class="optCategorie" id="m_8" style="display:none;"><table><h3>Supprimer un sticker</h3><tr><td style="text-align: left;padding-left:10%;"><label>Prévisualisation</label></td><td style="text-align:center;" id="m_8_img"></td></tr><tr><td style="text-align: left;padding-left:10%;"><label for="m_8_url">Adresse du sticker</label></td><td style="text-align: center;"><input type="url" id="m_8_url" style="text-align:center;border:1px solid red;" placeholder="http://image.noelshack.com/fichiers/exemple.png" size="50"></td></tr><tr><td style="text-align: left;padding-left:10%;"><label for="m_8_motif">Motif de suppression</label></td><td style="text-align:center;"><select id="m_8_motif"><option value="Droits d\'auteur">Non respect des droits d\'auteur</option><option value="Vie privée">Non respect de la vie privée</option><option value="Offensant">Sticker offensant</option><option value="Incitation à la haine">Sticker incitant à la haine</option><option value="Pornographie">Sticker pornographique</option><option value="Mauvaise qualité">Sticker de mauvaise qualité</option><option value="Inutile">Sticker inutile</option><option value="Autre motif" selected>Autre motif</option></select></td></tr><tr><td colspan="2" style="text-align: center;"><p id="m_8_txt"><b style="color:red;">Veuillez saisir l\'adresse d\'un sticker!</b></p></td></tr></table><br></div>'
                    + '</div><div class="modal-footer" style="text-align: center;"><button type="button" class="btn btn-default" data-dismiss="modal" id="envoyerReq" disabled>Envoyer</button> <a rel="modal:close" id="annulerReqBtn"><button type="button" class="btn btn-default" data-dismiss="modal" id="annuler">Annuler</button></a></div></div><p style="display:none;"><a href="#reqModal" id="toReqModal" rel="modal:open">Open Modal</a></p>';
                document.body.appendChild(addReq);

                var m_0_arrf = [];
                var m_0_sonsf = [];
                var m_0_unval0 = [];
                var m_0_unval1 = [];
                var m_0_unval2 = [];
                function m_0_verify() {
                    var m_0_tags = document.getElementById('m_0_tags').value;
                    var m_0_txt = document.getElementById('m_0_txt');
                    if (m_0_arrf.length > 0 && m_0_tags != '') {
                        document.getElementById('m_0_urls').setAttribute('style', 'text-align:center;');
                        document.getElementById('m_0_tags').setAttribute('style', 'text-align:center;');
                        var msg = '';
                        if (m_0_unval0.length > 0) {
                            document.getElementById('m_0_urls').setAttribute('style', 'text-align:center;border:1px solid darkorange;');
                            if (m_0_unval0.length > 1) {
                                msg += "<div style='color:darkorange;'><b>Des adresses ne sont pas valides et seront ignorées!</br>Adresses invalides: </b><i>" + m_0_unval0.join(", ") + "</i></div>";
                            } else {
                                msg += "<div style='color:darkorange;'><b>L'adresse </b><i>" + m_0_unval0[0] + "</i><b> n'est pas valide et sera ignorée!</b></div>";
                            }
                            if (m_0_unval1.length > 0 || m_0_unval2.length > 0) {
                                msg += "</br>";
                            }
                        }
                        if (m_0_unval1.length > 0) {
                            document.getElementById('m_0_urls').setAttribute('style', 'text-align:center;border:1px solid darkorange;');
                            if (m_0_unval1.length > 1) {
                                msg += "<div style='color:darkorange;'><b>Des adresses valides sont déjà intégrées et seront ignorées!</br>Adresses déjà intégrées: </b><i>" + m_0_unval1.join(", ") + "</i></div>";
                            } else {
                                msg += "<div style='color:darkorange;'><b>L'adresse </b><i>" + m_0_unval1[0] + "</i><b> est déjà intégrée et sera ignorée!</b></div>";
                            }
                            if (m_0_unval2.length > 0) {
                                msg += "</br>";
                            }
                        }
                        document.getElementById('m_0_sons').setAttribute('style', 'text-align:center;');
                        if (m_0_unval2.length > 0) {
                            document.getElementById('m_0_sons').setAttribute('style', 'text-align:center;border:1px solid darkorange;');
                            if (m_0_unval2.length > 1) {
                                msg += "<div style='color:darkorange;'><b>Des sons ne sont pas valides et seront ignorés!</br>Sons invalides: </b><i>" + m_0_unval2.join(", ") + "</i></div>";
                            } else {
                                msg += "<div style='color:darkorange;'><b>Le son </b><i>" + m_0_unval2[0] + "</i><b> n'est pas valide et sera ignoré!</b></div>";
                            }
                        }
                        if (m_0_unval0.length < 1 && m_0_unval1.length < 1 && m_0_unval2.length < 1) {
                            m_0_txt.parentElement.parentElement.setAttribute("style", "display:none;");
                        }
                        else {
                            m_0_txt.parentElement.parentElement.removeAttribute("style");
                        }
                        m_0_txt.innerHTML = msg;
                        document.getElementById('envoyerReq').disabled = false;
                    }
                    else {
                        document.getElementById('envoyerReq').disabled = true;
                        var msg = '';
                        if (document.getElementById('m_0_urls').value == '') {
                            document.getElementById('m_0_urls').setAttribute('style', 'text-align:center;border:1px solid red;');
                            msg += "<div style='color:red;'><b>Veuillez saisir au moins une adresse!</b></div>";
                        } else if (m_0_arrf.length < 1) {
                            document.getElementById('m_0_urls').setAttribute('style', 'text-align:center;border:1px solid red;');
                            if (m_0_unval0.length > 0) {
                                if (m_0_unval0.length > 1) {
                                    msg += "<div style='color:red;'><b>Aucune adresse n'est valide!</br>Adresses invalides: </b><i>" + m_0_unval0.join(", ") + "</i></div>";
                                } else {
                                    msg += "<div style='color:red;'><b>L'adresse </b><i>" + m_0_unval0[0] + "</i><b> n'est pas valide!</b></div>";
                                }
                                if (m_0_unval1.length > 0) {
                                    msg += "</br>";
                                }
                            }
                            if (m_0_unval1.length > 0) {
                                if (m_0_unval1.length > 1) {
                                    msg += "<div style='color:red;'><b>Toutes les adresses valides sont déjà intégrées!</br>Adresses déjà intégrées: </b><i>" + m_0_unval1.join(", ") + "</i></div>";
                                } else {
                                    msg += "<div style='color:red;'><b>L'adresse </b><i>" + m_0_unval1[0] + "</i><b> est déjà intégrée!</b></div>";
                                }
                            }
                        } else {
                            document.getElementById('m_0_urls').setAttribute('style', 'text-align:center;');
                        }
                        if (m_0_tags == '') {
                            document.getElementById('m_0_tags').setAttribute('style', 'text-align:center;border:1px solid red;');
                            if (msg == '') {
                                msg += "<div style='color:red;'><b>Veuillez saisir au moins un tag!</b></div>";
                            }
                        } else {
                            document.getElementById('m_0_tags').setAttribute('style', 'text-align:center;');
                        }
                        m_0_txt.parentElement.parentElement.removeAttribute("style");
                        m_0_txt.innerHTML = msg;
                    }
                }
                document.getElementById('m_0_urls').addEventListener('input', function() {
                    m_0_unval0 = [];
                    m_0_unval1 = [];
                    var m_0_imgs = document.getElementById('m_0_imgs');
                    if (m_0_imgs.firstChild) {
                        m_0_imgs.removeChild(m_0_imgs.firstChild);
                    }
                    var m_0_arr = document.getElementById('m_0_urls').value.replace(/\n/g,' ').replace(/,/g,' ').replace(/;/g,' ').split(' ');
                    var m_0_idx = m_0_arr.indexOf('');
                    while (m_0_idx != -1) {
                        m_0_arr.splice(m_0_idx, 1);
                        m_0_idx = m_0_arr.indexOf('');
                    }
                    m_0_arrf = uniq_fast(m_0_arr);
                    for (var za = 0; za < m_0_arrf.length; za++) {
                        var z = m_0_arrf[za];
                        if (!(((m_0_arrf[za].startsWith('http://') || m_0_arrf[za].startsWith('https://')) && m_0_arrf[za].includes('image.noelshack.com') && (m_0_arrf[za].endsWith('.png') || m_0_arrf[za].endsWith('.gif') || m_0_arrf[za].endsWith('.jpg') || m_0_arrf[za].endsWith('.jpeg'))) || (m_0_arrf[za].startsWith('http://jv.stkr.fr/p/') && m_0_arrf[za].endsWith('?f-ed=1')))) {
                            m_0_unval0.push(m_0_arrf[za]);
                            m_0_arrf.splice(za, 1);
                            za--;
                        }
                    }
                    for (var ya = 0; ya < m_0_arrf.length; ya++) {
                        for (var yb = 0; yb < catCache.length; yb++) {
                            if (catCache[yb].map(function(e) { return e.url; }).indexOf(m_0_arrf[ya]) != -1) {
                                m_0_unval1.push(m_0_arrf[ya]);
                                m_0_arrf.splice(ya, 1);
                                ya--;
                                break;
                            }
                        }
                    }
                    var addImgs = document.createElement('div');
                    var content = '';
                    for (var imNb = 0; imNb < m_0_arrf.length; imNb++) {
                        if (m_0_arrf[imNb].startsWith('http://jv.stkr.fr/p/')) {
                            content += '<img height="48" width="48" src="' + m_0_arrf[imNb] + '"/>';
                        } else {
                            content += '<img height="48" width="64" src="' + m_0_arrf[imNb] + '"/>';
                        }
                        if ((imNb+1) % 5 == 0 && (imNb+1) != m_0_arrf.length) {
                            content += '</br>';
                        }
                    }
                    addImgs.innerHTML = content;
                    m_0_imgs.appendChild(addImgs);
                    m_0_verify();
                });
                document.getElementById('m_0_tags').addEventListener('input', function() {
                    m_0_verify();
                });
                document.getElementById('m_0_sons').addEventListener('input', function() {
                    m_0_unval2 = [];
                    var m_0_sons = document.getElementById('m_0_sons').value.replace(/\n/g,' ').replace(/,/g,' ').replace(/;/g,' ').split(' ');
                    var m_0_idx = m_0_sons.indexOf('');
                    while (m_0_idx != -1) {
                        m_0_sons.splice(m_0_idx, 1);
                        m_0_idx = m_0_sons.indexOf('');
                    }
                    m_0_sonsf = uniq_fast(m_0_sons);
                    for (var za = 0; za < m_0_sonsf.length; za++) {
                        var z = m_0_sonsf[za];
                        if (!(m_0_sonsf[za].startsWith('http://') || m_0_sonsf[za].startsWith('https://'))) {
                            m_0_unval2.push(m_0_sonsf[za]);
                            m_0_sonsf.splice(za, 1);
                            za--;
                        }
                    }
                    m_0_verify();
                });

                var m_1_result = false;
                function m_1_verify() {
                    var m_1_txt = document.getElementById('m_1_txt');
                    if (document.getElementById('m_1_name').value != '' && m_1_result == '') {
                        m_1_txt.parentElement.parentElement.setAttribute("style", "display:none;");
                        document.getElementById('m_1_name').setAttribute('style', 'text-align:center;');
                        document.getElementById('envoyerReq').disabled = false;
                    }
                    else {
                        document.getElementById('envoyerReq').disabled = true;
                        var msg = '';
                        m_1_txt.parentElement.parentElement.removeAttribute("style");
                        if (document.getElementById('m_1_name').value == '') {
                            document.getElementById('m_1_name').setAttribute('style', 'text-align:center;border:1px solid red;');
                            msg += "<div style='color:red;'><b>Veuillez saisir le nom de la catégorie</b></div>";
                        } else if (m_1_result != '') {
                            document.getElementById('m_1_name').setAttribute('style', 'text-align:center;border:1px solid red;');
                            msg += "<div style='color:red;'><b>Le nom </b><i>" + m_1_result + "</i><b> est déjà pris</b></div>";
                        } else {
                            document.getElementById('m_1_name').setAttribute('style', 'text-align:center;');
                        }
                        m_1_txt.parentElement.parentElement.removeAttribute("style");
                        m_1_txt.innerHTML = msg;
                    }
                }
                document.getElementById('m_1_name').addEventListener('input', function() {
                    m_1_result = '';
                    var m_1_name = document.getElementById('m_1_name').value.latinize().toLowerCase().replace(/ /g,'');
                    for (var b = 0; b < catRealNames.length; b++) {
                        if (m_1_name == catRealNames[b].latinize().toLowerCase().replace(/ /g,'')) {
                            m_1_result = catRealNames[b];
                        }
                    }
                    m_1_verify();
                });

                var m_2_arrf = [];
                var m_2_unval0 = [];
                var m_2_unval1 = [];
                var m_2_unval2 = [];
                function m_2_verify() {
                    var m_2_cat = document.getElementById('m_2_cat');
                    var m_2_txt = document.getElementById('m_2_txt');
                    if (m_2_arrf.length > 0) {
                        document.getElementById('m_2_urls').setAttribute('style', 'text-align:center;');
                        var msg = '';
                        if (m_2_unval0.length > 0) {
                            document.getElementById('m_2_urls').setAttribute('style', 'text-align:center;border:1px solid darkorange;');
                            if (m_2_unval0.length > 1) {
                                msg += "<div style='color:darkorange;'><b>Des adresses ne sont pas valides et seront ignorées!</br>Adresses invalides: </b><i>" + m_2_unval0.join(", ") + "</i></div>";
                            } else {
                                msg += "<div style='color:darkorange;'><b>L'adresse </b><i>" + m_2_unval0[0] + "</i><b> n'est pas valide et sera ignorée!</b></div>";
                            }
                            if (m_2_unval1.length > 0 || m_2_unval2.length > 0) {
                                msg += "</br>";
                            }
                        }
                        if (m_2_unval1.length > 0) {
                            document.getElementById('m_2_urls').setAttribute('style', 'text-align:center;border:1px solid darkorange;');
                            if (m_2_unval1.length > 1) {
                                msg += "<div style='color:darkorange;'><b>Des stickers n'ont pas été trouvées et seront ignorés!</br>Adresses des stickers introuvables: </b><i>" + m_2_unval1.join(", ") + "</i></div>";
                            } else {
                                msg += "<div style='color:darkorange;'><b>Le sticker </b><i>" + m_2_unval1[0] + "</i><b> n'a pas été trouvé et sera ignorée!</b></div>";
                            }
                            if (m_2_unval2.length > 0) {
                                msg += "</br>";
                            }
                        }
                        if (m_2_unval2.length > 0) {
                            document.getElementById('m_2_urls').setAttribute('style', 'text-align:center;border:1px solid darkorange;');
                            if (m_2_unval2.length > 1) {
                                msg += "<div style='color:darkorange;'><b>Des stickers appartiennent déjà à la catégorie </b><i>" + m_2_cat.options[m_2_cat.selectedIndex].innerHTML + "</i><b> et seront ignorés!</br>Adresses des stickers ignorés: </b><i>" + m_2_unval2.join(", ") + "</i></div>";
                            } else {
                                msg += "<div style='color:darkorange;'><b>Le sticker </b><i>" + m_2_unval2[0] + "</i><b> appartient déjà à la catégorie </b><i>" + m_2_cat.options[m_2_cat.selectedIndex].innerHTML + "</i><b> et sera ignoré!</b></div>";
                            }
                        }
                        if (m_2_unval0.length < 1 && m_2_unval1.length < 1 && m_2_unval2.length < 1) {
                            m_2_txt.parentElement.parentElement.setAttribute("style", "display:none;");
                        }
                        else {
                            m_2_txt.parentElement.parentElement.removeAttribute("style");
                        }
                        m_2_txt.innerHTML = msg;
                        document.getElementById('envoyerReq').disabled = false;
                    }
                    else {
                        document.getElementById('envoyerReq').disabled = true;
                        var msg = '';
                        if (document.getElementById('m_2_urls').value == '') {
                            document.getElementById('m_2_urls').setAttribute('style', 'text-align:center;border:1px solid red;');
                            msg += "<div style='color:red;'><b>Veuillez saisir l'adresse d'au moins un sticker!</b></div>";
                        } else if (m_2_arrf.length < 1) {
                            document.getElementById('m_2_urls').setAttribute('style', 'text-align:center;border:1px solid red;');
                            if (m_2_unval0.length > 0) {
                                if (m_2_unval0.length > 1) {
                                    msg += "<div style='color:red;'><b>Aucune adresse n'est valide!</br>Adresses invalides: </b><i>" + m_2_unval0.join(", ") + "</i></div>";
                                } else {
                                    msg += "<div style='color:red;'><b>L'adresse </b><i>" + m_2_unval0[0] + "</i><b> n'est pas valide!</b></div>";
                                }
                                if (m_2_unval1.length > 0 || m_2_unval2.length > 0) {
                                    msg += "</br>";
                                }
                            }
                            if (m_2_unval1.length > 0) {
                                if (m_2_unval1.length > 1) {
                                    msg += "<div style='color:red;'><b>Aucun sticker n'a été trouvé!</br>Adresses des stickers introuvables: </b><i>" + m_2_unval1.join(", ") + "</i></div>";
                                } else {
                                    msg += "<div style='color:red;'><b>Le sticker </b><i>" + m_2_unval1[0] + "</i><b> n'a pas été trouvé!</b></div>";
                                }
                                if (m_2_unval2.length > 0) {
                                    msg += "</br>";
                                }
                            }
                            if (m_2_unval2.length > 0) {
                                if (m_2_unval2.length > 1) {
                                    msg += "<div style='color:red;'><b>Tous les stickers appartiennent déjà à la catégorie </b><i>" + m_2_cat.options[m_2_cat.selectedIndex].innerHTML + "</i><b>!</br>Adresses des stickers concernés: </b><i>" + m_2_unval2.join(", ") + "</i></div>";
                                } else {
                                    msg += "<div style='color:red;'><b>Le sticker </b><i>" + m_2_unval2[0] + "</i><b> appartient déjà à la catégorie </b><i>" + m_2_cat.options[m_2_cat.selectedIndex].innerHTML + "</i><b>!</b></div>";
                                }
                            }
                        } else {
                            document.getElementById('m_2_urls').setAttribute('style', 'text-align:center;');
                        }
                        m_2_txt.parentElement.parentElement.removeAttribute("style");
                        m_2_txt.innerHTML = msg;
                    }
                }
                document.getElementById('m_2_urls').addEventListener('input', function() {
                    m_2_arrf = [];
                    m_2_unval0 = [];
                    m_2_unval1 = [];
                    m_2_unval2 = [];
                    var m_2_imgs = document.getElementById('m_2_imgs');
                    if (m_2_imgs.firstChild) {
                        m_2_imgs.removeChild(m_2_imgs.firstChild);
                    }
                    var m_2_arr = document.getElementById('m_2_urls').value.replace(/\n/g,' ').replace(/,/g,' ').replace(/;/g,' ').split(' ');
                    var m_2_idx = m_2_arr.indexOf('');
                    while (m_2_idx != -1) {
                        m_2_arr.splice(m_2_idx, 1);
                        m_2_idx = m_2_arr.indexOf('');
                    }
                    m_2_arrf = uniq_fast(m_2_arr);
                    for (var za = 0; za < m_2_arrf.length; za++) {
                        var z = m_2_arrf[za];
                        if (!(((m_2_arrf[za].startsWith('http://') || m_2_arrf[za].startsWith('https://')) && m_2_arrf[za].includes('image.noelshack.com') && (m_2_arrf[za].endsWith('.png') || m_2_arrf[za].endsWith('.gif') || m_2_arrf[za].endsWith('.jpg') || m_2_arrf[za].endsWith('.jpeg'))) || (m_2_arrf[za].startsWith('http://jv.stkr.fr/p/') && m_2_arrf[za].endsWith('?f-ed=1')))) {
                            m_2_unval0.push(m_2_arrf[za]);
                            m_2_arrf.splice(za, 1);
                            za--;
                        } else {
                            var m_2_found = false;
                            for (var yb = 0; yb < catCache.length; yb++) {
                                if (catCache[yb].map(function(e) { return e.url; }).indexOf(m_2_arrf[za]) != -1) {
                                    console.log(allCat[yb]);
                                    if (allCat[yb] == document.getElementById("m_2_cat").options[document.getElementById("m_2_cat").selectedIndex].value) {
                                        m_2_unval2.push(m_2_arrf[za]);
                                        m_2_arrf.splice(za, 1);
                                        za--;
                                    }
                                    m_2_found = true;
                                    break;
                                }
                            }
                            if (!(m_2_found)) {
                                m_2_unval1.push(m_2_arrf[za]);
                                m_2_arrf.splice(za, 1);
                                za--;
                            }
                        }
                    }
                    var addImgs = document.createElement('div');
                    var content = '';
                    for (var imNb = 0; imNb < m_2_arrf.length; imNb++) {
                        if (m_2_arrf[imNb].startsWith('http://jv.stkr.fr/p/')) {
                            content += '<img height="48" width="48" src="' + m_2_arrf[imNb] + '"/>';
                        } else {
                            content += '<img height="48" width="64" src="' + m_2_arrf[imNb] + '"/>';
                        }
                        if ((imNb+1) % 5 == 0 && (imNb+1) != m_2_arrf.length) {
                            content += '</br>';
                        }
                    }
                    addImgs.innerHTML = content;
                    m_2_imgs.appendChild(addImgs);
                    m_2_verify();
                });
                document.getElementById('m_2_cat').addEventListener('change', function() {
                    m_2_arrf = [];
                    m_2_unval0 = [];
                    m_2_unval1 = [];
                    m_2_unval2 = [];
                    var m_2_imgs = document.getElementById('m_2_imgs');
                    if (m_2_imgs.firstChild) {
                        m_2_imgs.removeChild(m_2_imgs.firstChild);
                    }
                    var m_2_arr = document.getElementById('m_2_urls').value.replace(/\n/g,' ').replace(/,/g,' ').replace(/;/g,' ').split(' ');
                    var m_2_idx = m_2_arr.indexOf('');
                    while (m_2_idx != -1) {
                        m_2_arr.splice(m_2_idx, 1);
                        m_2_idx = m_2_arr.indexOf('');
                    }
                    m_2_arrf = uniq_fast(m_2_arr);
                    for (var za = 0; za < m_2_arrf.length; za++) {
                        var z = m_2_arrf[za];
                        if (!(((m_2_arrf[za].startsWith('http://') || m_2_arrf[za].startsWith('https://')) && m_2_arrf[za].includes('image.noelshack.com') && (m_2_arrf[za].endsWith('.png') || m_2_arrf[za].endsWith('.gif') || m_2_arrf[za].endsWith('.jpg') || m_2_arrf[za].endsWith('.jpeg'))) || (m_2_arrf[za].startsWith('http://jv.stkr.fr/p/') && m_2_arrf[za].endsWith('?f-ed=1')))) {
                            m_2_unval0.push(m_2_arrf[za]);
                            m_2_arrf.splice(za, 1);
                            za--;
                        } else {
                            var m_2_found = false;
                            for (var yb = 0; yb < catCache.length; yb++) {
                                if (catCache[yb].map(function(e) { return e.url; }).indexOf(m_2_arrf[za]) != -1) {
                                    console.log(allCat[yb]);
                                    if (allCat[yb] == document.getElementById("m_2_cat").options[document.getElementById("m_2_cat").selectedIndex].value) {
                                        m_2_unval2.push(m_2_arrf[za]);
                                        m_2_arrf.splice(za, 1);
                                        za--;
                                    }
                                    m_2_found = true;
                                    break;
                                }
                            }
                            if (!(m_2_found)) {
                                m_2_unval1.push(m_2_arrf[za]);
                                m_2_arrf.splice(za, 1);
                                za--;
                            }
                        }
                    }
                    var addImgs = document.createElement('div');
                    var content = '';
                    for (var imNb = 0; imNb < m_2_arrf.length; imNb++) {
                        if (m_2_arrf[imNb].startsWith('http://jv.stkr.fr/p/')) {
                            content += '<img height="48" width="48" src="' + m_2_arrf[imNb] + '"/>';
                        } else {
                            content += '<img height="48" width="64" src="' + m_2_arrf[imNb] + '"/>';
                        }
                        if ((imNb+1) % 5 == 0 && (imNb+1) != m_2_arrf.length) {
                            content += '</br>';
                        }
                    }
                    addImgs.innerHTML = content;
                    m_2_imgs.appendChild(addImgs);
                    m_2_verify();
                });

                var m_3_vUrl = false;
                var m_3_found = false;
                var m_3_tags = [];
                var m_3_dTags = [];
                function m_3_verify() {
                    var m_3_txt = document.getElementById('m_3_txt');
                    if (document.getElementById('m_3_url').value != '' && m_3_vUrl && m_3_found && JSON.stringify(m_3_tags.sort()) != JSON.stringify(m_3_dTags.sort()) && document.getElementById('m_3_tags').value != '') {
                        m_3_txt.parentElement.parentElement.setAttribute("style", "display:none;");
                        document.getElementById('m_3_url').setAttribute('style', 'text-align:center;');
                        document.getElementById('m_3_tags').setAttribute('style', 'text-align:center;');
                        document.getElementById('envoyerReq').disabled = false;
                    }
                    else {
                        document.getElementById('envoyerReq').disabled = true;
                        var msg = '';
                        m_3_txt.parentElement.parentElement.removeAttribute("style");
                        if (document.getElementById('m_3_url').value == '') {
                            document.getElementById('m_3_url').setAttribute('style', 'text-align:center;border:1px solid red;');
                            msg += "<div style='color:red;'><b>Veuillez saisir l'adresse d'un sticker!</b></div>";
                        } else if (!(m_3_vUrl)) {
                            document.getElementById('m_3_url').setAttribute('style', 'text-align:center;border:1px solid red;');
                            msg += "<div style='color:red;'><b>L'adresse </b><i>" + document.getElementById('m_3_url').value + "</i><b> n'est pas valide!</b></div>";
                        } else if (!(m_3_found)) {
                            document.getElementById('m_3_url').setAttribute('style', 'text-align:center;border:1px solid red;');
                            msg += "<div style='color:red;'><b>Le sticker </b><i>" + document.getElementById('m_3_url').value + "</i><b> n'a pas été trouvé!</b></div>";
                        } else {
                            document.getElementById('m_3_url').setAttribute('style', 'text-align:center;');
                        }
                        if (document.getElementById('m_3_tags').value == '') {
                            document.getElementById('m_3_tags').setAttribute('style', 'text-align:center;border:1px solid red;');
                            if (msg == '') {
                                msg += "<div style='color:red;'><b>Veuillez saisir au moins un tag!</b></div>";
                            }
                        } else if (JSON.stringify(m_3_tags.sort()) == JSON.stringify(m_3_dTags.sort())) {
                            document.getElementById('m_3_tags').setAttribute('style', 'text-align:center;border:1px solid red;');
                            if (msg == '') {
                                msg += "<div style='color:red;'><b>Tags identiques aux anciens</b></div>";
                            }
                        }
                        else {
                            document.getElementById('m_3_tags').setAttribute('style', 'text-align:center;');
                        }
                        m_3_txt.parentElement.parentElement.removeAttribute("style");
                        m_3_txt.innerHTML = msg;
                    }
                }
                document.getElementById('m_3_url').addEventListener('input', function() {
                    m_3_vUrl = false;
                    m_3_found = false;
                    document.getElementById('m_3_tags').disabled = true;
                    document.getElementById('m_3_tags').placeholder = "Indiquez d'abord l'adresse du sticker";
                    var m_3_url = document.getElementById('m_3_url').value;
                    var m_3_img = document.getElementById('m_3_img');
                    if (m_3_img.firstChild) {
                        m_3_img.removeChild(m_3_img.firstChild);
                    }
                    var addImg = document.createElement('div');
                    if (((m_3_url.startsWith('http://') || m_3_url.startsWith('https://')) && m_3_url.includes('image.noelshack.com') && (m_3_url.endsWith('.png') || m_3_url.endsWith('.gif') || m_3_url.endsWith('.jpg') || m_3_url.endsWith('.jpeg'))) || (m_3_url.startsWith('http://jv.stkr.fr/p/') && m_3_url.endsWith('?f-ed=1'))) {
                        m_3_vUrl = true;
                        for (var yb = 0; yb < catCache.length; yb++) {
                            var m_3_idx = catCache[yb].map(function(e) { return e.url; }).indexOf(m_3_url);
                            if (m_3_idx != -1) {
                                if (m_3_url.startsWith('http://jv.stkr.fr/p/')) {
                                    addImg.innerHTML = '<img height="96" width="96" src="' + m_3_url + '"/>';
                                } else {
                                    addImg.innerHTML = '<img height="96" width="128" src="' + m_3_url + '"/>';
                                }
                                m_3_dTags = catCache[yb][m_3_idx].tags;
                                m_3_tags = catCache[yb][m_3_idx].tags;
                                if (m_3_dTags.length > 0) {
                                    document.getElementById('m_3_tags').value = m_3_dTags.join(" ");
                                } else {
                                    m_3_dTags = [];
                                    m_3_tags = [];
                                    document.getElementById('m_3_tags').value = "";
                                }
                                document.getElementById('m_3_tags').disabled = false;
                                document.getElementById('m_3_tags').placeholder = 'tag1 tag2 tag3';
                                m_3_found = true;
                                break;
                            }
                        }
                        if (!(m_3_found)) {
                            m_3_dTags = [];
                            document.getElementById('m_3_tags').value = '';
                        }
                    } else {
                        m_3_dTags = [];
                        document.getElementById('m_3_tags').value = '';
                    }
                    m_3_img.appendChild(addImg);
                    m_3_verify();
                });
                document.getElementById('m_3_tags').addEventListener('input', function() {
                    m_3_tags = document.getElementById('m_3_tags').value.replace(/\n/g,' ').replace(/,/g,' ').replace(/;/g,' ').latinize().toLowerCase().split(' ');
                    var m_3_idx = m_3_tags.indexOf('');
                    while (m_3_idx != -1) {
                        m_3_tags.splice(m_3_idx, 1);
                        m_3_idx = m_3_tags.indexOf('');
                    }
                    m_3_tags = uniq_fast(m_3_tags);
                    m_3_verify();
                });

                var m_4_vUrl = false;
                var m_4_found = false;
                var m_4_dSons = [];
                var m_4_sonsf = [];
                var m_4_unval = [];
                function m_4_verify() {
                    var m_4_txt = document.getElementById('m_4_txt');
                    if (document.getElementById('m_4_url').value != '' && m_4_vUrl && m_4_found && JSON.stringify(m_4_sonsf.sort()) != JSON.stringify(m_4_dSons.sort())) {
                        document.getElementById('m_4_url').setAttribute('style', 'text-align:center;');
                        document.getElementById('m_4_sons').setAttribute('style', 'text-align:center;');
                        var msg = '';
                        if (m_4_unval.length > 0) {
                            document.getElementById('m_4_sons').setAttribute('style', 'text-align:center;border:1px solid darkorange;');
                            if (m_4_unval.length > 1) {
                                msg += "<div style='color:darkorange;'><b>Des sons ne sont pas valides et seront ignorés!</br>Sons invalides: </b><i>" + m_4_unval.join(", ") + "</i></div>";
                            } else {
                                msg += "<div style='color:darkorange;'><b>Le son </b><i>" + m_4_unval[0] + "</i><b> n'est pas valide et sera ignoré!</b></div>";
                            }
                            m_4_txt.parentElement.parentElement.removeAttribute("style");
                        } else {
                            m_4_txt.parentElement.parentElement.setAttribute("style", "display:none;");
                        }
                        m_4_txt.innerHTML = msg;
                        document.getElementById('envoyerReq').disabled = false;
                    }
                    else {
                        document.getElementById('envoyerReq').disabled = true;
                        var msg = '';
                        m_4_txt.parentElement.parentElement.removeAttribute("style");
                        if (document.getElementById('m_4_url').value == '') {
                            document.getElementById('m_4_url').setAttribute('style', 'text-align:center;border:1px solid red;');
                            msg += "<div style='color:red;'><b>Veuillez saisir l'adresse d'un sticker!</b></div>";
                        } else if (!(m_4_vUrl)) {
                            document.getElementById('m_4_url').setAttribute('style', 'text-align:center;border:1px solid red;');
                            msg += "<div style='color:red;'><b>L'adresse </b><i>" + document.getElementById('m_4_url').value + "</i><b> n'est pas valide!</b></div>";
                        } else if (!(m_4_found)) {
                            document.getElementById('m_4_url').setAttribute('style', 'text-align:center;border:1px solid red;');
                            msg += "<div style='color:red;'><b>Le sticker </b><i>" + document.getElementById('m_4_url').value + "</i><b> n'a pas été trouvé!</b></div>";
                        } else {
                            document.getElementById('m_4_url').setAttribute('style', 'text-align:center;');
                        }
                        if (JSON.stringify(m_4_sonsf.sort()) == JSON.stringify(m_4_dSons.sort())) {
                            document.getElementById('m_4_sons').setAttribute('style', 'text-align:center;border:1px solid red;');
                            if (msg == '') {
                                if (m_4_unval.length > 0) {
                                    if (m_4_unval.length > 1) {
                                        msg += "<div style='color:red;'><b>Sons identiques aux anciens car des sons ne sont pas valides!</br>Sons invalides: </b><i>" + m_4_unval.join(", ") + "</i></div>";
                                    } else {
                                        msg += "<div style='color:red;'><b>Sons identiques aux anciens car le son </b><i>" + m_4_unval[0] + "</i><b> n'est pas valide!</b></div>";
                                    }
                                } else {
                                    msg += "<div style='color:red;'><b>Sons identiques aux anciens!</b></div>";
                                }
                            }
                        } else {
                            if (document.getElementById('m_4_url').value == '' || !(m_4_vUrl) || !(m_4_found)) {
                                document.getElementById('m_4_sons').setAttribute('style', 'text-align:center;border:1px solid red;');
                            } else {
                                document.getElementById('m_4_sons').setAttribute('style', 'text-align:center;');
                            }
                        }
                        m_4_txt.parentElement.parentElement.removeAttribute("style");
                        m_4_txt.innerHTML = msg;
                    }
                }
                document.getElementById('m_4_url').addEventListener('input', function() {
                    m_4_vUrl = false;
                    m_4_found = false;
                    document.getElementById('m_4_sons').disabled = true;
                    document.getElementById('m_4_sons').placeholder = "Indiquez d'abord l'adresse du sticker";
                    var m_4_url = document.getElementById('m_4_url').value;
                    var m_4_img = document.getElementById('m_4_img');
                    if (m_4_img.firstChild) {
                        m_4_img.removeChild(m_4_img.firstChild);
                    }
                    var addImg = document.createElement('div');
                    if (((m_4_url.startsWith('http://') || m_4_url.startsWith('https://')) && m_4_url.includes('image.noelshack.com') && (m_4_url.endsWith('.png') || m_4_url.endsWith('.gif') || m_4_url.endsWith('.jpg') || m_4_url.endsWith('.jpeg'))) || (m_4_url.startsWith('http://jv.stkr.fr/p/') && m_4_url.endsWith('?f-ed=1'))) {
                        m_4_vUrl = true;
                        var m_4_sValue = document.getElementById('m_4_sons').value;
                        for (var yb = 0; yb < catCache.length; yb++) {
                            var m_4_idx = catCache[yb].map(function(e) { return e.url; }).indexOf(m_4_url);
                            if (m_4_idx != -1) {
                                if (m_4_url.startsWith('http://jv.stkr.fr/p/')) {
                                    addImg.innerHTML = '<img height="96" width="96" src="' + m_4_url + '"/>';
                                } else {
                                    addImg.innerHTML = '<img height="96" width="128" src="' + m_4_url + '"/>';
                                }
                                m_4_dSons = catCache[yb][m_4_idx].sounds;
                                m_4_sonsf = catCache[yb][m_4_idx].sounds;
                                if (m_4_dSons.length > 0) {
                                    document.getElementById('m_4_sons').value = m_4_dSons.join(" ");
                                } else {
                                    m_4_dSons = [];
                                    m_4_sonsf = [];
                                    document.getElementById('m_4_sons').value = "";
                                }
                                document.getElementById('m_4_sons').disabled = false;
                                document.getElementById('m_4_sons').placeholder = 'http://* https://*';
                                m_4_found = true;
                                break;
                            }
                        }
                        if (!(m_4_found)) {
                            m_4_dSons = [];
                            document.getElementById('m_4_sons').value = '';
                        }
                    } else {
                        m_4_dSons = [];
                        document.getElementById('m_4_sons').value = '';
                    }
                    m_4_img.appendChild(addImg);
                    if (m_4_sValue != document.getElementById('m_4_sons').value) {
                        m_4_unval = [];
                        var m_4_sons = document.getElementById('m_4_sons').value.replace(/\n/g,' ').replace(/,/g,' ').replace(/;/g,' ').split(' ');
                        var m_4_idx = m_4_sons.indexOf('');
                        while (m_4_idx != -1) {
                            m_4_sons.splice(m_4_idx, 1);
                            m_4_idx = m_4_sons.indexOf('');
                        }
                        m_4_sonsf = uniq_fast(m_4_sons);
                        for (var za = 0; za < m_4_sonsf.length; za++) {
                            var z = m_4_sonsf[za];
                            if (!(m_4_sonsf[za].startsWith('http://') || m_4_sonsf[za].startsWith('https://'))) {
                                m_4_unval.push(m_4_sonsf[za]);
                                m_4_sonsf.splice(za, 1);
                                za--;
                            }
                        }
                    }
                    m_4_verify();
                });
                document.getElementById('m_4_sons').addEventListener('input', function() {
                    m_4_unval = [];
                    var m_4_sons = document.getElementById('m_4_sons').value.replace(/\n/g,' ').replace(/,/g,' ').replace(/;/g,' ').split(' ');
                    var m_4_idx = m_4_sons.indexOf('');
                    while (m_4_idx != -1) {
                        m_4_sons.splice(m_4_idx, 1);
                        m_4_idx = m_4_sons.indexOf('');
                    }
                    m_4_sonsf = uniq_fast(m_4_sons);
                    for (var za = 0; za < m_4_sonsf.length; za++) {
                        var z = m_4_sonsf[za];
                        if (!(m_4_sonsf[za].startsWith('http://') || m_4_sonsf[za].startsWith('https://'))) {
                            m_4_unval.push(m_4_sonsf[za]);
                            m_4_sonsf.splice(za, 1);
                            za--;
                        }
                    }
                    m_4_verify();
                });

                var m_5_vUrl = false;
                var m_5_found = false;
                var m_5_dSons = [];
                var m_5_sonsf = [];
                var m_5_unval = [];
                function m_5_verify() {
                    var m_5_txt = document.getElementById('m_5_txt');
                    if (document.getElementById('m_5_url').value != '' && m_5_vUrl && m_5_found && m_5_dSons.length > 0 && JSON.stringify(m_5_sonsf.sort()) != JSON.stringify(m_5_dSons.sort())) {
                        document.getElementById('m_5_url').setAttribute('style', 'text-align:center;');
                        document.getElementById('m_5_sons').setAttribute('style', 'text-align:center;');
                        var msg = '';
                        if (m_5_unval.length > 0) {
                            document.getElementById('m_5_sons').setAttribute('style', 'text-align:center;border:1px solid darkorange;');
                            if (m_5_unval.length > 1) {
                                msg += "<div style='color:darkorange;'><b>Des sons ne sont pas valides et seront ignorés!</br>Sons invalides: </b><i>" + m_5_unval.join(", ") + "</i></div>";
                            } else {
                                msg += "<div style='color:darkorange;'><b>Le son </b><i>" + m_5_unval[0] + "</i><b> n'est pas valide et sera ignoré!</b></div>";
                            }
                            m_5_txt.parentElement.parentElement.removeAttribute("style");
                        } else {
                            m_5_txt.parentElement.parentElement.setAttribute("style", "display:none;");
                        }
                        m_5_txt.innerHTML = msg;
                        document.getElementById('envoyerReq').disabled = false;
                    }
                    else {
                        document.getElementById('envoyerReq').disabled = true;
                        var msg = '';
                        m_5_txt.parentElement.parentElement.removeAttribute("style");
                        if (document.getElementById('m_5_url').value == '') {
                            document.getElementById('m_5_url').setAttribute('style', 'text-align:center;border:1px solid red;');
                            msg += "<div style='color:red;'><b>Veuillez saisir l'adresse d'un sticker d'un groupe!</b></div>";
                        } else if (!(m_5_vUrl)) {
                            document.getElementById('m_5_url').setAttribute('style', 'text-align:center;border:1px solid red;');
                            msg += "<div style='color:red;'><b>L'adresse </b><i>" + document.getElementById('m_5_url').value + "</i><b> n'est pas valide!</b></div>";
                        } else if (!(m_5_found)) {
                            document.getElementById('m_5_url').setAttribute('style', 'text-align:center;border:1px solid red;');
                            msg += "<div style='color:red;'><b>Le sticker </b><i>" + document.getElementById('m_5_url').value + "</i><b> n'a pas été trouvé!</b></div>";
                        } else if (m_5_dSons.length < 1) {
                            document.getElementById('m_5_url').setAttribute('style', 'text-align:center;border:1px solid red;');
                            msg += "<div style='color:red;'><b>Le sticker </b><i>" + document.getElementById('m_5_url').value + "</i><b> n'appartient à aucun groupe!</b></div>";
                        } else {
                            document.getElementById('m_5_url').setAttribute('style', 'text-align:center;');
                        }
                        if (JSON.stringify(m_5_sonsf.sort()) == JSON.stringify(m_5_dSons.sort())) {
                            document.getElementById('m_5_sons').setAttribute('style', 'text-align:center;border:1px solid red;');
                            if (msg == '') {
                                if (m_5_unval.length > 0) {
                                    if (m_5_unval.length > 1) {
                                        msg += "<div style='color:red;'><b>Sons identiques aux anciens car des sons ne sont pas valides!</br>Sons invalides: </b><i>" + m_5_unval.join(", ") + "</i></div>";
                                    } else {
                                        msg += "<div style='color:red;'><b>Sons identiques aux anciens car le son </b><i>" + m_5_unval[0] + "</i><b> n'est pas valide!</b></div>";
                                    }
                                } else {
                                    msg += "<div style='color:red;'><b>Sons identiques aux anciens!</b></div>";
                                }
                            }
                        }
                        else {
                            if (document.getElementById('m_5_url').value == '' || !(m_5_vUrl) || !(m_5_found)) {
                                document.getElementById('m_5_sons').setAttribute('style', 'text-align:center;border:1px solid red;');
                            } else {
                                document.getElementById('m_5_sons').setAttribute('style', 'text-align:center;');
                            }
                        }
                        m_5_txt.parentElement.parentElement.removeAttribute("style");
                        m_5_txt.innerHTML = msg;
                    }
                }
                document.getElementById('m_5_url').addEventListener('input', function() {
                    m_5_vUrl = false;
                    m_5_found = false;
                    var m_5_sValue= document.getElementById('m_5_sons').value;
                    document.getElementById('m_5_sons').disabled = true;
                    document.getElementById('m_5_sons').placeholder = "Indiquez d'abord l'adresse d\'un sticker du groupe";
                    var m_5_url = document.getElementById('m_5_url').value;
                    var m_5_imgs = document.getElementById('m_5_imgs');
                    if (m_5_imgs.firstChild) {
                        m_5_imgs.removeChild(m_5_imgs.firstChild);
                    }
                    var addImg = document.createElement('div');
                    if (((m_5_url.startsWith('http://') || m_5_url.startsWith('https://')) && m_5_url.includes('image.noelshack.com') && (m_5_url.endsWith('.png') || m_5_url.endsWith('.gif') || m_5_url.endsWith('.jpg') || m_5_url.endsWith('.jpeg'))) || (m_5_url.startsWith('http://jv.stkr.fr/p/') && m_5_url.endsWith('?f-ed=1'))) {
                        m_5_vUrl = true;
                        for (var yb = 0; yb < catCache.length; yb++) {
                            var m_5_idx = catCache[yb].map(function(e) { return e.url; }).indexOf(m_5_url);
                            if (m_5_idx != -1) {
                                if (catCache[yb][m_5_idx].sounds.length > 0) {
                                    if (m_5_url.startsWith('http://jv.stkr.fr/p/')) {
                                        addImg.innerHTML = '<img height="96" width="96" src="' + m_5_url + '"/>';
                                    } else {
                                        addImg.innerHTML = '<img height="96" width="128" src="' + m_5_url + '"/>';
                                    }
                                    m_5_dSons = catCache[yb][m_5_idx].sounds;
                                    m_5_sonsf = catCache[yb][m_5_idx].sounds;
                                    if (m_5_dSons.length > 0) {
                                        document.getElementById('m_5_sons').value = m_5_dSons.join(" ");
                                    } else {
                                        m_5_dSons = [];
                                        m_5_sonsf = [];
                                        document.getElementById('m_5_sons').value = "";
                                    }
                                    document.getElementById('m_5_sons').disabled = false;
                                    document.getElementById('m_5_sons').placeholder = 'http://* https://*';
                                    var urlsOcc = [];
                                    for (var ya = 0; ya < catCache.length; ya++) {
                                        catCache[ya].reduce(function(n, sticker) {
                                            if (sticker.sounds.length > 0 && sticker.url != m_5_url) {
                                                if (JSON.stringify(sticker.sounds.sort()) == JSON.stringify(m_5_dSons.sort())) {
                                                    urlsOcc.push(sticker.url);
                                                }
                                            }
                                        }, 0);
                                    }
                                    if (urlsOcc.length > 0) {
                                        addImg.innerHTML += '</br>'
                                        var stckrMax = urlsOcc.length;
                                        if (stckrMax > 15) {
                                            stckrMax = 10;
                                        }
                                        var randUsed = [];
                                        for (var o = 0; o < stckrMax; o++) {
                                            var randNb = Math.floor((Math.random() * urlsOcc.length) + 1) - 1;
                                            while (randUsed.indexOf(randNb) != -1) {
                                                randNb = Math.floor((Math.random() * urlsOcc.length) + 1) - 1;
                                            }
                                            if (urlsOcc[randNb].startsWith('http://jv.stkr.fr/p/')) {
                                                addImg.innerHTML += '<img height="48" width="48" src="' + urlsOcc[randNb] + '"/>';
                                            } else {
                                                addImg.innerHTML += '<img height="48" width="64" src="' + urlsOcc[randNb] + '"/>';
                                            }
                                            if ((o+1) % 5 == 0 && (o+1) != stckrMax) {
                                                addImg.innerHTML  += '</br>';
                                            }
                                        }
                                        if (urlsOcc.length - stckrMax > 0) {
                                            addImg.innerHTML += '</br> et ' + (urlsOcc.length - stckrMax) + ' autres stickers';
                                        }
                                    }
                                }
                                m_5_found = true;
                                break;
                            }
                        }
                        if (!(m_5_found)) {
                            m_5_dSons = [];
                            document.getElementById('m_5_sons').value = '';
                        }
                    } else {
                        m_5_dSons = [];
                        document.getElementById('m_5_sons').value = '';
                    }
                    m_5_imgs.appendChild(addImg);
                    if (m_5_sValue != document.getElementById('m_5_sons').value) {
                        m_5_unval = [];
                        var m_5_sons = document.getElementById('m_5_sons').value.replace(/\n/g,' ').replace(/,/g,' ').replace(/;/g,' ').split(' ');
                        var m_5_idx = m_5_sons.indexOf('');
                        while (m_5_idx != -1) {
                            m_5_sons.splice(m_5_idx, 1);
                            m_5_idx = m_5_sons.indexOf('');
                        }
                        m_5_sonsf = uniq_fast(m_5_sons);
                        for (var za = 0; za < m_5_sonsf.length; za++) {
                            var z = m_5_sonsf[za];
                            if (!(m_5_sonsf[za].startsWith('http://') || m_5_sonsf[za].startsWith('https://'))) {
                                m_5_unval.push(m_5_sonsf[za]);
                                m_5_sonsf.splice(za, 1);
                                za--;
                            }
                        }
                    }
                    m_5_verify();
                });
                document.getElementById('m_5_sons').addEventListener('input', function() {
                    m_5_unval = [];
                    var m_5_sons = document.getElementById('m_5_sons').value.replace(/\n/g,' ').replace(/,/g,' ').replace(/;/g,' ').split(' ');
                    var m_5_idx = m_5_sons.indexOf('');
                    while (m_5_idx != -1) {
                        m_5_sons.splice(m_5_idx, 1);
                        m_5_idx = m_5_sons.indexOf('');
                    }
                    m_5_sonsf = uniq_fast(m_5_sons);
                    for (var za = 0; za < m_5_sonsf.length; za++) {
                        var z = m_5_sonsf[za];
                        if (!(m_5_sonsf[za].startsWith('http://') || m_5_sonsf[za].startsWith('https://'))) {
                            m_5_unval.push(m_5_sonsf[za]);
                            m_5_sonsf.splice(za, 1);
                            za--;
                        }
                    }
                    m_5_verify();
                });

                var m_6_vUrl0 = false;
                var m_6_found0 = false;
                var m_6_vUrl1 = false;
                var m_6_found1 = false;
                function m_6_verify() {
                    var m_6_txt = document.getElementById('m_6_txt');
                    if (document.getElementById('m_6_url').value != '' && m_6_vUrl0 && m_6_found0) {
                        document.getElementById('m_6_url').setAttribute('style', 'text-align:center;');
                        document.getElementById('m_6_newurl').setAttribute('style', 'text-align:center;');
                        var msg = '';
                        if (document.getElementById('m_6_url').value == document.getElementById('m_6_newurl').value) {
                            document.getElementById('m_6_newurl').setAttribute('style', 'text-align:center;border:1px solid darkorange;');
                            msg += "<div style='color:darkorange;'><b>L'adresse </b><i>" + document.getElementById('m_6_newurl').value + "</i><b> est identique et sera ignorée!</b></div>";
                            m_6_txt.parentElement.parentElement.removeAttribute("style");
                        } else if (document.getElementById('m_6_newurl').value != '' && !(m_6_vUrl1)) {
                            document.getElementById('m_6_newurl').setAttribute('style', 'text-align:center;border:1px solid darkorange;');
                            msg += "<div style='color:darkorange;'><b>L'adresse </b><i>" + document.getElementById('m_6_newurl').value + "</i><b> n'est pas valide et sera ignorée!</b></div>";
                            m_6_txt.parentElement.parentElement.removeAttribute("style");
                        } else if (document.getElementById('m_6_newurl').value != '' && m_6_found1) {
                            document.getElementById('m_6_newurl').setAttribute('style', 'text-align:center;border:1px solid darkorange;');
                            msg += "<div style='color:darkorange;'><b>L'adresse </b><i>" + document.getElementById('m_6_newurl').value + "</i><b> est déjà intégrée et sera ignorée!</b></div>";
                            m_6_txt.parentElement.parentElement.removeAttribute("style");
                        } else {
                            m_6_txt.parentElement.parentElement.setAttribute("style", "display:none;");
                        }
                        m_6_txt.innerHTML = msg;
                        document.getElementById('envoyerReq').disabled = false;
                    }
                    else {
                        document.getElementById('envoyerReq').disabled = true;
                        var msg = '';
                        m_6_txt.parentElement.parentElement.removeAttribute("style");
                        if (document.getElementById('m_6_url').value == '') {
                            document.getElementById('m_6_url').setAttribute('style', 'text-align:center;border:1px solid red;');
                            msg += "<div style='color:red;'><b>Veuillez saisir l'adresse d'un sticker!</b></div>";
                        } else if (!(m_6_vUrl0)) {
                            document.getElementById('m_6_url').setAttribute('style', 'text-align:center;border:1px solid red;');
                            msg += "<div style='color:red;'><b>L'adresse </b><i>" + document.getElementById('m_6_url').value + "</i><b> n'est pas valide!</b></div>";
                        } else if (!(m_6_found0)) {
                            document.getElementById('m_6_url').setAttribute('style', 'text-align:center;border:1px solid red;');
                            msg += "<div style='color:red;'><b>Le sticker </b><i>" + document.getElementById('m_6_url').value + "</i><b> n'a pas été trouvé!</b></div>";
                        } else {
                            document.getElementById('m_6_url').setAttribute('style', 'text-align:center;');
                        }
                        if (document.getElementById('m_6_newurl').value != '' && (!(m_6_vUrl1) || m_6_found1)) {
                            document.getElementById('m_6_newurl').setAttribute('style', 'text-align:center;border:1px solid darkorange;');
                        } else {
                            document.getElementById('m_6_newurl').setAttribute('style', 'text-align:center;');
                        }
                        m_6_txt.parentElement.parentElement.removeAttribute("style");
                        m_6_txt.innerHTML = msg;
                    }
                }
                document.getElementById('m_6_url').addEventListener('input', function() {
                    m_6_vUrl0 = false;
                    m_6_found0 = false;
                    var m_6_url = document.getElementById('m_6_url').value;
                    var m_6_img = document.getElementById('m_6_img');
                    if (m_6_img.firstChild) {
                        m_6_img.removeChild(m_6_img.firstChild);
                    }
                    var addImg = document.createElement('div');
                    if (((m_6_url.startsWith('http://') || m_6_url.startsWith('https://')) && m_6_url.includes('image.noelshack.com') && (m_6_url.endsWith('.png') || m_6_url.endsWith('.gif') || m_6_url.endsWith('.jpg') || m_6_url.endsWith('.jpeg'))) || (m_6_url.startsWith('http://jv.stkr.fr/p/') && m_6_url.endsWith('?f-ed=1'))) {
                        m_6_vUrl0 = true;
                        for (var yb = 0; yb < catCache.length; yb++) {
                            var m_6_idx = catCache[yb].map(function(e) { return e.url; }).indexOf(m_6_url);
                            if (m_6_idx != -1) {
                                var m_6_newurl = document.getElementById('m_6_newurl').value;
                                if (m_6_url.startsWith('http://jv.stkr.fr/p/')) {
                                    addImg.innerHTML = '<img height="96" width="96" src="' + m_6_url + '"/>';
                                    if (m_6_vUrl1 && !(m_6_found1)) {
                                        if (m_6_newurl.startsWith('http://jv.stkr.fr/p/')) {
                                            addImg.innerHTML += '<img height="96" width="96" src="https://image.noelshack.com/fichiers/2017/21/1495640890-fl.png"/><img height="96" width="96" src="' + m_6_newurl + '"/>';
                                        } else {
                                            addImg.innerHTML += '<img height="96" width="96" src="https://image.noelshack.com/fichiers/2017/21/1495640890-fl.png"/><img height="96" width="128" src="' + m_6_newurl + '"/>';
                                        }
                                    }
                                } else {
                                    addImg.innerHTML = '<img height="96" width="128" src="' + m_6_url + '"/>';
                                    if (m_6_vUrl1 && !(m_6_found1)) {
                                        if (m_6_newurl.startsWith('http://jv.stkr.fr/p/')) {
                                            addImg.innerHTML += '<img height="96" width="96" src="https://image.noelshack.com/fichiers/2017/21/1495640890-fl.png"/><img height="96" width="96" src="' + m_6_newurl + '"/>';
                                        } else {
                                            addImg.innerHTML += '<img height="96" width="96" src="https://image.noelshack.com/fichiers/2017/21/1495640890-fl.png"/><img height="96" width="128" src="' + m_6_newurl + '"/>';
                                        }
                                    }
                                }
                                var m_6_aTags = catCache[yb][m_6_idx].tags;
                                var m_6_aSons = catCache[yb][m_6_idx].sounds;
                                document.getElementById('m_6_cat').innerHTML = catRealNames[yb];
                                if (m_6_aTags.length > 0) {
                                    document.getElementById('m_6_tags').innerHTML = m_6_aTags.join(", ");
                                } else {
                                    document.getElementById('m_6_tags').innerHTML = "";
                                }
                                if (m_6_aSons.length > 0) {
                                    document.getElementById('m_6_sons').innerHTML = m_6_aSons.join(", ");
                                } else {
                                    document.getElementById('m_6_sons').innerHTML = "";
                                }
                                m_6_found0 = true;
                                break;
                            }
                        }
                        if (!(m_6_found0)) {
                            document.getElementById('m_6_cat').innerHTML = "";
                            document.getElementById('m_6_tags').innerHTML = "";
                            document.getElementById('m_6_sons').innerHTML = "";
                        }
                    } else {
                        document.getElementById('m_6_cat').innerHTML = "";
                        document.getElementById('m_6_tags').innerHTML = "";
                        document.getElementById('m_6_sons').innerHTML = "";
                    }
                    m_6_img.appendChild(addImg);
                    m_6_verify();
                });
                document.getElementById('m_6_newurl').addEventListener('input', function() {
                    m_6_vUrl1 = false;
                    m_6_found1 = false;
                    var m_6_newurl = document.getElementById('m_6_newurl').value;
                    var m_6_img = document.getElementById('m_6_img');
                    if (m_6_img.firstChild) {
                        m_6_img.removeChild(m_6_img.firstChild);
                    }
                    var addImg = document.createElement('div');
                    if (((m_6_newurl.startsWith('http://') || m_6_newurl.startsWith('https://')) && m_6_newurl.includes('image.noelshack.com') && (m_6_newurl.endsWith('.png') || m_6_newurl.endsWith('.gif') || m_6_newurl.endsWith('.jpg') || m_6_newurl.endsWith('.jpeg'))) || (m_6_newurl.startsWith('http://jv.stkr.fr/p/') && m_6_newurl.endsWith('?f-ed=1'))) {
                        m_6_vUrl1 = true;
                        for (var yb = 0; yb < catCache.length; yb++) {
                            var m_6_idx = catCache[yb].map(function(e) { return e.url; }).indexOf(m_6_newurl);
                            if (m_6_idx != -1) {
                                m_6_found1 = true;
                                break;
                            }
                        }
                    }
                    if (m_6_vUrl0 && m_6_found0) {
                        var m_6_url = document.getElementById('m_6_url').value;
                        if (m_6_url.startsWith('http://jv.stkr.fr/p/')) {
                            addImg.innerHTML = '<img height="96" width="96" src="' + m_6_url + '"/>';
                            if (m_6_vUrl1 && !(m_6_found1)) {
                                if (m_6_newurl.startsWith('http://jv.stkr.fr/p/')) {
                                    addImg.innerHTML += '<img height="96" width="96" src="https://image.noelshack.com/fichiers/2017/21/1495640890-fl.png"/><img height="96" width="96" src="' + m_6_newurl + '"/>';
                                } else {
                                    addImg.innerHTML += '<img height="96" width="96" src="https://image.noelshack.com/fichiers/2017/21/1495640890-fl.png"/><img height="96" width="128" src="' + m_6_newurl + '"/>';
                                }
                            }
                        } else {
                            addImg.innerHTML = '<img height="96" width="128" src="' + m_6_url + '"/>';
                            if (m_6_vUrl1 && !(m_6_found1)) {
                                if (m_6_newurl.startsWith('http://jv.stkr.fr/p/')) {
                                    addImg.innerHTML += '<img height="96" width="96" src="https://image.noelshack.com/fichiers/2017/21/1495640890-fl.png"/><img height="96" width="96" src="' + m_6_newurl + '"/>';
                                } else {
                                    addImg.innerHTML += '<img height="96" width="96" src="https://image.noelshack.com/fichiers/2017/21/1495640890-fl.png"/><img height="96" width="128" src="' + m_6_newurl + '"/>';
                                }
                            }
                        }
                    }
                    m_6_img.appendChild(addImg);
                    m_6_verify();
                });

                var m_7_vUrl0 = false;
                var m_7_found0 = false;
                var m_7_vUrl1 = false;
                var m_7_found1 = false;
                function m_7_verify() {
                    var m_7_txt = document.getElementById('m_7_txt');
                    if (document.getElementById('m_7_url').value != '' && document.getElementById('m_7_newurl').value != '' && document.getElementById('m_7_url').value != document.getElementById('m_7_newurl').value && m_7_vUrl0 && m_7_found0 && m_7_vUrl1 && m_7_found1) {
                        m_7_txt.parentElement.parentElement.setAttribute("style", "display:none;");
                        document.getElementById('m_7_url').setAttribute('style', 'text-align:center;');
                        document.getElementById('m_7_newurl').setAttribute('style', 'text-align:center;');
                        document.getElementById('envoyerReq').disabled = false;
                    }
                    else {
                        document.getElementById('envoyerReq').disabled = true;
                        var msg = '';
                        m_7_txt.parentElement.parentElement.removeAttribute("style");
                        if (document.getElementById('m_7_url').value == '') {
                            document.getElementById('m_7_url').setAttribute('style', 'text-align:center;border:1px solid red;');
                            msg += "<div style='color:red;'><b>Veuillez saisir l'adresse d'un sticker!</b></div>";
                        } else if (!(m_7_vUrl0)) {
                            document.getElementById('m_7_url').setAttribute('style', 'text-align:center;border:1px solid red;');
                            msg += "<div style='color:red;'><b>L'adresse </b><i>" + document.getElementById('m_7_url').value + "</i><b> n'est pas valide!</b></div>";
                        } else if (!(m_7_found0)) {
                            document.getElementById('m_7_url').setAttribute('style', 'text-align:center;border:1px solid red;');
                            msg += "<div style='color:red;'><b>Le sticker </b><i>" + document.getElementById('m_7_url').value + "</i><b> n'a pas été trouvé!</b></div>";
                        } else {
                            document.getElementById('m_7_url').setAttribute('style', 'text-align:center;');
                        }
                        if (document.getElementById('m_7_url').value == document.getElementById('m_7_newurl').value) {
                            document.getElementById('m_7_newurl').setAttribute('style', 'text-align:center;border:1px solid red;');
                            if (msg == '') {
                                msg += "<div style='color:red;'><b>L'adresse </b><i>" + document.getElementById('m_7_newurl').value + "</i><b> est identique!</b></div>";
                            }
                        } else if (document.getElementById('m_7_newurl').value == '') {
                            document.getElementById('m_7_newurl').setAttribute('style', 'text-align:center;border:1px solid red;');
                            if (msg == '') {
                                msg += "<div style='color:red;'><b>Veuillez saisir l'adresse du doublon!</b></div>";
                            }
                        } else if (!(m_7_vUrl1)) {
                            document.getElementById('m_7_newurl').setAttribute('style', 'text-align:center;border:1px solid red;');
                            if (msg == '') {
                                msg += "<div style='color:red;'><b>L'adresse du doublon </b><i>" + document.getElementById('m_7_newurl').value + "</i><b> n'est pas valide!</b></div>";
                            }
                        } else if (!(m_7_found1)) {
                            document.getElementById('m_7_newurl').setAttribute('style', 'text-align:center;border:1px solid red;');
                            if (msg == '') {
                                msg += "<div style='color:red;'><b>Le doublon </b><i>" + document.getElementById('m_7_newurl').value + "</i><b> n'a pas été trouvé!</b></div>";
                            }
                        } else {
                            document.getElementById('m_7_newurl').setAttribute('style', 'text-align:center;');
                        }
                        m_7_txt.parentElement.parentElement.removeAttribute("style");
                        m_7_txt.innerHTML = msg;
                    }
                }
                document.getElementById('m_7_url').addEventListener('input', function() {
                    m_7_vUrl0 = false;
                    m_7_found0 = false;
                    var m_7_url = document.getElementById('m_7_url').value;
                    var m_7_img = document.getElementById('m_7_img');
                    if (m_7_img.firstChild) {
                        m_7_img.removeChild(m_7_img.firstChild);
                    }
                    var addImg = document.createElement('div');
                    if (((m_7_url.startsWith('http://') || m_7_url.startsWith('https://')) && m_7_url.includes('image.noelshack.com') && (m_7_url.endsWith('.png') || m_7_url.endsWith('.gif') || m_7_url.endsWith('.jpg') || m_7_url.endsWith('.jpeg'))) || (m_7_url.startsWith('http://jv.stkr.fr/p/') && m_7_url.endsWith('?f-ed=1'))) {
                        m_7_vUrl0 = true;
                        for (var yb = 0; yb < catCache.length; yb++) {
                            var m_7_idx = catCache[yb].map(function(e) { return e.url; }).indexOf(m_7_url);
                            if (m_7_idx != -1) {
                                var m_7_newurl = document.getElementById('m_7_newurl').value;
                                if (m_7_url.startsWith('http://jv.stkr.fr/p/')) {
                                    addImg.innerHTML = '<img height="96" width="96" src="' + m_7_url + '"/>';
                                    if (m_7_vUrl1 && m_7_found1 && m_7_url != m_7_newurl) {
                                        if (m_7_newurl.startsWith('http://jv.stkr.fr/p/')) {
                                            addImg.innerHTML += '<img height="96" width="96" src="https://image.noelshack.com/fichiers/2017/21/1495700028-eq.png"/><img height="96" width="96" src="' + m_7_newurl + '"/>';
                                        } else {
                                            addImg.innerHTML += '<img height="96" width="96" src="https://image.noelshack.com/fichiers/2017/21/1495700028-eq.png"/><img height="96" width="128" src="' + m_7_newurl + '"/>';
                                        }
                                    }
                                } else {
                                    addImg.innerHTML = '<img height="96" width="128" src="' + m_7_url + '"/>';
                                    if (m_7_vUrl1 && m_7_found1 && m_7_url != m_7_newurl) {
                                        if (m_7_newurl.startsWith('http://jv.stkr.fr/p/')) {
                                            addImg.innerHTML += '<img height="96" width="96" src="https://image.noelshack.com/fichiers/2017/21/1495700028-eq.png"/><img height="96" width="96" src="' + m_7_newurl + '"/>';
                                        } else {
                                            addImg.innerHTML += '<img height="96" width="96" src="https://image.noelshack.com/fichiers/2017/21/1495700028-eq.png"/><img height="96" width="128" src="' + m_7_newurl + '"/>';
                                        }
                                    }
                                }
                                m_7_found0 = true;
                                break;
                            }
                        }
                    }
                    m_7_img.appendChild(addImg);
                    m_7_verify();
                });
                document.getElementById('m_7_newurl').addEventListener('input', function() {
                    m_7_vUrl1 = false;
                    m_7_found1 = false;
                    var m_7_newurl = document.getElementById('m_7_newurl').value;
                    var m_7_img = document.getElementById('m_7_img');
                    if (m_7_img.firstChild) {
                        m_7_img.removeChild(m_7_img.firstChild);
                    }
                    var addImg = document.createElement('div');
                    if (((m_7_newurl.startsWith('http://') || m_7_newurl.startsWith('https://')) && m_7_newurl.includes('image.noelshack.com') && (m_7_newurl.endsWith('.png') || m_7_newurl.endsWith('.gif') || m_7_newurl.endsWith('.jpg') || m_7_newurl.endsWith('.jpeg'))) || (m_7_newurl.startsWith('http://jv.stkr.fr/p/') && m_7_newurl.endsWith('?f-ed=1'))) {
                        m_7_vUrl1 = true;
                        for (var yb = 0; yb < catCache.length; yb++) {
                            var m_7_idx = catCache[yb].map(function(e) { return e.url; }).indexOf(m_7_newurl);
                            if (m_7_idx != -1) {
                                m_7_found1 = true;
                                break;
                            }
                        }
                    }
                    if (m_7_vUrl0 && m_7_found0) {
                        var m_7_url = document.getElementById('m_7_url').value;
                        if (m_7_url.startsWith('http://jv.stkr.fr/p/')) {
                            addImg.innerHTML = '<img height="96" width="96" src="' + m_7_url + '"/>';
                            if (m_7_vUrl1 && m_7_found1 && m_7_url != m_7_newurl) {
                                if (m_7_newurl.startsWith('http://jv.stkr.fr/p/')) {
                                    addImg.innerHTML += '<img height="96" width="96" src="https://image.noelshack.com/fichiers/2017/21/1495700028-eq.png"/><img height="96" width="96" src="' + m_7_newurl + '"/>';
                                } else {
                                    addImg.innerHTML += '<img height="96" width="96" src="https://image.noelshack.com/fichiers/2017/21/1495700028-eq.png"/><img height="96" width="128" src="' + m_7_newurl + '"/>';
                                }
                            }
                        } else {
                            addImg.innerHTML = '<img height="96" width="128" src="' + m_7_url + '"/>';
                            if (m_7_vUrl1 && m_7_found1 && m_7_url != m_7_newurl) {
                                if (m_7_newurl.startsWith('http://jv.stkr.fr/p/')) {
                                    addImg.innerHTML += '<img height="96" width="96" src="https://image.noelshack.com/fichiers/2017/21/1495700028-eq.png"/><img height="96" width="96" src="' + m_7_newurl + '"/>';
                                } else {
                                    addImg.innerHTML += '<img height="96" width="96" src="https://image.noelshack.com/fichiers/2017/21/1495700028-eq.png"/><img height="96" width="128" src="' + m_7_newurl + '"/>';
                                }
                            }
                        }
                    }
                    m_7_img.appendChild(addImg);
                    m_7_verify();
                });

                var m_8_vUrl = false;
                var m_8_found = false;
                function m_8_verify() {
                    var m_8_txt = document.getElementById('m_8_txt');
                    if (document.getElementById('m_8_url').value != '' && m_8_vUrl && m_8_found) {
                        m_8_txt.parentElement.parentElement.setAttribute("style", "display:none;");
                        document.getElementById('m_8_url').setAttribute('style', 'text-align:center;');
                        document.getElementById('envoyerReq').disabled = false;
                    }
                    else {
                        document.getElementById('envoyerReq').disabled = true;
                        var msg = '';
                        m_8_txt.parentElement.parentElement.removeAttribute("style");
                        if (document.getElementById('m_8_url').value == '') {
                            document.getElementById('m_8_url').setAttribute('style', 'text-align:center;border:1px solid red;');
                            msg += "<div style='color:red;'><b>Veuillez saisir l'adresse d'un sticker!</b></div>";
                        } else if (!(m_8_vUrl)) {
                            document.getElementById('m_8_url').setAttribute('style', 'text-align:center;border:1px solid red;');
                            msg += "<div style='color:red;'><b>L'adresse </b><i>" + document.getElementById('m_8_url').value + "</i><b> n'est pas valide!</b></div>";
                        } else if (!(m_8_found)) {
                            document.getElementById('m_8_url').setAttribute('style', 'text-align:center;border:1px solid red;');
                            msg += "<div style='color:red;'><b>Le sticker </b><i>" + document.getElementById('m_8_url').value + "</i><b> n'a pas été trouvé!</b></div>";
                        } else {
                            document.getElementById('m_8_url').setAttribute('style', 'text-align:center;');
                        }
                        m_8_txt.parentElement.parentElement.removeAttribute("style");
                        m_8_txt.innerHTML = msg;
                    }
                }
                document.getElementById('m_8_url').addEventListener('input', function() {
                    m_8_vUrl = false;
                    m_8_found = false;
                    var m_8_url = document.getElementById('m_8_url').value;
                    var m_8_img = document.getElementById('m_8_img');
                    if (m_8_img.firstChild) {
                        m_8_img.removeChild(m_8_img.firstChild);
                    }
                    var addImg = document.createElement('div');
                    if (((m_8_url.startsWith('http://') || m_8_url.startsWith('https://')) && m_8_url.includes('image.noelshack.com') && (m_8_url.endsWith('.png') || m_8_url.endsWith('.gif') || m_8_url.endsWith('.jpg') || m_8_url.endsWith('.jpeg'))) || (m_8_url.startsWith('http://jv.stkr.fr/p/') && m_8_url.endsWith('?f-ed=1'))) {
                        m_8_vUrl = true;
                        for (var yb = 0; yb < catCache.length; yb++) {
                            var m_8_idx = catCache[yb].map(function(e) { return e.url; }).indexOf(m_8_url);
                            if (m_8_idx != -1) {
                                if (m_8_url.startsWith('http://jv.stkr.fr/p/')) {
                                    addImg.innerHTML = '<img height="96" width="96" src="' + m_8_url + '"/>';
                                } else {
                                    addImg.innerHTML = '<img height="96" width="128" src="' + m_8_url + '"/>';
                                }
                                m_8_found = true;
                                break;
                            }
                        }
                    }
                    m_8_img.appendChild(addImg);
                    m_8_verify();
                });

                document.getElementById('m_sel').addEventListener('change', function() {
                    var m_sel = document.getElementById('m_sel').selectedIndex;
                    document.getElementById('m_0').setAttribute("style", "display:none;");
                    document.getElementById('m_1').setAttribute("style", "display:none;");
                    document.getElementById('m_2').setAttribute("style", "display:none;");
                    document.getElementById('m_3').setAttribute("style", "display:none;");
                    document.getElementById('m_4').setAttribute("style", "display:none;");
                    document.getElementById('m_5').setAttribute("style", "display:none;");
                    document.getElementById('m_6').setAttribute("style", "display:none;");
                    document.getElementById('m_7').setAttribute("style", "display:none;");
                    document.getElementById('m_8').setAttribute("style", "display:none;");
                    if (m_sel == 0) {
                        m_0_verify();
                        document.getElementById('m_0').removeAttribute("style");
                    } else if (m_sel == 1) {
                        m_1_verify();
                        document.getElementById('m_1').removeAttribute("style");
                    } else if (m_sel == 2) {
                        m_2_verify();
                        document.getElementById('m_2').removeAttribute("style");
                    } else if (m_sel == 3) {
                        m_3_verify();
                        document.getElementById('m_3').removeAttribute("style");
                    } else if (m_sel == 4) {
                        m_4_verify();
                        document.getElementById('m_4').removeAttribute("style");
                    } else if (m_sel == 5) {
                        m_5_verify();
                        document.getElementById('m_5').removeAttribute("style");
                    } else if (m_sel == 6) {
                        m_6_verify();
                        document.getElementById('m_6').removeAttribute("style");
                    } else if (m_sel == 7) {
                        m_7_verify();
                        document.getElementById('m_7').removeAttribute("style");
                    } else if (m_sel == 8) {
                        m_8_verify();
                        document.getElementById('m_8').removeAttribute("style");
                    }
                });

                var req_idle = true;
                document.getElementById('envoyerReq').addEventListener('click', function() {
                    if (!(document.getElementById('envoyerReq').disabled) && req_idle) {
                        req_idle = false;
                        var m_sel = document.getElementById('m_sel').selectedIndex;
                        if (m_sel == 0) {
                            var m_0_cat = document.getElementById('m_0_cat');
                            var m_0_tags = document.getElementById('m_0_tags').value.replace(/\n/g,' ').replace(/,/g,' ').replace(/;/g,' ').latinize().toLowerCase().split(' ');
                            var m_0_idx = m_0_tags.indexOf('');
                            while (m_0_idx != -1) {
                                m_0_tags.splice(m_0_idx, 1);
                                m_0_idx = m_0_tags.indexOf('');
                            }
                            m_0_tags = uniq_fast(m_0_tags);
                            var data_urls = m_0_arrf.slice(0);
                            var data_cat = m_0_cat.options[m_0_cat.selectedIndex].value;
                            var data_tags = m_0_tags.join(' ');
                            var data_sounds = m_0_sonsf.join(' ');
                            for (var h = 0; h < data_urls.length; h++) {
                                GM_xmlhttpRequest({
                                    method: "POST",
                                    url: "http://jvcsticker.esy.es/req/req_stickers.php",
                                    data: "url=" + data_urls[h] + "&categorie=" + data_cat + "&tags=" + data_tags + "&sounds=" + data_sounds,
                                    headers: {
                                        "Content-Type": "application/x-www-form-urlencoded"
                                    },
                                    onload: function(response) {
                                        document.getElementById('annuler').click();
                                        document.getElementById('envoyerReq').disabled = true;
                                        m_0_arrf = [];
                                        m_0_sonsf = [];
                                        m_0_unval0 = [];
                                        m_0_unval1 = [];
                                        m_0_unval2 = [];
                                        var m_0_imgs = document.getElementById('m_0_imgs');
                                        if (m_0_imgs.firstChild) {
                                            m_0_imgs.removeChild(m_0_imgs.firstChild);
                                        }
                                        var addImgs = document.createElement('div');
                                        m_0_imgs.appendChild(addImgs);
                                        document.getElementById('m_0_urls').value = "";
                                        document.getElementById('m_0_urls').setAttribute('style', 'text-align:center;border:1px solid red;');
                                        document.getElementById('m_0_cat').selectedIndex = document.getElementById('m_0_cat').options.length - 1;
                                        document.getElementById('m_0_tags').value = "";
                                        document.getElementById('m_0_tags').setAttribute('style', 'text-align:center;border:1px solid red;');
                                        document.getElementById('m_0_sons').value = "";
                                        document.getElementById('m_0_txt').parentElement.parentElement.removeAttribute("style");
                                        document.getElementById('m_0_txt').innerHTML = "<div style='color:red;'><b>Veuillez saisir au moins une adresse!</b></div>";
                                    }});
                            }
                            setTimeout(function() {req_idle = true}, 1000);
                        }
                        else if (m_sel == 1) {
                            var data_name = document.getElementById('m_1_name').value;
                            var data_icon = document.getElementById('m_1_icone').options[document.getElementById('m_1_icone').selectedIndex].value;
                            GM_xmlhttpRequest({
                                method: "POST",
                                url: "http://jvcsticker.esy.es/req/req_categories.php",
                                data: "name=" + data_name + "&icon=" + data_icon,
                                headers: {
                                    "Content-Type": "application/x-www-form-urlencoded"
                                },
                                onload: function(response) {
                                    document.getElementById('annuler').click();
                                    document.getElementById('envoyerReq').disabled = true;
                                    m_1_result = false;
                                    document.getElementById('m_1_name').value = "";
                                    document.getElementById('m_1_name').setAttribute('style', 'text-align:center;border:1px solid red;');
                                    document.getElementById('m_1_icone').selectedIndex = 0;
                                    document.getElementById('m_1_txt').parentElement.parentElement.removeAttribute("style");
                                    document.getElementById('m_1_txt').innerHTML = "<div style='color:red;'><b>Veuillez saisir le nom de la catégorie</b></div>";
                                }});
                            setTimeout(function() {req_idle = true}, 1000);
                        }
                        else if (m_sel == 2) {
                            var m_2_cat = document.getElementById('m_2_cat');
                            var data_urls = m_2_arrf.slice(0);
                            var data_cat = m_2_cat.options[m_2_cat.selectedIndex].value;
                            for (var h = 0; h < data_urls.length; h++) {
                                GM_xmlhttpRequest({
                                    method: "POST",
                                    url: "http://jvcsticker.esy.es/req/req_categorie.php",
                                    data: "url=" + data_urls[h] + "&categorie=" + data_cat,
                                    headers: {
                                        "Content-Type": "application/x-www-form-urlencoded"
                                    },
                                    onload: function(response) {
                                        document.getElementById('annuler').click();
                                        document.getElementById('envoyerReq').disabled = true;
                                        m_2_arrf = [];
                                        m_2_unval0 = [];
                                        m_2_unval1 = [];
                                        m_2_unval2 = [];
                                        var m_2_imgs = document.getElementById('m_2_imgs');
                                        if (m_2_imgs.firstChild) {
                                            m_2_imgs.removeChild(m_2_imgs.firstChild);
                                        }
                                        var addImgs = document.createElement('div');
                                        m_2_imgs.appendChild(addImgs);
                                        document.getElementById('m_2_urls').value = "";
                                        document.getElementById('m_2_urls').setAttribute('style', 'text-align:center;border:1px solid red;');
                                        document.getElementById('m_2_cat').selectedIndex = 0;
                                        document.getElementById('m_2_txt').parentElement.parentElement.removeAttribute("style");
                                        document.getElementById('m_2_txt').innerHTML = "<div style='color:red;'><b>Veuillez saisir l'adresse d'au moins un sticker!</b></div>";
                                    }});
                            }
                            setTimeout(function() {req_idle = true}, 1000);
                        }
                        else if (m_sel == 3) {
                            var data_url = document.getElementById('m_3_url').value;
                            var data_tags = m_3_tags.join(' ');
                            GM_xmlhttpRequest({
                                method: "POST",
                                url: "http://jvcsticker.esy.es/req/req_tags.php",
                                data: "url=" + data_url + "&tags=" + data_tags,
                                headers: {
                                    "Content-Type": "application/x-www-form-urlencoded"
                                },
                                onload: function(response) {
                                    document.getElementById('annuler').click();
                                    document.getElementById('envoyerReq').disabled = true;
                                    m_3_vUrl = false;
                                    m_3_found = false;
                                    m_3_tags = [];
                                    m_3_dTags = [];
                                    var m_3_img = document.getElementById('m_3_img');
                                    if (m_3_img.firstChild) {
                                        m_3_img.removeChild(m_3_img.firstChild);
                                    }
                                    var addImg = document.createElement('div');
                                    m_3_img.appendChild(addImg);
                                    document.getElementById('m_3_url').value = "";
                                    document.getElementById('m_3_url').setAttribute('style', 'text-align:center;border:1px solid red;');
                                    document.getElementById('m_3_tags').value = "";
                                    document.getElementById('m_3_tags').setAttribute('style', 'text-align:center;border:1px solid red;');
                                    document.getElementById('m_3_tags').placeholder = "Indiquez d'abord l'adresse du sticker";
                                    document.getElementById('m_3_tags').disabled = true;
                                    document.getElementById('m_3_txt').parentElement.parentElement.removeAttribute("style");
                                    document.getElementById('m_3_txt').innerHTML = "<div style='color:red;'><b>Veuillez saisir l'adresse d'un sticker!</b></div>";
                                }});
                            setTimeout(function() {req_idle = true}, 1000);
                        }
                        else if (m_sel == 4) {
                            var data_url = document.getElementById('m_4_url').value;
                            var data_reference_url = ''
                            var data_sounds = m_4_sonsf.join(' ');
                            GM_xmlhttpRequest({
                                method: "POST",
                                url: "http://jvcsticker.esy.es/req/req_sounds.php",
                                data: "url=" + data_url + "&sounds=" + data_sounds + "&reference_url=" + data_reference_url,
                                headers: {
                                    "Content-Type": "application/x-www-form-urlencoded"
                                },
                                onload: function(response) {
                                    document.getElementById('annuler').click();
                                    document.getElementById('envoyerReq').disabled = true;
                                    m_4_vUrl = false;
                                    m_4_found = false;
                                    m_4_dSons = [];
                                    m_4_sonsf = [];
                                    m_4_unval = [];
                                    var m_4_img = document.getElementById('m_4_img');
                                    if (m_4_img.firstChild) {
                                        m_4_img.removeChild(m_4_img.firstChild);
                                    }
                                    var addImg = document.createElement('div');
                                    m_4_img.appendChild(addImg);
                                    document.getElementById('m_4_url').value = "";
                                    document.getElementById('m_4_url').setAttribute('style', 'text-align:center;border:1px solid red;');
                                    document.getElementById('m_4_sons').value = "";
                                    document.getElementById('m_4_sons').setAttribute('style', 'text-align:center;border:1px solid red;');
                                    document.getElementById('m_4_sons').placeholder = "Indiquez d'abord l'adresse du sticker";
                                    document.getElementById('m_4_sons').disabled = true;
                                    document.getElementById('m_4_txt').parentElement.parentElement.removeAttribute("style");
                                    document.getElementById('m_4_txt').innerHTML = "<div style='color:red;'><b>Veuillez saisir l'adresse d'un sticker!</b></div>";
                                }});
                            setTimeout(function() {req_idle = true}, 1000);
                        }
                        else if (m_sel == 5) {
                            var data_url = '';
                            var data_reference_url = document.getElementById('m_5_url').value;
                            var data_sounds = m_5_sonsf.join(' ');
                            GM_xmlhttpRequest({
                                method: "POST",
                                url: "http://jvcsticker.esy.es/req/req_sounds.php",
                                data: "url=" + data_url + "&reference_url=" + data_reference_url + "&sounds=" + data_sounds,
                                headers: {
                                    "Content-Type": "application/x-www-form-urlencoded"
                                },
                                onload: function(response) {
                                    document.getElementById('annuler').click();
                                    document.getElementById('envoyerReq').disabled = true;
                                    m_5_vUrl = false;
                                    m_5_found = false;
                                    m_5_dSons = [];
                                    m_5_sonsf = [];
                                    m_5_unval = [];
                                    var m_5_imgs = document.getElementById('m_5_imgs');
                                    if (m_5_imgs.firstChild) {
                                        m_5_imgs.removeChild(m_5_imgs.firstChild);
                                    }
                                    var addImgs = document.createElement('div');
                                    m_5_imgs.appendChild(addImgs);
                                    document.getElementById('m_5_url').value = "";
                                    document.getElementById('m_5_url').setAttribute('style', 'text-align:center;border:1px solid red;');
                                    document.getElementById('m_5_sons').value = "";
                                    document.getElementById('m_5_sons').setAttribute('style', 'text-align:center;border:1px solid red;');
                                    document.getElementById('m_5_sons').placeholder = "Indiquez d'abord l'adresse d'un sticker du groupe";
                                    document.getElementById('m_5_sons').disabled = true;
                                    document.getElementById('m_5_txt').parentElement.parentElement.removeAttribute("style");
                                    document.getElementById('m_5_txt').innerHTML = "<div style='color:red;'><b>Veuillez saisir l'adresse d'un sticker d'un groupe!</b></div>";
                                }});
                            setTimeout(function() {req_idle = true}, 1000);
                        }
                        else if (m_sel == 6) {
                            var data_url = document.getElementById('m_6_url').value;
                            var data_reason = "Lien mort";
                            var data_new_url = "";
                            if (m_6_vUrl1 && !(m_6_found1)) {
                                data_new_url = document.getElementById('m_6_newurl').value;
                            }
                            GM_xmlhttpRequest({
                                method: "POST",
                                url: "http://jvcsticker.esy.es/req/req_delete.php",
                                data: "url=" + data_url + "&reason=" + data_reason + "&new_url=" + data_new_url,
                                headers: {
                                    "Content-Type": "application/x-www-form-urlencoded"
                                },
                                onload: function(response) {
                                    document.getElementById('annuler').click();
                                    document.getElementById('envoyerReq').disabled = true;
                                    m_6_vUrl0 = false;
                                    m_6_found0 = false;
                                    m_6_vUrl1 = false;
                                    m_6_found1 = false;
                                    var m_6_img = document.getElementById('m_6_img');
                                    if (m_6_img.firstChild) {
                                        m_6_img.removeChild(m_6_img.firstChild);
                                    }
                                    var addImg = document.createElement('div');
                                    m_6_img.appendChild(addImg);
                                    document.getElementById('m_6_url').value = "";
                                    document.getElementById('m_6_url').setAttribute('style', 'text-align:center;border:1px solid red;');
                                    document.getElementById('m_6_cat').innerHTML = "";
                                    document.getElementById('m_6_tags').innerHTML = "";
                                    document.getElementById('m_6_sons').innerHTML = "";
                                    document.getElementById('m_6_newurl').value = "";
                                    document.getElementById('m_6_newurl').setAttribute('style', 'text-align:center;');
                                    document.getElementById('m_6_txt').parentElement.parentElement.removeAttribute("style");
                                    document.getElementById('m_6_txt').innerHTML = "<div style='color:red;'><b>Veuillez saisir l'adresse d'un sticker!</b></div>";
                                }});
                            setTimeout(function() {req_idle = true}, 1000);
                        }
                        else if (m_sel == 7) {
                            var data_url = document.getElementById('m_7_url').value;
                            var data_reason = "Doublon";
                            var data_new_url = document.getElementById('m_7_newurl').value;
                            GM_xmlhttpRequest({
                                method: "POST",
                                url: "http://jvcsticker.esy.es/req/req_delete.php",
                                data: "url=" + data_url + "&reason=" + data_reason + "&new_url=" + data_new_url,
                                headers: {
                                    "Content-Type": "application/x-www-form-urlencoded"
                                },
                                onload: function(response) {
                                    document.getElementById('annuler').click();
                                    document.getElementById('envoyerReq').disabled = true;
                                    m_7_vUrl0 = false;
                                    m_7_found0 = false;
                                    m_7_vUrl1 = false;
                                    m_7_found1 = false;
                                    var m_7_img = document.getElementById('m_7_img');
                                    if (m_7_img.firstChild) {
                                        m_7_img.removeChild(m_7_img.firstChild);
                                    }
                                    var addImg = document.createElement('div');
                                    m_7_img.appendChild(addImg);
                                    document.getElementById('m_7_url').value = "";
                                    document.getElementById('m_7_url').setAttribute('style', 'text-align:center;border:1px solid red;');
                                    document.getElementById('m_7_newurl').value = "";
                                    document.getElementById('m_7_newurl').setAttribute('style', 'text-align:center;border:1px solid red;');
                                    document.getElementById('m_7_txt').parentElement.parentElement.removeAttribute("style");
                                    document.getElementById('m_7_txt').innerHTML = "<div style='color:red;'><b>Veuillez saisir l'adresse d'un sticker!</b></div>";
                                }});
                            setTimeout(function() {req_idle = true}, 1000);
                        }
                        else if (m_sel == 8) {
                            var data_url = document.getElementById('m_8_url').value;
                            var data_reason = document.getElementById('m_8_motif').options[document.getElementById('m_8_motif').selectedIndex].value;
                            var data_new_url = "";
                            GM_xmlhttpRequest({
                                method: "POST",
                                url: "http://jvcsticker.esy.es/req/req_delete.php",
                                data: "url=" + data_url + "&reason=" + data_reason + "&new_url=" + data_new_url,
                                headers: {
                                    "Content-Type": "application/x-www-form-urlencoded"
                                },
                                onload: function(response) {
                                    document.getElementById('annuler').click();
                                    document.getElementById('envoyerReq').disabled = true;
                                    m_8_vUrl = false;
                                    m_8_found = false;
                                    var m_8_img = document.getElementById('m_8_img');
                                    if (m_8_img.firstChild) {
                                        m_8_img.removeChild(m_8_img.firstChild);
                                    }
                                    var addImg = document.createElement('div');
                                    m_8_img.appendChild(addImg);
                                    document.getElementById('m_8_url').value = "";
                                    document.getElementById('m_8_url').setAttribute('style', 'text-align:center;border:1px solid red;');
                                    document.getElementById('m_8_motif').selectedIndex = document.getElementById('m_8_motif').options.length - 1;
                                    document.getElementById('m_8_txt').parentElement.parentElement.removeAttribute("style");
                                    document.getElementById('m_8_txt').innerHTML = "<div style='color:red;'><b>Veuillez saisir l'adresse d'un sticker!</b></div>";
                                }});
                            setTimeout(function() {req_idle = true}, 1000);
                        }
                    }
                });


                var shownListS = JSON.parse(GM_getValue('catToShow'));
                var listeBanS = JSON.parse(GM_getValue('stickersBanListe'));
                var MAJManuelle = false;
                var addOptions = document.createElement('div');
                addOptions.innerHTML = ' <div id="optionsModal" style="display:none;"><div class="modal-header"><h2 class="modal-title" style="color:#d13321; text-align:center;"><u>Options de JVCSticker++</u></h2></div><div class="modal-body"><div style="text-align: center;"><br><br>' + generateUpdate() + '<br></div><div class="optCategorie"><table><h3>Prévisualisation</h3><tr><td style="text-align: left;padding-left:10%;"><label for="homeCat">Catégorie d\'accueil</label></td><td style="text-align: center;"><select id="homeCat" style="width:60%"><option>Populaires</option><option>Mes Favoris</option></select></td></tr><tr><td style="text-align: left;padding-left:10%;"><label for="espacementStickers">Espacement des stickers de la prévisualisation</label></td><td style="text-align: center;"><input type="number" id="espacementStickers" min="5" max="15" style="text-align:center;" value="' + GM_getValue('espacementStickers') + '"></td></tr><tr><td style="text-align: left;padding-left:10%;"><label for="tailleFenetre">Taille de la fenêtre (en pixel)</label></td><td style="text-align: center;"><input type="number" id="tailleFenetre" min="75" max="450" style="text-align:center;" value="' + GM_getValue('tailleFenetre') + '"></td></tr><tr><td style="text-align: left;padding-left:10%;"><label for="tailleStickers">Taille des stickers (en pixel)</label></td><td style="text-align: center;"><input type="number" id="tailleStickers" min="22" max="88" style="text-align:center;" value="' + GM_getValue('tailleStickers') + '"></td></tr><tr><td style="text-align: left;padding-left:10%;"><label for="xBarreHistorique">Nombre de ligne(s) d\'historique</label></td><td style="text-align: center;"><input type="number" id="xBarreHistorique" min="0" max="3" style="text-align:center;" value="' + GM_getValue('xBarreHistorique') + '"></td></tr></table><br></div><div class="optCategorie"><table><h3>Catégories</h3><tr><td style="text-align: left;padding-left:10%;"><select id="afficher" style="width:60%">' + generateHiddenCat() + '</select></td><td style="text-align: center;"><button type="button" class="btn btn-default" id="afficherSel">Afficher la catégorie</button></td></tr><tr><td style="text-align: left;padding-left:10%;"><select id="cacher" style="width:60%">' + generateShownCat() + '</select></td><td style="text-align: center;"><button type="button" class="btn btn-default" id="cacherSel">Cacher la catégorie</button></td></tr></table><br></div><div class="optCategorie"><table><h3>Options des stickers</h3><tr><td style="text-align: left;padding-left:10%;"><label for="tailleSticker">Taille des stickers (en %)</label></td><td style="text-align: center;"><input type="number" id="tailleSticker" min="50" max="300" style="text-align:center;" value="' + GM_getValue('tailleSticker') + '"></td></tr><tr><td style="text-align: left;padding-left:10%;"><label for="stickerCliquable">Stickers cliquables</label></td><td style="text-align: center;"><input type="checkbox" id="stickerCliquable"></td></tr><tr><td style="text-align: left;padding-left:10%;"><label for="sons">Ajouter des sons aux stickers</label></td><td style="text-align: center;"><input type="checkbox" id="sons"></td></tr><tr><td style="text-align: left;padding-left:10%;"><label for="intervalle">Intervalle de rechargement des stickers</label></td><td style="text-align: center;"><select id="intervalle" style="width:60%"><option>Toutes les 15 minutes</option><option>Toutes les 30 minutes</option><option>Toutes les heures</option><option>Toutes les 3 heures</option><option>Toutes les 6 heures</option><option>Toutes les 12 heures</option><option>Toutes les 24 heures</option></select></td></tr><tr><td colSpan=\'2\'><button type="button" class="btn btn-default" id="MAJManuelle">Recharger les stickers maintenant</button></td></tr></table><br></div><div class="optCategorie"><table><h3>Thème général</h3><tr><td style="text-align: left;padding-left:10%;"><label for="modifierCouleurPosts">Modifier la couleur des posts bleus</label></td><td style="text-align: center;"><input type="checkbox" id="modifierCouleurPosts"></td></tr><tr><td style="text-align: left;padding-left:12.5%;"><label for="couleurBackground">Couleur de fond</label></td><td style="text-align: center;"><input type="text" id="couleurBackground" style="text-align:center;" value="' + GM_getValue('couleurBackground') + '"></td></tr><tr><td style="text-align: left;padding-left:12.5%;"><label for="couleurBordure">Couleur des bordures</label></td><td style="text-align: center;"><input type="text" id="couleurBordure" style="text-align:center;" value="' + GM_getValue('couleurBordure') + '"></td></tr><tr><td style="text-align: left;padding-left:10%;"><label for="supprimerFond">Supprimer le fond blanc des images transparentes</label></td><td style="text-align: center;"><input type="checkbox" id="supprimerFond"></td></tr></table><br></div><div class="optCategorie"><table><h3>Bannissement de stickers</h3><tr><td style="text-align: left;padding-left:10%;"><label for="supprStickersBan">Supprimer les stickers bannis</label></td><td style="text-align: center;"><input type="checkbox" id="supprStickersBan"></td></tr><tr><td style="text-align: left;padding-left:12.5%;"><label for="stickerMessageDeSuppr">Afficher un message pour remplacer les stickers/posts bannis</label></td><td style="text-align: center;"><input type="checkbox" id="stickerMessageDeSuppr"></td></tr><tr><td style="text-align: left;padding-left:12.5%;"><label for="supprDesPosts">Supprimer les posts contenant un sticker banni</label></td><td style="text-align: center;"><input type="checkbox" id="supprDesPosts"></td></tr><tr><td style="text-align: left;padding-left:10%;"><input type="text" id="banStickerName" style="width: 60%"></td><td style="text-align: center;"><button type="button" class="btn btn-default" id="ajouterBan">Ajouter à la liste des stickers bannis</button></td></tr><tr><td style="text-align: left;padding-left:10%;"><select id="listeBan" size="4" style="width:61%">' + generateBanList() + '</select></td><td style="text-align: center;"><button type="button" class="btn btn-default" id="supprBan">Supprimer de la liste</button></td></tr></table><br></div><div class="optCategorie"><table><h3>Autres</h3><tr><td style="text-align: left;padding-left:10%;"><label for="webmPlayer">Lecture des webm</label></td><td style="text-align: center;"><input type="checkbox" id="webmPlayer"></td></tr><tr><td style="text-align: left;padding-left:10%;"><label for="youtubePlayer">Lecture des vidéos Youtube</label></td><td style="text-align: center;"><input type="checkbox" id="youtubePlayer"></td></tr><tr><td style="text-align: left;padding-left:10%;"><label for="vocarooPlayer">Lecture des Vocaroo</label></td><td style="text-align: center;"><input type="checkbox" id="vocarooPlayer"></td></tr><tr><td style="text-align: left;padding-left:10%;"><label for="widthPlayer">Largeur des médias intégrés</label></td><td style="text-align: center;"><input type="number" id="widthPlayer" min="160" max="1280" style="text-align:center;" value="' + GM_getValue('widthPlayer') + '"></td></tr><tr><td style="text-align: left;padding-left:10%;"><label for="heightPlayer">Hauteur des médias intégrés</label></td><td style="text-align: center;"><input type="number" id="heightPlayer" min="120" max="960" style="text-align:center;" value="' + GM_getValue('heightPlayer') + '"></td></tr><tr><td style="text-align: left;padding-left:10%;"><label for="playerSignature">Jouer les médias des signatures</label></td><td style="text-align: center;"><input type="checkbox" id="playerSignature"></td></tr><tr><td colSpan=\'2\'><button type="button" class="btn btn-default" id="resetAll">Réinitialiser toutes les options par défaut</button></td></tr></table><br></div></div><div class="modal-footer" style="text-align: center;"><button type="button" class="btn btn-default" data-dismiss="modal" id="valider">Sauvegarder</button> <a rel="modal:close" id="annulerBtn"><button type="button" class="btn btn-default" data-dismiss="modal" id="annuler">Annuler</button></a></div></div><p style="display:none;"><a href="#optionsModal" id="toOptionsModal" rel="modal:open">Open Modal</a></p>';
                document.body.appendChild(addOptions);
                switch (intervalle) {
                    case '15':
                        document.getElementById('intervalle').selectedIndex = '0';
                        break;
                    case '30':
                        document.getElementById('intervalle').selectedIndex = '1';
                        break;
                    case '60':
                        document.getElementById('intervalle').selectedIndex = '2';
                        break;
                    case '180':
                        document.getElementById('intervalle').selectedIndex = '3';
                        break;
                    case '360':
                        document.getElementById('intervalle').selectedIndex = '4';
                        break;
                    case '720':
                        document.getElementById('intervalle').selectedIndex = '5';
                        break;
                    case '1440':
                        document.getElementById('intervalle').selectedIndex = '6';
                        break;
                    default:
                        document.getElementById('intervalle').selectedIndex = '3';
                }
                switch (homeCat) {
                    case 'Populaires':
                        document.getElementById('homeCat').selectedIndex = '0';
                        break;
                    case 'Mes Favoris':
                        document.getElementById('homeCat').selectedIndex = '1';
                        break;
                    default:
                        document.getElementById('homeCat').selectedIndex = '0';
                }
                var checkboxList = [
                    'stickerCliquable',
                    'sons',
                    'modifierCouleurPosts',
                    'supprimerFond',
                    'supprStickersBan',
                    'stickerMessageDeSuppr',
                    'supprDesPosts',
                    'webmPlayer',
                    'youtubePlayer',
                    'vocarooPlayer',
                    'playerSignature'
                ];
                for (var h = 0; h < checkboxList.length; h++) {
                    if (GM_getValue(checkboxList[h]) === true) {
                        document.getElementById(checkboxList[h]).setAttribute('checked', 'checked');
                    } else {
                        if (document.getElementById(checkboxList[h]).checked == 'checked') {
                            document.getElementById(checkboxList[h]).removeAttribute('checked');
                        }
                    }
                }
                document.getElementById('modifierCouleurPosts').addEventListener('click', function() {
                    if (!(document.getElementById('modifierCouleurPosts').checked)) {
                        document.getElementById('couleurBackground').parentElement.parentElement.style.display = 'none';
                        document.getElementById('couleurBordure').parentElement.parentElement.style.display = 'none';
                    } else {
                        document.getElementById('couleurBackground').parentElement.parentElement.removeAttribute('style');
                        document.getElementById('couleurBordure').parentElement.parentElement.removeAttribute('style');
                    }
                });
                document.getElementById('supprStickersBan').addEventListener('click', function() {
                    if (!(document.getElementById('supprStickersBan').checked)) {
                        document.getElementById('stickerMessageDeSuppr').parentElement.parentElement.style.display = 'none';
                        document.getElementById('supprDesPosts').parentElement.parentElement.style.display = 'none';
                    } else {
                        document.getElementById('stickerMessageDeSuppr').parentElement.parentElement.removeAttribute('style');
                        document.getElementById('supprDesPosts').parentElement.parentElement.removeAttribute('style');
                    }
                });
                if (!document.getElementById('modifierCouleurPosts').checked) {
                    document.getElementById('couleurBackground').parentElement.parentElement.style.display = 'none';
                    document.getElementById('couleurBordure').parentElement.parentElement.style.display = 'none';
                }
                if (!document.getElementById('supprStickersBan').checked) {
                    document.getElementById('stickerMessageDeSuppr').parentElement.parentElement.style.display = 'none';
                    document.getElementById('supprDesPosts').parentElement.parentElement.style.display = 'none';
                }
                document.getElementById('resetAll').addEventListener('click', function() {
                    document.getElementById('espacementStickers').value = '10';
                    document.getElementById('tailleFenetre').value = '150';
                    document.getElementById('tailleStickers').value = '44';
                    document.getElementById('xBarreHistorique').value = '1';
                    shownListS = JSON.parse(GM_getValue('catToShowS'));
                    var newAfficher = '';
                    for (var g = 0; g < allCat.length; g++) {
                        if (shownListS.indexOf(allCat[g]) == -1) {
                            newAfficher += '<option id="' + allCat[g] + '">' + catRealNames[g] + '</option>';
                        }
                    }
                    document.getElementById('afficher').innerHTML = newAfficher;
                    var newCacher = '';
                    for (var g = 0; g < shownListS.length; g++) {
                        var idxG = allCat.indexOf(shownListS[g]);
                        newCacher += '<option id="' + allCat[idxG] + '">' + catRealNames[idxG] + '</option>';
                    }
                    document.getElementById('cacher').innerHTML = newCacher;
                    document.getElementById('tailleSticker').value = '100';
                    document.getElementById('stickerCliquable').checked = true;
                    document.getElementById('sons').checked = true;
                    document.getElementById('intervalle').selectedIndex = '3';
                    document.getElementById('homeCat').selectedIndex = '0';
                    MAJManuelle = false;
                    document.getElementById('modifierCouleurPosts').checked = true;
                    document.getElementById('couleurBackground').parentElement.parentElement.removeAttribute('style');
                    document.getElementById('couleurBordure').parentElement.parentElement.removeAttribute('style');
                    document.getElementById('couleurBackground').value = '#FFF';
                    document.getElementById('couleurBordure').value = '#d5d5d5';
                    document.getElementById('supprimerFond').checked = true;
                    document.getElementById('supprStickersBan').checked = true;
                    document.getElementById('stickerMessageDeSuppr').parentElement.parentElement.removeAttribute('style');
                    document.getElementById('supprDesPosts').parentElement.parentElement.removeAttribute('style');
                    document.getElementById('stickerMessageDeSuppr').checked = true;
                    document.getElementById('supprDesPosts').checked = false;
                    document.getElementById('banStickerName').value = '';
                    listeBanS = [
                        'http://jv.stkr.fr/p/1miq',
                        'http://jv.stkr.fr/p/1min',
                        'http://jv.stkr.fr/p/1mim',
                        'http://jv.stkr.fr/p/1mig-fr',
                        'http://jv.stkr.fr/p/1mij-fr',
                        'http://jv.stkr.fr/p/1mio',
                        'http://jv.stkr.fr/p/1mik',
                        'http://jv.stkr.fr/p/1mip',
                        'http://jv.stkr.fr/p/1mif',
                        'http://jv.stkr.fr/p/1mii-fr',
                        'http://jv.stkr.fr/p/1mih-fr',
                        'http://jv.stkr.fr/p/1mil',
                        'http://jv.stkr.fr/p/1mie-fr',
                        'http://jv.stkr.fr/p/1mid-fr',
                        'http://jv.stkr.fr/p/1myf',
                        'http://jv.stkr.fr/p/1my7',
                        'http://jv.stkr.fr/p/1myc',
                        'http://jv.stkr.fr/p/1my9',
                        'http://jv.stkr.fr/p/1myb',
                        'http://jv.stkr.fr/p/1my6',
                        'http://jv.stkr.fr/p/1mye',
                        'http://jv.stkr.fr/p/1myx',
                        'http://jv.stkr.fr/p/1myd',
                        'http://jv.stkr.fr/p/1my4',
                        'http://jv.stkr.fr/p/1my8',
                        'http://jv.stkr.fr/p/1mya',
                        'http://jv.stkr.fr/p/1my5',
                        'http://jv.stkr.fr/p/1n28'
                    ];
                    var newBan = '';
                    for (var g = 0; g < listeBanS.length; g++) {
                        newBan += '<option>' + listeBanS[g] + '</option>';
                    }
                    document.getElementById('listeBan').innerHTML = newBan;
                    document.getElementById('webmPlayer').checked = true;
                    document.getElementById('youtubePlayer').checked = true;
                    document.getElementById('vocarooPlayer').checked = true;
                    document.getElementById('widthPlayer').value = '320';
                    document.getElementById('heightPlayer').value = '240';
                    document.getElementById('playerSignature').checked = false;
                });
                document.getElementById('afficherSel').addEventListener('click', function() {
                    var idx = document.getElementById('afficher').selectedIndex;
                    if (typeof document.getElementById('afficher').options[idx] != 'undefined') {
                        var nameIdx = catRealNames.indexOf(document.getElementById('afficher').options[idx].text);
                        var name = allCat[nameIdx];
                        var found = false;
                        if (shownListS.length === 0) {
                            shownListS.push(name);
                            found = true;
                        } else {
                            for (var z = 0; z < nameIdx; z++) {
                                if (shownListS.indexOf(allCat[nameIdx - z]) != -1) {
                                    shownListS.splice(shownListS.indexOf(allCat[nameIdx - z]) + 1, 0, name);
                                    found = true;
                                    break;
                                }
                            }
                        }
                        if (!(found)) {
                            for (var z = 0; z < allCat.length - nameIdx; z++) {
                                if (shownListS.indexOf(allCat[nameIdx + z]) != -1) {
                                    shownListS.splice(shownListS.indexOf(allCat[nameIdx + z]), 0, name);
                                    break;
                                }
                            }
                        }
                        var newAfficher = '';
                        var tempList = [];
                        for (var g = 0; g < allCat.length; g++) {
                            if (shownListS.indexOf(allCat[g]) == -1) {
                                tempList.push(allCat[g]);
                                newAfficher += '<option id="' + allCat[g] + '">' + catRealNames[g] + '</option>';
                            }
                        }
                        document.getElementById('afficher').innerHTML = newAfficher;
                        if (document.getElementById('afficher').options[idx] !== undefined) {
                            document.getElementById('afficher').selectedIndex = idx;
                        } else {
                            if (document.getElementById('afficher').options[idx-1] !== undefined) {
                                document.getElementById('afficher').selectedIndex = idx-1;
                            }
                        }
                        var newCacher = '';
                        var nameSave = name;
                        if (document.getElementById('cacher').options[document.getElementById('cacher').selectedIndex] !== undefined) {
                            nameSave = allCat[catRealNames.indexOf(document.getElementById('cacher').options[document.getElementById('cacher').selectedIndex].text)]
                        }
                        for (var g = 0; g < shownListS.length; g++) {
                            var idxG = allCat.indexOf(shownListS[g]);
                            newCacher += '<option id="' + allCat[idxG] + '">' + catRealNames[idxG] + '</option>';
                        }
                        document.getElementById('cacher').innerHTML = newCacher;
                        document.getElementById('cacher').selectedIndex = shownListS.indexOf(nameSave);
                    }
                });
                document.getElementById('cacherSel').addEventListener('click', function() {
                    var idx = document.getElementById('cacher').selectedIndex;
                    if (typeof document.getElementById('cacher').options[idx] != 'undefined') {
                        var name = allCat[catRealNames.indexOf(document.getElementById('cacher').options[idx].text)];
                        shownListS.splice(shownListS.indexOf(name), 1);
                        var nameSave = name;
                        if (document.getElementById('afficher').options[document.getElementById('afficher').selectedIndex] !== undefined) {
                            nameSave = allCat[catRealNames.indexOf(document.getElementById('afficher').options[document.getElementById('afficher').selectedIndex].text)]
                        }
                        var newAfficher = '';
                        var tempList = [];
                        for (var g = 0; g < allCat.length; g++) {
                            if (shownListS.indexOf(allCat[g]) == -1) {
                                tempList.push(allCat[g]);
                                newAfficher += '<option id="' + allCat[g] + '">' + catRealNames[g] + '</option>';
                            }
                        }
                        document.getElementById('afficher').innerHTML = newAfficher;
                        document.getElementById('afficher').selectedIndex = tempList.indexOf(nameSave);
                        var newCacher = '';
                        for (var g = 0; g < shownListS.length; g++) {
                            var idxG = allCat.indexOf(shownListS[g]);
                            newCacher += '<option id="' + allCat[idxG] + '">' + catRealNames[idxG] + '</option>';
                        }
                        document.getElementById('cacher').innerHTML = newCacher;
                        if (document.getElementById('cacher').options[idx] !== undefined) {
                            document.getElementById('cacher').selectedIndex = idx;
                        } else {
                            if (document.getElementById('cacher').options[idx-1] !== undefined) {
                                document.getElementById('cacher').selectedIndex = idx-1;
                            }
                        }
                    }
                });
                document.getElementById('MAJManuelle').addEventListener('click', function() {
                    MAJManuelle = true;
                });
                document.getElementById('ajouterBan').addEventListener('click', function() {
                    listeBanS.push(document.getElementById('banStickerName').value);
                    var text = '';
                    for (var g = 0; g < listeBanS.length; g++) {
                        text += '<option>' + listeBanS[g] + '</option>';
                    }
                    document.getElementById('listeBan').innerHTML = text;
                    document.getElementById('listeBan').selectedIndex = listeBanS.indexOf(document.getElementById('banStickerName').value);
                    document.getElementById('banStickerName').value = '';
                });
                document.getElementById('supprBan').addEventListener('click', function() {
                    var idx = document.getElementById('listeBan').selectedIndex;
                    if (idx != -1) {
                        listeBanS.splice(idx, 1);
                        var text = '';
                        for (var g = 0; g < listeBanS.length; g++) {
                            text += '<option>' + listeBanS[g] + '</option>';
                        }
                        document.getElementById('listeBan').innerHTML = text;
                        document.getElementById('listeBan').selectedIndex = idx;
                        if (document.getElementById('listeBan').selectedIndex == -1) {
                            document.getElementById('listeBan').selectedIndex = idx - 1;
                        }
                    }
                });
                $('#optionsModal').on('modal:close', function() {
                    var textList = [
                        'espacementStickers',
                        'tailleFenetre',
                        'tailleStickers',
                        'xBarreHistorique',
                        'tailleSticker',
                        'couleurBackground',
                        'couleurBordure',
                        'widthPlayer',
                        'heightPlayer'
                    ];
                    for (var h = 0; h < textList.length; h++) {
                        document.getElementById(textList[h]).value = GM_getValue(textList[h]);
                    }
                    var checkboxList = [
                        'stickerCliquable',
                        'sons',
                        'modifierCouleurPosts',
                        'supprimerFond',
                        'supprStickersBan',
                        'stickerMessageDeSuppr',
                        'supprDesPosts',
                        'webmPlayer',
                        'youtubePlayer',
                        'vocarooPlayer',
                        'playerSignature'
                    ];
                    for (var h = 0; h < checkboxList.length; h++) {
                        document.getElementById(checkboxList[h]).checked = GM_getValue(checkboxList[h]);
                    }
                    document.getElementById('banStickerName').value = '';
                    shownListS = JSON.parse(GM_getValue('catToShow'));
                    var newAfficher = '';
                    for (var g = 0; g < allCat.length; g++) {
                        if (shownListS.indexOf(allCat[g]) == -1) {
                            newAfficher += '<option id="' + allCat[g] + '">' + catRealNames[g] + '</option>';
                        }
                    }
                    document.getElementById('afficher').innerHTML = newAfficher;
                    var newCacher = '';
                    for (var g = 0; g < shownListS.length; g++) {
                        var idxG = allCat.indexOf(shownListS[g]);
                        newCacher += '<option id="' + allCat[idxG] + '">' + catRealNames[idxG] + '</option>';
                    }
                    document.getElementById('cacher').innerHTML = newCacher;
                    listeBanS = JSON.parse(GM_getValue('stickersBanListe'));
                    var newBan = '';
                    for (var g = 0; g < listeBanS.length; g++) {
                        newBan += '<option>' + listeBanS[g] + '</option>';
                    }
                    switch (GM_getValue('intervalle')) {
                        case '15':
                            document.getElementById('intervalle').selectedIndex = '0';
                            break;
                        case '30':
                            document.getElementById('intervalle').selectedIndex = '1';
                            break;
                        case '60':
                            document.getElementById('intervalle').selectedIndex = '2';
                            break;
                        case '180':
                            document.getElementById('intervalle').selectedIndex = '3';
                            break;
                        case '360':
                            document.getElementById('intervalle').selectedIndex = '4';
                            break;
                        case '720':
                            document.getElementById('intervalle').selectedIndex = '5';
                            break;
                        case '1440':
                            document.getElementById('intervalle').selectedIndex = '6';
                            break;
                        default:
                            document.getElementById('intervalle').selectedIndex = '3';
                    }
                    switch (GM_getValue('homeCat')) {
                        case 'Populaires':
                            document.getElementById('homeCat').selectedIndex = '0';
                            break;
                        case 'Mes Favoris':
                            document.getElementById('homeCat').selectedIndex = '1';
                            break;
                        default:
                            document.getElementById('homeCat').selectedIndex = '0';
                    }
                    MAJManuelle = false;
                    document.getElementById('listeBan').innerHTML = newBan;
                });
                document.getElementById('valider').addEventListener('click', function() {
                    var optNames = [
                        'espacementStickers',
                        'tailleFenetre',
                        'tailleStickers',
                        'xBarreHistorique',
                        'tailleSticker',
                        'couleurBackground',
                        'couleurBordure',
                        'widthPlayer',
                        'heightPlayer'
                    ];
                    for (var d = 0; d < optNames.length; d++) {
                        GM_setValue(optNames[d], document.getElementById(optNames[d]).value);
                    }
                    GM_setValue('catToShow', JSON.stringify(shownListS));
                    var optCheckbox = [
                        'stickerCliquable',
                        'sons',
                        'modifierCouleurPosts',
                        'supprimerFond',
                        'supprStickersBan',
                        'stickerMessageDeSuppr',
                        'supprDesPosts',
                        'webmPlayer',
                        'youtubePlayer',
                        'vocarooPlayer',
                        'playerSignature'
                    ];
                    for (var d = 0; d < optCheckbox.length; d++) {
                        if (document.getElementById(optCheckbox[d]).checked === true || document.getElementById(optCheckbox[d]).checked == 'checked') {
                            GM_setValue(optCheckbox[d], true);
                        } else {
                            GM_setValue(optCheckbox[d], false);
                        }
                    }
                    switch (document.getElementById('intervalle').selectedIndex) {
                        case 0:
                            GM_setValue('intervalle', '15');
                            break;
                        case 1:
                            GM_setValue('intervalle', '30');
                            break;
                        case 2:
                            GM_setValue('intervalle', '60');
                            break;
                        case 3:
                            GM_setValue('intervalle', '180');
                            break;
                        case 4:
                            GM_setValue('intervalle', '360');
                            break;
                        case 5:
                            GM_setValue('intervalle', '720');
                            break;
                        case 6:
                            GM_setValue('intervalle', '1440');
                            break;
                        default:
                            GM_setValue('intervalle', '180');
                    }
                    switch (document.getElementById('homeCat').selectedIndex) {
                        case 0:
                            GM_setValue('homeCat', 'Populaires');
                            break;
                        case 1:
                            GM_setValue('homeCat', 'Mes Favoris');
                            break;
                        default:
                            GM_setValue('homeCat', 'Populaires');
                    }
                    if (MAJManuelle) {
                        GM_deleteValue('lTimestamp');
                    }
                    GM_setValue('stickersBanListe', JSON.stringify(listeBanS));
                    document.getElementById('annulerBtn').click();
                });
                var canvasHide = document.querySelector('div[data-flg-tt="Biblioth&egrave;que"]').parentElement.parentElement;
                var refresh = document.createElement('div');
                refresh.className = 'f-tabs-r';
                refresh.style.height = '46px';
                refresh.innerHTML = '<img src="http://image.noelshack.com/fichiers/2016/23/1465690515-refresh.png" height="23" width="25"/><img src="https://image.noelshack.com/fichiers/2017/20/1495391867-fazb.png" onclick="var win = window.open(\'https://www.facebook.com/\', \'_blank\'); win.focus();" height="23" width="25"/><img src="http://image.noelshack.com/fichiers/2017/19/1494772404-to-do-list-ched2.png" height="23" width="25"/><img src="http://image.noelshack.com/fichiers/2016/24/1465930183-application-x-desktop.png" height="23" width="25"/></br><input id="g_search" type="search" placeholder="Rechercher..." style="width:100px;">';
                canvasHide.insertBefore(refresh, canvasHide.secondChild);
                refresh.firstChild.addEventListener('click', function() {
                    window.open('https://ticki84.github.io/JVCSticker++.user.js', '_blank');
                });
                refresh.children[2].addEventListener('click', function() {
                    document.getElementById('toReqModal').click();
                });
                refresh.children[3].addEventListener('click', function() {
                    document.getElementById('toOptionsModal').click();
                });
                document.getElementById('g_search').addEventListener('input', function() {
                    isCatPopulaires = false;
                    isCatFavoris = false;
                    isSearching = true;
                    if (document.getElementById('g_search').value != '') {
                        var searchTags = document.getElementById('g_search').value.replace(/,/g,' ').replace(/;/g,' ').latinize().toLowerCase().split(' ');
                        var search_idx = searchTags.indexOf('');
                        while (search_idx != -1) {
                            searchTags.splice(search_idx, 1);
                            search_idx = searchTags.indexOf('');
                        }
                        searchTags = uniq_fast(searchTags);
                        var occ = [];
                        for (var ya = 0; ya < catCache.length; ya++) {
                            catCache[ya].reduce(function(n, sticker) {
                                if (sticker.tags.length > 0) {
                                    var stckrTags = sticker.tags;
                                    var nocc = 0;
                                    for (var b = 0; b < searchTags.length; b++) {
                                        for (var c = 0; c < stckrTags.length; c++) {
                                            if (searchTags[b].levenstein(stckrTags[c].substr(0,searchTags[b].length)) == 0) {
                                                nocc += 1;
                                            }
                                        }
                                    }
                                    occ.push({
                                        'url': sticker.url,
                                        'occ': nocc
                                    });
                                }
                            }, 0);
                        }
                        occ.sort(function(a, b) {
                            return b.occ - a.occ;
                        });
                        var foo = document.getElementsByClassName('f-stkrs f-cfx')[0];
                        while (foo.firstChild) {
                            foo.removeChild(foo.firstChild);
                        }
                        var canvas = document.querySelector('div[data-flg-tt="Populaires"]');
                        var numChildren = canvas.parentElement.children.length;
                        for (var i = 0; i < numChildren; i++) {
                            if (canvas.parentElement.children[i].className.indexOf('f-active') != -1) {
                                canvas.parentElement.children[i].className = canvas.parentElement.children[i].className.slice(0, -9);
                                break;
                            }
                        }
                        var nbUrls = 0;
                        for (var q = 0; q < occ.length; q++) {
                            if (occ[q].occ != 0) {
                                nbUrls++;
                            } else {
                                break;
                            }
                            if (nbUrls > 24) {
                                break;
                            }
                        }
                        for (var q = 0; q < nbUrls; q++) {
                            ajouterSticker(occ[q].url);
                        }
                        var stkrItems = document.getElementsByClassName('f-stkrs f-cfx')[0].children;
                        for (var q = 0; q < stkrItems.length; q++) {
                            stkrItems[q].firstChild.children[1].firstChild.click();
                        }
                    } else {
                        charger(homeCat);
                    }
                });
                canvasPop.style.width = $(canvasPop.parentElement).outerWidth(true) - $(canvasPop.parentElement).children().last().outerWidth(true) + 'px';
                var canHeight = Math.ceil(((catToShow.length+2)*saveWidth)/($(canvasPop.parentElement).outerWidth(true) - $(canvasPop.parentElement).children().last().outerWidth(true))) * saveHeight;
                if (canHeight < 2 * saveHeight) {
                    canHeight = 2 * saveHeight;
                }
                canvasPop.style.height = canHeight + 'px';
                var fenSticker = document.getElementsByClassName('f-stkrs-w f-mid-fill-h')[0];
                fenSticker.style.height = tailleFenetre + 'px';
                fenSticker.parentElement.style.height = tailleFenetre + 'px';
                fenSticker.parentElement.parentElement.style.height = tailleFenetre + 'px';
                var editModifier = document.getElementsByClassName('picto-msg-crayon');
                if (editModifier.length > 0) {
                    for (var i = 0; i < editModifier.length; i++) {
                        editModifier[i].addEventListener('click', function() {
                            var observerLoad = new MutationObserver(function(mutations, mutLoad) {
                                if (document.querySelector('div[data-flg-tt="Hap"]')) {
                                    mutLoad.disconnect();
                                    chargerBarre();
                                }
                                return;
                            });
                            observerLoad.observe(document.body, {
                                childList: true,
                                subtree: true,
                                attributes: false,
                                characterData: false
                            });
                        });
                        var observerStickers = new MutationObserver(function(mutations, mutStickers) {
                            var canvasStickers = document.querySelector('button[data-edit="stickers"]');
                            if (canvasStickers) {
                                mutStickers.disconnect();
                                canvasStickers.onclick = function() {
                                    check();
                                };
                            }
                            return;
                        });
                        observerStickers.observe(document.body, {
                            childList: true,
                            subtree: true,
                            attributes: false,
                            characterData: false
                        });
                        var observerButtonA = new MutationObserver(function(mutations, mutButtons) {
                            var canvasButtons = document.getElementsByClassName('btn-annuler-modif-msg');
                            if (canvasButtons) {
                                mutButtons.disconnect();
                                for (var j = 0; j < canvasButtons.length; j++) {
                                    canvasButtons[j].addEventListener('click', function() {
                                        var observerTArea = new MutationObserver(function(mutations, mutTArea) {
                                            var canvasTArea = document.getElementById('message_topic');
                                            if (canvasTArea) {
                                                mutTArea.disconnect();
                                                canvasTArea.addEventListener('click', tArea());
                                            }
                                            return;
                                        });
                                        observerTArea.observe(document.body, {
                                            childList: true,
                                            subtree: true,
                                            attributes: false,
                                            characterData: false
                                        });
                                    });
                                }
                            }
                            return;
                        });
                        observerButtonA.observe(document.body, {
                            childList: true,
                            subtree: true,
                            attributes: false,
                            characterData: false
                        });
                        var observerButtonE = new MutationObserver(function(mutations, mutButtons) {
                            var canvasButtons = document.getElementsByClassName('btn btn-editer-msg');
                            if (canvasButtons) {
                                mutButtons.disconnect();
                                for (var j = 0; j < canvasButtons.length; j++) {
                                    canvasButtons[j].addEventListener('click', function() {
                                        var observerTArea = new MutationObserver(function(mutations, mutTArea) {
                                            var canvasTArea = document.getElementById('message_topic');
                                            if (canvasTArea) {
                                                mutTArea.disconnect();
                                                canvasTArea.addEventListener('click', tArea());
                                            }
                                            return;
                                        });
                                        observerTArea.observe(document.body, {
                                            childList: true,
                                            subtree: true,
                                            attributes: false,
                                            characterData: false
                                        });
                                    });
                                }
                            }
                            return;
                        });
                        observerButtonE.observe(document.body, {
                            childList: true,
                            subtree: true,
                            attributes: false,
                            characterData: false
                        });
                    }
                }
                allCat = JSON.parse(GM_getValue('allCat'));
                catRealNames = JSON.parse(GM_getValue('catRealNames'));
                catIcons = JSON.parse(GM_getValue('catIcons'));
                catToShow = JSON.parse(GM_getValue('catToShow'));
                var funcs = [];

                function createfunc(g) {
                    return function() {
                        var idx = allCat.indexOf(catToShow[g]);
                        var tab = document.createElement('div');
                        tab.className = 'f-tab f-h';
                        tab.style.width = '25px';
                        tab.style.height = '23px';
                        tab.style.lineHeight = '23px';
                        tab.style.fontSize = '14px';
                        tab.setAttribute('data-flg-tt', catRealNames[idx]);
                        tab.innerHTML = catIcons[idx] + '<div style="display: none;" class="f-ttw"><div style="top: -26px; left: -9px;" class="f-inner"><div class="f-tt">' + catRealNames[idx] + '</div><div class="f-arr"></div></div></div>';
                        tab.addEventListener('click', function() {
                            charger(catRealNames[idx]);
                        });
                        canvasPop.appendChild(tab);
                    };
                }
                for (var g = 0; g < catToShow.length; g++) {
                    funcs[g] = createfunc(g);
                }
                for (var g = 0; g < catToShow.length; g++) {
                    funcs[g]();
                }
            }, 100);
        }

        function check() {
            if (document.querySelector('button[data-edit="stickers"]').className.indexOf('active') != -1) {
                var observerLoad = new MutationObserver(function(mutations, mutLoad) {
                    if (document.querySelector('div[data-flg-tt="Hap"]')) {
                        mutLoad.disconnect();
                        chargerBarre();
                    }
                    return;
                });
                observerLoad.observe(document.body, {
                    childList: true,
                    subtree: true,
                    attributes: false,
                    characterData: false
                });
            }
        }
        var observerHap = new MutationObserver(function(mutations, mutHap) {
            if (document.querySelector('div[data-flg-tt="Hap"]')) {
                mutHap.disconnect();
                chargerBarre();
            }
            return;
        });
        observerHap.observe(document.body, {
            childList: true,
            subtree: true,
            attributes: false,
            characterData: false
        });
        if (supprStickersBan) {
            var observerCharg = new MutationObserver(function(mutations, mutCharg) {
                var canvasCharg = document.getElementsByClassName('bloc-pagi-default');
                if (canvasCharg[1]) {
                    mutCharg.disconnect();
                    var imgs = document.getElementsByClassName('img-stickers');
                    var funcs = [];
                    for (var i = 0; i < imgs.length; i++) {
                        if ($.inArray(imgs[i].src, stickersBanListe) > -1) {
                            if (!(supprDesPosts)) {
                                if (stickerMessageDeSuppr) {
                                    if (!(imgs[i].previousSibling) || !(imgs[i].previousSibling.style) || imgs[i].previousSibling.style.color != 'red') {
                                        var message = document.createElement('div');
                                        message.style.color = 'red';
                                        message.innerHTML = 'Sticker supprimé! ';
                                        imgs[i].parentElement.insertBefore(message, imgs[i]);
                                    } else {
                                        imgs[i].previousSibling.innerHTML += 'Sticker supprimé! ';
                                    }
                                }
                                imgs[i].parentNode.removeChild(imgs[i]);
                                i--;
                            } else {
                                if (imgs[i].offsetParent != null) {
                                    var img = imgs[i];
                                    while (typeof img.parentElement != 'undefined' && img.parentElement.className != 'bloc-message-forum ') {
                                        img = img.parentElement;
                                    }
                                    img.style.display = 'none';
                                    if (stickerMessageDeSuppr) {
                                        var showBan = document.createElement('div');
                                        showBan.className = 'conteneur-message n-displayed';
                                        showBan.style.color = 'red';
                                        showBan.style.textAlign = 'center';
                                        showBan.style.marginBottom = '15px';
                                        showBan.innerHTML = 'Ce post a été supprimé car il contenait un sticker banni. Cliquez ici pour l\'afficher.';
                                        showBan.addEventListener('click', function() {
                                            if (this.className.endsWith('n-displayed')) {
                                                this.parentElement.lastElementChild.removeAttribute('style');
                                                this.innerHTML = 'Ce post a été supprimé car il contenait un sticker banni. Cliquez ici pour le cacher.';
                                                $(this).removeClass('n-displayed');
                                            } else {
                                                this.parentElement.lastElementChild.style.display = 'none';
                                                this.innerHTML = 'Ce post a été supprimé car il contenait un sticker banni. Cliquez ici pour l\'afficher.';
                                                this.className += ' n-displayed';
                                            }
                                        });
                                        img.parentElement.insertBefore(showBan, img);
                                    }
                                }
                            }
                        }
                    }
                }
                return;
            });
            observerCharg.observe(document.body, {
                childList: true,
                subtree: true,
                attributes: false,
                characterData: false
            });
        }
        var observerHide = new MutationObserver(function(mutations, mutHide) {
            var canvasHide = document.querySelector('div[data-flg-tt="Biblioth&egrave;que"]');
            if (canvasHide) {
                mutHide.disconnect();
                canvasHide.parentElement.setAttribute('style', 'display:none !important;height:0px !important;width:0px !important');
            }
            return;
        });
        observerHide.observe(document.body, {
            childList: true,
            subtree: true,
            attributes: false,
            characterData: false
        });
        var observerStickers = new MutationObserver(function(mutations, mutStickers) {
            var canvasStickers = document.querySelector('button[data-edit="stickers"]');
            if (canvasStickers) {
                mutStickers.disconnect();
                setTimeout(chargerEdit(), 100);
                canvasStickers.onclick = function() {
                    check();
                };
            }
            return;
        });
        observerStickers.observe(document.body, {
            childList: true,
            subtree: true,
            attributes: false,
            characterData: false
        });
        setTimeout(function() {
            if (!isLoaded) {
                chargerBarre();
            }
        }, 10000);
    }
}