// ==UserScript==
// @name        JVCSticker++
// @namespace   JVCSticker++
// @include     http://www.jeuxvideo.com/forums/*
// @include     https://www.jeuxvideo.com/forums/*
// @version     1.5.24
// @grant       GM_addStyle
// @updateURL   https://ticki84.github.io/JVCSticker++.meta.js
// @downloadURL https://ticki84.github.io/JVCSticker++.user.js
// @require     http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js
// @require     https://ticki84.github.io/jquery.modal.min.js
// @connect     github.io
// @grant       GM_xmlhttpRequest
// @grant       GM_getValue
// @grant       GM_setValue
// @grant       GM_listValues
// @grant       GM_deleteValue
// @icon        http://jv.stkr.fr/p/1kki
// @author      Ticki84
// @copyright   2016+, Ticki84
// @noframes
// ==/UserScript==
function deleteGlobalVar() {
  var keys = GM_listValues();
  for (var i = 0, key = null; key = keys[i]; i++) {
    GM_deleteValue(key);
  }
}
function addStyle(style) {
  var head = document.getElementsByTagName('HEAD') [0];
  var ele = head.appendChild(window.document.createElement('style'));
  ele.innerHTML = style;
  return ele;
}
addStyle('@import "https://ticki84.github.io/jquery.modal.min.css";');
if (GM_getValue('dontShowUpTo') === undefined) {
  GM_setValue('dontShowUpTo', GM_info.script.version);
}
GM_xmlhttpRequest({
  method: 'GET',
  url: 'https://ticki84.github.io/JVCSticker++.xml',
  headers: {
    'User-Agent': 'Mozilla/5.0',
    'Accept': 'text/xml'
  },
  onload: function (response) {
    var responseXML = null;
    if (!response.responseXML) {
      responseXML = new DOMParser().parseFromString(response.responseText, 'text/xml');
    }
    var scriptElement = document.createElement('script');
    scriptElement.type = 'text/javascript';
    scriptElement.src = 'https://ticki84.github.io/jquery.modal.min.js';
    document.body.appendChild(scriptElement);
    function generateChangelog() {
      var i = 0;
      var text = '';
      while (response.responseXML.getElementsByTagName('version') [i]) {
        if (response.responseXML.getElementsByTagName('version') [i].childNodes[0].nodeValue > GM_info.script.version) {
          text += '<p>Version ' + response.responseXML.getElementsByTagName('version') [i].childNodes[0].nodeValue + ':';
          var changelog = response.responseXML.getElementsByTagName('changelog') [i].childNodes[0].nodeValue.replace(new RegExp('-', 'g'), '<br>-');
          text += changelog;
          text += '<br>';
        } 
        else {
          break;
        }
        i++;
      }
      return text;
    }
    if (response.responseXML.getElementsByTagName('version') [0].childNodes[0].nodeValue > GM_info.script.version && response.responseXML.getElementsByTagName('version') [0].childNodes[0].nodeValue > GM_getValue('dontShowUpTo', '1.0.0')) {
      var addHTML = document.createElement('div');
      addHTML.innerHTML = ' <div id="updateModal" style="display:none; text-align: center;"><div class="modal-header"><h3 class="modal-title">Nouvelle version!</h3></div><div class="modal-body"><br><a href="https://ticki84.github.io/JVCSticker++.user.js" target="_blank">Une nouvelle version de JVCSticker++ est disponible!</a><br><br><br><p>Version actuelle: '
      + GM_info.script.version + '</p><p>Dernière version: '
      + response.responseXML.getElementsByTagName('version') [0].childNodes[0].nodeValue + '</p><br><br><p><u>Changelog:</u></p>'
      + generateChangelog() + '<br><br><p><h4>Voulez-vous effectuez la mise à jour maintenant?</h4></p><br><label><input type="checkbox" id="showNV"> Ne plus m\'avertir jusqu\'à la prochaine mise à jour</label><br><br></div><div class="modal-footer"><button type="button" id="updateOui" class="btn btn-default" data-dismiss="modal">Oui</button> <a rel="modal:close"><button type="button" id="updateNon" class="btn btn-default" data-dismiss="modal">Non</button></a></div></div><p style="display:none;"><a href="#updateModal" id="toUpdateModal" rel="modal:open">Open Modal</a></p>';
      document.body.appendChild(addHTML);
      document.getElementById('updateOui').addEventListener('click', function () {
        window.open('https://ticki84.github.io/JVCSticker++.user.js', '_blank');
      });
      document.getElementById('updateNon').addEventListener('click', function () {
        if (document.getElementById('showNV').checked) {
          GM_setValue('dontShowUpTo', response.responseXML.getElementsByTagName('version') [0].childNodes[0].nodeValue);
        }
        init(response.responseXML.getElementsByTagName('version') [0].childNodes[0].nodeValue);
      });
      document.getElementById('toUpdateModal').click();
    } 
    else {
      init(response.responseXML.getElementsByTagName('version') [0].childNodes[0].nodeValue);
    }
  }
});
function init(lastVersion) {
  var varNames = [
    'espacementStickers',
    'tailleStickers',
    'tailleFenetre',
    'modifierCouleurPosts',
    'couleurBackground',
    'couleurBordure',
    'supprimerFond',
    'tailleSticker',
    'stickerCliquable',
    'supprStickersBan',
    'supprDesPosts',
    'stickerMessageDeSuppr',
    'sons',
    'webmPlayer',
    'youtubePlayer',
    'playerSignature',
    'xBarreIcones'
  ];
  var valDefault = [
    '10',
    '44',
    '150',
    true,
    '#FFF',
    '#d5d5d5',
    true,
    '100',
    true,
    true,
    false,
    true,
    true,
    true,
    true,
    false,
    '2'
  ];
  for (var i = 0; i < varNames.length; i++) {
    if (GM_getValue(varNames[i]) === undefined) {
      GM_setValue(varNames[i], valDefault[i]);
    }
  }
  var espacementStickers = GM_getValue('espacementStickers');
  var tailleStickers = GM_getValue('tailleStickers');
  var tailleFenetre = GM_getValue('tailleFenetre');
  var modifierCouleurPosts = GM_getValue('modifierCouleurPosts');
  var couleurBackground = GM_getValue('couleurBackground');
  var couleurBordure = GM_getValue('couleurBordure');
  var supprimerFond = GM_getValue('supprimerFond');
  var tailleSticker = GM_getValue('tailleSticker');
  var stickerCliquable = GM_getValue('stickerCliquable');
  var supprStickersBan = GM_getValue('supprStickersBan');
  var supprDesPosts = GM_getValue('supprDesPosts');
  var stickerMessageDeSuppr = GM_getValue('stickerMessageDeSuppr');
  var sons = GM_getValue('sons');
  var webmPlayer = GM_getValue('webmPlayer');
  var youtubePlayer = GM_getValue('youtubePlayer');
  var playerSignature = GM_getValue('playerSignature');
  var xBarreIcones = GM_getValue('xBarreIcones');
  if (GM_getValue('allCat') != undefined && GM_getValue('allCat').indexOf('catYoutube') === - 1) {
    GM_deleteValue('allCat');
    GM_deleteValue('catRealNames');
    GM_deleteValue('catIcons');
    GM_deleteValue('catToShow');
  }
  if (GM_getValue('allCat') != undefined && GM_getValue('allCat').indexOf('catSmiley') === - 1) {
    GM_deleteValue('allCat');
    GM_deleteValue('catRealNames');
    GM_deleteValue('catIcons');
    GM_deleteValue('catToShow');
    GM_setValue('xBarreIcones', '2');
  }
  var varLNames = [
    'stickersBanListe',
    'allCat',
    'catRealNames',
    'catIcons',
    'catToShow'
  ];
  var valLDefault = [
    [
    'http://jv.stkr.fr/p/1miq',
    'http://jv.stkr.fr/p/1min',
    'http://jv.stkr.fr/p/1mim',
    'http://jv.stkr.fr/p/1mig-fr',
    'http://jv.stkr.fr/p/1mij-fr',
    'http://jv.stkr.fr/p/1mio',
    'http://jv.stkr.fr/p/1mik',
    'http://jv.stkr.fr/p/1mip',
    'http://jv.stkr.fr/p/1mif',
    'http://jv.stkr.fr/p/1mii-fr',
    'http://jv.stkr.fr/p/1mih-fr',
    'http://jv.stkr.fr/p/1mil',
    'http://jv.stkr.fr/p/1mie-fr',
    'http://jv.stkr.fr/p/1mid-fr',
    'http://jv.stkr.fr/p/1myf',
    'http://jv.stkr.fr/p/1my7',
    'http://jv.stkr.fr/p/1myc',
    'http://jv.stkr.fr/p/1my9',
    'http://jv.stkr.fr/p/1myb',
    'http://jv.stkr.fr/p/1my6',
    'http://jv.stkr.fr/p/1mye',
    'http://jv.stkr.fr/p/1myx',
    'http://jv.stkr.fr/p/1myd',
    'http://jv.stkr.fr/p/1my4',
    'http://jv.stkr.fr/p/1my8',
    'http://jv.stkr.fr/p/1mya',
    'http://jv.stkr.fr/p/1my5',
    'http://jv.stkr.fr/p/1n28'
    ],
    [
      'catHap',
      'catNoel',
      'catBridgely',
      'catDomDeVill',
      'catSaumon',
      'catRisitas',
      'catFaces',
      'catYoutube',
      'catTelevision',
      'catPolitique',
      'catJeuxVideo',
      'catEcoPlus',
      'catSmiley',
      'catAutres',
      'catRex',
      'catBud',
      'catGrukk',
      'catLamasticot',
      'catVolt',
      'catFluffy',
      'catStore',
      'catAnime',
      'catFootball',
      'catDuracell',
      'catXbox',
      'catXMen'
    ],
    [
      'Hap',
      'Noel',
      'Bridgely',
      'Créations de DomDeVill',
      'Créations de SaumonArcEnCiel',
      'Risitas',
      'Faces',
      'Youtube',
      'Télévision',
      'Politique',
      'Jeux Vidéo',
      'Eco+',
      'Smiley',
      'Autres',
      'Rex Ryder',
      'Bud',
      'Grukk',
      'Lamasticot',
      'Volt le Chien',
      'Fluffy',
      'Store',
      'Anime',
      'Football Fans',
      'Duracell',
      'Xbox One',
      'X-Men Apocalypse'
    ],
    [
      'º',
      '¹',
      '¬',
      'Ó',
      '×',
      'x',
      'Ü',
      'Z',
      '¨',
      'Û',
      '|',
      'E',
      'º',
      'Ã',
      'Ö',
      'N',
      'Ð',
      '²',
      'Å',
      '»',
      '!',
      'r',
      'ì',
      'U',
      'ê',
      'å'
    ],
    [
      'catHap',
      'catNoel',
      'catBridgely',
      'catDomDeVill',
      'catSaumon',
      'catRisitas',
      'catFaces',
      'catYoutube',
      'catTelevision',
      'catPolitique',
      'catJeuxVideo',
      'catEcoPlus',
      'catSmiley',
      'catAutres',
      'catRex',
      'catBud',
      'catGrukk',
      'catLamasticot',
      'catVolt',
      'catFluffy',
      'catStore',
      'catFootball'
    ]
  ];
  for (var i = 0; i < varLNames.length; i++) {
    if (GM_getValue(varLNames[i]) === undefined) {
      GM_setValue(varLNames[i], JSON.stringify(valLDefault[i]));
    }
  }
  var stickersBanListe = JSON.parse(GM_getValue('stickersBanListe'));
  var allCat = JSON.parse(GM_getValue('allCat'));
  var catRealNames = JSON.parse(GM_getValue('catRealNames'));
  var catIcons = JSON.parse(GM_getValue('catIcons'));
  var catToShow = JSON.parse(GM_getValue('catToShow'));
  var isLoaded = false;
  if (modifierCouleurPosts) {
    GM_addStyle('.bloc-message-forum:nth-of-type(2n+1){background: ' + couleurBackground + ';border: 1px solid ' + couleurBordure + ';} .stickersM{max-width:' + 200 + 'px !important; max-height: ' + 200 + 'px !important}');
  }
  function clique(url) {
    var area = document.getElementsByClassName('area-editor') [0];
    var start = area.selectionStart;
    var end = area.selectionEnd;
    var text = area.value;
    var before = text.substring(0, start);
    var after = text.substring(end, text.length);
    if (url.substr(0, 18) == 'http://jv.stkr.fr/') {
      var newText = '[[sticker:' + url.slice(18, - 7) + ']]';
      area.value = (before + newText + after);
      area.selectionStart = area.selectionEnd = start + newText.length;
    } 
    else {
      var newText = ' ' + url + ' ';
      area.value = (before + newText + after);
      area.selectionStart = area.selectionEnd = start + newText.length;
    }
    area.focus();
    area.trigger('change');
  }
  function ajouterSticker(url) {
    var nSticker = document.createElement('li');
    nSticker.className = 'f-stkr-w';
    nSticker.innerHTML = '<div class="f-stkr f-no-sml" style="width:' + document.getElementsByClassName('f-stkrs f-cfx') [0].offsetWidth / espacementStickers + 'px"><span class="f-hlpr"></span><img src="' + url + '" style="max-height:' + tailleStickers + 'px;max-width:' + tailleStickers + 'px" data-code=""></div>';
    nSticker.onclick = function () {
      clique(url);
    };
    document.getElementsByClassName('f-stkrs f-cfx') [0].appendChild(nSticker);
  }
  function ajouterStickers(section) {
    if (section == 'Populaires' && (!(document.getElementsByClassName('f-stkrs f-cfx') [0].firstChild) || document.getElementsByClassName('f-stkrs f-cfx') [0].firstChild.firstChild.secondChild.src != 'http://jv.stkr.fr/p/1kki?f-ed=1'))
    {
      ajouterSticker('http://jv.stkr.fr/p/1kki?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1lmb?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1jnc?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1lmh?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1jnh?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1lmk?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1kkn?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1jne?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1jng?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1kks?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1jnf?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1jnj?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1kkl?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1ljr?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1mr0?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1klb?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1ljp?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1lgd?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1kkh?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1kgx?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1ljj?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1kku?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1kkr?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1kkp?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1kkq?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1kko?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1kkk?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1jni?f-ed=1');
    } 
    else if (section == 'Hap' && (!(document.getElementsByClassName('f-stkrs f-cfx') [0].firstChild) || document.getElementsByClassName('f-stkrs f-cfx') [0].firstChild.firstChild.secondChild.src != 'http://jv.stkr.fr/p/1kkh?f-ed=1'))
    {
      ajouterSticker('http://jv.stkr.fr/p/1kkh?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1kkg?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1kki?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1kkj?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1kkk?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1kkl?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1kkm?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1kkn?f-ed=1');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465759062-hap1.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465759064-hap2.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465698410-hitl.png');
    } 
    else if (section == 'Noel' && (!(document.getElementsByClassName('f-stkrs f-cfx') [0].firstChild) || document.getElementsByClassName('f-stkrs f-cfx') [0].firstChild.firstChild.secondChild.src != 'http://jv.stkr.fr/p/1kkr?f-ed=1'))
    {
      ajouterSticker('http://jv.stkr.fr/p/1kkr?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1kko?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1kkp?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1kkq?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1kks?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1kkt?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1kku?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1kkv?f-ed=1');
    } 
    else if (section == 'Bridgely' && (!(document.getElementsByClassName('f-stkrs f-cfx') [0].firstChild) || document.getElementsByClassName('f-stkrs f-cfx') [0].firstChild.firstChild.secondChild.src != 'http://jv.stkr.fr/p/1jnd?f-ed=1'))
    {
      ajouterSticker('http://jv.stkr.fr/p/1jnd?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1jnc?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1jne?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1jnf?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1jng?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1jnh?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1jni?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1jnj?f-ed=1');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465720817-bridgely1.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465720819-evra1.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465720815-evra2.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465720821-evra3.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465739682-bridgely.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494446-img9.png');
    } 
    else if (section == 'Créations de DomDeVill' && (!(document.getElementsByClassName('f-stkrs f-cfx') [0].firstChild) || document.getElementsByClassName('f-stkrs f-cfx') [0].firstChild.firstChild.secondChild.src != 'http://jv.stkr.fr/p/1ljl?f-ed=1'))
    {
      ajouterSticker('http://jv.stkr.fr/p/1ljl?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1ljj?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1ljm?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1ljn?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1ljo?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1ljp?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1ljr?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1ljq?f-ed=1');
    } 
    else if (section == 'Créations de SaumonArcEnCiel' && (!(document.getElementsByClassName('f-stkrs f-cfx') [0].firstChild) || document.getElementsByClassName('f-stkrs f-cfx') [0].firstChild.firstChild.secondChild.src != 'http://jv.stkr.fr/p/1lmi?f-ed=1'))
    {
      ajouterSticker('http://jv.stkr.fr/p/1lmi?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1lml?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1lmh?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1lmj?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1lmk?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1lmm?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1lmn?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1lmo?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1lmp?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1mqv?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1mqw?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1mqx?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1mqy?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1mqz?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1mr0?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1mr1?f-ed=1');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465726763-img1.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465726764-img2.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465726762-img3.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465726763-img4.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465726763-img5.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465726763-img6.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465726765-img7.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465726765-img8.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465726771-img9.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465726769-img10.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465726764-img11.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465726775-img12.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465726765-img13.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465726767-img14.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465726765-img15.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465726766-img16.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465726772-img17.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465726773-img18.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465726771-img19.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465726777-img20.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465726776-img21.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465726770-img22.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465726770-img23.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465726776-img24.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465726773-img25.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465726769-img26.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465726776-img27.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465726772-img28.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541905-saumon.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465726774-img29.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470918973-saumon.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465726774-img30.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465739699-saumon1.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465739711-saumon2.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465739721-saumon3.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465739724-saumon4.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465739684-saumon5.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465739707-saumon6.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465739700-saumon7.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465739708-saumon8.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465739719-saumon9.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465739723-saumon10.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465739722-saumon11.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465739709-saumon12.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465739703-saumon13.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465739694-saumon14.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465739703-saumon15.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465739725-saumon16.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465739691-saumon17.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465739724-saumon18.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465739700-saumon19.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465739692-saumon20.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465739700-saumon21.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469970837-saumon1.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469970837-saumon2.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469970843-saumon3.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469970848-saumon4.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469970836-saumon5.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469970836-saumon6.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469970851-saumon7.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469970849-saumon8.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469970851-saumon9.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469970842-saumon10.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469970849-saumon11.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469970846-saumon12.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469970842-saumon13.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469970840-saumon14.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469970845-saumon15.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467378677-div1.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467378679-div2.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467378677-div3.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467378678-div4.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467378679-div5.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467378678-div6.png');
    } 
    else if (section == 'Risitas' && (!(document.getElementsByClassName('f-stkrs f-cfx') [0].firstChild) || document.getElementsByClassName('f-stkrs f-cfx') [0].firstChild.firstChild.secondChild.src != 'http://image.noelshack.com/fichiers/2016/26/1467335935-jesus1.png'))
    {
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467335935-jesus1.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469405913-jesus2.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467335935-jesus3.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467335935-jesus4.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/36/1473263674-jesus5.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494264-jesus6.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494265-jesus8.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470920800-jesus9.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470920814-jesus10.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470920799-jesus11.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470920800-jesus12.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470920800-jesus13.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470920800-jesus14.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470920805-jesus15.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470920806-jesus16.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470920801-jesus17.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366189-risitas1.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366191-risitas2.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366191-risitas3.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366681-risitas4.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366189-risitas5.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366188-risitas6.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366195-risitas7.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366192-risitas8.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366195-risitas9.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366197-risitas10.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366197-risitas11.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366196-risitas12.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366200-risitas13.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366203-risitas14.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366200-risitas15.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366202-risitas16.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366202-risitas17.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366203-risitas18.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366344-risitas19.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366202-risitas20.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366203-risitas21.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366206-risitas22.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366207-risitas23.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366209-risitas24.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366209-risitas25.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366209-risitas26.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366210-risitas27.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366214-risitas28.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366214-risitas29.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366666-risitas30.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366218-risitas31.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366212-risitas32.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/36/1473263957-risitas33.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366325-risitas34.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366224-risitas35.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366244-risitas36.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366330-risitas37.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366228-risitas38.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366339-risitas39.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366268-risitas40.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366269-risitas41.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366292-risitas42.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366277-risitas43.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366232-risitas44.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366310-risitas45.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366330-risitas46.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366329-risitas47.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366220-risitas48.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366324-risitas49.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366318-risitas50.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366261-risitas51.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366306-risitas52.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366250-risitas53.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366219-risitas54.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366240-risitas55.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366251-risitas56.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366269-risitas57.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366224-risitas58.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366342-risitas59.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366296-risitas60.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366334-risitas61.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366281-risitas62.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366260-risitas63.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366344-risitas64.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366281-risitas65.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366218-risitas66.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366335-risitas67.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366253-risitas68.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366276-risitas69.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366306-risitas70.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366324-risitas71.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366275-risitas72.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366258-risitas73.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466554357-risitas74.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366344-risitas75.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366299-risitas76.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366257-risitas77.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366265-risitas78.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366349-risitas79.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366232-risitas80.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366584-risitas81.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366586-risitas82.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366667-risitas83.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366587-risitas84.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366588-risitas85.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366597-risitas86.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366594-risitas87.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366594-risitas88.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366592-risitas89.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366601-risitas90.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366598-risitas91.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366596-risitas92.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366605-risitas93.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366602-risitas94.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366603-risitas95.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366609-risitas96.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366606-risitas97.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366609-risitas98.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366609-risitas99.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366611-risitas100.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366611-risitas101.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366614-risitas102.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366615-risitas103.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366614-risitas104.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366619-risitas105.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366614-risitas106.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366616-risitas107.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366618-risitas108.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366618-risitas109.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366621-risitas110.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366628-risitas111.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366621-risitas112.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366696-risitas113.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366637-risitas114.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366648-risitas115.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366648-risitas116.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366664-risitas117.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366677-risitas118.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469405913-risitas119.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366644-risitas120.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366668-risitas121.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366655-risitas123.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467309404-risitas124.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366661-risitas125.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366661-risitas126.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466435859-risitas127.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466715113-risitas128.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466715109-risitas129.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466715106-risitas130.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466715107-risitas131.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466715111-risitas132.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466715113-risitas133.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466715112-risitas134.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466715223-risitas135.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466715116-risitas136.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466715116-risitas137.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466715155-risitas138.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466715116-risitas139.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466715458-risitas140.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466715124-risitas141.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466715120-risitas142.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466715125-risitas143.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466715128-risitas144.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466715125-risitas145.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466715203-risitas147.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466715141-risitas148.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466715156-risitas149.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466715156-risitas150.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466715212-risitas151.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466729105-risitas152.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466729105-risitas153.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466729105-risitas154.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467309317-risitas155.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467309317-risitas156.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467309317-risitas157.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467309320-risitas158.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469405727-risitas159.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469405726-risitas160.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469405726-risitas161.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469405730-risitas162.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469405728-risitas163.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469405727-risitas164.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469405728-risitas165.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469405738-risitas166.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469405729-risitas167.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469405728-risitas168.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469405729-risitas169.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469405730-risitas170.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469405730-risitas171.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469405729-risitas172.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541950-risitas173.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541950-risitas174.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541951-risitas175.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541950-risitas176.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541950-risitas177.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541950-risitas178.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541951-risitas179.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541951-risitas180.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541952-risitas181.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541952-risitas182.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541951-risitas183.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541953-risitas184.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541953-risitas185.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541953-risitas187.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541953-risitas188.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541953-risitas189.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541954-risitas190.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541983-risitas191.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541975-risitas192.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541955-risitas193.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541954-risitas194.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541955-risitas195.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541955-risitas196.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541956-risitas197.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541957-risitas198.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541957-risitas199.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541957-risitas200.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541958-risitas201.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541957-risitas202.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541962-risitas203.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541958-risitas204.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541959-risitas205.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541959-risitas206.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541959-risitas207.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541960-risitas208.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541961-risitas209.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541961-risitas210.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541962-risitas211.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541962-risitas212.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541962-risitas213.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541964-risitas214.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541964-risitas215.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541976-risitas216.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541963-risitas217.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541965-risitas218.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541965-risitas219.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541964-risitas220.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/36/1473264104-risitas221.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541966-risitas222.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541967-risitas223.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541968-risitas224.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541968-risitas225.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541970-risitas226.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541969-risitas227.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541969-risitas228.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541972-risitas229.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541972-risitas230.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541972-risitas231.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541973-risitas232.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469971029-risitas233.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469971030-risitas234.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469971029-risitas235.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469971029-risitas236.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469971030-risitas237.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469971030-risitas238.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469971030-risitas239.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469971031-risitas240.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469971032-risitas241.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469971032-risitas242.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469971032-risitas243.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469971032-risitas244.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469971032-risitas245.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469971032-risitas246.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469971033-risitas247.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469971035-risitas248.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469971034-risitas249.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469971035-risitas250.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469971035-risitas251.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469971035-risitas252.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469971035-risitas253.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469971035-risitas254.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469971057-risitas255.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469971037-risitas256.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469971037-risitas257.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469971038-risitas258.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494335-risitas259.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494333-risitas260.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494337-risitas261.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494334-risitas262.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494332-risitas263.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494339-risitas265.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494341-risitas267.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494343-risitas272.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494344-risitas273.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494345-risitas274.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494345-risitas275.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494347-risitas276.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494349-risitas277.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494347-risitas278.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494348-risitas279.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494353-risitas280.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494367-risitas281.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494481-risitas282.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494366-risitas283.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494361-risitas284.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494491-risitas285.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494356-risitas286.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494359-risitas287.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494488-risitas288.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494478-risitas289.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494474-risitas290.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494464-risitas291.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494358-risitas292.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494392-risitas293.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494493-risitas294.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494353-risitas295.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494496-risitas296.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494483-risitas297.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494495-risitas298.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494359-risitas299.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494355-risitas300.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494480-risitas301.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494485-risitas302.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494365-risitas303.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494378-risitas304.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494363-risitas305.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494470-risitas306.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494371-risitas307.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494477-risitas308.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494372-risitas309.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494388-risitas310.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494500-risitas311.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494355-risitas312.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494491-risitas313.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494431-risitas314.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494545-risitas315.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494349-risitas316.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494380-risitas317.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494487-risitas318.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494475-risitas319.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494496-risitas320.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494414-risitas321.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494370-risitas322.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494363-risitas323.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470920767-risitas324.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470920768-risitas325.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470920767-risitas326.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470920767-risitas327.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470920767-risitas328.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470920767-risitas329.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470920768-risitas330.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470920769-risitas331.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470920769-risitas332.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470920769-risitas333.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470920769-risitas334.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470920769-risitas335.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470920769-risitas336.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470920773-risitas337.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470920783-risitas338.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470920779-risitas339.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470920775-risitas340.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470920771-risitas341.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470920773-risitas342.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470920771-risitas343.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470920774-risitas344.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470920781-risitas345.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470920773-risitas346.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470920771-risitas347.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470920778-risitas348.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470920781-risitas349.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470920781-risitas350.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470920779-risitas351.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470920771-risitas352.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470920781-risitas353.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470920776-risitas354.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470920779-risitas355.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470920783-risitas356.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470920777-risitas357.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470920772-risitas358.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470920774-risitas359.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470920782-risitas360.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470920776-risitas361.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470920798-risitas362.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470920788-risitas363.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470920780-risitas364.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470920782-risitas365.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470920778-risitas366.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470920783-risitas367.png');
    } 
    else if (section == 'Faces' && (!(document.getElementsByClassName('f-stkrs f-cfx') [0].firstChild) || document.getElementsByClassName('f-stkrs f-cfx') [0].firstChild.firstChild.secondChild.src != 'http://image.noelshack.com/fichiers/2016/23/1465684343-1465588408-1465585187628.png'))
    {
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465684343-1465588408-1465585187628.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465683902-gne.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465720822-pornologue.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465687464-hahaha.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465696905-img31.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465687987-lorient1.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465687986-lorient2.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467237308-img.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465739680-img1.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467110206-img1.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467110206-img2.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366803-img1.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366815-img2.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366839-img5.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465693255-img3.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465693007-img9.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465693008-img10.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465693009-img12.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465693973-img13.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465724053-img31.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465725382-img13.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465725383-img14.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465725381-img15.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465725379-img16.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465723887-img2.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467378765-img39.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402313-img13.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402314-img14.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465723887-img1.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465739681-img4.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465757377-img1.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465757376-img2.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465757376-img3.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465757377-img4.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465757376-img6.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465757380-img7.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465739733-img40.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465757420-img9.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465757414-img10.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465757408-img11.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465757383-img12.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465820978-img.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465821566-img.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465821848-img.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465730187-cohn.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465774145-img24.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470919011-img1.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470918991-img2.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470918991-img3.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470918991-img4.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470918991-img5.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470918991-img6.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470918992-img7.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470918993-img8.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470918993-img9.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470918993-img10.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470918993-img11.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470918993-img12.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470918994-img13.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470918995-img14.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470918998-img15.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470918999-img16.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470918996-img17.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470918999-img18.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470919000-img19.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470919001-img20.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470919003-img21.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470919003-img22.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470919003-img23.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470919005-img24.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470919041-img29.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470919016-img41.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470919014-img42.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470919038-img43.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470919013-img44.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470919031-img45.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470919030-img46.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470919016-img47.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470919019-img51.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470919021-img53.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470919022-img54.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470919022-img55.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470919034-img56.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470919026-img57.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470919033-img58.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470919032-img60.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470919033-img62.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470919032-img64.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470919035-img65.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470920704-img66.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470920705-img67.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470920704-img68.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470920704-img69.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470920704-img70.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470920704-img71.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470920705-img72.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470920705-img73.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470920706-img74.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470920706-img75.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470920706-img76.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470920706-img77.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470920706-img78.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465696904-img21.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465696904-img22.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465696903-img23.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465696906-img30.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465696906-img33.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465772083-img.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465829021-img1.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465829016-img2.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465829018-img3.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465829021-img4.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465829014-img7.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465750035-img4.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465750042-img20.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465750074-img23.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465750045-img26.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465750046-img27.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465750047-img28.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465750048-img31.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465750048-img32.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465750048-img33.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465750047-img34.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465750142-img35.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465746228-caprice3.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465746228-caprice2b.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465746228-caprice1b.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402313-img16.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402316-img19.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467309294-img.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465764639-img2.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465739682-img7.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465739705-img11.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465739702-img13.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465739716-img19.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465739696-img22.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465739719-img23.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465739711-img25.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465739724-img26.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465739717-img28.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465739713-img30.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465739715-img31.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465739691-img32.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465739689-img33.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465739696-img34.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465739692-img35.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465739686-img36.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465739715-img37.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465739712-img38.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465739705-img39.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465739711-img41.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465739706-img44.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465729877-new-file-3-000689.gif');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465731001-soral2.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465843418-img1.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465739694-img42.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465739690-img46.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494453-img2.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465740260-img43.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465752900-img37.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465752900-img38.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465752900-img39.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465752900-img40.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465752902-img41.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465752902-img44.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465752909-img47.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465752904-img48.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465752905-img49.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465752904-img50.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465861345-pf-gg.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465873649-jeand.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465900174-img5.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465900184-img6.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465900178-img7.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465900186-img8.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465900191-img9.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465900271-img27.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465900261-img28.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465900266-img29.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467378689-img30.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465900283-img34.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465754382-img1.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465843407-img2.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465843406-img3.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465699902-img3.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465847273-nelly1.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465847279-nelly2.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465847289-nelly4.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465847293-nelly5.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465847298-nelly6.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465847301-nelly7.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494440-img1.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494405-img6.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494411-img7.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494469-img13.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494425-img14.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494473-img15.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494438-img16.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494460-img17.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494408-img18.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494457-img19.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494395-img20.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494453-img21.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494441-img22.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494398-img23.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494456-img24.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469970935-img1.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469970936-img3.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469970936-img6.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469970936-img7.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465725372-img1.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465725372-img3.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402334-img22.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402329-img23.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541880-img2.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541879-img3.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541879-img4.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470919018-img49.png');
    } 
    else if (section == 'Youtube' && (!(document.getElementsByClassName('f-stkrs f-cfx') [0].firstChild) || document.getElementsByClassName('f-stkrs f-cfx') [0].firstChild.firstChild.secondChild.src != 'http://image.noelshack.com/fichiers/2016/26/1467110205-ibra1.png'))
    {
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467110205-ibra1.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467110204-ibra2.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467110205-ibra3.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467110204-ibra4.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467110204-ibra5.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467110204-ibra6.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467110205-ibra7.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467110205-ibra8.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467110205-ibra9.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467110205-ibra10.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466715154-img.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494449-img4.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465696905-img28.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465696905-img29.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465699906-img5.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465699548-lopez.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465696324-payou.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465696324-zoulman.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469970936-img2.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469970938-img4.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470495118-img3.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494460-img5.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465732180-contentz.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465739689-img21.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465739685-img10.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465739706-img18.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465900255-img23.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465900250-img24.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467309294-img1.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467309294-img2.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467309294-img3.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402312-img7.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402312-img8.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402312-img9.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402312-img10.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402312-img11.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402314-img12.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402319-img15.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541879-img5.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541880-img6.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541880-img7.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541881-img8.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541881-img9.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541881-img10.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541881-img11.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541882-img12.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541882-img13.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541881-img14.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541882-img15.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541882-img16.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541882-img17.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494418-img25.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402327-img18.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469970936-img5.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467049176-lbratv-pouce-bleu.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465750040-img7.png');
    } 
    else if (section == 'Télévision' && (!(document.getElementsByClassName('f-stkrs f-cfx') [0].firstChild) || document.getElementsByClassName('f-stkrs f-cfx') [0].firstChild.firstChild.secondChild.src != 'http://image.noelshack.com/fichiers/2016/25/1466702435-pawn1.png'))
    {
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466702435-pawn1.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466702427-pawn2.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466702396-pawn3.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467237350-pawn4.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466702393-pawn5.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466702392-pawn6.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466702400-pawn7.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466702396-pawn8.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466702415-pawn9.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466702403-pawn10.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466702407-pawn11.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466702410-pawn12.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466702435-pawn13.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466702423-pawn14.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466702416-pawn15.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466702418-pawn16.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466702424-pawn17.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465739699-img15.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465739706-img16.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465739725-img17.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465725382-rick.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465740263-rick1.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465752908-rick1.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465754135-rick2.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465739685-img9.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470919018-img48.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465919394-jamy.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402310-img1.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402310-img2.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402311-img3.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402310-img4.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402310-img5.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465861355-soublyme.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465750037-img10.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465725373-img2.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465725372-img4.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465725371-img5.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465725372-img6.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465720822-ruquier.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465725376-img7.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470919005-img25.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465720821-imgr.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465693005-img1.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465758600-img1.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470919033-img59.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465919346-seagal.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465900217-img10.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465687987-hanou1.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465687986-hanou2.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465739714-img27.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470919005-img28.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470919007-img30.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470919007-img31.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470919007-img32.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470919009-img33.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470919010-img34.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470919009-img35.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470919011-img36.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470919010-img37.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470919011-img38.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470919011-img39.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470919032-img61.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470919019-img50.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470919024-img63.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466425576-img1.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466425576-img2.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466425576-img3.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466425576-img4.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466425575-img5.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466425575-img6.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466425578-img7.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466425578-img8.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466425579-img9.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466425578-img10.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466425578-img11.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466425578-img12.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402314-img17.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402326-img20.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402335-img21.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465728900-img33.png');
    } 
    else if (section == 'Politique' && (!(document.getElementsByClassName('f-stkrs f-cfx') [0].firstChild) || document.getElementsByClassName('f-stkrs f-cfx') [0].firstChild.firstChild.secondChild.src != 'http://image.noelshack.com/fichiers/2016/23/1465725375-img9.png'))
    {
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465725375-img9.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465725376-img10.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465725378-img8.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/21/1464198142-vivaelblancos.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465725386-img11.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465725385-img12.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465732942-valls.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465731617-valls.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465750077-img15.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470919006-img26.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470919010-img27.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465829011-img5.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465829017-img6.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465699914-img4.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465900204-img12.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465693007-img4.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470919011-img40.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465693008-img7.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465739683-img5.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465739686-img8.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465754599-img2.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465693008-img11.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465720819-img6.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465720820-img7.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465693005-img5.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465693007-img6.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465696905-img32.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465693007-img8.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465739680-img3.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465693975-img15.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465739780-img47.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465900210-img15.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465900270-img16.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465900275-img17.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465900255-img18.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465900233-img19.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465900240-img20.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465900226-img21.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465900229-img22.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465735219-erdogan.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465753033-erod.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465696903-img24.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465696904-img25.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465696904-img26.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465696906-img27.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467378776-img31.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467378745-img32.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465750034-img5.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465900202-img14.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465900211-img11.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467250892-sans-titre-2.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465842168-holland1.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465842173-holland2.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465750038-img8.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465687987-chirac.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465900220-img40.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465752901-img42.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465752901-img43.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465900253-img25.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465900276-img26.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465739687-img48.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465739710-img49.png');
    } 
    else if (section == 'Jeux Vidéo' && (!(document.getElementsByClassName('f-stkrs f-cfx') [0].firstChild) || document.getElementsByClassName('f-stkrs f-cfx') [0].firstChild.firstChild.secondChild.src != 'http://image.noelshack.com/fichiers/2016/25/1466769615-sonex-1.png'))
    {
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466769615-sonex-1.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466769615-sonex-2.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466769615-sonex-3.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466769615-sonex-4.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466769615-sonex-5.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466769615-sonex-6.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466769615-sonex-7.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466769615-sonex-8.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466769615-sonex-9.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466773674-10.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466773674-11.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466773674-13.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466773674-14.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466773674-15.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467116126-sonex-16.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467116126-sonex-17.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467370226-sonex-18.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467501013-sonex-19.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467501013-sonex-20.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467370226-objection.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467370226-prends-ca.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467370226-un-instant.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467335792-dva1.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467335792-dva2.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467335793-dva3.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467335793-dva4.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467335794-dva5.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467461524-overwatch1.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467335794-dva7.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467335793-dva8.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467335793-dva9.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467335797-dva10.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467335799-dva11.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467335796-dva12.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467335794-dva13.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467335797-dva14.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467335796-dva15.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467335795-dva16.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467335799-dva17.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467335797-dva18.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467335798-dva19.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467335798-dva20.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467335797-dva21.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467335797-dva22.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467335794-dva23.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467335796-dva24.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467335797-dva25.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467335799-dva26.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467335799-dva27.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467335794-dva28.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467335794-dva29.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402325-overwatch2.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402327-overwatch3.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402329-overwatch4.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402329-overwatch5.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467461524-overwatch2.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467461525-overwatch3.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467461526-overwatch4.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402330-overwatch7.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402331-overwatch8.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467461525-overwatch5.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467461524-overwatch6.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467461528-overwatch7.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467461531-overwatch8.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467461532-overwatch9.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467461530-overwatch10.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467461528-overwatch11.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402329-overwatch6.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467461528-overwatch12.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467461530-overwatch13.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467461531-overwatch14.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467461531-overwatch15.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467461534-overwatch16.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467461534-overwatch17.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467461534-overwatch18.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402323-overwatch1.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467461534-overwatch19.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402332-overwatch9.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467461533-overwatch20.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467461535-overwatch21.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467461538-overwatch22.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467461537-overwatch23.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467461537-overwatch24.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467461538-overwatch25.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467461541-overwatch26.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467461537-overwatch27.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467461539-overwatch28.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467461540-overwatch29.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366819-img3.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1466366816-img4.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465919240-eggman.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465919257-doom-guy.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465919268-doom-guy-ii.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465919277-doom-guy-iii.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470918972-jeu1.png');
    } 
    else if (section == 'Eco+' && (!(document.getElementsByClassName('f-stkrs f-cfx') [0].firstChild) || document.getElementsByClassName('f-stkrs f-cfx') [0].firstChild.firstChild.secondChild.src != 'http://image.noelshack.com/fichiers/2016/26/1467316403-img5.png'))
    {
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467316403-img5.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/26/1467316404-img6.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402330-pic1.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402331-pic2.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402331-pic3.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402332-pic4.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402333-pic5.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402333-pic6.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402333-pic7.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402337-pic8.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402335-pic9.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402335-pic10.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402333-pic11.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402336-pic12.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402335-pic13.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402336-pic14.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402336-pic15.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402336-pic16.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402336-pic17.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402336-pic18.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402336-pic19.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402337-pic20.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402337-pic22.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402337-pic23.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402337-pic24.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402337-pic25.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402337-pic26.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402338-pic27.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402337-pic28.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402337-pic29.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402337-pic30.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402338-pic31.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402338-pic32.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402338-pic33.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402339-pic34.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402341-pic35.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402340-pic36.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402344-pic37.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402342-pic38.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402339-pic39.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402341-pic40.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402340-pic41.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402341-pic42.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402341-pic43.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402342-pic44.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402342-pic45.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402341-pic46.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402342-pic48.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402382-pic49.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402377-pic50.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402380-pic51.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402378-pic52.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402381-pic53.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402383-pic54.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402377-pic55.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402378-pic56.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402379-pic57.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402382-pic58.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402383-pic59.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402380-pic60.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541876-eco1.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541876-eco2.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541877-eco3.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541876-eco4.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541877-eco5.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541877-eco6.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541877-eco7.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541877-eco8.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541877-eco9.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541877-eco10.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541877-eco11.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541877-eco12.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541877-eco13.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541878-eco14.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541877-eco15.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541878-eco16.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541881-eco17.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541879-eco18.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469970962-eco.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469970962-eco2.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469970961-eco3.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469970961-eco4.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469970961-eco5.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469970961-eco6.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469970961-eco7.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494399-eco1.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494449-eco2.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494399-eco3.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494433-eco4.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470918976-eco1.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470918976-eco2.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470918972-eco3.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470918972-eco4.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470918973-eco5.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470918972-eco6.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470918976-eco7.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469970962-smiley1.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469970962-smiley2.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469970962-smiley3.png');
    } 
    else if (section == 'Smiley' && (!(document.getElementsByClassName('f-stkrs f-cfx') [0].firstChild) || document.getElementsByClassName('f-stkrs f-cfx') [0].firstChild.firstChild.secondChild.src != 'http://image.noelshack.com/fichiers/2016/30/1469402383-smiley1.png'))
    {
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402383-smiley1.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402383-smiley2.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402384-smiley3.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402383-smiley4.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402391-smiley5.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402385-smiley6.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402390-smiley7.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402389-smiley8.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402390-smiley9.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402385-smiley10.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402390-smiley11.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402387-smiley12.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402389-smiley13.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402391-smiley14.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402387-smiley15.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402389-smiley16.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402389-smiley17.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402387-smiley18.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402385-smiley19.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402391-smiley20.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465728885-img31.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541887-smiley1.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541888-smiley2.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541889-smiley3.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541888-smiley4.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541889-smiley5.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541889-smiley6.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541889-smiley7.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541897-smiley8.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541893-smiley9.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541893-smiley10.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541892-smiley11.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541895-smiley12.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541895-smiley13.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541892-smiley14.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541897-smiley15.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541895-smiley16.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541894-smiley17.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541898-smiley18.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541897-smiley19.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541892-smiley20.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541891-smiley21.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541896-smiley22.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541897-smiley23.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541896-smiley24.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541898-smiley25.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541894-smiley26.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541891-smiley27.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541891-smiley28.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541892-smiley29.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541898-smiley30.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541895-smiley31.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541897-smiley32.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541898-smiley33.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541897-smiley34.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541891-smiley35.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541893-smiley36.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541896-smiley37.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541894-smiley38.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541893-smiley39.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541893-smiley40.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541890-smiley41.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541895-smiley42.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541898-smiley43.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541893-smiley44.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541894-smiley45.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541891-smiley46.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541889-smiley47.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541892-smiley48.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541894-smiley49.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541897-smiley50.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541893-smiley51.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541890-smiley52.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541891-smiley53.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541890-smiley54.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541896-smiley55.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541895-smiley56.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541894-smiley57.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465723888-jpp.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465750043-img24.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402383-smiley24.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402388-smiley25.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402384-smiley26.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402387-smiley27.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402387-smiley28.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402385-smiley29.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402388-smiley30.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402390-smiley31.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402387-smiley32.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402384-smiley33.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402386-smiley34.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402385-smiley35.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402387-smiley36.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402384-smiley37.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402386-smiley38.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402386-smiley39.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402387-smiley40.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402385-smiley41.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402386-smiley42.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402385-smiley43.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402387-smiley44.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402384-smiley45.png');
    } 
    else if (section == 'Autres' && (!(document.getElementsByClassName('f-stkrs f-cfx') [0].firstChild) || document.getElementsByClassName('f-stkrs f-cfx') [0].firstChild.firstChild.secondChild.src != 'http://image.noelshack.com/fichiers/2016/25/1466769615-sonex-1.png'))
    {
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465696906-img34.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/18/1462724927-pornologue-signal.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465750046-img30.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465720816-img1.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465720818-img2.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465723927-img6.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465723932-img5.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465723983-img4.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465728874-gilbert1.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465728875-gilbert2.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465739680-hummer.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465919298-grantekar.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465750034-img1.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465750035-img2.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465750035-img3.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402338-pic21.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465750044-img21.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465750045-img22.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465750036-img6.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465750037-img9.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465752900-img36.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470918971-autre.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465752902-img45.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465752903-img46.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465900207-img13.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465900202-img36.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465900268-img37.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465900235-img38.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465900213-img33.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465757376-img5.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465860819-img2.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465917054-img1.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465917054-img2.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465917054-img3.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402321-img24.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469402311-img6.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/30/1469541878-img1.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494429-autre.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494517-img8.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494444-img10.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/31/1470494451-img12.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470919020-img52.png');
    } 
    else if (section == 'Rex Ryder' && (!(document.getElementsByClassName('f-stkrs f-cfx') [0].firstChild) || document.getElementsByClassName('f-stkrs f-cfx') [0].firstChild.firstChild.secondChild.src != 'http://image.noelshack.com/fichiers/2016/23/1465696906-img34.png'))
    {
      ajouterSticker('http://jv.stkr.fr/p/1lm9?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1lma?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1lmb?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1lmc?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1lmd?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1lme?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1lmf?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1lmg?f-ed=1');
    } 
    else if (section == 'Bud' && (!(document.getElementsByClassName('f-stkrs f-cfx') [0].firstChild) || document.getElementsByClassName('f-stkrs f-cfx') [0].firstChild.firstChild.secondChild.src != 'http://jv.stkr.fr/p/zuc?f-ed=1'))
    {
      ajouterSticker('http://jv.stkr.fr/p/zuc?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/zu2?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/zu6?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/zu7?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/zu8?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/zu9?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/zua?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/zub?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1f88?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1f89?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1f8a?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1f8b?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1f8c?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1f8d?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1f8e?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1f8f?f-ed=1');
    } 
    else if (section == 'Grukk' && (!(document.getElementsByClassName('f-stkrs f-cfx') [0].firstChild) || document.getElementsByClassName('f-stkrs f-cfx') [0].firstChild.firstChild.secondChild.src != 'http://jv.stkr.fr/p/1lgg?f-ed=1'))
    {
      ajouterSticker('http://jv.stkr.fr/p/1lgg?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1lgb?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1lga?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1lgc?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1lgd?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1lge?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1lgf?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1lgh?f-ed=1');
    } 
    else if (section == 'Lamasticot' && (!(document.getElementsByClassName('f-stkrs f-cfx') [0].firstChild) || document.getElementsByClassName('f-stkrs f-cfx') [0].firstChild.firstChild.secondChild.src != 'http://jv.stkr.fr/p/1kgu?f-ed=1'))
    {
      ajouterSticker('http://jv.stkr.fr/p/1kgu?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1kgw?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1kgv?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1kgz?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1kh0?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1kgx?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1kgy?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1kh1?f-ed=1');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465720820-lamasticot1.png');
    } 
    else if (section == 'Volt le Chien' && (!(document.getElementsByClassName('f-stkrs f-cfx') [0].firstChild) || document.getElementsByClassName('f-stkrs f-cfx') [0].firstChild.firstChild.secondChild.src != 'http://jv.stkr.fr/p/1kgu?f-ed=1'))
    {
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465779567-volt1.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465779560-volt2.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465779560-volt3.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465779559-volt4.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465779555-volt5.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465779555-volt6.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465779562-volt7.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465779561-volt8.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465779565-volt9.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465779566-volt10.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465779566-volt11.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465946107-volt1.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465946107-volt2.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465946108-volt3.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465946106-volt4.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465946107-volt5.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465946115-volt6.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465946117-volt7.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465946112-volt8.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465946117-volt9.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465946118-volt10.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465946113-volt11.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465946116-volt12.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465946116-volt13.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465946114-volt14.png');
    } 
    else if (section == 'Fluffy' && (!(document.getElementsByClassName('f-stkrs f-cfx') [0].firstChild) || document.getElementsByClassName('f-stkrs f-cfx') [0].firstChild.firstChild.secondChild.src != 'http://jv.stkr.fr/p/1kl6?f-ed=1'))
    {
      ajouterSticker('http://jv.stkr.fr/p/1kl6?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1kky?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1kl0?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1kl3?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1kl1?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1klb?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1kla?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1kl2?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1kkz?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1kl4?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1kl5?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1kl7?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1kl8?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1kl9?f-ed=1');
    } 
    else if (section == 'Store' && (!(document.getElementsByClassName('f-stkrs f-cfx') [0].firstChild) || document.getElementsByClassName('f-stkrs f-cfx') [0].firstChild.firstChild.secondChild.src != 'http://jv.stkr.fr/p/1n2c?f-ed=1'))
    {
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466715681-store11.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466715399-store3.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466715366-store5.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466715405-store12.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466715398-store7.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466715395-store4.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466715397-store1.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466715425-store10.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466715397-store6.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466715410-store2.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466717056-store13.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466717059-store14.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466717056-store15.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466717055-store16.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466717055-store17.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466717055-store18.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466717056-store19.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466717058-store20.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466717057-store21.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466717057-store22.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466717058-store23.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466715419-store8.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466715401-store9.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466715434-store20.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465739721-store.png');
    } 
    else if (section == 'Anime' && (!(document.getElementsByClassName('f-stkrs f-cfx') [0].firstChild) || document.getElementsByClassName('f-stkrs f-cfx') [0].firstChild.firstChild.secondChild.src != 'http://image.noelshack.com/fichiers/2016/24/1465772083-img1.png'))
    {
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465772083-img1.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465772085-img2.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465772084-img3.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465772084-img4.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465772084-img5.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465772085-img6.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465772085-img7.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465772085-img8.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465772086-img9.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465772086-img10.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465772087-img11.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465772088-img13.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465772088-img14.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465772088-img15.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465772089-img16.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465772089-img17.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465772089-img18.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465772089-img19.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465772089-img20.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465772089-img21.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465772090-img22.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465772090-img23.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470918971-anime1.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470918971-anime2.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470918971-anime3.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470918971-anime4.png');
ajouterSticker('http://image.noelshack.com/fichiers/2016/32/1470918971-anime5.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465833248-sacha1.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465831804-buzz4.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465831798-buzz3.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465831793-buzz2.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465831786-buzz1.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465832373-pierre1.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465832381-pierre2.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/24/1465860819-img1.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466554153-img1.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466554153-img2.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466554153-img3.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466554155-img4.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466554154-img5.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466554154-img6.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466554156-img7.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466554155-img8.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466554156-img9.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466554156-img10.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466554155-img11.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466554155-img12.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/25/1466554155-img13.png');
    } 
    else if (section == 'Football Fans' && (!(document.getElementsByClassName('f-stkrs f-cfx') [0].firstChild) || document.getElementsByClassName('f-stkrs f-cfx') [0].firstChild.firstChild.secondChild.src != 'http://jv.stkr.fr/p/1n1p?f-ed=1'))
    {
      ajouterSticker('http://jv.stkr.fr/p/1n1p?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1n1m?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1n1n?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1n1o?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1n1r?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1n1s?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1n1t?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1n1q?f-ed=1');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465758563-img2.png');
      ajouterSticker('http://jv.stkr.fr/p/1n1p-de?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1n1m-de?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1n1n-de?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1n1o-de?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1n1r-de?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1n1t-de?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1n1q-de?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1n1p-it?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1n1m-it?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1n1n-it?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1n1o-it?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1n1r-it?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1n1t-it?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1n1q-it?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1n1p-es?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1n1m-es?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1n1n-es?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1n1o-es?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1n1r-es?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1n1t-es?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1n1q-es?f-ed=1');
    } 
    else if (section == 'Duracell' && (!(document.getElementsByClassName('f-stkrs f-cfx') [0].firstChild) || document.getElementsByClassName('f-stkrs f-cfx') [0].firstChild.firstChild.secondChild.src != 'http://jv.stkr.fr/p/1li4?f-ed=1'))
    {
      ajouterSticker('http://jv.stkr.fr/p/1li4?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1li3?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1li5?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1lej-fr?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1leb?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1jch?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1jc3-fr?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1leq-fr?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1jc5?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1jcg?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1jcl?f-ed=1');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465750037-img11.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465750040-img12.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465750040-img13.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465750040-img14.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465750042-img16.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465750041-img17.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465750041-img18.png');
      ajouterSticker('http://image.noelshack.com/fichiers/2016/23/1465750042-img19.png');
    } 
    else if (section == 'Xbox One' && (!(document.getElementsByClassName('f-stkrs f-cfx') [0].firstChild) || document.getElementsByClassName('f-stkrs f-cfx') [0].firstChild.firstChild.secondChild.src != 'http://jv.stkr.fr/p/1myf?f-ed=1'))
    {
      ajouterSticker('http://jv.stkr.fr/p/1myf?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1my7?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1myc?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1my9?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1myb?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1my6?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1mye?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1myx?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1myd?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1my4?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1my8?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1mya?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1my5?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1n28?f-ed=1');
    } 
    else if (section == 'X-Men Apocalypse' && (!(document.getElementsByClassName('f-stkrs f-cfx') [0].firstChild) || document.getElementsByClassName('f-stkrs f-cfx') [0].firstChild.firstChild.secondChild.src != 'http://jv.stkr.fr/p/1miq?f-ed=1'))
    {
      ajouterSticker('http://jv.stkr.fr/p/1miq?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1min?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1mim?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1mig-fr?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1mij-fr?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1mio?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1mik?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1mip?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1mif?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1mii-fr?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1mih-fr?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1mil?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1mie-fr?f-ed=1');
      ajouterSticker('http://jv.stkr.fr/p/1mid-fr?f-ed=1');
    }
  }
  function charger(section) {
    var foo = document.getElementsByClassName('f-stkrs f-cfx') [0];
    while (foo.firstChild) {
      foo.removeChild(foo.firstChild);
    }
    var canvas = document.querySelector('div[data-flg-tt="' + section + '"]');
    var numChildren = canvas.parentElement.children.length;
    for (var i = 0; i < numChildren; i++) {
      if (canvas.parentElement.children[i].className.indexOf('f-active') != - 1)
      {
        canvas.parentElement.children[i].className = canvas.parentElement.children[i].className.slice(0, - 9);
        break;
      }
    }
    canvas.className += ' f-active';
    ajouterStickers(section);
  }
  function tArea() {
    document.getElementById('message_topic').removeEventListener('click', tArea);
    check();
  }
  function generateUpdate() {
    if (lastVersion > GM_info.script.version) {
      return '<a href="https://ticki84.github.io/JVCSticker++.user.js" target="_blank">Une nouvelle version de JVCSticker++ est disponible!</a><br><br><p>Version actuelle: '
      + GM_info.script.version + '</p><p>Dernière version: '
      + lastVersion + '</p>';
    } 
    else {
      return '<p>Vous possédez la dernière version de JVCSticker++ (' + GM_info.script.version + ')</p>';
    }
  }
  function generateBanList() {
    var text = '';
    for (var g = 0; g < stickersBanListe.length; g++) {
      text += '<option>' + stickersBanListe[g] + '</option>';
    }
    return text;
  }
  function generateShownCat() {
    var text = '';
    for (var g = 0; g < catToShow.length; g++) {
      var idx = allCat.indexOf(catToShow[g]);
      text += '<option id="' + allCat[idx] + '">' + catRealNames[idx] + '</option>';
    }
    return text;
  }
  function generateHiddenCat() {
    var text = '';
    for (var g = 0; g < allCat.length; g++) {
      if (catToShow.indexOf(allCat[g]) == - 1) {
        text += '<option id="' + allCat[g] + '">' + catRealNames[g] + '</option>';
      }
    }
    return text;
  }
  function chargerBarre() {
    isLoaded = true;
    setTimeout(function () {
      var canvasPop = document.querySelector('div[data-flg-tt="Populaires"]').parentElement;
      while (canvasPop.firstChild) {
        canvasPop.removeChild(canvasPop.firstChild);
      }
      var populaires = document.createElement('div');
      populaires.className = 'f-tab f-h f-active';
      populaires.style.width = '25px';
      populaires.style.height = '23px';
      populaires.style.lineHeight = '23px';
      populaires.style.fontSize = '14px';
      populaires.setAttribute('data-flg-tt', 'Populaires');
      populaires.innerHTML = '$<div style="display: none;" class="f-ttw"><div style="top: -26px; left: -9px;" class="f-inner"><div class="f-tt">Populaires</div><div class="f-arr"></div></div></div>';
      populaires.onclick = function () {
        charger('Populaires');
      };
      document.querySelector('div[data-flg-tt="Biblioth&egrave;que"]').parentElement.setAttribute('style', 'display:none !important;height:0px !important;width:0px !important');
      canvasPop.appendChild(populaires);
      charger('Populaires');
      var shownListS = JSON.parse(GM_getValue('catToShow'));
      var listeBanS = JSON.parse(GM_getValue('stickersBanListe'));
      var addOptions = document.createElement('div');
      addOptions.innerHTML = ' <div id="optionsModal" style="display:none;"><div class="modal-header"><h2 class="modal-title" style="color:#d13321; text-align:center;"><u>Options de JVCSticker++</u></h2></div><div class="modal-body"><div style="text-align: center;"><br><br>'
      + generateUpdate() + '<br></div><div class="optCategorie"><table><h3>Prévisualisation</h3><tr><td style="text-align: left;padding-left:10%;"><label for="espacementStickers">Espacement des stickers de la prévisualisation</label></td><td style="text-align: center;"><input type="number" id="espacementStickers" min="5" max="15" style="text-align:center;" value="'
      + GM_getValue('espacementStickers') + '"></td></tr><tr><td style="text-align: left;padding-left:10%;"><label for="tailleFenetre">Taille de la fenêtre (en pixel)</label></td><td style="text-align: center;"><input type="number" id="tailleFenetre" min="75" max="450" style="text-align:center;" value="'
      + GM_getValue('tailleFenetre') + '"></td></tr><tr><td style="text-align: left;padding-left:10%;"><label for="tailleStickers">Taille des stickers (en pixel)</label></td><td style="text-align: center;"><input type="number" id="tailleStickers" min="22" max="88" style="text-align:center;" value="'
      + GM_getValue('tailleStickers') + '"></td></tr><tr><td style="text-align: left;padding-left:10%;"><label for="xBarreIcones">Nombre de ligne(s) d\'icônes</label></td><td style="text-align: center;"><input type="number" id="xBarreIcones" min="1" max="5" style="text-align:center;" value="'
      + GM_getValue('xBarreIcones') + '"></td></tr></table><br></div><div class="optCategorie"><table><h3>Catégories</h3><tr><td style="text-align: left;padding-left:10%;"><select id="afficher" style="width:60%">'
      + generateHiddenCat() + '</select></td><td style="text-align: center;"><button type="button" class="btn btn-default" id="afficherSel">Afficher la catégorie</button></td></tr><tr><td style="text-align: left;padding-left:10%;"><select id="cacher" style="width:60%">'
      + generateShownCat() + '</select></td><td style="text-align: center;"><button type="button" class="btn btn-default" id="cacherSel">Cacher la catégorie</button></td></tr></table><br></div><div class="optCategorie"><table><h3>Options des stickers</h3><tr><td style="text-align: left;padding-left:10%;"><label for="tailleSticker">Taille des stickers (en %)</label></td><td style="text-align: center;"><input type="number" id="tailleSticker" min="50" max="300" style="text-align:center;" value="'
      + GM_getValue('tailleSticker') + '"></td></tr><tr><td style="text-align: left;padding-left:10%;"><label for="stickerCliquable">Stickers cliquables</label></td><td style="text-align: center;"><input type="checkbox" id="stickerCliquable"></td></tr><tr><td style="text-align: left;padding-left:10%;"><label for="sons">Ajouter des sons aux stickers</label></td><td style="text-align: center;"><input type="checkbox" id="sons"></td></tr></table><br></div><div class="optCategorie"><table><h3>Thème général</h3><tr><td style="text-align: left;padding-left:10%;"><label for="modifierCouleurPosts">Modifier la couleur des posts bleus</label></td><td style="text-align: center;"><input type="checkbox" id="modifierCouleurPosts"></td></tr><tr><td style="text-align: left;padding-left:12.5%;"><label for="couleurBackground">Couleur de fond</label></td><td style="text-align: center;"><input type="text" id="couleurBackground" style="text-align:center;" value="'
      + GM_getValue('couleurBackground') + '"></td></tr><tr><td style="text-align: left;padding-left:12.5%;"><label for="couleurBordure">Couleur des bordures</label></td><td style="text-align: center;"><input type="text" id="couleurBordure" style="text-align:center;" value="'
      + GM_getValue('couleurBordure') + '"></td></tr><tr><td style="text-align: left;padding-left:10%;"><label for="supprimerFond">Supprimer le fond blanc des images transparentes</label></td><td style="text-align: center;"><input type="checkbox" id="supprimerFond"></td></tr></table><br></div><div class="optCategorie"><table><h3>Bannissement de stickers</h3><tr><td style="text-align: left;padding-left:10%;"><label for="supprStickersBan">Supprimer les stickers bannis</label></td><td style="text-align: center;"><input type="checkbox" id="supprStickersBan"></td></tr><tr><td style="text-align: left;padding-left:12.5%;"><label for="stickerMessageDeSuppr">Afficher un message pour remplacer les stickers/posts bannis</label></td><td style="text-align: center;"><input type="checkbox" id="stickerMessageDeSuppr"></td></tr><tr><td style="text-align: left;padding-left:12.5%;"><label for="supprDesPosts">Supprimer les posts contenant un sticker banni</label></td><td style="text-align: center;"><input type="checkbox" id="supprDesPosts"></td></tr><tr><td style="text-align: left;padding-left:10%;"><input type="text" id="banStickerName" style="width: 60%"></td><td style="text-align: center;"><button type="button" class="btn btn-default" id="ajouterBan">Ajouter à la liste des stickers bannis</button></td></tr><tr><td style="text-align: left;padding-left:10%;"><select id="listeBan" size="4" style="width:61%">'
      + generateBanList() + '</select></td><td style="text-align: center;"><button type="button" class="btn btn-default" id="supprBan">Supprimer de la liste</button></td></tr></table><br></div><div class="optCategorie"><table><h3>Autres</h3><tr><td style="text-align: left;padding-left:10%;"><label for="webmPlayer">Lecture des webm</label></td><td style="text-align: center;"><input type="checkbox" id="webmPlayer"></td></tr><tr><td style="text-align: left;padding-left:10%;"><label for="youtubePlayer">Lecture des vidéos Youtube</label></td><td style="text-align: center;"><input type="checkbox" id="youtubePlayer"></td></tr><tr><td style="text-align: left;padding-left:10%;"><label for="playerSignature">Jouer les médias des signatures</label></td><td style="text-align: center;"><input type="checkbox" id="playerSignature"></td></tr><tr><td colSpan=\'2\'><button type="button" class="btn btn-default" id="resetAll">Réinitialiser toutes les options par défaut</button></td></tr></table><br></div></div><div class="modal-footer" style="text-align: center;"><button type="button" class="btn btn-default" data-dismiss="modal" id="valider">Sauvegarder</button> <a rel="modal:close" id="annulerBtn"><button type="button" class="btn btn-default" data-dismiss="modal" id="annuler">Annuler</button></a></div></div><p style="display:none;"><a href="#optionsModal" id="toOptionsModal" rel="modal:open">Open Modal</a></p>';
      document.body.appendChild(addOptions);
      var checkboxList = [
        'stickerCliquable',
        'sons',
        'modifierCouleurPosts',
        'supprimerFond',
        'supprStickersBan',
        'stickerMessageDeSuppr',
        'supprDesPosts',
        'webmPlayer',
        'youtubePlayer',
        'playerSignature'
      ];
      for (var h = 0; h < checkboxList.length; h++) {
        if (GM_getValue(checkboxList[h]) === true)
        {
          document.getElementById(checkboxList[h]).setAttribute('checked', 'checked');
        } 
        else {
          if (document.getElementById(checkboxList[h]).checked == 'checked') {
            document.getElementById(checkboxList[h]).removeAttribute('checked');
          }
        }
      }
      document.getElementById('modifierCouleurPosts').addEventListener('click', function () {
        if (!(document.getElementById('modifierCouleurPosts').checked)) {
          document.getElementById('couleurBackground').parentElement.parentElement.style.display = 'none';
          document.getElementById('couleurBordure').parentElement.parentElement.style.display = 'none';
        } 
        else {
          document.getElementById('couleurBackground').parentElement.parentElement.removeAttribute('style');
          document.getElementById('couleurBordure').parentElement.parentElement.removeAttribute('style');
        }
      });
      document.getElementById('supprStickersBan').addEventListener('click', function () {
        if (!(document.getElementById('supprStickersBan').checked)) {
          document.getElementById('stickerMessageDeSuppr').parentElement.parentElement.style.display = 'none';
          document.getElementById('supprDesPosts').parentElement.parentElement.style.display = 'none';
        } 
        else {
          document.getElementById('stickerMessageDeSuppr').parentElement.parentElement.removeAttribute('style');
          document.getElementById('supprDesPosts').parentElement.parentElement.removeAttribute('style');
        }
      });
      if (!(document.getElementById('modifierCouleurPosts').checked)) {
        document.getElementById('couleurBackground').parentElement.parentElement.style.display = 'none';
        document.getElementById('couleurBordure').parentElement.parentElement.style.display = 'none';
      }
      if (!(document.getElementById('supprStickersBan').checked)) {
        document.getElementById('stickerMessageDeSuppr').parentElement.parentElement.style.display = 'none';
        document.getElementById('supprDesPosts').parentElement.parentElement.style.display = 'none';
      }
      document.getElementById('resetAll').addEventListener('click', function () {
        document.getElementById('espacementStickers').value = '10';
        document.getElementById('tailleFenetre').value = '150';
        document.getElementById('tailleStickers').value = '44';
        document.getElementById('xBarreIcones').value = '2';
        shownListS = [
          'catHap',
          'catNoel',
          'catBridgely',
          'catDomDeVill',
          'catSaumon',
          'catRisitas',
          'catFaces',
          'catYoutube',
          'catTelevision',
          'catPolitique',
          'catJeuxVideo',
          'catEcoPlus',
          'catSmiley',
          'catAutres',
          'catRex',
          'catBud',
          'catGrukk',
          'catLamasticot',
          'catVolt',
          'catFluffy',
          'catStore',
          'catFootball'
        ];
        var newAfficher = '';
        for (var g = 0; g < allCat.length; g++) {
          if (shownListS.indexOf(allCat[g]) == - 1) {
            newAfficher += '<option id="' + allCat[g] + '">' + catRealNames[g] + '</option>';
          }
        }
        document.getElementById('afficher').innerHTML = newAfficher;
        var newCacher = '';
        for (var g = 0; g < shownListS.length; g++) {
          var idxG = allCat.indexOf(shownListS[g]);
          newCacher += '<option id="' + allCat[idxG] + '">' + catRealNames[idxG] + '</option>';
        }
        document.getElementById('cacher').innerHTML = newCacher;
        document.getElementById('tailleSticker').value = '100';
        document.getElementById('stickerCliquable').checked = true;
        document.getElementById('sons').checked = true;
        document.getElementById('modifierCouleurPosts').checked = true;
        document.getElementById('couleurBackground').parentElement.parentElement.removeAttribute('style');
        document.getElementById('couleurBordure').parentElement.parentElement.removeAttribute('style');
        document.getElementById('couleurBackground').value = '#FFF';
        document.getElementById('couleurBordure').value = '#d5d5d5';
        document.getElementById('supprimerFond').checked = true;
        document.getElementById('supprStickersBan').checked = true;
        document.getElementById('stickerMessageDeSuppr').parentElement.parentElement.removeAttribute('style');
        document.getElementById('supprDesPosts').parentElement.parentElement.removeAttribute('style');
        document.getElementById('stickerMessageDeSuppr').checked = true;
        document.getElementById('supprDesPosts').checked = false;
        document.getElementById('banStickerName').value = '';
        listeBanS = [
          'http://jv.stkr.fr/p/1miq',
          'http://jv.stkr.fr/p/1min',
          'http://jv.stkr.fr/p/1mim',
          'http://jv.stkr.fr/p/1mig-fr',
          'http://jv.stkr.fr/p/1mij-fr',
          'http://jv.stkr.fr/p/1mio',
          'http://jv.stkr.fr/p/1mik',
          'http://jv.stkr.fr/p/1mip',
          'http://jv.stkr.fr/p/1mif',
          'http://jv.stkr.fr/p/1mii-fr',
          'http://jv.stkr.fr/p/1mih-fr',
          'http://jv.stkr.fr/p/1mil',
          'http://jv.stkr.fr/p/1mie-fr',
          'http://jv.stkr.fr/p/1mid-fr',
          'http://jv.stkr.fr/p/1myf',
          'http://jv.stkr.fr/p/1my7',
          'http://jv.stkr.fr/p/1myc',
          'http://jv.stkr.fr/p/1my9',
          'http://jv.stkr.fr/p/1myb',
          'http://jv.stkr.fr/p/1my6',
          'http://jv.stkr.fr/p/1mye',
          'http://jv.stkr.fr/p/1myx',
          'http://jv.stkr.fr/p/1myd',
          'http://jv.stkr.fr/p/1my4',
          'http://jv.stkr.fr/p/1my8',
          'http://jv.stkr.fr/p/1mya',
          'http://jv.stkr.fr/p/1my5',
          'http://jv.stkr.fr/p/1n28'
        ];
        var newBan = '';
        for (var g = 0; g < listeBanS.length; g++) {
          newBan += '<option>' + listeBanS[g] + '</option>';
        }
        document.getElementById('listeBan').innerHTML = newBan;
        document.getElementById('webmPlayer').checked = true;
        document.getElementById('youtubePlayer').checked = true;
        document.getElementById('playerSignature').checked = false;
      });
      document.getElementById('afficherSel').addEventListener('click', function () {
        var idx = document.getElementById('afficher').selectedIndex;
        if (typeof document.getElementById('afficher').options[idx] != 'undefined') {
          var nameIdx = catRealNames.indexOf(document.getElementById('afficher').options[idx].text);
          var found = false;
          if (shownListS.length === 0) {
            shownListS.push(allList[nameIdx]);
            found = true;
          }
          if (!(found)) {
            for (var z = 0; z < nameIdx; z++) {
              if (shownListS.indexOf(allCat[nameIdx - z]) != - 1) {
                found = true;
                shownListS.splice(shownListS.indexOf(allCat[nameIdx - z]) + 1, 0, allCat[nameIdx]);
                break;
              }
            }
          }
          if (!(found)) {
            for (var z = 0; z < allCat.length - nameIdx; z++) {
              if (shownListS.indexOf(allCat[nameIdx + z]) != - 1) {
                shownListS.splice(shownListS.indexOf(allCat[nameIdx + z]) + 1, 0, allCat[nameIdx]);
                break;
              }
            }
          }
          var newAfficher = '';
          for (var g = 0; g < allCat.length; g++) {
            if (shownListS.indexOf(allCat[g]) == - 1) {
              newAfficher += '<option id="' + allCat[g] + '">' + catRealNames[g] + '</option>';
            }
          }
          document.getElementById('afficher').innerHTML = newAfficher;
          var nameSave = allCat[catRealNames.indexOf(document.getElementById('cacher').options[document.getElementById('cacher').selectedIndex].text)];
          var newCacher = '';
          for (var g = 0; g < shownListS.length; g++) {
            var idxG = allCat.indexOf(shownListS[g]);
            newCacher += '<option id="' + allCat[idxG] + '">' + catRealNames[idxG] + '</option>';
          }
          document.getElementById('cacher').innerHTML = newCacher;
          document.getElementById('cacher').selectedIndex = shownListS.indexOf(nameSave);
        }
      });
      document.getElementById('cacherSel').addEventListener('click', function () {
        var idx = document.getElementById('cacher').selectedIndex;
        if (typeof document.getElementById('cacher').options[idx] != 'undefined') {
          var name = allCat[catRealNames.indexOf(document.getElementById('cacher').options[idx].text)];
          shownListS.splice(shownListS.indexOf(name), 1);
          var nameSave = allCat[catRealNames.indexOf(document.getElementById('afficher').options[document.getElementById('afficher').selectedIndex].text)];
          var newAfficher = '';
          var tempList = [
          ];
          for (var g = 0; g < allCat.length; g++) {
            if (shownListS.indexOf(allCat[g]) == - 1) {
              tempList.push(allCat[g]);
              newAfficher += '<option id="' + allCat[g] + '">' + catRealNames[g] + '</option>';
            }
          }
          document.getElementById('afficher').innerHTML = newAfficher;
          document.getElementById('afficher').selectedIndex = tempList.indexOf(nameSave);
          var newCacher = '';
          for (var g = 0; g < shownListS.length; g++) {
            var idxG = allCat.indexOf(shownListS[g]);
            newCacher += '<option id="' + allCat[idxG] + '">' + catRealNames[idxG] + '</option>';
          }
          document.getElementById('cacher').innerHTML = newCacher;
        }
      });
      document.getElementById('ajouterBan').addEventListener('click', function () {
        listeBanS.push(document.getElementById('banStickerName').value);
        var text = '';
        for (var g = 0; g < listeBanS.length; g++) {
          text += '<option>' + listeBanS[g] + '</option>';
        }
        document.getElementById('listeBan').innerHTML = text;
        document.getElementById('listeBan').selectedIndex = listeBanS.indexOf(document.getElementById('banStickerName').value);
        document.getElementById('banStickerName').value = '';
      });
      document.getElementById('supprBan').addEventListener('click', function () {
        var idx = document.getElementById('listeBan').selectedIndex;
        if (idx != - 1) {
          listeBanS.splice(idx, 1);
          var text = '';
          for (var g = 0; g < listeBanS.length; g++) {
            text += '<option>' + listeBanS[g] + '</option>';
          }
          document.getElementById('listeBan').innerHTML = text;
          document.getElementById('listeBan').selectedIndex = idx;
          if (document.getElementById('listeBan').selectedIndex == - 1) {
            document.getElementById('listeBan').selectedIndex = idx - 1;
          }
        }
      });
      $('#optionsModal').on('modal:close', function () {
        var textList = [
          'espacementStickers',
          'tailleFenetre',
          'tailleStickers',
          'xBarreIcones',
          'tailleSticker',
          'couleurBackground',
          'couleurBordure'
        ];
        for (var h = 0; h < textList.length; h++) {
          document.getElementById(textList[h]).value = GM_getValue(textList[h]);
        }
        var checkboxList = [
          'stickerCliquable',
          'sons',
          'modifierCouleurPosts',
          'supprimerFond',
          'supprStickersBan',
          'stickerMessageDeSuppr',
          'supprDesPosts',
          'webmPlayer',
          'youtubePlayer',
          'playerSignature'
        ];
        for (var h = 0; h < checkboxList.length; h++) {
          document.getElementById(checkboxList[h]).checked = GM_getValue(checkboxList[h]);
        }
        document.getElementById('banStickerName').value = '';
        shownListS = JSON.parse(GM_getValue('catToShow'));
        var newAfficher = '';
        for (var g = 0; g < allCat.length; g++) {
          if (shownListS.indexOf(allCat[g]) == - 1) {
            newAfficher += '<option id="' + allCat[g] + '">' + catRealNames[g] + '</option>';
          }
        }
        document.getElementById('afficher').innerHTML = newAfficher;
        var newCacher = '';
        for (var g = 0; g < shownListS.length; g++) {
          var idxG = allCat.indexOf(shownListS[g]);
          newCacher += '<option id="' + allCat[idxG] + '">' + catRealNames[idxG] + '</option>';
        }
        document.getElementById('cacher').innerHTML = newCacher;
        listeBanS = JSON.parse(GM_getValue('stickersBanListe'));
        var newBan = '';
        for (var g = 0; g < listeBanS.length; g++) {
          newBan += '<option>' + listeBanS[g] + '</option>';
        }
        document.getElementById('listeBan').innerHTML = newBan;
      });
      document.getElementById('valider').addEventListener('click', function () {
        var optNames = [
          'espacementStickers',
          'tailleFenetre',
          'tailleStickers',
          'xBarreIcones',
          'tailleSticker',
          'couleurBackground',
          'couleurBordure'
        ];
        for (var d = 0; d < optNames.length; d++) {
          GM_setValue(optNames[d], document.getElementById(optNames[d]).value);
        }
        GM_setValue('catToShow', JSON.stringify(shownListS));
        var optCheckbox = [
          'stickerCliquable',
          'sons',
          'modifierCouleurPosts',
          'supprimerFond',
          'supprStickersBan',
          'stickerMessageDeSuppr',
          'supprDesPosts',
          'webmPlayer',
          'youtubePlayer',
          'playerSignature'
        ];
        for (var d = 0; d < optCheckbox.length; d++) {
          if (document.getElementById(optCheckbox[d]).checked === true || document.getElementById(optCheckbox[d]).checked == 'checked') {
            GM_setValue(optCheckbox[d], true);
          } else {
            GM_setValue(optCheckbox[d], false);
          }
        }
        GM_setValue('stickersBanListe', JSON.stringify(listeBanS));
        document.getElementById('annulerBtn').click();
      });
      var canvasHide = document.querySelector('div[data-flg-tt="Biblioth&egrave;que"]').parentElement.parentElement;
      var refresh = document.createElement('div');
      refresh.className = 'f-tabs-r';
      refresh.innerHTML = '<img src="http://image.noelshack.com/fichiers/2016/23/1465690515-refresh.png" height="23" width="25"></img><img src="http://image.noelshack.com/fichiers/2016/24/1465930183-application-x-desktop.png" height="23" width="25"></img>';
      canvasHide.insertBefore(refresh, canvasHide.secondChild);
      refresh.firstChild.addEventListener('click', function () {
        window.open('https://ticki84.github.io/JVCSticker++.user.js', '_blank');
      });
      refresh.children[1].addEventListener('click', function () {
        document.getElementById('toOptionsModal').click();
      });
      canvasPop.style.width = $(canvasPop.parentElement).outerWidth(true) - $(canvasPop.parentElement).children().last().outerWidth(true) + 'px';
      canvasPop.style.height = xBarreIcones * $(canvasPop.parentElement).outerHeight(true) + 'px';
      var fenSticker = document.getElementsByClassName('f-stkrs-w f-mid-fill-h') [0];
      fenSticker.style.height = tailleFenetre + 'px';
      fenSticker.parentElement.style.height = tailleFenetre + 'px';
      fenSticker.parentElement.parentElement.style.height = tailleFenetre + 'px';
      if (supprimerFond) {
        var f = [
        ];
        var x = document.getElementsByClassName('img-shack');
        function createf(i) {
          return function () {
            var z = x[i].alt;
            var newSrc = x[i].src;
            if (z.includes('/fichiers/') && (z.endsWith('.png') || z.endsWith('.gif'))) {
              newSrc = x[i].alt;
            } 
            else if (z.includes('/minis/') && (z.endsWith('.png') || z.endsWith('.gif'))) {
              newSrc = x[i].alt;
              newSrc = newSrc.replace('/minis/', '/fichiers/');
            } 
            else if (x[i].src.includes('/minis/') && (z.endsWith('.png') || z.endsWith('.gif'))) {
              newSrc = newSrc.replace('/minis/', '/fichiers/');
              if (z.endsWith('.gif')) {
                newSrc = newSrc.replace('.png', '.gif');
              }
            } 
            else if (x[i].src.includes('/minis/') && (z.endsWith('.jpg') || z.endsWith('.jpeg'))) {
              newSrc = newSrc.replace('/minis/', '/fichiers/');
              if (z.endsWith('.jpg')) {
                newSrc = newSrc.replace('.png', '.jpg');
              } 
              else if (z.endsWith('.jpeg')) {
                newSrc = newSrc.replace('.png', '.jpeg');
              }
            }
            var newImg = new Image();
            newImg.addEventListener('load', function () {
              if (this.width / this.height > 4 / 3 * 0.99 && this.width / this.height < 4 / 3 * 1.01) {
                if (!stickerCliquable) {
                  x[i].parentElement.style.display = 'none';
                  x[i].parentElement.parentElement.insertBefore(x[i], x[i].parentElement);
                }
                x[i].src = this.src;
              }
            });
            newImg.src = newSrc;
          };
        }
        for (var i = 0; i < x.length; i++) {
          f[i] = createf(i);
          f[i]();
        }
        if (tailleSticker != 100) {
          var y = document.getElementsByClassName('img-stickers');
          for (var i = 0; i < y.length; i++)
          {
            y[i].width = y[i].width * tailleSticker / 100;
            y[i].height = y[i].height * tailleSticker / 100;
            if (!(y[i].className.includes('stickersM'))) {
              y[i].className += ' stickersM';
            }
          }
        }
      }
      if (!supprimerFond && tailleSticker != 100) {
        var x = document.getElementsByClassName('img-shack');
        for (var i = 0; i < x.length; i++)
        {
          x[i].width = x[i].width * tailleSticker / 100;
          x[i].height = x[i].height * tailleSticker / 100;
        }
        if (tailleSticker != 100) {
          var y = document.getElementsByClassName('img-stickers');
          for (var i = 0; i < y.length; i++)
          {
            y[i].width = y[i].width * tailleSticker / 100;
            y[i].height = y[i].height * tailleSticker / 100;
            if (!(y[i].className.includes('stickersM'))) {
              y[i].className += ' stickersM';
            }
          }
        }
      }
      if (sons) {
        var audioList = [
        ];
        var audioN = 0;
        var imgsList = document.getElementsByClassName('img-shack');
        for (var i = 0; i < imgsList.length; i++) {
          {
            if ($.inArray(imgsList[i].alt, [
              'http://image.noelshack.com/fichiers/2016/24/1466366189-risitas1.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366191-risitas2.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366191-risitas3.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366681-risitas4.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366189-risitas5.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366188-risitas6.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366195-risitas7.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366192-risitas8.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366195-risitas9.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366197-risitas10.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366197-risitas11.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366196-risitas12.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366200-risitas13.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366203-risitas14.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366200-risitas15.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366202-risitas16.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366202-risitas17.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366203-risitas18.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366344-risitas19.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366202-risitas20.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366203-risitas21.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366206-risitas22.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366207-risitas23.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366209-risitas24.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366209-risitas25.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366209-risitas26.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366210-risitas27.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366214-risitas28.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366214-risitas29.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366666-risitas30.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366218-risitas31.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366212-risitas32.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366325-risitas34.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366224-risitas35.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366244-risitas36.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366330-risitas37.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366339-risitas39.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366268-risitas40.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366292-risitas42.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366277-risitas43.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366232-risitas44.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366310-risitas45.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366329-risitas47.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366220-risitas48.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366324-risitas49.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366318-risitas50.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366250-risitas53.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366219-risitas54.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366240-risitas55.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366251-risitas56.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366269-risitas57.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366224-risitas58.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366342-risitas59.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366296-risitas60.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366334-risitas61.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366281-risitas62.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366260-risitas63.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366344-risitas64.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366281-risitas65.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366218-risitas66.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366276-risitas69.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366324-risitas71.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366258-risitas73.png',
              'http://image.noelshack.com/fichiers/2016/25/1466554357-risitas74.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366344-risitas75.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366299-risitas76.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366257-risitas77.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366265-risitas78.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366349-risitas79.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366232-risitas80.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366584-risitas81.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366586-risitas82.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366667-risitas83.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366587-risitas84.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366588-risitas85.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366597-risitas86.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366594-risitas87.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366594-risitas88.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366592-risitas89.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366601-risitas90.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366598-risitas91.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366596-risitas92.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366605-risitas93.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366602-risitas94.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366603-risitas95.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366609-risitas96.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366606-risitas97.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366609-risitas98.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366611-risitas100.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366611-risitas101.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366614-risitas102.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366615-risitas103.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366614-risitas104.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366619-risitas105.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366614-risitas106.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366616-risitas107.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366618-risitas109.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366621-risitas110.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366628-risitas111.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366696-risitas113.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366637-risitas114.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366648-risitas115.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366664-risitas117.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366677-risitas118.png',
              'http://image.noelshack.com/fichiers/2016/30/1469405913-risitas119.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366668-risitas121.png',
              'http://image.noelshack.com/fichiers/2016/26/1467309404-risitas124.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366661-risitas125.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366661-risitas126.png',
              'http://image.noelshack.com/fichiers/2016/25/1466715113-risitas128.png',
              'http://image.noelshack.com/fichiers/2016/25/1466715109-risitas129.png',
              'http://image.noelshack.com/fichiers/2016/25/1466715106-risitas130.png',
              'http://image.noelshack.com/fichiers/2016/25/1466715107-risitas131.png',
              'http://image.noelshack.com/fichiers/2016/25/1466715113-risitas133.png',
              'http://image.noelshack.com/fichiers/2016/25/1466715112-risitas134.png',
              'http://image.noelshack.com/fichiers/2016/25/1466715223-risitas135.png',
              'http://image.noelshack.com/fichiers/2016/25/1466715116-risitas137.png',
              'http://image.noelshack.com/fichiers/2016/25/1466715155-risitas138.png',
              'http://image.noelshack.com/fichiers/2016/25/1466715458-risitas140.png',
              'http://image.noelshack.com/fichiers/2016/25/1466715124-risitas141.png',
              'http://image.noelshack.com/fichiers/2016/25/1466715120-risitas142.png',
              'http://image.noelshack.com/fichiers/2016/25/1466715125-risitas143.png',
              'http://image.noelshack.com/fichiers/2016/25/1466715128-risitas144.png',
              'http://image.noelshack.com/fichiers/2016/25/1466715125-risitas145.png',
              'http://image.noelshack.com/fichiers/2016/25/1466715203-risitas147.png',
              'http://image.noelshack.com/fichiers/2016/25/1466715141-risitas148.png',
              'http://image.noelshack.com/fichiers/2016/25/1466715156-risitas149.png',
              'http://image.noelshack.com/fichiers/2016/25/1466715156-risitas150.png',
              'http://image.noelshack.com/fichiers/2016/25/1466715212-risitas151.png',
              'http://image.noelshack.com/fichiers/2016/25/1466729105-risitas152.png',
              'http://image.noelshack.com/fichiers/2016/25/1466729105-risitas153.png',
              'http://image.noelshack.com/fichiers/2016/25/1466729105-risitas154.png',
              'http://image.noelshack.com/fichiers/2016/26/1467309317-risitas155.png',
              'http://image.noelshack.com/fichiers/2016/26/1467309317-risitas156.png',
              'http://image.noelshack.com/fichiers/2016/26/1467309317-risitas157.png',
              'http://image.noelshack.com/fichiers/2016/30/1469405727-risitas159.png',
              'http://image.noelshack.com/fichiers/2016/30/1469405726-risitas160.png',
              'http://image.noelshack.com/fichiers/2016/30/1469405726-risitas161.png',
              'http://image.noelshack.com/fichiers/2016/30/1469405728-risitas165.png',
              'http://image.noelshack.com/fichiers/2016/30/1469541951-risitas180.png',
              'http://image.noelshack.com/fichiers/2016/30/1469541952-risitas182.png',
              'http://image.noelshack.com/fichiers/2016/30/1469541983-risitas191.png',
              'http://image.noelshack.com/fichiers/2016/30/1469541975-risitas192.png',
              'http://image.noelshack.com/fichiers/2016/30/1469541956-risitas197.png',
              'http://image.noelshack.com/fichiers/2016/30/1469541957-risitas199.png',
              'http://image.noelshack.com/fichiers/2016/30/1469541957-risitas202.png',
              'http://image.noelshack.com/fichiers/2016/30/1469541961-risitas210.png',
              'http://image.noelshack.com/fichiers/2016/30/1469541962-risitas211.png',
              'http://image.noelshack.com/fichiers/2016/30/1469541962-risitas212.png',
              'http://image.noelshack.com/fichiers/2016/30/1469541962-risitas213.png',
              'http://image.noelshack.com/fichiers/2016/30/1469541964-risitas214.png',
              'http://image.noelshack.com/fichiers/2016/30/1469541964-risitas215.png',
              'http://image.noelshack.com/fichiers/2016/30/1469541963-risitas217.png',
              'http://image.noelshack.com/fichiers/2016/30/1469541965-risitas219.png',
              'http://image.noelshack.com/fichiers/2016/30/1469541964-risitas220.png',
              'http://image.noelshack.com/fichiers/2016/36/1473264104-risitas221.png',
              'http://image.noelshack.com/fichiers/2016/30/1469541972-risitas229.png',
              'http://image.noelshack.com/fichiers/2016/30/1469541972-risitas230.png',
              'http://image.noelshack.com/fichiers/2016/30/1469541972-risitas231.png',
              'http://image.noelshack.com/fichiers/2016/30/1469971029-risitas233.png',
              'http://image.noelshack.com/fichiers/2016/30/1469971029-risitas235.png',
              'http://image.noelshack.com/fichiers/2016/30/1469971029-risitas236.png',
              'http://image.noelshack.com/fichiers/2016/30/1469971030-risitas238.png',
              'http://image.noelshack.com/fichiers/2016/30/1469971030-risitas239.png',
              'http://image.noelshack.com/fichiers/2016/30/1469971031-risitas240.png',
              'http://image.noelshack.com/fichiers/2016/30/1469971032-risitas241.png',
              'http://image.noelshack.com/fichiers/2016/30/1469971032-risitas242.png',
              'http://image.noelshack.com/fichiers/2016/30/1469971032-risitas243.png',
              'http://image.noelshack.com/fichiers/2016/30/1469971032-risitas244.png',
              'http://image.noelshack.com/fichiers/2016/30/1469971035-risitas248.png',
              'http://image.noelshack.com/fichiers/2016/31/1470494333-risitas260.png',
              'http://image.noelshack.com/fichiers/2016/31/1470494337-risitas261.png',
              'http://image.noelshack.com/fichiers/2016/31/1470494334-risitas262.png',
              'http://image.noelshack.com/fichiers/2016/31/1470494332-risitas263.png',
              'http://image.noelshack.com/fichiers/2016/31/1470494341-risitas267.png',
              'http://image.noelshack.com/fichiers/2016/31/1470494345-risitas274.png',
              'http://image.noelshack.com/fichiers/2016/31/1470494345-risitas275.png',
              'http://image.noelshack.com/fichiers/2016/31/1470494347-risitas276.png',
              'http://image.noelshack.com/fichiers/2016/31/1470494366-risitas283.png',
              'http://image.noelshack.com/fichiers/2016/31/1470494361-risitas284.png',
              'http://image.noelshack.com/fichiers/2016/31/1470494491-risitas285.png',
              'http://image.noelshack.com/fichiers/2016/31/1470494356-risitas286.png',
              'http://image.noelshack.com/fichiers/2016/31/1470494464-risitas291.png',
              'http://image.noelshack.com/fichiers/2016/31/1470494358-risitas292.png',
              'http://image.noelshack.com/fichiers/2016/31/1470494483-risitas297.png',
              'http://image.noelshack.com/fichiers/2016/31/1470494359-risitas299.png',
              'http://image.noelshack.com/fichiers/2016/31/1470494480-risitas301.png',
              'http://image.noelshack.com/fichiers/2016/31/1470494378-risitas304.png',
              'http://image.noelshack.com/fichiers/2016/31/1470494470-risitas306.png',
              'http://image.noelshack.com/fichiers/2016/31/1470494371-risitas307.png',
              'http://image.noelshack.com/fichiers/2016/31/1470494372-risitas309.png',
              'http://image.noelshack.com/fichiers/2016/31/1470494500-risitas311.png',
              'http://image.noelshack.com/fichiers/2016/31/1470494355-risitas312.png',
              'http://image.noelshack.com/fichiers/2016/31/1470494431-risitas314.png',
              'http://image.noelshack.com/fichiers/2016/31/1470494545-risitas315.png',
              'http://image.noelshack.com/fichiers/2016/31/1470494496-risitas320.png',
              'http://image.noelshack.com/fichiers/2016/31/1470494414-risitas321.png'
            ]) > - 1) {
              imgsList[i].addEventListener('mouseover', function () {
                audioList.push(new Audio('http://jscore.comli.com/risitas' + Math.floor((Math.random() * 5) + 1) + '.ogg'));
                audioList[audioN].play();
                audioN++;
              });
              imgsList[i].addEventListener('mouseout', function () {
                audioList[audioN - 1].pause();
              });
            } 
            else if ($.inArray(imgsList[i].alt, [
              'http://image.noelshack.com/fichiers/2016/36/1473263957-risitas33.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366228-risitas38.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366269-risitas41.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366330-risitas46.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366261-risitas51.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366306-risitas52.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366335-risitas67.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366253-risitas68.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366306-risitas70.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366275-risitas72.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366609-risitas99.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366618-risitas108.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366621-risitas112.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366648-risitas116.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366644-risitas120.png',
              'http://image.noelshack.com/fichiers/2016/24/1466366655-risitas123.png',
              'http://image.noelshack.com/fichiers/2016/25/1466435859-risitas127.png',
              'http://image.noelshack.com/fichiers/2016/25/1466715111-risitas132.png',
              'http://image.noelshack.com/fichiers/2016/25/1466715116-risitas136.png',
              'http://image.noelshack.com/fichiers/2016/25/1466715116-risitas139.png',
              'http://image.noelshack.com/fichiers/2016/30/1469405730-risitas162.png',
              'http://image.noelshack.com/fichiers/2016/30/1469405728-risitas163.png',
              'http://image.noelshack.com/fichiers/2016/30/1469405727-risitas164.png',
              'http://image.noelshack.com/fichiers/2016/30/1469541950-risitas174.png',
              'http://image.noelshack.com/fichiers/2016/30/1469541951-risitas175.png',
              'http://image.noelshack.com/fichiers/2016/30/1469541950-risitas176.png',
              'http://image.noelshack.com/fichiers/2016/30/1469541950-risitas177.png',
              'http://image.noelshack.com/fichiers/2016/30/1469541950-risitas178.png',
              'http://image.noelshack.com/fichiers/2016/30/1469541951-risitas179.png',
              'http://image.noelshack.com/fichiers/2016/30/1469541953-risitas185.png',
              'http://image.noelshack.com/fichiers/2016/30/1469541953-risitas189.png',
              'http://image.noelshack.com/fichiers/2016/30/1469541954-risitas190.png',
              'http://image.noelshack.com/fichiers/2016/30/1469541955-risitas193.png',
              'http://image.noelshack.com/fichiers/2016/30/1469541954-risitas194.png',
              'http://image.noelshack.com/fichiers/2016/30/1469541955-risitas195.png',
              'http://image.noelshack.com/fichiers/2016/30/1469541955-risitas196.png',
              'http://image.noelshack.com/fichiers/2016/30/1469541957-risitas198.png',
              'http://image.noelshack.com/fichiers/2016/30/1469541958-risitas201.png',
              'http://image.noelshack.com/fichiers/2016/30/1469541959-risitas205.png',
              'http://image.noelshack.com/fichiers/2016/30/1469541959-risitas206.png',
              'http://image.noelshack.com/fichiers/2016/30/1469541959-risitas207.png',
              'http://image.noelshack.com/fichiers/2016/30/1469541960-risitas208.png',
              'http://image.noelshack.com/fichiers/2016/30/1469541961-risitas209.png',
              'http://image.noelshack.com/fichiers/2016/30/1469541965-risitas218.png',
              'http://image.noelshack.com/fichiers/2016/30/1469541966-risitas222.png',
              'http://image.noelshack.com/fichiers/2016/30/1469541967-risitas223.png',
              'http://image.noelshack.com/fichiers/2016/30/1469541968-risitas224.png',
              'http://image.noelshack.com/fichiers/2016/30/1469541968-risitas225.png',
              'http://image.noelshack.com/fichiers/2016/30/1469541970-risitas226.png',
              'http://image.noelshack.com/fichiers/2016/30/1469541969-risitas227.png',
              'http://image.noelshack.com/fichiers/2016/30/1469541969-risitas228.png',
              'http://image.noelshack.com/fichiers/2016/30/1469541973-risitas232.png',
              'http://image.noelshack.com/fichiers/2016/30/1469971030-risitas234.png',
              'http://image.noelshack.com/fichiers/2016/30/1469971030-risitas237.png',
              'http://image.noelshack.com/fichiers/2016/30/1469971032-risitas245.png',
              'http://image.noelshack.com/fichiers/2016/30/1469971032-risitas246.png',
              'http://image.noelshack.com/fichiers/2016/30/1469971033-risitas247.png',
              'http://image.noelshack.com/fichiers/2016/30/1469971035-risitas250.png',
              'http://image.noelshack.com/fichiers/2016/30/1469971035-risitas251.png',
              'http://image.noelshack.com/fichiers/2016/30/1469971035-risitas252.png',
              'http://image.noelshack.com/fichiers/2016/30/1469971035-risitas253.png',
              'http://image.noelshack.com/fichiers/2016/30/1469971035-risitas254.png',
              'http://image.noelshack.com/fichiers/2016/30/1469971057-risitas255.png',
              'http://image.noelshack.com/fichiers/2016/30/1469971037-risitas256.png',
              'http://image.noelshack.com/fichiers/2016/30/1469971037-risitas257.png',
              'http://image.noelshack.com/fichiers/2016/30/1469971038-risitas258.png',
              'http://image.noelshack.com/fichiers/2016/31/1470494335-risitas259.png',
              'http://image.noelshack.com/fichiers/2016/31/1470494339-risitas265.png',
              'http://image.noelshack.com/fichiers/2016/31/1470494349-risitas277.png',
              'http://image.noelshack.com/fichiers/2016/31/1470494347-risitas278.png',
              'http://image.noelshack.com/fichiers/2016/31/1470494353-risitas280.png',
              'http://image.noelshack.com/fichiers/2016/31/1470494481-risitas282.png',
              'http://image.noelshack.com/fichiers/2016/31/1470494359-risitas287.png',
              'http://image.noelshack.com/fichiers/2016/31/1470494488-risitas288.png',
              'http://image.noelshack.com/fichiers/2016/31/1470494478-risitas289.png',
              'http://image.noelshack.com/fichiers/2016/31/1470494474-risitas290.png',
              'http://image.noelshack.com/fichiers/2016/31/1470494392-risitas293.png',
              'http://image.noelshack.com/fichiers/2016/31/1470494495-risitas298.png',
              'http://image.noelshack.com/fichiers/2016/31/1470494355-risitas300.png',
              'http://image.noelshack.com/fichiers/2016/31/1470494485-risitas302.png',
              'http://image.noelshack.com/fichiers/2016/31/1470494365-risitas303.png',
              'http://image.noelshack.com/fichiers/2016/31/1470494363-risitas305.png',
              'http://image.noelshack.com/fichiers/2016/31/1470494388-risitas310.png',
              'http://image.noelshack.com/fichiers/2016/31/1470494349-risitas316.png',
              'http://image.noelshack.com/fichiers/2016/31/1470494380-risitas317.png',
              'http://image.noelshack.com/fichiers/2016/31/1470494487-risitas318.png',
              'http://image.noelshack.com/fichiers/2016/31/1470494475-risitas319.png',
              'http://image.noelshack.com/fichiers/2016/31/1470494370-risitas322.png',
              'http://image.noelshack.com/fichiers/2016/31/1470494363-risitas323.png'
            ]) > - 1) {
              imgsList[i].addEventListener('mouseover', function () {
                audioList.push(new Audio('http://jscore.comli.com/risitas6.ogg'));
                audioList[audioN].play();
                audioN++;
              });
              imgsList[i].addEventListener('mouseout', function () {
                audioList[audioN - 1].pause();
              });
            } 
            else if ($.inArray(imgsList[i].alt, [
              'http://image.noelshack.com/fichiers/2016/26/1467309320-risitas158.png',
              'http://image.noelshack.com/fichiers/2016/30/1469405738-risitas166.png',
              'http://image.noelshack.com/fichiers/2016/30/1469405729-risitas167.png',
              'http://image.noelshack.com/fichiers/2016/30/1469405728-risitas168.png',
              'http://image.noelshack.com/fichiers/2016/30/1469405729-risitas169.png',
              'http://image.noelshack.com/fichiers/2016/30/1469405730-risitas170.png',
              'http://image.noelshack.com/fichiers/2016/30/1469405730-risitas171.png',
              'http://image.noelshack.com/fichiers/2016/30/1469405729-risitas172.png',
              'http://image.noelshack.com/fichiers/2016/30/1469541950-risitas173.png',
              'http://image.noelshack.com/fichiers/2016/30/1469541952-risitas181.png',
              'http://image.noelshack.com/fichiers/2016/30/1469541951-risitas183.png',
              'http://image.noelshack.com/fichiers/2016/30/1469541953-risitas184.png',
              'http://image.noelshack.com/fichiers/2016/30/1469541953-risitas187.png',
              'http://image.noelshack.com/fichiers/2016/30/1469541953-risitas188.png',
              'http://image.noelshack.com/fichiers/2016/30/1469541957-risitas200.png',
              'http://image.noelshack.com/fichiers/2016/30/1469541962-risitas203.png',
              'http://image.noelshack.com/fichiers/2016/30/1469541958-risitas204.png',
              'http://image.noelshack.com/fichiers/2016/30/1469541976-risitas216.png',
              'http://image.noelshack.com/fichiers/2016/30/1469971034-risitas249.png',
              'http://image.noelshack.com/fichiers/2016/31/1470494343-risitas272.png',
              'http://image.noelshack.com/fichiers/2016/31/1470494344-risitas273.png',
              'http://image.noelshack.com/fichiers/2016/31/1470494348-risitas279.png',
              'http://image.noelshack.com/fichiers/2016/31/1470494367-risitas281.png',
              'http://image.noelshack.com/fichiers/2016/31/1470494493-risitas294.png',
              'http://image.noelshack.com/fichiers/2016/31/1470494353-risitas295.png',
              'http://image.noelshack.com/fichiers/2016/31/1470494496-risitas296.png',
              'http://image.noelshack.com/fichiers/2016/31/1470494477-risitas308.png',
              'http://image.noelshack.com/fichiers/2016/31/1470494491-risitas313.png'
            ]) > - 1) {
              imgsList[i].addEventListener('mouseover', function () {
                audioList.push(new Audio('http://jscore.comli.com/risitas' + Math.floor((Math.random() * 4) + 7) + '.ogg'));
                audioList[audioN].play();
                audioN++;
              });
              imgsList[i].addEventListener('mouseout', function () {
                audioList[audioN - 1].pause();
              });
            }
            else if ($.inArray(imgsList[i].alt, [
              'http://image.noelshack.com/fichiers/2016/26/1467335935-jesus1.png',
              'http://image.noelshack.com/fichiers/2016/30/1469405913-jesus2.png',
              'http://image.noelshack.com/fichiers/2016/26/1467335935-jesus3.png',
              'http://image.noelshack.com/fichiers/2016/26/1467335935-jesus4.png',
              'http://image.noelshack.com/fichiers/2016/36/1473263674-jesus5.png',
              'http://image.noelshack.com/fichiers/2016/31/1470494264-jesus6.png',
              'http://image.noelshack.com/fichiers/2016/31/1470494265-jesus8.png',
              'http://image.noelshack.com/fichiers/2016/32/1470920800-jesus9.png',
              'http://image.noelshack.com/fichiers/2016/32/1470920814-jesus10.png',
              'http://image.noelshack.com/fichiers/2016/32/1470920799-jesus11.png',
              'http://image.noelshack.com/fichiers/2016/32/1470920800-jesus12.png',
              'http://image.noelshack.com/fichiers/2016/32/1470920800-jesus13.png',
              'http://image.noelshack.com/fichiers/2016/32/1470920800-jesus14.png',
              'http://image.noelshack.com/fichiers/2016/32/1470920805-jesus15.png',
              'http://image.noelshack.com/fichiers/2016/32/1470920806-jesus16.png',
              'http://image.noelshack.com/fichiers/2016/32/1470920801-jesus17.png'
            ]) > - 1) {
              imgsList[i].addEventListener('mouseover', function () {
                audioList.push(new Audio('http://jscore.comli.com/jesus1.ogg'));
                audioList[audioN].play();
                audioN++;
              });
              imgsList[i].addEventListener('mouseout', function () {
                audioList[audioN - 1].pause();
              });
            }
            else if ($.inArray(imgsList[i].alt, [
              'http://image.noelshack.com/fichiers/2016/23/1465723887-img2.png',
              'http://image.noelshack.com/fichiers/2016/26/1467378765-img39.png',
              'http://image.noelshack.com/fichiers/2016/30/1469402313-img13.png',
              'http://image.noelshack.com/fichiers/2016/30/1469402314-img14.png'
            ]) > - 1) {
              imgsList[i].addEventListener('mouseover', function () {
                if (Math.floor(Math.random() * 2) == 0) {
                  audioList.push(new Audio('http://jscore.comli.com/finkiel.ogg'));
                } else {
                  audioList.push(new Audio('http://jscore.comli.com/6.ogg'));
                }
                audioList[audioN].play();
                audioN++;
              });
              imgsList[i].addEventListener('mouseout', function () {
                audioList[audioN - 1].pause();
              });
            } 
            else if ($.inArray(imgsList[i].alt, [
              'http://image.noelshack.com/fichiers/2016/30/1469402312-img11.png',
              'http://image.noelshack.com/fichiers/2016/30/1469402314-img12.png',
              'http://image.noelshack.com/fichiers/2016/30/1469402319-img15.png',
              'http://image.noelshack.com/fichiers/2016/30/1469541881-img9.png',
              'http://image.noelshack.com/fichiers/2016/30/1469541882-img13.png',
              'http://image.noelshack.com/fichiers/2016/30/1469541882-img16.png'
            ]) > - 1) {
              imgsList[i].addEventListener('mouseover', function () {
                audioList.push(new Audio('http://jscore.comli.com/kairi' + Math.floor((Math.random() * 2) + 1) + '.ogg'));
                audioList[audioN].play();
                audioN++;
              });
              imgsList[i].addEventListener('mouseout', function () {
                audioList[audioN - 1].pause();
              });
            }
            else if (imgsList[i].alt == 'http://image.noelshack.com/fichiers/2016/30/1469541878-img1.png') {
              imgsList[i].addEventListener('mouseover', function () {
                audioList.push(new Audio('http://jscore.comli.com/succ.ogg'));
                audioList[audioN].play();
                audioN++;
              });
              imgsList[i].addEventListener('mouseout', function () {
                audioList[audioN - 1].pause();
              });
            }
            else if (imgsList[i].alt == 'http://image.noelshack.com/fichiers/2016/23/1465693975-img15.png') {
              imgsList[i].addEventListener('mouseover', function () {
                audioList.push(new Audio('http://jscore.comli.com/1.ogg'));
                audioList[audioN].play();
                audioN++;
              });
              imgsList[i].addEventListener('mouseout', function () {
                audioList[audioN - 1].pause();
              });
            }
            else if ($.inArray(imgsList[i].alt, [
              'http://image.noelshack.com/fichiers/2016/32/1470919031-img45.png',
              'http://image.noelshack.com/fichiers/2016/32/1470919030-img46.png'
            ]) > - 1) {
              imgsList[i].addEventListener('mouseover', function () {
                audioList.push(new Audio('http://jscore.comli.com/2.ogg'));
                audioList[audioN].play();
                audioN++;
              });
              imgsList[i].addEventListener('mouseout', function () {
                audioList[audioN - 1].pause();
              });
            }
            else if (imgsList[i].alt == 'http://image.noelshack.com/fichiers/2016/30/1469402310-img5.png') {
              imgsList[i].addEventListener('mouseover', function () {
                audioList.push(new Audio('http://jscore.comli.com/3.ogg'));
                audioList[audioN].play();
                audioN++;
              });
              imgsList[i].addEventListener('mouseout', function () {
                audioList[audioN - 1].pause();
              });
            }
            else if ($.inArray(imgsList[i].alt, [
              'http://image.noelshack.com/fichiers/2016/25/1466425578-img8.png',
              'http://image.noelshack.com/fichiers/2016/25/1466425579-img9.png'
            ]) > - 1) {
              imgsList[i].addEventListener('mouseover', function () {
                audioList.push(new Audio('http://jscore.comli.com/4.ogg'));
                audioList[audioN].play();
                audioN++;
              });
              imgsList[i].addEventListener('mouseout', function () {
                audioList[audioN - 1].pause();
              });
            }
            else if (imgsList[i].alt == 'http://image.noelshack.com/fichiers/2016/32/1470919013-img44.png') {
              imgsList[i].addEventListener('mouseover', function () {
                audioList.push(new Audio('http://jscore.comli.com/5.ogg'));
                audioList[audioN].play();
                audioN++;
              });
              imgsList[i].addEventListener('mouseout', function () {
                audioList[audioN - 1].pause();
              });
            }
            else if (imgsList[i].alt == 'http://image.noelshack.com/fichiers/2016/26/1467250892-sans-titre-2.png') {
              imgsList[i].addEventListener('mouseover', function () {
                audioList.push(new Audio('http://jscore.comli.com/7.ogg'));
                audioList[audioN].play();
                audioN++;
              });
              imgsList[i].addEventListener('mouseout', function () {
                audioList[audioN - 1].pause();
              });
            }
            else if (imgsList[i].alt == 'http://image.noelshack.com/fichiers/2016/23/1465750045-img22.png') {
              imgsList[i].addEventListener('mouseover', function () {
                audioList.push(new Audio('http://jscore.comli.com/8.ogg'));
                audioList[audioN].play();
                audioN++;
              });
              imgsList[i].addEventListener('mouseout', function () {
                audioList[audioN - 1].pause();
              });
            }
            else if (imgsList[i].alt == 'http://image.noelshack.com/fichiers/2016/21/1464198142-vivaelblancos.png') {
              imgsList[i].addEventListener('mouseover', function () {
                audioList.push(new Audio('http://jscore.comli.com/9.ogg'));
                audioList[audioN].play();
                audioN++;
              });
              imgsList[i].addEventListener('mouseout', function () {
                audioList[audioN - 1].pause();
              });
            }
            else if ($.inArray(imgsList[i].alt, [
              'http://image.noelshack.com/fichiers/2016/24/1465847273-nelly1.png',
      'http://image.noelshack.com/fichiers/2016/24/1465847279-nelly2.png',
      'http://image.noelshack.com/fichiers/2016/24/1465847289-nelly4.png',
      'http://image.noelshack.com/fichiers/2016/24/1465847293-nelly5.png',
      'http://image.noelshack.com/fichiers/2016/24/1465847298-nelly6.png'
            ]) > - 1) {
              imgsList[i].addEventListener('mouseover', function () {
                audioList.push(new Audio('http://jscore.comli.com/10.ogg'));
                audioList[audioN].play();
                audioN++;
              });
              imgsList[i].addEventListener('mouseout', function () {
                audioList[audioN - 1].pause();
              });
            }
            else if ($.inArray(imgsList[i].alt, [
              'http://image.noelshack.com/fichiers/2016/25/1466425576-img2.png',
              'http://image.noelshack.com/fichiers/2016/25/1466425576-img3.png',
              'http://image.noelshack.com/fichiers/2016/25/1466425578-img7.png'
            ]) > - 1) {
              imgsList[i].addEventListener('mouseover', function () {
                audioList.push(new Audio('http://jscore.comli.com/11.ogg'));
                audioList[audioN].play();
                audioN++;
              });
              imgsList[i].addEventListener('mouseout', function () {
                audioList[audioN - 1].pause();
              });
            }
            else if ($.inArray(imgsList[i].alt, [
              'http://image.noelshack.com/fichiers/2016/23/1465696903-img24.png',
              'http://image.noelshack.com/fichiers/2016/23/1465696904-img25.png',
              'http://image.noelshack.com/fichiers/2016/23/1465696904-img26.png',
              'http://image.noelshack.com/fichiers/2016/23/1465696906-img27.png'
            ]) > - 1) {
              imgsList[i].addEventListener('mouseover', function () {
                audioList.push(new Audio('http://jscore.comli.com/12.ogg'));
                audioList[audioN].play();
                audioN++;
              });
              imgsList[i].addEventListener('mouseout', function () {
                audioList[audioN - 1].pause();
              });
            }
            else if (imgsList[i].alt == 'http://image.noelshack.com/fichiers/2016/23/1465750044-img21.png') {
              imgsList[i].addEventListener('mouseover', function () {
                audioList.push(new Audio('http://jscore.comli.com/14.ogg'));
                audioList[audioN].play();
                audioN++;
              });
              imgsList[i].addEventListener('mouseout', function () {
                audioList[audioN - 1].pause();
              });
            }
            else if (imgsList[i].alt == 'http://image.noelshack.com/fichiers/2016/32/1470919005-img28.png') {
              imgsList[i].addEventListener('mouseover', function () {
                audioList.push(new Audio('http://jscore.comli.com/15.ogg'));
                audioList[audioN].play();
                audioN++;
              });
              imgsList[i].addEventListener('mouseout', function () {
                audioList[audioN - 1].pause();
              });
            }
            else if (imgsList[i].alt == 'http://image.noelshack.com/fichiers/2016/23/1465739711-img41.png') {
              imgsList[i].addEventListener('mouseover', function () {
                audioList.push(new Audio('http://jscore.comli.com/16.ogg'));
                audioList[audioN].play();
                audioN++;
              });
              imgsList[i].addEventListener('mouseout', function () {
                audioList[audioN - 1].pause();
              });
            }
            else if (imgsList[i].alt == 'http://image.noelshack.com/fichiers/2016/23/1465731617-valls.png') {
              imgsList[i].addEventListener('mouseover', function () {
                audioList.push(new Audio('http://jscore.comli.com/17.ogg'));
                audioList[audioN].play();
                audioN++;
              });
              imgsList[i].addEventListener('mouseout', function () {
                audioList[audioN - 1].pause();
              });
            }
          }
        }
      }
      if (webmPlayer || youtubePlayer) {
        var links = document.getElementsByClassName('xXx');
        for (var i = 0; i < links.length; i++) {
          if (playerSignature || (!(playerSignature) && !(links[i].parentElement.parentElement.className.includes('signature-msg')))) {
            if (webmPlayer) {
              if (links[i].href.startsWith('http://webm.land/w/')) {
                links[i].style.display = 'none';
                var videoId = links[i].href.replace('http://webm.land/w/', '').replace('/', '') + '.webm';
                video = document.createElement('video');
                video.width = '320';
                video.height = '240';
                video.innerHTML = '<source src="http://webm.land/media/' + videoId + '" type="video/webm">';
                video.setAttribute('loop', 'loop');
                links[i].parentElement.insertBefore(video, links[i]);
                video.addEventListener('mouseover', function () {
                  $(this).get(0).currentTime = 0;
                  $(this).get(0).play();
                });
                video.addEventListener('mouseout', function () {
                  $(this).get(0).pause();
                });
              } 
              else if (links[i].href.endsWith('.webm')) {
                links[i].style.display = 'none';
                video = document.createElement('video');
                video.width = '320';
                video.height = '240';
                video.innerHTML = '<source src="' + links[i].href + '" type="video/webm">';
                video.setAttribute('loop', 'loop');
                links[i].parentElement.insertBefore(video, links[i]);
                video.addEventListener('mouseover', function () {
                  $(this).get(0).currentTime = 0;
                  $(this).get(0).play();
                });
                video.addEventListener('mouseout', function () {
                  $(this).get(0).pause();
                });
              }
            }
            if (youtubePlayer) {
              if (links[i].href.startsWith('https://www.youtube.com/watch?v=') || links[i].href.startsWith('https://youtu.be/')) {
                var getId = '';
                if (links[i].href.startsWith('https://www.youtube.com/watch?v=')) {
                  var videoIndex = links[i].href.indexOf('v=');
                  var upIndex = links[i].href.indexOf('&', videoIndex);
                  if (upIndex == - 1) {
                    getId = links[i].href.substring(videoIndex + 2);
                  } 
                  else {
                    getId = links[i].href.substring(videoIndex + 2, upIndex);
                  }
                } 
                else if (links[i].href.startsWith('https://youtu.be/')) {
                  var upIndex = links[i].href.indexOf('?', 17);
                  if (upIndex == - 1) {
                    getId = links[i].href.substring(17);
                  } 
                  else {
                    getId = links[i].href.substring(17, upIndex);
                  }
                }
                links[i].style.display = 'none';
                video = document.createElement('object');
                video.setAttribute('style', 'width:100%;height:100%;width: 320px; height: 240px; float: none; clear: both; margin: 2px auto;');
                var timeIndex = links[i].href.indexOf('t=');
                if (timeIndex != - 1) {
                  var totalSec = 0;
                  var upTo = links[i].href.indexOf('&', timeIndex);
                  if (upTo == - 1) {
                    links[i].href = links[i].href + '&';
                    upTo = links[i].href.indexOf('&', timeIndex);
                  }
                  var hourIndex = links[i].href.indexOf('h', timeIndex);
                  var minIndex = links[i].href.indexOf('m', timeIndex);
                  var secIndex = links[i].href.indexOf('s', timeIndex);
                  var isHour = false;
                  if (hourIndex != - 1 && hourIndex < upTo) {
                    isHour = true;
                  }
                  var isMin = false;
                  if (minIndex != - 1 && minIndex < upTo) {
                    isMin = true;
                  }
                  var isSec = false;
                  if (secIndex != - 1 && secIndex < upTo) {
                    isSec = true;
                  }
                  if (isHour) {
                    totalSec += Number((links[i].href.substring(timeIndex + 2, hourIndex)) * 3600);
                  }
                  if (isMin) {
                    if (isHour) {
                      totalSec += Number((links[i].href.substring(hourIndex + 1, minIndex)) * 60);
                    } 
                    else {
                      totalSec += Number((links[i].href.substring(timeIndex + 2, minIndex)) * 60);
                    }
                  }
                  if (isSec) {
                    if (isMin) {
                      totalSec += Number(links[i].href.substring(minIndex + 1, secIndex));
                    } 
                    else if (isHour) {
                      totalSec += Number(links[i].href.substring(hourIndex + 1, secIndex));
                    } 
                    else {
                      totalSec += Number(links[i].href.substring(timeIndex + 2, secIndex));
                    }
                  }
                  if (!isHour && !isMin && !isSec) {
                    totalSec += Number(links[i].href.substring(timeIndex + 2, upTo));
                  }
                  getId += '?start=' + totalSec;
                }
                video.setAttribute('data', 'http://www.youtube.com/embed/' + getId);
                links[i].parentElement.insertBefore(video, links[i]);
              }
            }
          }
        }
      }
      if (!stickerCliquable && !supprimerFond) {
        var fc = [
        ];
        var ix = document.getElementsByClassName('img-shack');
        function createfc(i) {
          return function () {
            var z = ix[i].alt;
            var newSrc = ix[i].src;
            if (z.includes('/fichiers/') && (z.endsWith('.png') || z.endsWith('.gif'))) {
              newSrc = ix[i].alt;
            } 
            else if (z.includes('/minis/') && (z.endsWith('.png') || z.endsWith('.gif'))) {
              newSrc = ix[i].alt;
              newSrc = newSrc.replace('/minis/', '/fichiers/');
            } 
            else if (ix[i].src.includes('/minis/') && (z.endsWith('.png') || z.endsWith('.gif'))) {
              newSrc = newSrc.replace('/minis/', '/fichiers/');
              if (z.endsWith('.gif')) {
                newSrc = newSrc.replace('.png', '.gif');
              }
            } 
            else if (ix[i].src.includes('/minis/') && (z.endsWith('.jpg') || z.endsWith('.jpeg'))) {
              newSrc = newSrc.replace('/minis/', '/fichiers/');
              if (z.endsWith('.jpg')) {
                newSrc = newSrc.replace('.png', '.jpg');
              } 
              else if (z.endsWith('.jpeg')) {
                newSrc = newSrc.replace('.png', '.jpeg');
              }
            }
            var newImg = new Image();
            newImg.addEventListener('load', function () {
              if (this.width / this.height > 4 / 3 * 0.99 && this.width / this.height < 4 / 3 * 1.01) {
                ix[i].parentElement.style.display = 'none';
                ix[i].parentElement.parentElement.insertBefore(ix[i], ix[i].parentElement);
              }
            });
            newImg.src = newSrc;
          };
        }
        for (var i = 0; i < ix.length; i++) {
          fc[i] = createfc(i);
          fc[i]();
        }
      }
      var editModifier = document.getElementsByClassName('picto-msg-crayon');
      if (editModifier.length > 0) {
        for (var i = 0; i < editModifier.length; i++) {
          editModifier[i].addEventListener('click', function () {
            var observerLoad = new MutationObserver(function (mutations, mutLoad) {
              if (document.querySelector('div[data-flg-tt="Hap"]')) {
                mutLoad.disconnect();
                chargerBarre();
              }
              return;
            });
            observerLoad.observe(document.body, {
              childList: true,
              subtree: true,
              attributes: false,
              characterData: false
            });
          });
          var observerStickers = new MutationObserver(function (mutations, mutStickers) {
            var canvasStickers = document.querySelector('button[data-edit="stickers"]');
            if (canvasStickers) {
              mutStickers.disconnect();
              canvasStickers.onclick = function () {
                check();
              };
            }
            return;
          });
          observerStickers.observe(document.body, {
            childList: true,
            subtree: true,
            attributes: false,
            characterData: false
          });
          var observerButtonA = new MutationObserver(function (mutations, mutButtons) {
            var canvasButtons = document.getElementsByClassName('btn-annuler-modif-msg');
            if (canvasButtons) {
              mutButtons.disconnect();
              for (var j = 0; j < canvasButtons.length; j++) {
                canvasButtons[j].addEventListener('click', function () {
                  var observerTArea = new MutationObserver(function (mutations, mutTArea) {
                    var canvasTArea = document.getElementById('message_topic');
                    if (canvasTArea) {
                      mutTArea.disconnect();
                      canvasTArea.addEventListener('click', tArea());
                    }
                    return;
                  });
                  observerTArea.observe(document.body, {
                    childList: true,
                    subtree: true,
                    attributes: false,
                    characterData: false
                  });
                });
              }
            }
            return;
          });
          observerButtonA.observe(document.body, {
            childList: true,
            subtree: true,
            attributes: false,
            characterData: false
          });
          var observerButtonE = new MutationObserver(function (mutations, mutButtons) {
            var canvasButtons = document.getElementsByClassName('btn btn-editer-msg');
            if (canvasButtons) {
              mutButtons.disconnect();
              for (var j = 0; j < canvasButtons.length; j++) {
                canvasButtons[j].addEventListener('click', function () {
                  var observerTArea = new MutationObserver(function (mutations, mutTArea) {
                    var canvasTArea = document.getElementById('message_topic');
                    if (canvasTArea) {
                      mutTArea.disconnect();
                      canvasTArea.addEventListener('click', tArea());
                    }
                    return;
                  });
                  observerTArea.observe(document.body, {
                    childList: true,
                    subtree: true,
                    attributes: false,
                    characterData: false
                  });
                });
              }
            }
            return;
          });
          observerButtonE.observe(document.body, {
            childList: true,
            subtree: true,
            attributes: false,
            characterData: false
          });
        }
      }
      allCat = JSON.parse(GM_getValue('allCat'));
      catRealNames = JSON.parse(GM_getValue('catRealNames'));
      catIcons = JSON.parse(GM_getValue('catIcons'));
      catToShow = JSON.parse(GM_getValue('catToShow'));
      var funcs = [
      ];
      function createfunc(g) {
        return function () {
          var idx = allCat.indexOf(catToShow[g]);
          var tab = document.createElement('div');
          tab.className = 'f-tab f-h';
          tab.style.width = '25px';
          tab.style.height = '23px';
          tab.style.lineHeight = '23px';
          tab.style.fontSize = '14px';
          tab.setAttribute('data-flg-tt', catRealNames[idx]);
          tab.innerHTML = catIcons[idx] + '<div style="display: none;" class="f-ttw"><div style="top: -26px; left: -9px;" class="f-inner"><div class="f-tt">' + catRealNames[idx] + '</div><div class="f-arr"></div></div></div>';
          tab.addEventListener('click', function () {
            charger(catRealNames[idx]);
          });
          canvasPop.appendChild(tab);
        };
      }
      for (var g = 0; g < catToShow.length; g++) {
        funcs[g] = createfunc(g);
      }
      for (var g = 0; g < catToShow.length; g++) {
        funcs[g]();
      }
    }, 100);
  }
  function check() {
    if (document.querySelector('button[data-edit="stickers"]').className.indexOf('active') != - 1)
    {
      var observerLoad = new MutationObserver(function (mutations, mutLoad) {
        if (document.querySelector('div[data-flg-tt="Hap"]')) {
          mutLoad.disconnect();
          chargerBarre();
        }
        return;
      });
      observerLoad.observe(document.body, {
        childList: true,
        subtree: true,
        attributes: false,
        characterData: false
      });
    }
  }
  var observerHap = new MutationObserver(function (mutations, mutHap) {
    if (document.querySelector('div[data-flg-tt="Hap"]')) {
      mutHap.disconnect();
      chargerBarre();
    }
    return;
  });
  observerHap.observe(document.body, {
    childList: true,
    subtree: true,
    attributes: false,
    characterData: false
  });
  if (supprStickersBan) {
    var observerCharg = new MutationObserver(function (mutations, mutCharg) {
      var canvasCharg = document.getElementsByClassName('bloc-pagi-default');
      if (canvasCharg[1]) {
        mutCharg.disconnect();
        var imgs = document.getElementsByClassName('img-stickers');
        var funcs = [
        ];
        for (var i = 0; i < imgs.length; i++) {
          if ($.inArray(imgs[i].src, stickersBanListe) > - 1)
          {
            if (!(supprDesPosts)) {
              if (stickerMessageDeSuppr) {
                if (!(imgs[i].previousSibling) || !(imgs[i].previousSibling.style) || imgs[i].previousSibling.style.color != 'red') {
                  var message = document.createElement('div');
                  message.style.color = 'red';
                  message.innerHTML = 'Sticker supprimé! ';
                  imgs[i].parentElement.insertBefore(message, imgs[i]);
                } 
                else {
                  imgs[i].previousSibling.innerHTML += 'Sticker supprimé! ';
                }
              }
              imgs[i].parentNode.removeChild(imgs[i]);
              i--;
            } 
            else {
              if (imgs[i].offsetParent != null) {
                var img = imgs[i];
                while (typeof img.parentElement != 'undefined' && img.parentElement.className != 'bloc-message-forum ') {
                  img = img.parentElement;
                }
                img.style.display = 'none';
                if (stickerMessageDeSuppr) {
                  var showBan = document.createElement('div');
                  showBan.className = 'conteneur-message n-displayed';
                  showBan.style.color = 'red';
                  showBan.style.textAlign = 'center';
                  showBan.style.marginBottom = '15px';
                  showBan.innerHTML = 'Ce post a été supprimé car il contenait un sticker banni. Cliquez ici pour l\'afficher.';
                  showBan.addEventListener('click', function () {
                    if (this.className.endsWith('n-displayed')) {
                      this.parentElement.lastElementChild.removeAttribute('style');
                      this.innerHTML = 'Ce post a été supprimé car il contenait un sticker banni. Cliquez ici pour le cacher.';
                      $(this).removeClass('n-displayed');
                    } 
                    else {
                      this.parentElement.lastElementChild.style.display = 'none';
                      this.innerHTML = 'Ce post a été supprimé car il contenait un sticker banni. Cliquez ici pour l\'afficher.';
                      this.className += ' n-displayed';
                    }
                  });
                  img.parentElement.insertBefore(showBan, img);
                }
              }
            }
          }
        }
      }
      return;
    });
    observerCharg.observe(document.body, {
      childList: true,
      subtree: true,
      attributes: false,
      characterData: false
    });
  }
  var observerHide = new MutationObserver(function (mutations, mutHide) {
    var canvasHide = document.querySelector('div[data-flg-tt="Biblioth&egrave;que"]');
    if (canvasHide) {
      mutHide.disconnect();
      canvasHide.parentElement.setAttribute('style', 'display:none !important;height:0px !important;width:0px !important');
    }
    return;
  });
  observerHide.observe(document.body, {
    childList: true,
    subtree: true,
    attributes: false,
    characterData: false
  });
  var observerStickers = new MutationObserver(function (mutations, mutStickers) {
    var canvasStickers = document.querySelector('button[data-edit="stickers"]');
    if (canvasStickers) {
      mutStickers.disconnect();
      canvasStickers.onclick = function () {
        check();
      };
    }
    return;
  });
  observerStickers.observe(document.body, {
    childList: true,
    subtree: true,
    attributes: false,
    characterData: false
  });
  setTimeout(function () {
    if (!isLoaded) {
      chargerBarre();
    }
  }, 10000);
}
