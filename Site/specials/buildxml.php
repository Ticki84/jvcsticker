<?php
try {
    $bdd = new PDO('mysql:host=localhost;dbname=***REMOVED***_stckr;charset=utf8', '***REMOVED***_master', '***REMOVED***');
    
    $reponseA = $bdd->query('SELECT * FROM stickers ORDER BY count desc');
    $reponseB = $bdd->query('SELECT * FROM categories ORDER BY id asc');
    
    $urls       = array();
    $counts     = array();
    $categories = array();
    $tags       = array();
    $sounds     = array();
    $names      = array();
    $variables  = array();
    $icons      = array();
    $byDefault  = array();
    while ($donnees = $reponseA->fetch()) {
        array_push($urls, $donnees['url']);
        array_push($counts, $donnees['count']);
        array_push($categories, $donnees['categorie']);
        array_push($tags, $donnees['tags']);
        array_push($sounds, $donnees['sounds']);
    }
    $reponseA->closeCursor();
    while ($donnees = $reponseB->fetch()) {
        array_push($names, $donnees['name']);
        array_push($variables, $donnees['variable']);
        array_push($icons, $donnees['icon']);
        array_push($byDefault, $donnees['byDefault']);
    }
    $reponseB->closeCursor();
    $xml  = new SimpleXMLElement("<?xml version=\"1.0\" encoding=\"utf-8\" ?><racine></racine>");
    $cats = $xml->addChild('categories');
    for ($i = 0; $i < count($names); $i++) {
        $cat = $cats->addChild('categorie');
        $cat->addChild('nom', $names[$i]);
        $cat->addChild('var', $variables[$i]);
        $cat->addChild('icone', $icons[$i]);
        $cat->addChild('default', $byDefault[$i]);
        $stickers = $cat->addChild('stickers');
        for ($j = 0; $j < count($urls); $j++) {
            if ($categories[$j] == $variables[$i]) {
                $sticker = $stickers->addChild('sticker');
                $sticker->addChild('url', '<![CDATA[' . $urls[$j] . ']]>');
                $sticker->addChild('counter', $counts[$j]);
                $sticker->addChild('tags', $tags[$j]);
                $sticker->addChild('sounds', $sounds[$j]);
            }
        }
    }
    Header('Content-type: text/xml; charset=utf-8');
    $dom = new DOMDocument("1.0");
    $dom->formatOutput = true;
    $dom->loadXML($xml->asXML());
    $nxml = new SimpleXMLElement($dom->saveXML());
    print($nxml->asXML());
    file_put_contents('./CoreJVCSticker++.xml', htmlspecialchars_decode($xml->asXML()), FILE_USE_INCLUDE_PATH);
    //file_put_contents('./CoreJVCSticker++.xml', htmlspecialchars_decode($nxml->saveXML()), FILE_USE_INCLUDE_PATH);
}
catch (Exception $e) {
    die();
}
?>