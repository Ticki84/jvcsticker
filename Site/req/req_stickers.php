<?php
if (isset($_POST['url']) && isset($_POST['categorie']) && isset($_POST['tags']) && isset($_POST['sounds']))
{
	try
	{
		$bdd = new PDO('mysql:host=localhost;dbname=***REMOVED***_stckr;charset=utf8', '***REMOVED***_master', '***REMOVED***');
		$req = $bdd->prepare('INSERT INTO req_stickers (url, categorie, tags, sounds) VALUES(:url, :categorie, :tags, :sounds)');
		$req->execute(array(
			'url' => $_POST['url'],
			'categorie' => $_POST['categorie'],
			'tags' => $_POST['tags'],
			'sounds' => $_POST['sounds']
			));
	}
	catch (Exception $e)
	{
		die();
	}
}
?>