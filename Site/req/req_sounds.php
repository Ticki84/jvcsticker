<?php
if (isset($_POST['url']) && isset($_POST['reference_url']) && isset($_POST['sounds']))
{
	try
	{
		$bdd = new PDO('mysql:host=localhost;dbname=***REMOVED***_stckr;charset=utf8', '***REMOVED***_master', '***REMOVED***');
		$req = $bdd->prepare('INSERT INTO req_sounds (url, reference_url, sounds) VALUES(:url, :reference_url, :sounds)');
		$req->execute(array(
			'url' => $_POST['url'],
			'reference_url' => $_POST['reference_url'],
			'sounds' => $_POST['sounds']
			));
	}
	catch (Exception $e)
	{
		die();
	}
}
?>