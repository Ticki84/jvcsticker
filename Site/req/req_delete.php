<?php
if (isset($_POST['url']) && isset($_POST['reason']) && isset($_POST['new_url']))
{
	try
	{
		$bdd = new PDO('mysql:host=localhost;dbname=***REMOVED***_stckr;charset=utf8', '***REMOVED***_master', '***REMOVED***');
		$req = $bdd->prepare('INSERT INTO req_delete (url, reason, new_url) VALUES(:url, :reason, :new_url)');
		$req->execute(array(
			'url' => $_POST['url'],
			'reason' => $_POST['reason'],
			'new_url' => $_POST['new_url']
			));
	}
	catch (Exception $e)
	{
		die();
	}
}
?>