<html>
<head>
	<script type="text/javascript" src="jquery-3.2.1.min.js"></script>
	<style>
		@font-face {font-family: 'flgstickers'; src: url('https://d3c1755qzw8zz7.cloudfront.net/fonts/flgstickers/opf6cd.woff');}
		body {font-family: "Open Sans", sans-serif;line-height: 1.25;}
		table {border: 1px solid #ccc;border-collapse: collapse;margin: 0;padding: 0;width: 100%;table-layout: fixed;}
		table tr {background: #f8f8f8;border: 1px solid #ddd;padding: .35em;}
		table th,table td {padding: .625em;text-align: center;word-wrap: break-word;}
		table th {font-size: .85em;letter-spacing: .1em;text-transform: uppercase;}
		img {width: 64px; height: 48px; padding: 3px;}
        .actions img {width: 48px;}
		input {text-align: center;}
		textarea {text-align: center;}
		input[type=checkbox] {transform: scale(2);}
		.t1 {letter-spacing: 8px; text-shadow: 2px 1px grey; font-weight: bold; font-style: italic; font-size: 115%;}
		.t2 {letter-spacing: 3px; font-weight: bold}
		.categ {font-family: 'flgstickers'; transform: scale(2);}
        .categ option {font-size: 2em;}
	</style>
</head>
<body>



<script type="text/javascript">
function validerA(ligne) {
	var id = "A_l"+ligne;
	var element = document.getElementById(id);
	
	$(document).ready(function () {
		$.ajax({
			url: 'valid_req_sticker.php',
			type:'POST',
		data:
		{
			url: element.children[0].children[1].childNodes[0].nodeValue,
			cat: element.children[1].childNodes[0].value,
			tags: element.children[2].childNodes[0].value,
			sounds: element.children[3].childNodes[0].value
		},
		success: function(msg)
		{
			element.setAttribute('style','display:none;');
		}
		});
	});
}
function declinerA(ligne) {
	var id = "A_l"+ligne;
	var element = document.getElementById(id);
	
	$(document).ready(function () {
		$.ajax({
			url: 'suppr_req_sticker.php',
			type:'POST',
		data:
		{
			url: element.children[0].children[1].childNodes[0].nodeValue
		},
		success: function(msg)
		{
			element.setAttribute('style','display:none;');
		}
		});
	});
}

function validerB(ligne) {
	var id = "B_l"+ligne;
	var element = document.getElementById(id);
	
	$(document).ready(function () {
		$.ajax({
			url: 'valid_req_delete.php',
			type:'POST',
		data:
		{
			url: element.children[0].children[1].childNodes[0].nodeValue,
			new_url: element.children[5].children[1].value
		},
		success: function(msg)
		{
			element.setAttribute('style','display:none;');
		}
		});
	});
}
function declinerB(ligne) {
	var id = "B_l"+ligne;
	var element = document.getElementById(id);
	
	$(document).ready(function () {
		$.ajax({
			url: 'suppr_req_delete.php',
			type:'POST',
		data:
		{
			url: element.children[0].children[1].childNodes[0].nodeValue
		},
		success: function(msg)
		{
			element.setAttribute('style','display:none;');
		}
		});
	});
}

function validerC(ligne) {
	var id = "C_l"+ligne;
	var element = document.getElementById(id);
	
	var isChecked = 'false';
	if (element.children[3].childNodes[0].checked)
	{
		isChecked = 'true';
	}
	
	$(document).ready(function () {
		$.ajax({
			url: 'valid_req_categories.php',
			type:'POST',
		data:
		{
			name: element.children[0].childNodes[0].value,
			variable: element.children[1].childNodes[0].value,
			icon: element.children[2].children[0].options[element.children[2].children[0].selectedIndex].value,
			byDefault: isChecked
		},
		success: function(msg)
		{
			element.setAttribute('style','display:none;');
		}
		});
	});
}
function declinerC(ligne) {
	var id = "C_l"+ligne;
	var element = document.getElementById(id);
	
	$(document).ready(function () {
		$.ajax({
			url: 'suppr_req_categories.php',
			type:'POST',
		data:
		{
			name: element.children[0].childNodes[0].value
		},
		success: function(msg)
		{
			element.setAttribute('style','display:none;');
		}
		});
	});
}

function validerD(ligne) {
	var id = "D_l"+ligne;
	var element = document.getElementById(id);
	
	$(document).ready(function () {
		$.ajax({
			url: 'valid_req_categorie.php',
			type:'POST',
		data:
		{
			url: element.children[0].children[1].childNodes[0].nodeValue,
			categorie: element.children[2].childNodes[0].value
		},
		success: function(msg)
		{
			element.setAttribute('style','display:none;');
		}
		});
	});
}
function declinerD(ligne) {
	var id = "D_l"+ligne;
	var element = document.getElementById(id);
	
	$(document).ready(function () {
		$.ajax({
			url: 'suppr_req_categorie.php',
			type:'POST',
		data:
		{
			url: element.children[0].children[1].childNodes[0].nodeValue
		},
		success: function(msg)
		{
			element.setAttribute('style','display:none;');
		}
		});
	});
}

function validerE(ligne) {
	var id = "E_l"+ligne;
	var element = document.getElementById(id);
	
	$(document).ready(function () {
		$.ajax({
			url: 'valid_req_tags.php',
			type:'POST',
		data:
		{
			url: element.children[0].children[1].childNodes[0].nodeValue,
			tags: element.children[2].childNodes[0].value
		},
		success: function(msg)
		{
			element.setAttribute('style','display:none;');
		}
		});
	});
}
function declinerE(ligne) {
	var id = "E_l"+ligne;
	var element = document.getElementById(id);
	
	$(document).ready(function () {
		$.ajax({
			url: 'suppr_req_tags.php',
			type:'POST',
		data:
		{
			url: element.children[0].children[1].childNodes[0].nodeValue
		},
		success: function(msg)
		{
			element.setAttribute('style','display:none;');
		}
		});
	});
}

function validerF(ligne) {
	var id = "F_l"+ligne;
	var element = document.getElementById(id);
	
	$(document).ready(function () {
		$.ajax({
			url: 'valid_req_sounds.php',
			type:'POST',
		data:
		{
			url: element.children[0].children[1].value,
			reference_url: element.children[2].children[1].value,
			sounds: element.children[4].childNodes[0].value
		},
		success: function(msg)
		{
			element.setAttribute('style','display:none;');
		}
		});
	});
}
function declinerF(ligne) {
	var id = "F_l"+ligne;
	var element = document.getElementById(id);
	
	$(document).ready(function () {
		$.ajax({
			url: 'suppr_req_sounds.php',
			type:'POST',
		data:
		{
			url: element.children[0].children[1].value,
			reference_url: element.children[2].children[1].value
		},
		success: function(msg)
		{
			element.setAttribute('style','display:none;');
		}
		});
	});
}

function validerG(ligne) {
	var id = "G_l"+ligne;
	var element = document.getElementById(id);
	
	$(document).ready(function () {
		$.ajax({
			url: 'valid_req_delete.php',
			type:'POST',
		data:
		{
			url: element.children[0].children[1].childNodes[0].nodeValue,
			new_url: ''
		},
		success: function(msg)
		{
			element.setAttribute('style','display:none;');
		}
		});
	});
}

function validerH(ligne) {
	var id = "H_l"+ligne;
	var element = document.getElementById(id);
	
	$(document).ready(function () {
		$.ajax({
			url: 'valid_req_categorie.php',
			type:'POST',
		data:
		{
			url: element.children[0].children[1].childNodes[0].nodeValue,
			categorie: element.children[1].childNodes[0].value
		},
		success: function(msg)
		{
			element.setAttribute('style','display:none;');
		}
		});
	});
}

function validerI(ligne) {
	var id = "I_l"+ligne;
	var element = document.getElementById(id);
	
	$(document).ready(function () {
		$.ajax({
			url: 'valid_req_tags.php',
			type:'POST',
		data:
		{
			url: element.children[0].children[1].childNodes[0].nodeValue,
			tags: element.children[1].childNodes[0].value
		},
		success: function(msg)
		{
			element.setAttribute('style','display:none;');
		}
		});
	});
}

function validerJ(ligne) {
	var id = "J_l"+ligne;
	var element = document.getElementById(id);
	
	$(document).ready(function () {
		$.ajax({
			url: 'valid_req_categorie.php',
			type:'POST',
		data:
		{
			url: element.children[0].children[1].childNodes[0].nodeValue,
			categorie: element.children[1].childNodes[0].value
		},
		success: function(msg)
		{
			element.setAttribute('style','display:none;');
		}
		});
	});
}
</script>


<?php
try {
    $bdd = new PDO('mysql:host=localhost;dbname=***REMOVED***_stckr;charset=utf8', '***REMOVED***_master', '***REMOVED***');
    
    $reponseA   = $bdd->query('SELECT * FROM req_stickers');
    $urls       = array();
    $categories = array();
    $tags       = array();
    $sounds     = array();
    while ($donnees = $reponseA->fetch()) {
        array_push($urls, $donnees['url']);
        array_push($categories, $donnees['categorie']);
        array_push($tags, $donnees['tags']);
        array_push($sounds, $donnees['sounds']);
    }
    $reponseA->closeCursor();
    echo '<table>
	<tr class="t1">
		<td>NOUVEAU STICKER</td>
		<td colspan="3">INFORMATIONS</td>
		<td></td>
	</tr>
	<tr class="t2">
		<td>Image</td>
		<td>Cat&eacute;gorie</td>
		<td>Tags</td>
		<td>Sons</td>
		<td>Actions</td>
	</tr>';
    for ($i = 0; $i < count($urls); $i++) {
        echo '
	<tr id="A_l' . $i . '">
		<td>
			<img src="' . $urls[$i] . '"/>
			<p>' . $urls[$i] . '</p>
		</td>
		<td><textarea rows=5 cols=30>' . $categories[$i] . '</textarea></td>
		<td><textarea rows=5 cols=30>' . $tags[$i] . '</textarea></td>
		<td><textarea rows=5 cols=30>' . $sounds[$i] . '</textarea></td>
		<td class="actions"><img src="https://image.noelshack.com/fichiers/2017/19/1494787339-camera-test.png" onclick="validerA(' . $i . ')"/><img src="https://image.noelshack.com/fichiers/2017/19/1494787338-close.png" onclick="declinerA(' . $i . ')"/></td>
	</tr>';
    }
    echo '
</table></br>
';
    
    
    $reponseB = $bdd->query('SELECT * FROM req_delete');
    $urls     = array();
    $reasons  = array();
    $new_urls = array();
    while ($donnees = $reponseB->fetch()) {
        array_push($urls, $donnees['url']);
        array_push($reasons, $donnees['reason']);
        array_push($new_urls, $donnees['new_url']);
    }
    $reponseB->closeCursor();
    $reponseB_bis   = $bdd->query('SELECT * FROM stickers WHERE url IN ("' . implode('","', $urls) . '");');
    $urls_bis       = array();
    $counts_bis     = array();
    $categories_bis = array();
    $tags_bis       = array();
    $sounds_bis     = array();
    while ($donnees = $reponseB_bis->fetch()) {
        array_push($urls_bis, $donnees['url']);
        array_push($counts_bis, $donnees['count']);
        array_push($categories_bis, $donnees['categorie']);
        array_push($tags_bis, $donnees['tags']);
        array_push($sounds_bis, $donnees['sounds']);
    }
    $reponseB_bis->closeCursor();
    $counts     = array();
    $categories = array();
    $tags       = array();
    $sounds     = array();
    for ($i = 0; $i < count($urls); $i++) {
        $found = 0;
        for ($j = 0; $j < count($urls_bis); $j++) {
            if ($urls[$i] == $urls_bis[$j]) {
                array_push($counts, $counts_bis[$j]);
                array_push($categories, $categories_bis[$j]);
                array_push($tags, $tags_bis[$j]);
                array_push($sounds, $sounds_bis[$j]);
                $found = 1;
                break;
            }
        }
        if ($found == 0) {
            array_push($counts, '');
            array_push($categories, '');
            array_push($tags, '');
            array_push($sounds, '');
        }
    }
    echo '<table>
	<tr class="t1">
		<td colspan="5">STICKER ACTUEL</td>
		<td colspan="2">REMPLAC&Eacute; PAR</td>
		<td></td>
	</tr>
	<tr class="t2">
		<td>Image</td>
		<td>Utilisations</td>
		<td>Cat&eacute;gorie</td>
		<td>Tags</td>
		<td>Sons</td>
		<td>Image</td>
		<td>Motif</td>
		<td>Actions</td>
	</tr>';
    for ($i = 0; $i < count($urls); $i++) {
        echo '
	<tr id="B_l' . $i . '">
		<td>
			<img src="' . $urls[$i] . '"/>
			<p>' . $urls[$i] . '</p>
		</td>
		<td>' . $counts[$i] . '</td>
		<td>' . $categories[$i] . '</td>
		<td>' . $tags[$i] . '</td>
		<td>' . $sounds[$i] . '</td>
		<td>
			<img src="' . $new_urls[$i] . '" id="1_img_l' . $i . '"/>
			<textarea rows=6 cols=20 id="1_textarea_l' . $i . '" oninput="document.getElementById(\'1_img_l' . $i . '\').src = document.getElementById(\'1_textarea_l' . $i . '\').value;">' . $new_urls[$i] . '</textarea>
		</td>
		<td>' . $reasons[$i] . '</td>
		<td class="actions"><img src="https://image.noelshack.com/fichiers/2017/19/1494787339-camera-test.png" onclick="validerB(' . $i . ')"/><img src="https://image.noelshack.com/fichiers/2017/19/1494787338-close.png" onclick="declinerB(' . $i . ')"/></td>
	</tr>';
    }
    echo '
</table></br>
';
    
    
    $reponseC = $bdd->query('SELECT * FROM req_categories');
    $values   = explode(' ', '! " # $ % ' . "& ' ( ) 1 2 3 4 5 6 7 8 9 : ; < = ? @ A B C D E F G H I J K L M N O P Q R S T U V W X Y Z [ \ ] ^ _ ` a b c d e f g h i j k l m n o p q r s t u v w x y z { | } ~ ¡ ¢ £ ¤ ¥ ¦ § ¨ © ª « ¬ ® ¯ ° ± ² ³ ´ µ ¶ ¸ ¹ º » ¼ ½ ¾ ¿ À Á Â Ã Ä Å Æ Ç È É Ê Ë Ì Í Î Ï Ð Ñ Ò Ó Ô Õ Ö × Ø Ù Ú Û Ü à á â ã ä å æ ç è é ê ë ì í ï ð ñ ò ó ô õ ö ÷ ø ù ú û ü ý þ");
    $names    = array();
    $icons    = array();
    while ($donnees = $reponseC->fetch()) {
        array_push($names, $donnees['name']);
        array_push($icons, $donnees['icon']);
    }
    $reponseC->closeCursor();
    echo '<table>
	<tr class="t1">
		<td colspan="4">NOUVELLE CAT&Eacute;GORIE</td>
		<td></td>
	</tr>
	<tr class="t2">
		<td>Nom</td>
		<td>Variable</td>
		<td>Ic&ocirc;ne</td>
		<td>Par d&eacute;faut</td>
		<td>Actions</td>
	</tr>';
    for ($i = 0; $i < count($names); $i++) {
        echo '
	<tr id="C_l' . $i . '">
		<td><textarea rows=3 cols=40>' . $names[$i] . '</textarea></td>
		<td><textarea rows=3 cols=40>cat' . $names[$i] . '</textarea></td>
        <td>
            <select class="categ">';
        for ($j = 0; $j < count($values); $j++) {
            if ($values[$j] != $icons[$i]) {
                echo '
                <option value="' . $values[$j] . '">' . $values[$j] . '</option>';
            } else {
                echo '
                <option value="' . $values[$j] . '" selected>' . $values[$j] . '</option>';
            }
        }
        echo '
            </select>
        </td>';
        echo '</td>
		<td><input type="checkbox"></td>
		<td class="actions"><img src="https://image.noelshack.com/fichiers/2017/19/1494787339-camera-test.png" onclick="validerC(' . $i . ')"/><img src="https://image.noelshack.com/fichiers/2017/19/1494787338-close.png" onclick="declinerC(' . $i . ')"/></td>
	</tr>';
    }
    echo '
</table></br>
';
    
    
    $reponseD       = $bdd->query('SELECT * FROM req_categorie');
    $urls           = array();
    $new_categories = array();
    while ($donnees = $reponseD->fetch()) {
        array_push($urls, $donnees['url']);
        array_push($new_categories, $donnees['categorie']);
    }
    $reponseD->closeCursor();
    $reponseD_bis   = $bdd->query('SELECT * FROM stickers WHERE url IN ("' . implode('","', $urls) . '");');
    $urls_bis       = array();
    $categories_bis = array();
    while ($donnees = $reponseD_bis->fetch()) {
        array_push($urls_bis, $donnees['url']);
        array_push($categories_bis, $donnees['categorie']);
    }
    $reponseD_bis->closeCursor();
    $categories = array();
    for ($i = 0; $i < count($urls); $i++) {
        $found = 0;
        for ($j = 0; $j < count($urls_bis); $j++) {
            if ($urls[$i] == $urls_bis[$j]) {
                array_push($categories, $categories_bis[$j]);
                $found = 1;
                break;
            }
        }
        if ($found == 0) {
            array_push($categories, '');
        }
    }
    echo '<table>
	<tr class="t1">
		<td colspan="2">STICKER ACTUEL</td>
		<td>MODIFICATION</td>
		<td></td>
	</tr>
	<tr class="t2">
		<td>Image</td>
		<td>Catégorie</td>
		<td>Catégorie</td>
		<td>Actions</td>
	</tr>';
    for ($i = 0; $i < count($urls); $i++) {
        echo '
	<tr id="D_l' . $i . '">
		<td>
			<img src="' . $urls[$i] . '"/>
			<p>' . $urls[$i] . '</p>
		</td>
		<td>' . $categories[$i] . '</td>
		<td><textarea rows=5 cols=50>' . $new_categories[$i] . '</textarea></td>
		<td class="actions"><img src="https://image.noelshack.com/fichiers/2017/19/1494787339-camera-test.png" onclick="validerD(' . $i . ')"/><img src="https://image.noelshack.com/fichiers/2017/19/1494787338-close.png" onclick="declinerD(' . $i . ')"/></td>
	</tr>';
    }
    echo '
</table></br>
';
    
    
    $reponseE = $bdd->query('SELECT * FROM req_tags');
    $urls     = array();
    $new_tags = array();
    while ($donnees = $reponseE->fetch()) {
        array_push($urls, $donnees['url']);
        array_push($new_tags, $donnees['tags']);
    }
    $reponseE->closeCursor();
    $reponseE_bis = $bdd->query('SELECT * FROM stickers WHERE url IN ("' . implode('","', $urls) . '");');
    $urls_bis     = array();
    $tags_bis     = array();
    while ($donnees = $reponseE_bis->fetch()) {
        array_push($urls_bis, $donnees['url']);
        array_push($tags_bis, $donnees['tags']);
    }
    $reponseE_bis->closeCursor();
    $tags = array();
    for ($i = 0; $i < count($urls); $i++) {
        $found = 0;
        for ($j = 0; $j < count($urls_bis); $j++) {
            if ($urls[$i] == $urls_bis[$j]) {
                array_push($tags, $tags_bis[$j]);
                $found = 1;
                break;
            }
        }
        if ($found == 0) {
            array_push($tags, '');
        }
    }
    echo '<table>
	<tr class="t1">
		<td colspan="2">STICKER ACTUEL</td>
		<td>MODIFICATION</td>
		<td></td>
	</tr>
	<tr class="t2">
		<td>Image</td>
		<td>Tags</td>
		<td>Tags</td>
		<td>Actions</td>
	</tr>';
    for ($i = 0; $i < count($urls); $i++) {
        echo '
	<tr id="E_l' . $i . '">
		<td>
			<img src="' . $urls[$i] . '"/>
			<p>' . $urls[$i] . '</p>
		</td>
		<td>' . $tags[$i] . '</td>
		<td><textarea rows=5 cols=50>' . $new_tags[$i] . '</textarea></td>
		<td class="actions"><img src="https://image.noelshack.com/fichiers/2017/19/1494787339-camera-test.png" onclick="validerE(' . $i . ')"/><img src="https://image.noelshack.com/fichiers/2017/19/1494787338-close.png" onclick="declinerE(' . $i . ')"/></td>
	</tr>';
    }
    echo '
</table></br>
';
    
    
    $reponseF       = $bdd->query('SELECT * FROM req_sounds');
    $urls           = array();
    $reference_urls = array();
    $new_sounds     = array();
    while ($donnees = $reponseF->fetch()) {
        array_push($urls, $donnees['url']);
        array_push($reference_urls, $donnees['reference_url']);
        array_push($new_sounds, $donnees['sounds']);
    }
    $reponseF->closeCursor();
    $reponseF_bis = $bdd->query('SELECT * FROM stickers WHERE url IN ("' . implode('","', $urls) . '","' . implode('","', $reference_urls) . '");');
    $urls_bis     = array();
    $sounds_bis   = array();
    while ($donnees = $reponseF_bis->fetch()) {
        array_push($urls_bis, $donnees['url']);
        array_push($sounds_bis, $donnees['sounds']);
    }
    $reponseF_bis->closeCursor();
    $sounds           = array();
    $reference_sounds = array();
    for ($i = 0; $i < count($urls); $i++) {
        $foundA = 0;
        $foundB = 0;
        for ($j = 0; $j < count($urls_bis); $j++) {
            if ($urls[$i] == $urls_bis[$j]) {
                array_push($sounds, $sounds_bis[$j]);
                $foundA = 1;
                break;
            }
        }
        if ($foundA == 0) {
            array_push($sounds, '');
        }
        for ($j = 0; $j < count($urls_bis); $j++) {
            if ($reference_urls[$i] == $urls_bis[$j]) {
                array_push($reference_sounds, $sounds_bis[$j]);
                $foundB = 1;
                break;
            }
        }
        if ($foundB == 0) {
            array_push($reference_sounds, '');
        }
    }
    echo '
<table>
	<tr class="t1">
		<td colspan="2">STICKER ACTUEL</td>
		<td colspan="2">STICKER DE R&Eacute;F&Eacute;RENCE</td>
		<td>MODIFICATION</td>
		<td></td>
	</tr>
	<tr class="t2">
		<td>Image</td>
		<td>Sons</td>
		<td>Image</td>
		<td>Sons</td>
		<td>Sons</td>
		<td>Actions</td>
	</tr>';
    for ($i = 0; $i < count($urls); $i++) {
        echo '
	<tr id="F_l' . $i . '">
		<td>
			<img src="' . $urls[$i] . '" id="4_img0_l' . $i . '"/>
			<textarea rows=4 cols=30 id="4_input0_l' . $i . '" oninput="document.getElementById(\'4_img0_l' . $i . '\').src = document.getElementById(\'4_input0_l' . $i . '\').value;">' . $urls[$i] . '</textarea>
		</td>
		<td>' . $sounds[$i] . '</td>
		<td>
			<img src="' . $reference_urls[$i] . '" id="4_img1_l' . $i . '"/>
			<textarea rows=4 cols=30 id="4_input1_l' . $i . '" oninput="document.getElementById(\'4_img1_l' . $i . '\').src = document.getElementById(\'4_input1_l' . $i . '\').value;">' . $reference_urls[$i] . '</textarea>
		</td>
		<td>' . $reference_sounds[$i] . '</td>
';
        if ($new_sounds[$i] == '') {
            echo '		<td><textarea rows=7 cols=30>' . $reference_sounds[$i] . '</textarea></td>
';
        } else {
            echo '		<td><textarea rows=7 cols=30>' . $new_sounds[$i] . '</textarea></td>
';
        }
        echo '		<td class="actions"><img src="https://image.noelshack.com/fichiers/2017/19/1494787339-camera-test.png" onclick="validerF(' . $i . ')"/><img src="https://image.noelshack.com/fichiers/2017/19/1494787338-close.png" onclick="declinerF(' . $i . ')"/></td>
	</tr>';
    }
    echo '
</table></br>
';
    
    
    /*$reponseG = $bdd->query('SELECT * FROM stickers WHERE count < 10 ORDER BY count asc');
    //$reponseG = $bdd->query('SELECT * FROM stickers LIMIT 220 OFFSET 2800');
    $urls       = array();
    $counts    = array();
    $categories   = array();
    $tags      = array();
    $sounds      = array();
    while ($donnees = $reponseG->fetch()) {
    array_push($urls, $donnees['url']);
    array_push($counts, $donnees['count']);
    array_push($categories, $donnees['categorie']);
    array_push($tags, $donnees['tags']);
    array_push($sounds, $donnees['sounds']);
    }
    $reponseG->closeCursor();
    echo 
    '<table>
    <tr class="t1">
    <td colspan="5">INFORMATIONS SUR LA SUPPRESSION</td>
    <td></td>
    </tr>
    <tr class="t2">
    <td>Image</td>
    <td>Utilisations</td>
    <td>Cat&eacute;gorie</td>
    <td>Tags</td>
    <td>Sons</td>
    <td>Actions</td>
    </tr>';
    for ($i = 0; $i < count($urls); $i++) {
    echo 
    '
    <tr id="G_l'.$i.'">
    <td>
    <img src="'.$urls[$i].'"/>
    <p>'.$urls[$i].'</p>
    </td>
    <td>'.$counts[$i].'</td>
    <td>'.$categories[$i].'</td>
    <td>'.$tags[$i].'</td>
    <td>'.$sounds[$i].'</td>
    <td class="actions"><img src="https://image.noelshack.com/fichiers/2017/19/1494787339-camera-test.png" onclick="validerG('.$i.')"/></td>
    </tr>';
    }
    echo '
    </table></br>
    ';//*/
    
    
    /*$reponseH = $bdd->query('SELECT * FROM stickers WHERE categorie = \'catRisitas\' LIMIT 60 OFFSET 150');
    //$reponseH = $bdd->query('SELECT * FROM stickers WHERE categorie = \'catRisitas\'');
    $urls       = array();
    $categories   = array();
    while ($donnees = $reponseH->fetch()) {
    array_push($urls, $donnees['url']);
    array_push($categories, $donnees['categorie']);
    }
    $reponseH->closeCursor();
    echo 
    '<table>
    <tr class="t1">
    <td>STICKER ACTUEL</td>
    <td>MODIFICATION</td>
    <td></td>
    </tr>
    <tr class="t2">
    <td>Image</td>
    <td>Cat&eacute;gorie</td>
    <td>Actions</td>
    </tr>';
    for ($i = 0; $i < count($urls); $i++) {
    echo 
    '
    <tr id="H_l'.$i.'">
    <td>
    <img src="'.$urls[$i].'"/>
    <p>'.$urls[$i].'</p>
    </td>
    <td><textarea rows=3 cols=60>'.$categories[$i].'</textarea></td>
    <td class="actions"><img src="https://image.noelshack.com/fichiers/2017/19/1494787339-camera-test.png" onclick="validerH('.$i.')"/></td>
    </tr>';
    }
    echo '
    </table></br>
    ';//*/
    
    
    //$reponseI = $bdd->query('SELECT * FROM stickers WHERE categorie = \'catRisitas\' LIMIT 60 OFFSET 150');
    $reponseI = $bdd->query('SELECT * FROM stickers LIMIT 50 OFFSET 150');
    $urls     = array();
    $tags     = array();
    while ($donnees = $reponseI->fetch()) {
        array_push($urls, $donnees['url']);
        array_push($tags, $donnees['tags']);
    }
    $reponseI->closeCursor();
    echo '<table>
	<tr class="t1">
		<td>STICKER ACTUEL</td>
		<td>MODIFICATION</td>
		<td></td>
	</tr>
	<tr class="t2">
		<td>Image</td>
		<td>Tags</td>
		<td>Actions</td>
	</tr>';
    for ($i = 0; $i < count($urls); $i++) {
        echo '
	<tr id="I_l' . $i . '">
		<td>
			<img src="' . $urls[$i] . '"/>
			<p>' . $urls[$i] . '</p>
		</td>
		<td><textarea rows=6 cols=60>' . $tags[$i] . '</textarea></td>
		<td class="actions"><img src="https://image.noelshack.com/fichiers/2017/19/1494787339-camera-test.png" onclick="validerI(' . $i . ')"/></td>
	</tr>';
    }
    echo '
</table></br>
'; //*/
    
    
    $reponseJ  = $bdd->query('SELECT * FROM categories');
    $variables = array();
    while ($donnees = $reponseJ->fetch()) {
        array_push($variables, $donnees['variable']);
    }
    $reponseJ->closeCursor();
    $reponseJ_bis = $bdd->query('SELECT * FROM stickers WHERE categorie NOT IN ("' . implode('","', $variables) . '")');
    $urls         = array();
    $categories   = array();
    while ($donnees = $reponseJ_bis->fetch()) {
        array_push($urls, $donnees['url']);
        array_push($categories, $donnees['categorie']);
    }
    $reponseJ_bis->closeCursor();
    echo '<table>
	<tr class="t1">
		<td>STICKER ACTUEL</td>
		<td>CORRECTION</td>
		<td></td>
	</tr>
	<tr class="t2">
		<td>Image</td>
		<td>Cat&eacute;gorie</td>
		<td>Actions</td>
	</tr>';
    for ($i = 0; $i < count($urls); $i++) {
        echo '
	<tr id="J_l' . $i . '">
		<td>
			<img src="' . $urls[$i] . '"/>
			<p>' . $urls[$i] . '</p>
		</td>
		<td><textarea rows=3 cols=60>' . $categories[$i] . '</textarea></td>
		<td class="actions"><img src="https://image.noelshack.com/fichiers/2017/19/1494787339-camera-test.png" onclick="validerJ(' . $i . ')"/></td>
	</tr>';
    }
    echo '
</table></br>
'; //*/
}
catch (Exception $e) {
    //die();
    echo $e;
}
?>
</body>
</html>