<?php
if (isset($_POST['url']) && isset($_POST['reference_url']) && isset($_POST['sounds']))
{
	try
	{
		$bdd = new PDO('mysql:host=localhost;dbname=***REMOVED***_stckr;charset=utf8', '***REMOVED***_master', '***REMOVED***');
		if ($_POST['url'] != '')
		{
			$reqA = $bdd->prepare('UPDATE stickers SET sounds = :sounds WHERE url = :url;');
			$reqA->execute(array(
				'url' => $_POST['url'],
				'sounds' => $_POST['sounds']
				));
			$reqB = $bdd->prepare('DELETE FROM req_sounds WHERE url = :url');
			$reqB->execute(array(
				'url' => $_POST['url']
				));
		}
		else
		{
			$reponse = $bdd->prepare('SELECT * FROM stickers WHERE url = :reference_url');
			$reponse->execute(array(
				'reference_url' => $_POST['reference_url']
				));
			$sounds = array();
		    while ($donnees = $reponse->fetch()) {
		        array_push($sounds, $donnees['sounds']);
		    }
			$reponse->closeCursor();
			$reponse_bis = $bdd->prepare('SELECT * FROM stickers WHERE sounds = :sounds');
			$reponse_bis->execute(array(
				'sounds' => $sounds[0]
				));
		    $urls = array();
		    while ($donnees = $reponse_bis->fetch()) {
		        array_push($urls, $donnees['url']);
		    }
			$reponse_bis->closeCursor();
			$reqA = $bdd->prepare('UPDATE stickers SET sounds = :sounds WHERE url = :url;');
			for ($i = 0; $i < count($urls); $i++) {
				$reqA->execute(array(
					'url' => $urls[$i],
					'sounds' => $_POST['sounds']
					));
			}
			$reqB = $bdd->prepare('DELETE FROM req_sounds WHERE reference_url = :reference_url');
			$reqB->execute(array(
				'reference_url' => $_POST['reference_url']
				));
		}
	}
	catch (Exception $e)
	{
		die();
	}
}
?>