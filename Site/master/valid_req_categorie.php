<?php
if (isset($_POST['url']) && isset($_POST['categorie']))
{
	try
	{
		$bdd = new PDO('mysql:host=localhost;dbname=***REMOVED***_stckr;charset=utf8', '***REMOVED***_master', '***REMOVED***');
		$reqA = $bdd->prepare('UPDATE stickers SET categorie = :categorie WHERE url = :url;');
		$reqA->execute(array(
			'url' => $_POST['url'],
			'categorie' => $_POST['categorie']
			));
		$reqB = $bdd->prepare('DELETE FROM req_categorie WHERE url = :url');
		$reqB->execute(array(
			'url' => $_POST['url']
			));
	}
	catch (Exception $e)
	{
		die();
	}
}
?>