<?php
if (isset($_POST['url']) && isset($_POST['reference_url']))
{
	try
	{
		$bdd = new PDO('mysql:host=localhost;dbname=***REMOVED***_stckr;charset=utf8', '***REMOVED***_master', '***REMOVED***');
		if ($_POST['url'] != '')
		{
			$req = $bdd->prepare('DELETE FROM req_sounds WHERE url = :url');
			$req->execute(array(
				'url' => $_POST['url']
				));
		}
		else
		{
			$req = $bdd->prepare('DELETE FROM req_sounds WHERE reference_url = :reference_url');
			$req->execute(array(
				'reference_url' => $_POST['reference_url']
				));
		}
	}
	catch (Exception $e)
	{
		die();
	}
}
?>