<?php
if (isset($_POST['url']) && isset($_POST['tags']))
{
	try
	{
		$bdd = new PDO('mysql:host=localhost;dbname=***REMOVED***_stckr;charset=utf8', '***REMOVED***_master', '***REMOVED***');
		$reqA = $bdd->prepare('UPDATE stickers SET tags = :tags WHERE url = :url;');
		$reqA->execute(array(
			'url' => $_POST['url'],
			'tags' => $_POST['tags']
			));
		$reqB = $bdd->prepare('DELETE FROM req_tags WHERE url = :url');
		$reqB->execute(array(
			'url' => $_POST['url']
			));
	}
	catch (Exception $e)
	{
		die();
	}
}
?>