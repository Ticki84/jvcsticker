<?php
if (isset($_POST['url']) && isset($_POST['cat']) && isset($_POST['tags']) && isset($_POST['sounds']))
{
	try
	{
		$bdd = new PDO('mysql:host=localhost;dbname=***REMOVED***_stckr;charset=utf8', '***REMOVED***_master', '***REMOVED***');
		$reqA = $bdd->prepare('INSERT INTO stickers (url, count, categorie, tags, sounds) VALUES(:url, :count, :categorie, :tags, :sounds)');
		$reqA->execute(array(
			'url' => $_POST['url'],
			'count' => '0',
			'categorie' => $_POST['cat'],
			'tags' => $_POST['tags'],
			'sounds' => $_POST['sounds']
			));
		$reqB = $bdd->prepare('DELETE FROM req_stickers WHERE url = :url');
		$reqB->execute(array(
			'url' => $_POST['url']
			));
	}
	catch (Exception $e)
	{
		die();
	}
}
?>