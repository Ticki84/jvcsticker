<?php
if (isset($_POST['url']) && isset($_POST['new_url']))
{
	try
	{
		$bdd = new PDO('mysql:host=localhost;dbname=***REMOVED***_stckr;charset=utf8', '***REMOVED***_master', '***REMOVED***');
		if ($_POST['new_url'] == "") //suppression simple
		{
			$reqA = $bdd->prepare('DELETE FROM stickers WHERE url = :url');
			$reqA->execute(array(
				'url' => $_POST['url']
				));
		}
		else
		{
			$occ = $bdd->prepare('select count(*) FROM stickers WHERE url = :new_url');
			$occ->execute(array(
				'new_url' => $_POST['new_url']
				));
			if ($occ->fetchColumn() == 0) { //remplacement
				$reqA = $bdd->prepare('UPDATE stickers SET url = :new_url WHERE url = :url;');
				$reqA->execute(array(
					'url' => $_POST['url'],
					'new_url' => $_POST['new_url']
					));
			}
			else //suppression doublon
			{
				$reponse = $bdd->prepare('SELECT SUM(count) AS somme FROM stickers WHERE url IN (:url,:new_url)');
				$reponse->execute(array(
					'url' => $_POST['url'],
					'new_url' => $_POST['new_url']
					));
				$sum = 0;
				while ($donnees = $reponse->fetch())
				{
					$sum = $donnees['somme'];
				}
				$reponse->closeCursor();
				$reqA = $bdd->prepare('UPDATE stickers SET count = :sum WHERE url = :url;');
				$reqA->execute(array(
					'url' => $_POST['url'],
					'sum' => $sum
					));
				$reqB = $bdd->prepare('DELETE FROM stickers WHERE url = :new_url');
				$reqB->execute(array(
					'new_url' => $_POST['new_url']
					));
			}
		}
		$reqC = $bdd->prepare('DELETE FROM req_delete WHERE url = :url');
		$reqC->execute(array(
			'url' => $_POST['url']
			));
	}
	catch (Exception $e)
	{
		die();
	}
}
?>