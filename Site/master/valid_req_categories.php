<?php
if (isset($_POST['name']) && isset($_POST['variable']) && isset($_POST['icon']) && isset($_POST['byDefault']))
{
	try
	{
		$bdd = new PDO('mysql:host=localhost;dbname=***REMOVED***_stckr;charset=utf8', '***REMOVED***_master', '***REMOVED***');
		$reqA = $bdd->prepare('INSERT INTO categories (name, variable, icon, byDefault) VALUES(:name, :variable, :icon, :byDefault)');
		$reqA->execute(array(
			'name' => $_POST['name'],
			'variable' => $_POST['variable'],
			'icon' => $_POST['icon'],
			'byDefault' => $_POST['byDefault']
			));
		$reqB = $bdd->prepare('DELETE FROM req_categories WHERE name = :name');
		$reqB->execute(array(
			'name' => $_POST['name']
			));
	}
	catch (Exception $e)
	{
		die();
	}
}
?>